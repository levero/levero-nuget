CREATE TYPE [build].[TT_Columns] AS TABLE
(
[ColIndex] [int] NOT NULL,
[ColName] [sys].[sysname] NOT NULL,
[SqlType] [sys].[sysname] NOT NULL,
[ColDef] [sys].[sysname] NOT NULL,
PRIMARY KEY CLUSTERED  ([ColIndex])
)
GO
