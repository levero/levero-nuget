IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'build')
CREATE LOGIN [build] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [build] FOR LOGIN [build] WITH DEFAULT_SCHEMA=[build]
GO
