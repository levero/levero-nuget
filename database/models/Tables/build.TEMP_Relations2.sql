CREATE TABLE [build].[TEMP_Relations2]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[objectID] [int] NOT NULL,
[relationName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[parentTable] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[parentColumns] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[childTable] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[childColumns] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[done] [bit] NOT NULL CONSTRAINT [DF_TEMP_Relations2_done] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Relations2] ADD CONSTRAINT [PK_TEMP_Relations2] PRIMARY KEY CLUSTERED ([appPackageID], [generator], [objectID], [relationName]) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Relations2] ADD CONSTRAINT [FK_TEMP_Relations2_TEMP_Procedures2] FOREIGN KEY ([appPackageID], [generator], [objectID]) REFERENCES [build].[TEMP_Procedures2] ([appPackageID], [generator], [objectID]) ON DELETE CASCADE
GO
