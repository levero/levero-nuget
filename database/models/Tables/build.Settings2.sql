CREATE TABLE [build].[Settings2]
(
[AppID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[AppPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[ModulVersion] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_ModulVersion] DEFAULT (''),
[DatabaseName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[BackendActions_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings2_BackendActions_Generate] DEFAULT ((0)),
[BackendActions_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_BackendActions_ListOfSchema] DEFAULT (''),
[BackendActions_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_BackendActions_ListOfPrefix] DEFAULT (''),
[BackendActions_ListOfModules] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_BackendActions_UseModules] DEFAULT (''),
[BackendReports_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings2_BackendReports_Generate] DEFAULT ((0)),
[BackendReports_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_BackendReports_ListOfSchema] DEFAULT (''),
[BackendReports_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_BackendReports_ListOfPrefix] DEFAULT (''),
[BackendReports_ListOfModules] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_BackendReports_UseModules] DEFAULT (''),
[Typescript_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings2_Typescript_Generate] DEFAULT ((0)),
[Typescript_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_Typescript_ListOfSchema] DEFAULT (''),
[Typescript_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_Typescript_ListOfPrefix] DEFAULT (''),
[Typescript_IncludeReports] [bit] NOT NULL CONSTRAINT [DF_Settings2_Typescript_IncludeReports] DEFAULT ((0)),
[Typescript_ReportPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_Typescript_ReportPrefix] DEFAULT (''),
[Typescript_SupportRelations] [bit] NOT NULL CONSTRAINT [DF_Settings2_Typescript_SupportRelations] DEFAULT ((0)),
[Typescript_ListOfModules] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_Typescript_ListOfModules] DEFAULT (''),
[CSharp_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings2_CSharp_Generate] DEFAULT ((0)),
[CSharp_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_CSharp_ListOfSchema] DEFAULT (''),
[CSharp_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_CSharp_ListOfPrefix] DEFAULT (''),
[CSharp_ListOfModules] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_CSharp_ListOfModules] DEFAULT (''),
[CSharp_RemovePrefix] [bit] NOT NULL CONSTRAINT [DF_Settings2_CSharp_RemovePrefix] DEFAULT ((0)),
[CSharp_ModelNamespace] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_CSharp_ModelNamespace] DEFAULT (''),
[CSharp_Mode] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings2_CSharp_Mode] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [build].[Settings2] ADD CONSTRAINT [CK_Settings2_CSharp_Mode] CHECK (([CSharp_Generate]=(0) OR [CSharp_Mode]='backendhub' OR [CSharp_Mode] like 'backendhub:%' OR [CSharp_Mode]='sql' OR [CSharp_Mode] like 'sql:%'))
GO
ALTER TABLE [build].[Settings2] ADD CONSTRAINT [PK_Settings2] PRIMARY KEY CLUSTERED ([AppPackageID]) ON [PRIMARY]
GO
