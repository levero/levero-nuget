CREATE TABLE [build].[TEMP_Prefixes]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[prefix] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[done] [bit] NOT NULL CONSTRAINT [DF_TEMP_Prefix_done] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Prefixes] ADD CONSTRAINT [PK_TEMP_Prefix] PRIMARY KEY CLUSTERED  ([appPackageID], [generator], [prefix]) ON [PRIMARY]
GO
