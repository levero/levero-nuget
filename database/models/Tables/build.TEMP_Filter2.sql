CREATE TABLE [build].[TEMP_Filter2]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[modulID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[databaseName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[schemaName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[schemaID] [int] NULL,
[listOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[targetVersion] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[frontendModels] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[done] [bit] NOT NULL CONSTRAINT [DF_TEMP_Filter2_done] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Filter2] ADD CONSTRAINT [PK_TEMP_Filter2] PRIMARY KEY CLUSTERED ([appPackageID], [modulID], [generator], [schemaName]) ON [PRIMARY]
GO
