CREATE TABLE [build].[Settings]
(
[AppID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[AppPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[DatabaseName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[BackendActions_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings_BackendActions_Generate] DEFAULT ((0)),
[BackendActions_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_BackendActions_ListOfSchema] DEFAULT (''),
[BackendActions_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_BackendActions_ListOfPrefix] DEFAULT (''),
[BackendReports_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings_BackendReports_Generate] DEFAULT ((0)),
[BackendReports_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_BackendReports_ListOfSchema] DEFAULT (''),
[BackendReports_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_BackendReports_ListOfPrefix] DEFAULT (''),
[Typescript_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings_Typescript_Generate] DEFAULT ((0)),
[Typescript_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_Typescript_ListOfSchema] DEFAULT (''),
[Typescript_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_Typescript_ListOfPrefix] DEFAULT (''),
[CSharp_Generate] [bit] NOT NULL CONSTRAINT [DF_Settings_CSharp_Generate] DEFAULT ((0)),
[CSharp_ListOfSchema] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_CSharp_ListOfSchema] DEFAULT (''),
[CSharp_ListOfPrefix] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_CSharp_ListOfPrefix] DEFAULT (''),
[CSharp_RemovePrefix] [bit] NOT NULL CONSTRAINT [DF_Settings_CSharp_RemovePrefix] DEFAULT ((0)),
[CSharp_ModelNamespace] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_CSharp_ModelNamespace] DEFAULT (''),
[CSharp_Mode] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_Settings_CSharp_Mode] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [build].[Settings] ADD CONSTRAINT [CK_Settings_CSharp_Mode] CHECK (([CSharp_Generate]=(0) OR [CSharp_Mode]='backendhub' OR [CSharp_Mode] like 'backendhub:%' OR [CSharp_Mode]='sql' OR [CSharp_Mode] like 'sql:%'))
GO
ALTER TABLE [build].[Settings] ADD CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED ([AppPackageID]) ON [PRIMARY]
GO
