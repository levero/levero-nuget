CREATE TABLE [build].[TEMP_Columns]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[objectID] [int] NOT NULL,
[resultsetIndex] [tinyint] NOT NULL,
[resultset] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[colName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[colIndex] [smallint] NOT NULL,
[colType] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_TEMP_Columns_colType] DEFAULT ('C'),
[sqlType] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[tableTypeObjectID] [int] NULL,
[colDefinition] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Columns] ADD CONSTRAINT [PK_TEMP_Columns] PRIMARY KEY CLUSTERED ([appPackageID], [generator], [objectID], [resultset], [colName]) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Columns] ADD CONSTRAINT [FK_TEMP_Columns_TEMP_Procedures] FOREIGN KEY ([appPackageID], [generator], [objectID]) REFERENCES [build].[TEMP_Procedures] ([appPackageID], [generator], [objectID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
