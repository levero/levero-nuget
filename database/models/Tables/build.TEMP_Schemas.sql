CREATE TABLE [build].[TEMP_Schemas]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[schemaName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[schemaID] [int] NULL,
[frontendModels] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[done] [bit] NOT NULL CONSTRAINT [DF_TEMP_Schemas_isDone] DEFAULT ((0)),
[modify_date] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Schemas] ADD CONSTRAINT [PK_TEMP_Schemas] PRIMARY KEY CLUSTERED  ([appPackageID], [generator], [schemaName]) ON [PRIMARY]
GO
