CREATE TABLE [build].[TEMP_Columns2]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[objectID] [int] NOT NULL,
[resultsetIndex] [tinyint] NOT NULL,
[resultset] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[colName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[colIndex] [smallint] NOT NULL,
[colType] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_TEMP_Columns2_colType] DEFAULT ('C'),
[sqlType] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[tableTypeObjectID] [int] NULL,
[isNullable] [bit] NOT NULL CONSTRAINT [DF_TEMP_Columns2_isNullable] DEFAULT ((1)),
[colDefinition] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[defaultValue] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[translatedDefaultValue] AS ([build].[TranslateDefaultValue]([sqlType],[isNullable],[tableTypeObjectID],[defaultValue])),
[useDefaultValue] [bit] NOT NULL CONSTRAINT [DF_TEMP_Columns2_useDefaultValue] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Columns2] ADD CONSTRAINT [PK_TEMP_Columns2] PRIMARY KEY CLUSTERED ([appPackageID], [generator], [objectID], [resultset], [colName]) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Columns2] ADD CONSTRAINT [FK_TEMP_Columns2_TEMP_Procedures2] FOREIGN KEY ([appPackageID], [generator], [objectID]) REFERENCES [build].[TEMP_Procedures2] ([appPackageID], [generator], [objectID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'([build].[TranslateDefaultValue]([sqlType],[isNullable],[tableTypeObjectID],[defaultValue]))', 'SCHEMA', N'build', 'TABLE', N'TEMP_Columns2', 'COLUMN', N'translatedDefaultValue'
GO
