CREATE TABLE [build].[TEMP_Procedures]
(
[databaseName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[objectID] [int] NOT NULL,
[modify_date] [datetime] NOT NULL,
[schemaName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[name] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[modelName] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[modelNamespace] [nvarchar] (128) COLLATE Latin1_General_CI_AS NULL,
[procVersion] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[isExternal] [bit] NOT NULL CONSTRAINT [DF_TEMP_Procedures_isExternal] DEFAULT ((0)),
[done] [bit] NOT NULL CONSTRAINT [DF_TEMP_Procedures_done] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Procedures] ADD CONSTRAINT [PK_TEMP_Procedures] PRIMARY KEY CLUSTERED  ([appPackageID], [generator], [objectID]) ON [PRIMARY]
GO
