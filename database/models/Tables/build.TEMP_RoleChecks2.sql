CREATE TABLE [build].[TEMP_RoleChecks2]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[objectID] [int] NOT NULL,
[moduleName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[roleName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_RoleChecks2] ADD CONSTRAINT [PK_TEMP_RoleChecks2] PRIMARY KEY CLUSTERED ([appPackageID], [generator], [objectID], [moduleName], [roleName]) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_RoleChecks2] ADD CONSTRAINT [FK_TEMP_RoleChecks2_TEMP_Procedures2] FOREIGN KEY ([appPackageID], [generator], [objectID]) REFERENCES [build].[TEMP_Procedures2] ([appPackageID], [generator], [objectID]) ON DELETE CASCADE
GO
