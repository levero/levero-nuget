CREATE TABLE [build].[TEMP_RoleChecks]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[objectID] [int] NOT NULL,
[moduleName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[roleName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_RoleChecks] ADD CONSTRAINT [PK_TEMP_RoleChecks] PRIMARY KEY CLUSTERED ([appPackageID], [generator], [objectID], [moduleName], [roleName]) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_RoleChecks] ADD CONSTRAINT [FK_TEMP_RoleChecks_TEMP_Procedures] FOREIGN KEY ([appPackageID], [generator], [objectID]) REFERENCES [build].[TEMP_Procedures] ([appPackageID], [generator], [objectID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
