CREATE TABLE [build].[TEMP_Flags]
(
[appPackageID] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[generator] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[objectID] [int] NOT NULL,
[flag] [nvarchar] (128) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Flags] ADD CONSTRAINT [PK_TEMP_Flags] PRIMARY KEY CLUSTERED ([appPackageID], [generator], [objectID], [flag]) ON [PRIMARY]
GO
ALTER TABLE [build].[TEMP_Flags] ADD CONSTRAINT [FK_TEMP_Flags_TEMP_Procedures] FOREIGN KEY ([appPackageID], [generator], [objectID]) REFERENCES [build].[TEMP_Procedures] ([appPackageID], [generator], [objectID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
