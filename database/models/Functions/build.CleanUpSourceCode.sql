SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      Ing. Karl Prokupek
-- Create date: 2024-01-19
-- Description: remove all comments from procedure source code
-- =============================================
CREATE FUNCTION [build].[CleanUpSourceCode]
(
    @SourceCode             NVARCHAR(max),
    @KeepCRLF               BIT
)
RETURNS NVARCHAR(max)
AS
--DECLARE @SourceCode             NVARCHAR(max) = OBJECT_DEFINITION(OBJECT_ID('build.CleanUpSourceCode'));
--DECLARE @KeepCRLF               BIT = 0;
BEGIN
    DECLARE @Result NVARCHAR(max) = '', @Line NVARCHAR(max) = '', @ch NCHAR(1), @next NCHAR(1), @ch2 NVARCHAR(2);
    DECLARE @pos INT = 1, @i INT, @len INT = LEN(@SourceCode);
    DECLARE @isStringLiteral BIT = 0, @isQuotedName BIT = 0, @isDoubleHyphenComment BIT = 0, @isSlashStarComment BIT = 0;
    DECLARE @SP NCHAR(1) = NCHAR(32);
    DECLARE @TAB NCHAR(1) = NCHAR(9);
    DECLARE @LF NCHAR(1) = NCHAR(10);
    DECLARE @CR NCHAR(1) = NCHAR(13);
    DECLARE @lastCRLF INT = 0, @skip INT = 0;

    SET @next = SUBSTRING(@SourceCode, @pos, 1);
    WHILE @pos <= @len
    BEGIN
        SET @ch = @next;
        SET @next = SUBSTRING(@SourceCode, @pos + 1, 1);
        SET @ch2 = @ch + @next;

        -- is end of string literal? '
        IF @isStringLiteral = 1
        BEGIN
            IF @ch = '''' 
                IF @next = ''''
                    --skip this position
                    SET @skip = 1;
                ELSE
                    SET @isStringLiteral = 0;
        END
        -- is in quoted name? ]
        ELSE IF @isQuotedName = 1
        BEGIN
            IF @ch = ']'
                IF @next = ']'
                    --skip this position
                    SET @skip = 1;
                ELSE
                    SET @isQuotedName = 0;
        END

        -- is end of line comment? CR and/or LF
        ELSE IF @isDoubleHyphenComment = 1
        BEGIN
            IF @ch = @LF
                SET @isDoubleHyphenComment = 0;
        END
        -- is end of block comment? */
        ELSE IF @isSlashStarComment = 1
        BEGIN
            IF @ch2 = '*/'
                SET @isSlashStarComment = 0;
        END

        -- skip?
        ELSE IF @skip > 0
            SET @skip += 0;

        -- is begin of string literal? '
        ELSE IF @ch = ''''
            SET @isStringLiteral = 1;

        -- is begin of quoted name? [
        ELSE IF @ch = '['
            SET @isQuotedName = 1;

        -- is begin of line comment? --
        ELSE IF @ch2 = '--'
            SET @isDoubleHyphenComment = 1;

        -- is begin of block comment? /*
        ELSE IF @ch2 = '/*'
            SET @isSlashStarComment = 1;

        -- remove tabs
        ELSE IF @ch IN (@SP, @TAB) AND (@next IN (@SP, @TAB) OR LEN(@Line) = 0)
            SET @skip += 1;

        IF @isDoubleHyphenComment = 0 AND @isSlashStarComment = 0 AND @ch NOT IN (@CR, @LF) AND @skip = 0
        BEGIN
            IF @ch = @TAB
                SET @Line += @SP;
            ELSE
                SET @Line += @ch;
        END

        -- take line (if not empty)
        IF @ch = @LF
        BEGIN
            IF TRIM(@Line) <> ''
            BEGIN
                --PRINT REPLACE(REPLACE(REPLACE(@Line, @CR, '<cr>'), @LF, '<lf>'), NCHAR(9), '<tab>');
                SET @Result += @Line;
                IF @KeepCRLF = 1
                    SET @Result += @CR + @LF;
                ELSE IF @line NOT LIKE '% '
                    SET @Result += @SP;
            END

            SET @Line = '';
        END

        SET @pos += 1;
        IF @skip > 0
            SET @skip -= 1;
    END

    IF TRIM(@Line) <> ''
    BEGIN
        SET @Result += @Line;
        IF @KeepCRLF = 1
            SET @Result += @CR + @LF;
    END

    RETURN @Result;
    --PRINT @Result;

    -- Test
    -- PRINT build.CleanUpSourceCode(OBJECT_DEFINITION(OBJECT_ID('build.CleanUpSourceCode')), 0);
END
GO
