SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-11
-- Description:	Prepares sql server datatype for frontend (typescript)
-- =============================================
CREATE FUNCTION [build].[GetParamPrepareForFrontend] 
(
	@SqlDataType sysname
)
RETURNS sysname
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result sysname;

	IF CHARINDEX('(', @SqlDataType) > 0
		SET @SqlDataType = SUBSTRING(@SqlDataType, 1, CHARINDEX('(', @SqlDataType) - 1);

	IF @SqlDataType = 'date'
		SET @Result = 'prepareDate';

	ELSE IF @SqlDataType = 'time'
		SET @Result = 'prepareTime';

	ELSE IF @SqlDataType IN ('smalldatetime', 'smalldatetime2', 'datetime', 'datetime2', 'datetimeoffset')
		SET @Result = 'prepareDateTime';

	ELSE 
		SET @Result = NULL;

	-- Return the result of the function
	RETURN @Result;
END
GO
