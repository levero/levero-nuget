SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      Ing. Karl Prokupek
-- Create date: 2024-01-19
-- Description: get parameters from procedure source code
-- =============================================
CREATE FUNCTION [build].[ParseProcedureParameters]
(
    @SourceCode             NVARCHAR(max)
)
RETURNS @Result TABLE 
(
    ParamName       SYSNAME NOT NULL,
    SqlType         SYSNAME NOT NULL,
    DefaultValue    NVARCHAR(max) NULL
    --,
    --Space1          INT NOT NULL,
    --Space2          INT NOT NULL,
    --Equal           INT NOT NULL,
    --Comma           INT NOT NULL
)
AS
BEGIN
    --INSERT INTO @Result (ParamName, SqlType, DefaultValue, Space1, Space2, Equal, Comma)
    --VALUES ('@1', '', @SourceCode, 0, 0, 0, 0);

    SET @SourceCode = build.CleanUpSourceCode(@SourceCode, 0);

    --INSERT INTO @Result (ParamName, SqlType, DefaultValue, Space1, Space2, Equal, Comma)
    --VALUES ('@2', '', @SourceCode, 0, 0, 0, 0);

    DECLARE @posStart   INT = CHARINDEX(' @', @SourceCode);
    DECLARE @posEnd     INT = CHARINDEX(' AS ', @SourceCode);
    IF @posStart > 0 AND @posStart < @posEnd
    BEGIN
        SET @SourceCode = TRIM(SUBSTRING(@SourceCode, @posStart, @posEnd - @posStart + 1)) + ',';
        DECLARE @stop INT = 100;
        DECLARE @len INT;

        --INSERT INTO @Result (ParamName, SqlType, DefaultValue, Space1, Space2, Equal, Comma)
        --VALUES ('@3', '', @SourceCode, 0, 0, 0, 0);

        WHILE @SourceCode <> '' AND @stop > 0
        BEGIN
            DECLARE @posComma   INT = CHARINDEX(',', @SourceCode);
            DECLARE @posEqual   INT = CHARINDEX('=', @SourceCode);
            IF @posEqual < 1 OR @posEqual > @posComma
                SET @posEqual = @posComma;
            DECLARE @posSpace1  INT = CHARINDEX(' ', @SourceCode);
            IF @posSpace1 < 1 OR @posSpace1 > @posComma
                SET @posSpace1 = @posComma;
            ELSE IF @posSpace1 > @posEqual
                SET @posSpace1 = @posEqual;
            DECLARE @posSpace2  INT = CHARINDEX(' ', @SourceCode, @posSpace1 + 1);
            IF @posSpace2 < 1 OR @posSpace2 > @posComma
                SET @posSpace2 = @posComma - 1;
            ELSE IF @posSpace2 > @posEqual
                SET @posSpace2 = @posEqual - 1;

            DECLARE @ParamName      SYSNAME = TRIM(SUBSTRING(@SourceCode, 1, @posSpace1));
            SET @len = @posSpace2 - @posSpace1;
            IF @len < 0 SET @len = 0;
            DECLARE @SqlType        SYSNAME = TRIM(SUBSTRING(@SourceCode, @posSpace1 + 1, @len));
            SET @len = @posComma - @posEqual - 1;
            IF @len < 0 SET @len = 0;
            DECLARE @DefaultValue   SYSNAME = NULLIF(TRIM(SUBSTRING(@SourceCode, @posEqual + 1, @len)), '');

            -- dates: remove '' from date literals
            IF @SqlType IN ('date', 'datetime') AND @DefaultValue LIKE '''%'''
                SET @DefaultValue = SUBSTRING(@DefaultValue, 2, LEN(@DefaultValue) - 2);

            INSERT INTO @Result (ParamName, SqlType, DefaultValue) --, Space1, Space2, Equal, Comma)
            VALUES (@ParamName, @SqlType, @DefaultValue); --, @posSpace1, @posSpace2, @posEqual, @posComma);

            SET @SourceCode = LTRIM(SUBSTRING(@SourceCode, @posComma + 1, LEN(@SourceCode)));
            SET @stop -= 1;
        END
    END


    RETURN;

    -- Tests
    -- SELECT * FROM LeveroModels.build.ParseProcedureParameters(OBJECT_DEFINITION(OBJECT_ID('tt.REP_Zeitnachweis_V1_0')));
END
GO
