SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-11
-- Description:	Gets normalized datatype for models description
-- ATTENTION: no space in result allowed!
-- =============================================
CREATE FUNCTION [build].[GetSqlDataType] 
(
	@typeName			sysname,
	@schemaName			sysname,
	@max_length			smallint,
	@precision			tinyint, 
	@scale				tinyint
)
RETURNS sysname
AS
BEGIN
	-- Name des Types suchen
	DECLARE @Result sysname = @typeName;
	--DECLARE @sql nvarchar(max) = N'
	--SELECT @Result = [name], @Schema = SCHEMA_NAME([schema_id])
	--FROM [' + @databaseName + N'].sys.types
	--WHERE user_type_id = ' + Convert(nvarchar, @user_type_id);
	--EXEC sys.sp_executesql @sql, N'@Result sysname OUTPUT, @Schema sysname OUTPUT', @Result = @Result OUTPUT, @Schema = @Schema OUTPUT;

	IF @Result IS NULL
		RETURN '-Unknown Type-';

	IF @schemaName <> N'sys'
		SET @Result = @schemaName + N'.' + @Result;
	
	-- Brauchen wir eine Länge?
	IF @Result LIKE '%char' OR @Result LIKE '%binary'
	BEGIN
		IF @max_length = -1
			SET @Result = @Result + '(max)';
		ELSE IF SUBSTRING(@Result, 1, 1) = 'N'
			SET @Result = @Result + '(' + CONVERT(NVARCHAR, @max_length / 2) + ')';
		ELSE
			SET @Result = @Result + '(' + CONVERT(NVARCHAR, @max_length) + ')';
	END
	
	-- Brauchen wir Precision und Scale?
	ELSE IF @Result IN ('decimal', 'numeric')
		SET @Result = @Result + '(' + CONVERT(NVARCHAR, @precision) + ',' + CONVERT(NVARCHAR, @scale) + ')';
	
	-- Brauchen wir nur Precision?
	ELSE IF @Result IN ('float', 'real')
		SET @Result = @Result + '(' + CONVERT(NVARCHAR, @precision) + ')';

	RETURN @Result;
END
GO
