SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Get model name of procedure
-- =============================================
CREATE FUNCTION [build].[GetModelNameFromProcName] 
(
	@ProcedureName	sysname,
	@RemovePrefix	bit
)
RETURNS sysname
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result sysname;

	-- Last characters must be in format _V0_0 or _V0
	DECLARE @ch char, @i int = LEN(@ProcedureName), @mode int = 1;
	WHILE @i > 0
	BEGIN
		SET @ch = SUBSTRING(@ProcedureName, @i, 1);
		IF @ch = '_' AND @mode = 1
		BEGIN
			-- first search is right part of comma
			SET @mode += 1;
		END
		ELSE IF @ch = 'V' AND @mode = 1
		BEGIN
			-- format _V0
			SET @mode += 2;
		END
		ELSE IF @ch = 'V' AND @mode = 2
		BEGIN
			-- second mode is left part of comma
			SET @mode += 1;
		END
		ELSE IF @ch = '_' AND @mode = 3
		BEGIN
			-- third mode is _V found -> ready
			DECLARE @PrefixLen int = 0;
			if @RemovePrefix = 1
				SET @PrefixLen = CHARINDEX('_', @ProcedureName);
			SET @Result = SUBSTRING(@ProcedureName, @PrefixLen + 1, @i - 1 - @PrefixLen); 
			SET @i = 0;
		END
		ELSE
		BEGIN
			-- parse digit
			IF CHARINDEX(@ch, '0123456789') < 1
				RETURN NULL;
		END

		-- next character
		SET @i -= 1;
	END

	-- Return the result of the function
	RETURN @Result;

	-- Tests
	-- SELECT build.GetModelNameFromProcName('ACT_Roles_SetRoleManager_V0_4', 1);
	-- SELECT build.GetModelNameFromProcName('ACT_Roles_SetRoleManager_V11_4', 1);
	-- SELECT build.GetModelNameFromProcName('ACT_Roles_SetRoleManager_V322_922', 1);
	-- SELECT build.GetModelNameFromProcName('ACT_Roles_SetRoleManager_V4_84', 1);
	-- SELECT build.GetModelNameFromProcName('ACT_Settings_LoadFromUser_V0_0', 1);
	-- SELECT build.GetModelNameFromProcName('ACT_Roles_SetRoleManager_V4', 1);
	-- SELECT build.GetModelNameFromProcName('ACT_Settings_LoadFromUser_V123', 1);
	-- SELECT build.GetModelNameFromProcName('ILS_Roles_SetRoleManager_V4_84', 0);
	-- SELECT build.GetModelNameFromProcName('ILS_Settings_LoadFromUser_V0_0', 0);
	-- SELECT build.GetModelNameFromProcName('ILS_Roles_SetRoleManager_V4', 0);
	-- SELECT build.GetModelNameFromProcName('ILS_Settings_LoadFromUser_V123', 0);
END
GO
