SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [build].[Split2] (
	@InputString			NVARCHAR(MAX),
	@Delimiter				VARCHAR(50),
	@RemoveSpaces			BIT,
	@SuppressEmptyItems		BIT
)
RETURNS @Items TABLE (
	Item			NVARCHAR(MAX),
	ItemIndex		INT
)
AS
BEGIN
	-- normalize parameters
	IF @Delimiter = ' '
	BEGIN
		SET @Delimiter = ',';
		SET @InputString = REPLACE(@InputString, ' ', @Delimiter);
		SET @RemoveSpaces = 0;
	END

	SET @Delimiter = IsNull(NullIf(@Delimiter, ''), ',');
	SET @RemoveSpaces = IsNull(@RemoveSpaces, 0);
	SET @SuppressEmptyItems = IsNull(@SuppressEmptyItems, 0);

	-- remove spaces?
	IF @RemoveSpaces = 1
		SET @InputString = REPLACE(@InputString, ' ', '');

--INSERT INTO @Items VALUES (@Delimiter); -- Diagnostic
--INSERT INTO @Items VALUES (@InputString); -- Diagnostic

	DECLARE @Item				NVARCHAR(MAX);
	DECLARE @ItemList			NVARCHAR(MAX);
	DECLARE @DelimIndex			INT;
	DECLARE @Index				INT = 0;

	SET @ItemList = @InputString;
	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0);
	WHILE @DelimIndex != 0
	BEGIN
		SET @Item = SUBSTRING(@ItemList, 0, @DelimIndex);
		IF @SuppressEmptyItems = 0 OR Len(@Item) > 0
		BEGIN
			INSERT INTO @Items VALUES (@Item, @Index);
			SET @Index += 1;
		END

		SET @ItemList = SUBSTRING(@ItemList, @DelimIndex + LEN(@Delimiter), LEN(@ItemList) - @DelimIndex);
		SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0);
	END -- End WHILE

	IF @Item IS NOT NULL 
	BEGIN
		-- At least one delimiter was encountered in @InputString
		IF @SuppressEmptyItems = 0 OR Len(@ItemList) > 0
			INSERT INTO @Items VALUES (@ItemList, @Index);
	END
	ELSE 
	BEGIN
		-- No delimiters were encountered in @InputString, so just return @InputString
		IF @SuppressEmptyItems = 0 OR Len(@InputString) > 0
			INSERT INTO @Items VALUES (@InputString, @Index);
	END

	RETURN;

	-- Tests
	-- SELECT * FROM build.Split2('', ',', 1, 1);
	-- SELECT * FROM build.Split2(' item1  ', ',', 1, 1);
	-- SELECT * FROM build.Split2(' item1,item2  ', ',', 1, 1);
	-- SELECT * FROM build.Split2('item1, item2  ,,item3,', ',', 1, 1);
END
GO
