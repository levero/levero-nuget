SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      Ing. Karl Prokupek
-- Create date: 2024-01-24
-- Description: translate a default value to Typescript?
-- =============================================
CREATE FUNCTION [build].[GenerateDefaultValueToTypeScript]
(
    @SqlType            SYSNAME,
    @IsNullable         BIT,
    @DefaultValue       NVARCHAR(max)
)
RETURNS NVARCHAR(max)
AS
BEGIN
    DECLARE @Translated NVARCHAR(max) = @DefaultValue;

    -- is it a date?
    IF @SqlType = 'DATE'
    BEGIN
        IF @DefaultValue = 'NULL'
            SET @Translated = 'null';
        ELSE IF @DefaultValue = '[TODAY]'
            SET @Translated = 'new Date()';
        ELSE --IF @DefaultValue LIKE '[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'
            SET @Translated = 'new Date(''' + @DefaultValue + ''')';
    END

    RETURN @Translated;

    -- Tests
    -- PRINT build.GenerateDefaultValueToTypeScript('date', 1, '[TODAY]');
    -- PRINT build.GenerateDefaultValueToTypeScript('date', 1, '2024-01-24');
    -- PRINT build.GenerateDefaultValueToTypeScript('date', 1, 'NULL');
END
GO
