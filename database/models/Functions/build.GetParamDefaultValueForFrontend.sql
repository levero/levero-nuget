SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-11
-- Description:	Gets default literal for sql server datatype for frontend (typescript)
-- =============================================
CREATE FUNCTION [build].[GetParamDefaultValueForFrontend] 
(
	@SqlDataType sysname
)
RETURNS sysname
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result sysname;

	IF CHARINDEX('(', @SqlDataType) > 0
		SET @SqlDataType = SUBSTRING(@SqlDataType, 1, CHARINDEX('(', @SqlDataType) - 1);

	IF @SqlDataType IN ('char', 'varchar', 'nchar', 'nvarchar', 'text', 'ntext', 'xml')
		SET @Result = CHAR(39) + CHAR(39);

	ELSE IF @SqlDataType IN ('int', 'tinyint', 'smallint', 'bigint', 'real', 'float', 'smallmoney', 'money', 'numeric', 'decimal')
		SET @Result = '0';

	ELSE IF @SqlDataType = 'bit'
		SET @Result = 'false';

	ELSE -- uniqueidentifier, date*, time, binary
		SET @Result = 'null';

	-- Return the result of the function
	RETURN @Result;
END
GO
