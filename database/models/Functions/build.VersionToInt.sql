SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-25
-- Description:	Converts a version (as string) into a float so that it can be compared
-- =============================================
CREATE FUNCTION [build].[VersionToInt] 
(
	@Version varchar(10)
)
RETURNS int
AS
BEGIN
	DECLARE @posPoint int = CHARINDEX('.', @Version);
	DECLARE @major int = 0, @minor int = 0;
	IF @posPoint = 0
		SET @major = CONVERT(int, @version);
	ELSE
	BEGIN
		SET @major = CONVERT(int, Substring(@Version, 1, @posPoint - 1));
		SET @minor = CONVERT(int, Substring(@Version, @posPoint + 1, 99));
	END

	RETURN @major * 65536 + @minor;

	-- Tests
	-- SELECT build.VersionToInt('0.5');
	-- SELECT build.VersionToInt('2.5');
	-- SELECT build.VersionToInt('2.50');
	-- SELECT build.VersionToInt('3.0');
	-- SELECT build.VersionToInt('3');
	-- SELECT build.VersionToInt('0.8');
	-- SELECT build.VersionToInt('0.10');
END
GO
