SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-08-31
-- Description:	selects a formatter-function for frontend (typescript)
-- =============================================
CREATE FUNCTION [build].[GetFormatterForFrontend] 
(
	@SqlDataType sysname
)
RETURNS sysname
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result sysname;

	IF CHARINDEX('(', @SqlDataType) > 0
		SET @SqlDataType = SUBSTRING(@SqlDataType, 1, CHARINDEX('(', @SqlDataType) - 1);

	IF @SqlDataType IN ('char', 'varchar', 'nchar', 'nvarchar', 'text', 'ntext', 'uniqueidentifier', 'xml')
		SET @Result = 'formatString';

	ELSE IF @SqlDataType IN ('int', 'tinyint', 'smallint', 'bigint', 'real', 'float', 'smallmoney', 'money', 'numeric', 'decimal')
		SET @Result = 'formatNumber';

	ELSE IF @SqlDataType = 'bit'
		SET @Result = 'formatBoolean';

	ELSE IF @SqlDataType IN ('date')
		SET @Result = 'formatDate';

	ELSE IF @SqlDataType IN ('time')
		SET @Result = 'formatTime';

	ELSE IF @SqlDataType IN ('smalldatetime', 'smalldatetime2', 'datetime', 'datetime2', 'datetimeoffset')
		SET @Result = 'formatDateTime';

	--ELSE IF @SqlDataType IN ('binary', 'varbinary', 'image')
	--	SET @Result = 'formatBinary';

	ELSE 
		SET @Result = 'INVALID';

	-- Return the result of the function
	RETURN @Result;
END
GO
