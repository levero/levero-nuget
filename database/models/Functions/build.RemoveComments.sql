SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-04-02
-- Description:	removes all comments from t-sql source code
-- =============================================
CREATE FUNCTION [build].[RemoveComments]
(
	@sourceCode	nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @Result NVARCHAR(MAX) = '';
	DECLARE @inString BIT = 0, @inBlockComment TINYINT = 0, @inLineComment bit = 0;
	DECLARE @l INT = LEN(@sourceCode), @pos INT = 0, @char CHAR(1), @part CHAR(3);

	-- in T-SQL there are line comments starting with --
	-- and block comments starting with /* and ending with */
	
	WHILE @pos < @l
	BEGIN
		-- get current character
		SET @char = SUBSTRING(@sourceCode, @pos, 1);
		-- get next 3 characters for evaluation
		SET @part = SUBSTRING(@sourceCode, @pos, 3);

		-- check for inline comment '--' BUT ignore '--§'
		IF @part LIKE '--%' AND @part <> '--§' AND @inString = 0 AND @inBlockComment = 0
			SET @inLineComment = 1;

		-- check for block comment start '/*' BUT ignore it in a string and inline comments
		ELSE IF @part LIKE '/*%' AND @inString = 0 AND @inLineComment = 0
			SET @inBlockComment = @inBlockComment + 1;

		-- check for quotes ''' BUT ignore it in block and inline comments
		ELSE IF @part like '''%' AND @inBlockComment = 0 AND @inLineComment = 0
			SET @inString = ~@inString;
	
		-- check for line feed 'CHAR(10)'
		ELSE IF @part LIKE CHAR(10) + '%'
		BEGIN
			-- line ended
			IF @inLineComment = 1
				SET @inLineComment = 0; -- inline comment ends
		END
	
		-- only take current character if we are not in a comment section
		IF @inBlockComment = 0 AND @inLineComment = 0
			SET @Result = @Result + @char;

		-- check for block comment end
		IF @part LIKE '*/%' AND @inBlockComment > 0
		BEGIN
			SET @inBlockComment = @inBlockComment - 1;
			-- skip the next character
			SET @pos = @pos + 2;
		END
		ELSE
			SET @pos = @pos + 1;
	END
	
	RETURN @Result;

	-- Tests
	-- PRINT build.RemoveComments('');
END
GO
