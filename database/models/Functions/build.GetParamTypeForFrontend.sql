SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-11
-- Description:	Prepares sql server datatype for frontend (typescript)
-- =============================================
CREATE FUNCTION [build].[GetParamTypeForFrontend] 
(
	@SqlDataType sysname
)
RETURNS sysname
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result sysname;

	IF CHARINDEX('(', @SqlDataType) > 0
		SET @SqlDataType = SUBSTRING(@SqlDataType, 1, CHARINDEX('(', @SqlDataType) - 1);

	IF @SqlDataType IN ('char', 'varchar', 'nchar', 'nvarchar', 'text', 'ntext', 'uniqueidentifier', 'xml')
		SET @Result = 'string';

	ELSE IF @SqlDataType IN ('int', 'tinyint', 'smallint', 'bigint', 'real', 'float', 'smallmoney', 'money', 'numeric', 'decimal')
		SET @Result = 'number';

	ELSE IF @SqlDataType = 'bit'
		SET @Result = 'boolean';

	ELSE IF @SqlDataType IN ('date', 'time', 'smalldatetime', 'smalldatetime2', 'datetime', 'datetime2', 'datetimeoffset')
		SET @Result = 'Date';

	ELSE IF @SqlDataType IN ('binary', 'varbinary', 'image')
		SET @Result = 'base64String';

	ELSE 
		SET @Result = 'any';

	-- Return the result of the function
	RETURN @Result;
END
GO
