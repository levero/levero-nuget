SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Gets .NET data type from normalized sql data type
-- =============================================
CREATE FUNCTION [build].[ConvertToNetType] 
(
	@sqlType			sysname
)
RETURNS @Result TABLE 
(													-- examples:
	SqlType					sysname NOT NULL,		-- varchar(10)	nvarchar(max)	nchar(1)			int			decimal(18,6)	dbo.TT_SaveVerkauf_V1
	TypeNameSpace			sysname NULL,			-- System		System			System				System		System			null
	BaseType				sysname NOT NULL,		-- String		String			Char				Int32		Decimal			SaveVerkauf
	NullableType			sysname NOT NULL,		-- String		String			Char?				Int32?		Decimal?		SaveVerkauf
	IsBaseTypeNullable		bit NOT NULL,			-- true			true			false				false		false			true
	ReaderMethod			sysname NULL,			-- GetString	GetString		GetCharFromString	GetInt32	GetDecimal		null
	SetMethod				sysname NULL,			-- SetString	SetString		SetString			SetInt32	SetDecimal		null
	[Length]				int NULL,				-- 10			null			null				null		null			null
	[Precision]				tinyint NULL,			-- null			null			null				null		18				null
	[Scale]					tinyint NULL,			-- null			null			null				null		6				null
	[SizeInBytes]			int NULL,				-- 10			2147483647		2					null		null			null
	SqlDbType				sysname NULL,			-- VarChar		NVarChar		NChar				Int32		Decimal			null
	SqlLength				int NULL				-- 10			null			1					null		null			null
)
AS
BEGIN
	DECLARE @len int = null, @scale tinyint = 0, @rawType sysname = REPLACE(@sqlType, ' ', ''), @size int = null, @unicodeFactor int = 1;
	IF CHARINDEX('(', @rawType) > 0
	BEGIN
		-- funktioniert nicht bei 2 Zahlen z.B. decimal(8,2)
		DECLARE @s sysname = SUBSTRING(REPLACE(@rawType, ')', ''), CHARINDEX('(', @rawType) + 1, 999);
		IF CHARINDEX(',', @s) > 0
		BEGIN
			SET @scale = CONVERT(tinyint, SUBSTRING(@s, CHARINDEX(',', @s) + 1, 999));
			SET @s = SUBSTRING(@s, 1, CHARINDEX(',', @s) - 1);
		END
		IF @s = 'max'
			SET @size = 2147483647;
		ELSE
		BEGIN
			SET @len = CONVERT(int, @s);
			SET @size = @len;
		END
		-- Klammern wegnehmen
		SET @rawType = SUBSTRING(@rawType, 1, CHARINDEX('(', @rawType) - 1);
	END

	IF @rawType IN ('nchar', 'nvarchar', 'ntext', 'xml')
	BEGIN
		SET @unicodeFactor = 2;
		IF @size = 2147483647
			SET @size = 1073741823;
	END

	IF @rawType IN ('char', 'varchar', 'nchar', 'nvarchar') AND @len = 1
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Char', 'Char?', 0, 'GetCharFromString', 'SetString', null, null, null, @unicodeFactor, 
			Replace(Replace(Replace(@rawType, 'n', 'N'), 'var', 'Var'), 'char', 'Char'), 1);

	ELSE IF @rawType IN ('char', 'varchar', 'nchar', 'nvarchar')
		INSERT INTO @Result VALUES(@sqlType, 'System', 'String', 'String?', 1, 'GetString', 'SetString', @len, null, null, IsNull(@size, 30) * @unicodeFactor, 
			Replace(Replace(Replace(@rawType, 'n', 'N'), 'var', 'Var'), 'char', 'Char'), @len);

	ELSE IF @rawType IN ('text', 'ntext', 'xml')
		INSERT INTO @Result VALUES(@sqlType, 'System', 'String', 'String?', 1, 'GetString', 'SetString', @len, null, null, 2147483646, 
			Replace(Replace(Replace(@rawType, 'n', 'N'), 'text', 'Text'), 'xml', 'Xml'), @len);

	ELSE IF @rawType IN ('sysname')
		INSERT INTO @Result VALUES(@sqlType, 'System', 'String', 'String?', 1, 'GetString', 'GetString', 128, null, null, 256,
			'NVarChar', 128);

	ELSE IF @rawType IN ('uniqueidentifier')
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Guid', 'Guid?', 0, 'GetGuid', 'SetGuid', @len, null, null, NULL,
			'UniqueIdentifier', null);

	ELSE IF @rawType IN ('binary', 'varbinary', 'image')
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Byte[]', 'Byte[]?', 1, 'GetBytes', 'SetBytes', @len, null, null, IsNull(@size, 30),
			Replace(Replace(Replace(@rawType, 'binary', 'Binary'), 'var', 'Var'), 'image', 'Image'), @len);

	ELSE IF @rawType IN ('smallmoney', 'money', 'numeric', 'decimal')
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Decimal', 'Decimal?', 0, 'GetDecimal', 'SetDecimal', null, @len, @scale, null, 
			Replace(Replace(Replace(Replace(@rawType, 'small', 'Small'), 'money', 'Money'), 'numeric', 'Decimal'), 'decimal', 'Decimal'), null);

	ELSE IF @rawType = 'float'
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Double', 'Double?', 0, 'GetDouble', 'SetDouble', null, null, null, null, 
			Replace(@rawType, 'float', 'Float'), null);

	ELSE IF @rawType = 'real'
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Single', 'Single?', 0, 'GetFloat', 'SetFloat', null, null, null, null, 
			Replace(@rawType, 'real', 'Real'), null);

	ELSE IF @rawType IN ('date', 'time', 'smalldatetime', 'smalldatetime2', 'datetime', 'datetime2', 'datetimeoffset')
		INSERT INTO @Result VALUES(@sqlType, 'System', 'DateTime', 'DateTime?', 0, 'GetDateTime', 'SetDateTime', null, @len, @scale, null,
			Replace(Replace(Replace(Replace(@rawType, 'date', 'Date'), 'time', 'Time'), 'small', 'Small'), 'offset', 'Offset'), null);

	ELSE IF @sqlType = 'bigint'
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Int64', 'Int64?', 0, 'GetInt64', 'SetInt64', null, null, null, null, 'BigInt', null);

	ELSE IF @sqlType = 'int'
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Int32', 'Int32?', 0, 'GetInt32', 'SetInt32', null, null, null, null, 'Int', null);

	ELSE IF @sqlType = 'smallint'
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Int16', 'Int16?', 0, 'GetInt16', 'SetInt16', null, null, null, null, 'SmallInt', null);

	ELSE IF @sqlType = 'tinyint'
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Byte', 'Byte?', 0, 'GetByte', 'SetByte', null, null, null, null, 'TinyInt', null);

	ELSE IF @sqlType = 'bit'
		INSERT INTO @Result VALUES(@sqlType, 'System', 'Boolean', 'Boolean?', 0, 'GetBoolean', 'SetBoolean', null, null, null, null, 'Bit', null);

	ELSE IF @sqlType LIKE '%.%'
	BEGIN
		SET @rawType = SUBSTRING(@sqlType, CHARINDEX('.', @sqlType) + 1, 999);
		IF @rawType LIKE 'TT~_%' ESCAPE '~'
			SET @rawType = SUBSTRING(@rawType, 4, 999);
		IF @rawType LIKE '%~_V[0123456789]' ESCAPE '~'
			INSERT INTO @Result VALUES(@sqlType, null, SUBSTRING(@rawType, 1, LEN(@rawType) - 3), '****', 1, null, null, null, null, null, null, null, null);

		ELSE IF @rawType LIKE '%~_V[0123456789][0123456789]' ESCAPE '~'
			INSERT INTO @Result VALUES(@sqlType, null, SUBSTRING(@rawType, 1, LEN(@rawType) - 4), '****', 1, null, null, null, null, null, null, null, null);

		ELSE IF @rawType LIKE '%~_V[0123456789][0123456789][0123456789]' ESCAPE '~'
			INSERT INTO @Result VALUES(@sqlType, null, SUBSTRING(@rawType, 1, LEN(@rawType) - 5), '****', 1, null, null, null, null, null, null, null, null);

		ELSE IF @rawType LIKE '%~_V[0123456789][0123456789][0123456789][0123456789]' ESCAPE '~'
			INSERT INTO @Result VALUES(@sqlType, null, SUBSTRING(@rawType, 1, LEN(@rawType) - 6), '****', 1, null, null, null, null, null, null, null, null);

		ELSE IF @rawType LIKE '%~_V[0123456789][0123456789][0123456789][0123456789][0123456789]' ESCAPE '~'
			INSERT INTO @Result VALUES(@sqlType, null, SUBSTRING(@rawType, 1, LEN(@rawType) - 7), '****', 1, null, null, null, null, null, null, null, null);

		ELSE 
			INSERT INTO @Result VALUES(@sqlType, null, @rawType, @rawType, 1, null, null, null, null, null, null, null, null);

		UPDATE @Result 
		SET NullableType = BaseType
		WHERE NullableType = '****';
	END

	IF NOT EXISTS(SELECT * FROM @Result)
		INSERT INTO @Result VALUES(@sqlType, null, '<not implemented: ' + @sqlType + '>', '<not implemented: ' + @sqlType + '>', 0, null, null, null, null, null, null, null, null);

	RETURN;

	-- Tests
	-- SELECT * FROM [build].[ConvertToNetType]('nvarchar(10)')
	-- SELECT * FROM [build].[ConvertToNetType]('nvarchar(max)')
	-- SELECT * FROM [build].[ConvertToNetType]('varchar(max)')
	-- SELECT * FROM [build].[ConvertToNetType]('char(1)')
	-- SELECT * FROM [build].[ConvertToNetType]('float(53)')
	-- SELECT * FROM [build].[ConvertToNetType]('dbo.TT_SaveVerkauf_V1');
END
GO
