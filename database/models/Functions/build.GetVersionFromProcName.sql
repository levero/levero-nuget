SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Get version from procedure name as float
-- =============================================
CREATE FUNCTION [build].[GetVersionFromProcName] 
(
	@ProcedureName sysname
)
RETURNS varchar(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(10);

	-- Last characters must be in format _V0_0 or _V0
	DECLARE @ch char, @i int = LEN(@ProcedureName), @mode int = 1, @value varchar(10) = '';
	WHILE @i > 0
	BEGIN
		SET @ch = SUBSTRING(@ProcedureName, @i, 1);
		IF @ch = '_' AND @mode = 1
		BEGIN
			-- first search is right part of comma
			SET @Result = @value;
			SET @mode += 1;
			SET @value = '';
		END
		ELSE IF @ch = 'V' AND @mode = 1
		BEGIN
			-- format _V0
			SET @Result = @value;
			SET @mode += 2;
		END
		ELSE IF @ch = 'V' AND @mode = 2
		BEGIN
			-- second mode is left part of comma
			SET @Result = @value + '.' + @Result;
			SET @mode += 1;
		END
		ELSE IF @ch = '_' AND @mode = 3
		BEGIN
			-- third mode is _V found -> ready
			SET @i = 0;
		END
		ELSE
		BEGIN
			-- parse digit
			IF CHARINDEX(@ch, '0123456789') < 1
				RETURN NULL;
			-- build value from digit
			SET @value = @ch + @value;
		END

		-- next character
		SET @i -= 1;
	END

	-- Return the result of the function
	RETURN Convert(varchar(10), @Result);

	-- Tests
	-- PRINT build.GetVersionFromProcName('ACT_Roles_SetRoleManager_V0_4');
	-- PRINT build.GetVersionFromProcName('ACT_Roles_SetRoleManager_V11_4');
	-- PRINT build.GetVersionFromProcName('ACT_Roles_SetRoleManager_V322_922');
	-- PRINT build.GetVersionFromProcName('ACT_Roles_SetRoleManager_V4_84');
	-- PRINT build.GetVersionFromProcName('ACT_Roles_SetRoleManager_V3');
	-- PRINT build.GetVersionFromProcName('ACT_Roles_SetRoleManager_V123');
	-- PRINT build.GetVersionFromProcName('ILS_TestMethod_V4_99');
	-- PRINT build.GetVersionFromProcName('ILS_TestMethod_V4_100');
	-- PRINT build.GetVersionFromProcName('ILS_TestMethod_V4_101');
END
GO
