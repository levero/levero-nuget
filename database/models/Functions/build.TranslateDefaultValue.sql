SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      Ing. Karl Prokupek
-- Create date: 2024-01-24
-- Description: can a default value of a SQL parameter be used 
--              as a Typescript or c# default value?
-- =============================================
CREATE FUNCTION [build].[TranslateDefaultValue]
(
    @SqlType            SYSNAME,
    @IsNullable         BIT,
    @TableTypeObjectID  INT,
    @DefaultValue       NVARCHAR(max)
)
RETURNS NVARCHAR(max)
--WITH SCHEMABINDING
AS
BEGIN
    DECLARE @Translated NVARCHAR(max);

    -- is table type?
    IF @TableTypeObjectID IS NOT NULL
        SET @Translated = '[]';

    -- is nullable?
    ELSE IF @DefaultValue = 'NULL' AND @IsNullable = 1
        SET @Translated = 'null';

    -- is it a bit?
    ELSE IF @SqlType = 'bit'
    BEGIN
        IF @DefaultValue = '0'
            SET @Translated = 'false';
        ELSE IF @DefaultValue = '1'
            SET @Translated = 'true';
    END

    -- is it a varchar?
    ELSE IF @SqlType LIKE 'VARCHAR(%)' OR @SqlType LIKE 'NVARCHAR(%)' OR 
        @SqlType LIKE 'CHAR(%)' OR @SqlType LIKE 'NCHAR(%)' OR
        @SqlType IN ('TEXT', 'NTEXT')
    BEGIN
        IF @DefaultValue LIKE '''%''' OR @DefaultValue = ''''''
            SET @Translated = @DefaultValue;
    END

    -- is it an integer?
    ELSE IF @SqlType IN ('TINYINT', 'SMALLINT', 'INT', 'BIGINT')
    BEGIN
        DECLARE @i BIGINT = TRY_PARSE(@DefaultValue AS BIGINT);
        IF @i IS NOT NULL
            SET @Translated = FORMAT(@i, '0');
    END

    -- is it a number?
    ELSE IF @SqlType LIKE 'DECIMAL(%)' OR @SqlType LIKE 'DECIMAL(%,%)' OR
        @SqlType LIKE 'NUMBER(%,%)' OR @SqlType LIKE 'NUMBER(%)' OR @SqlType LIKE 'FLOAT(%)' OR
        @SqlType IN ('FLOAT', 'REAL', 'MONEY', 'SMALLMONEY')
    BEGIN
        DECLARE @n DECIMAL(18,6) = ISNULL(TRY_PARSE(@DefaultValue AS DECIMAL(18,6)), 
               TRY_CONVERT(DECIMAL(18,6), TRY_PARSE(@DefaultValue AS FLOAT(53))));
        IF @n IS NOT NULL
            SET @Translated = FORMAT(@n, '#0.######', 'en-US');
    END

    -- is it a date?
    ELSE IF @SqlType = 'DATE'
    BEGIN
        DECLARE @d DATE = ISNULL(TRY_CONVERT(DATE, @DefaultValue, 112), -- ISO          yyyymmdd
                                 TRY_CONVERT(DATE, @DefaultValue, 23)); -- ISO 8601     yyyy-mm-dd
        IF @d IS NOT NULL
            SET @Translated = FORMAT(@d, 'yyyy-MM-dd', 'en-US');
        IF @DefaultValue IN ('getdate()', 'sysdatetime()')
            SET @Translated = '[TODAY]';
    END

    RETURN @Translated;

    -- Tests
    -- PRINT build.TranslateDefaultValue('bit', 1, null, null);     -- no default value
    -- PRINT build.TranslateDefaultValue('bit', 1, null, 'NULL');   -- null
    -- PRINT build.TranslateDefaultValue('bit', 1, null, '0');      -- false
    -- PRINT build.TranslateDefaultValue('bit', 1, null, '1');      -- true
    -- PRINT build.TranslateDefaultValue('bit', 1, null, '2');      -- not usable
    -- PRINT build.TranslateDefaultValue('char(10)', 0, null, 'null');      -- usable
    -- PRINT build.TranslateDefaultValue('char(10)', 1, null, 'null');      -- usable
    -- PRINT build.TranslateDefaultValue('char(10)', 1, null, '''char(10)''');      -- usable
    -- PRINT build.TranslateDefaultValue('nchar(10)', 1, null, '''nchar(10)''');      -- usable
    -- PRINT build.TranslateDefaultValue('varchar(10)', 1, null, '''varchar(10)''');      -- usable
    -- PRINT build.TranslateDefaultValue('nvarchar(10)', 1, null, '''nvarchar(10)''');      -- usable
    -- PRINT build.TranslateDefaultValue('nvarchar(max)', 1, null, '''nvarchar(max)''');      -- usable
    -- PRINT build.TranslateDefaultValue('text', 1, null, '''text''');      -- usable
    -- PRINT build.TranslateDefaultValue('text', 1, null, 'text');      -- not usable
    -- PRINT build.TranslateDefaultValue('decimal(18,6)', 1, null, '100227.34');      -- usable
    -- PRINT build.TranslateDefaultValue('decimal(18,6)', 1, null, '1.3e4');      -- usable
    -- PRINT build.TranslateDefaultValue('tt.ID_Type', 1, 12345678, null);      -- usable
END
GO
