SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-10-19
-- Description:	Generates a backend model description
-- =============================================
CREATE PROCEDURE [build].[GenerateBackendReportModels2]
	@AppPackageID			varchar(15),
	@AppPackageVersion		varchar(10),
	@ReportModels			nvarchar(max) OUT
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @AppVersionComparable int = build.VersionToInt(@AppPackageVersion);
	DECLARE @generator varchar(15) = 'reports';
	DECLARE @True	bit = 1;
	DECLARE @False	bit = 0;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- collect procedures
	---------------------------------------------------------------------------------------------------------------------------------------------
	
	-- which database, schema and prefix?
	DECLARE @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max), @listOfModules nvarchar(max);
	SELECT 
		@databaseName = DatabaseName,
		@listOfSchema = BackendReports_ListOfSchema,
		@listOfPrefix = BackendReports_ListOfPrefix,
		@listOfModules = BackendReports_ListOfModules
	FROM build.Settings2
	WHERE AppPackageID = @AppPackageID;
	
	-- get models and fill table build.TEMP_Procedures2 
	EXEC build.CollectModels2 @generator, @AppPackageID, '', @databaseName, @AppPackageVersion, @listOfSchema, @listOfPrefix, @True;


	-- get modules 
	DECLARE @Modules TABLE(
		ModulID varchar(15) PRIMARY KEY,
		IsDone bit DEFAULT 0
	);
	INSERT INTO @Modules (ModulID)
	SELECT Item
	FROM build.Split2(@listOfModules, ',', @True, @True);

	DECLARE @ModulID varchar(30), @ModulDb sysname, @ModulVersion varchar(10);
	WHILE EXISTS(SELECT * FROM @Modules WHERE IsDone = 0)
	BEGIN
		SELECT TOP(1) 
			@ModulID = m.ModulID,
			@ModulDb = IsNull(NullIf(s.DatabaseName, '*'), @databaseName),
			@ModulVersion = IsNull(NullIf(s.ModulVersion, '*'), @AppPackageVersion),
			@listOfSchema = s.BackendReports_ListOfSchema,
			@listOfPrefix = s.BackendReports_ListOfPrefix,
			@listOfModules = s.BackendReports_ListOfModules
		FROM @Modules m
			LEFT OUTER JOIN build.Settings2 s ON s.AppID = '*' AND s.AppPackageID = m.ModulID
		WHERE IsDone = 0;
		UPDATE @Modules 
		SET IsDone = 1
		WHERE ModulID = @ModulID;

		-- let modules be recursive
		INSERT INTO @Modules (ModulID)
		SELECT s.Item
		FROM build.Split2(@listOfModules, ',', @True, @True) s
		WHERE s.Item NOT IN (SELECT ModulID FROM @Modules);

		-- get models and fill table build.TEMP_Procedures2 
		EXEC build.CollectModels2 @Generator, @AppPackageID, @ModulID, @ModulDb, @ModulVersion, @listOfSchema, @listOfPrefix, @True;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- check duplicates
	---------------------------------------------------------------------------------------------------------------------------------------------
	PRINT 'check for duplicate model names...';
	IF EXISTS(SELECT modelName, COUNT(*) 
			  FROM build.TEMP_Procedures2 
			  WHERE appPackageID = @AppPackageID AND generator = @Generator AND IsExternal = 0
			  GROUP BY modelName, modulID
			  HAVING COUNT(*) > 1)
	BEGIN
		RAISERROR('There are procedures from different schemas but with the same name!', 16, 1);
		RETURN;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @objectID int, @schemaName sysname, @name sysname, @modelName sysname, @modify_date datetime;
	DECLARE @BackendProcedures nvarchar(max) = null;

	WHILE EXISTS(SELECT * FROM build.TEMP_Procedures2 WHERE appPackageID = @AppPackageID AND generator = @generator AND isExternal = 0 AND done = 0)
	BEGIN
		-- get details of procedure
		SELECT TOP 1 @objectID = objectID, @schemaName = schemaName, @name = name, @modelName = modelName, @modify_date = modify_date
		FROM build.TEMP_Procedures2
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND isExternal = 0
			AND done = 0;

		UPDATE build.TEMP_Procedures2 
		SET done = 1
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND objectID = @objectID;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- build procedure
		---------------------------------------------------------------------------------------------------------------------------------------------
		DECLARE @backendDef nvarchar(max);
		--PRINT 'EXEC build.GenerateBackendProcedure2 ''' + @AppPackageID + ''', ''' + @generator + ''', ' + Convert(varchar, @objectID) + ', @s out;';
		EXEC build.GenerateBackendProcedure2 @AppPackageID, @generator, @objectID, @backendDef OUT;

		SET @BackendProcedures = IsNull(@BackendProcedures + ',', '') + @backendDef;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- save backend models
	---------------------------------------------------------------------------------------------------------------------------------------------
	SET @ReportModels = '{
' + IsNull(@BackendProcedures, '') + '
}';

	-- Tests
	-- DECLARE @s nvarchar(max); EXEC build.GenerateBackendReportModels2 'levero-trans', '0.27', @s OUTPUT; PRINT @s;
END
GO
