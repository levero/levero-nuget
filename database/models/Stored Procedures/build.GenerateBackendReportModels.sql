SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-10-19
-- Description:	Generates a backend model description
-- =============================================
CREATE PROCEDURE [build].[GenerateBackendReportModels]
	@AppPackageID			varchar(15),
	@AppPackageVersion		varchar(10),
	@ReportModels			nvarchar(max) OUT
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @AppVersionComparable int = build.VersionToInt(@AppPackageVersion);
	DECLARE @generator varchar(15) = 'reports';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- collect procedures
	---------------------------------------------------------------------------------------------------------------------------------------------
	
	-- which database, schema and prefix?
	DECLARE @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max);
	SELECT 
		@databaseName = DatabaseName,
		@listOfSchema = BackendReports_ListOfSchema,
		@listOfPrefix = BackendReports_ListOfPrefix
	FROM build.Settings
	WHERE AppPackageID = @AppPackageID;
	
	-- get models and fill table build.TEMP_Procedures 
	EXEC build.CollectModels @generator, @databaseName, @AppPackageID, @AppPackageVersion, @listOfSchema, @listOfPrefix, 1;
	
	DECLARE @objectID int, @schemaName sysname, @name sysname, @modelName sysname, @modify_date datetime;
	DECLARE @BackendProcedures nvarchar(max) = null;

	WHILE EXISTS(SELECT * FROM build.TEMP_Procedures WHERE appPackageID = @AppPackageID AND generator = @generator AND isExternal = 0 AND done = 0)
	BEGIN
		-- get details of procedure
		SELECT TOP 1 @objectID = objectID, @schemaName = schemaName, @name = name, @modelName = modelName, @modify_date = modify_date
		FROM build.TEMP_Procedures
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND isExternal = 0
			AND done = 0;

		UPDATE build.TEMP_Procedures 
		SET done = 1
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND objectID = @objectID;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- build procedure
		---------------------------------------------------------------------------------------------------------------------------------------------
		DECLARE @backendDef nvarchar(max);
		--PRINT 'EXEC build.GenerateBackendProcedure ''' + @AppPackageID + ''', ''' + @generator + ''', ' + Convert(varchar, @objectID) + ', @s out;';
		EXEC build.GenerateBackendProcedure @AppPackageID, @generator, @objectID, @backendDef OUT;

		SET @BackendProcedures = IsNull(@BackendProcedures + ',', '') + @backendDef;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- save backend models
	---------------------------------------------------------------------------------------------------------------------------------------------
	SET @ReportModels = '{
' + IsNull(@BackendProcedures, '') + '
}';

	-- Tests
	-- DECLARE @s nvarchar(max); EXEC build.GenerateBackendReportModels 'levero-trans', '0.27', @s OUTPUT; PRINT @s;
END
GO
