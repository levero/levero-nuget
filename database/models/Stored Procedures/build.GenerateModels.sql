SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-03-11
-- Description:	Creates models for a specific app package and version
-- =============================================
CREATE PROCEDURE [build].[GenerateModels] 
	@AppPackageID			varchar(15), 
	@AppPackageVersion		varchar(10),
	@FrontendGenerator		varchar(15) = NULL,
	@WatcherCommand			nvarchar(max) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @FrontendGenerator = ISNULL(LOWER(@FrontendGenerator), 'typescript');
	IF @FrontendGenerator NOT IN ('csharp', 'typescript')
	BEGIN
		RAISERROR('Parameter @FrontendGenerator must be charp or typescript!', 16, 1);
		RETURN;
	END

	-- which database 
	DECLARE @appID varchar(15), @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max), @removePrefix bit, @modelNamespace nvarchar(max);
	DECLARE @sql nvarchar(max), @models nvarchar(max);
	SELECT
		@appID = AppID, 
		@databaseName = DatabaseName
	FROM build.Settings
	WHERE AppPackageID = @AppPackageID;

	IF @@ROWCOUNT = 0
	BEGIN
		RAISERROR('No app package with ID %s found!', 16, 1, @AppPackageID);
		RETURN;
	END

	-- prepare temp tables
	DELETE FROM build.TEMP_Procedures WHERE appPackageID = @AppPackageID;
	DELETE FROM build.TEMP_Prefixes WHERE appPackageID = @AppPackageID;
	DELETE FROM build.TEMP_Schemas WHERE appPackageID = @AppPackageID;

	-- backend actions?
	IF EXISTS(SELECT * FROM build.Settings WHERE AppPackageID = @AppPackageID AND BackendActions_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateBackendActionModels @AppPackageID, @AppPackageVersion, @models OUTPUT;

		-- and save them into settings table of target database
		DECLARE @SettingInstance nvarchar(max) = @AppPackageID + '|' + @AppPackageVersion;
		SET @sql = N'EXEC [' + @databaseName + N'].dbo.SYS_Settings_SaveSystem @AppID, ''Backend-Actions'', @SettingInstance, ''s:max'', @models;';
		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
			@AppID = @appID, @SettingInstance = @SettingInstance, @models = @models;

-- LEGACY: remove after switching all backends to V5
		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
			@AppID = @appID, @SettingInstance = @AppPackageVersion, @models = @models;
	END

	-- backend reports?
	IF EXISTS(SELECT * FROM build.Settings WHERE AppPackageID = @AppPackageID AND BackendReports_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateBackendReportModels @AppPackageID, @AppPackageVersion, @models OUTPUT;

		-- and save them into settings table of target database
		SET @SettingInstance = @AppPackageID + '|' + @AppPackageVersion;
		SET @sql = N'EXEC [' + @databaseName + N'].dbo.SYS_Settings_SaveSystem @AppID, ''Backend-Reports'', @SettingInstance, ''s:max'', @models;';
		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
			@AppID = @appID, @SettingInstance = @SettingInstance, @models = @models;

-- LEGACY: remove after switching all backends to V5
		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
			@AppID = @appID, @SettingInstance = @AppPackageVersion, @models = @models;
	END

	-- typescript models?
	IF @FrontendGenerator = 'typescript'
		AND EXISTS(SELECT * FROM build.Settings WHERE AppPackageID = @AppPackageID AND Typescript_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateTypescriptModels @AppPackageID, @AppPackageVersion;

		-- which database, schema and prefix?
		SELECT 
			@listOfSchema = Typescript_ListOfSchema,
			@listOfPrefix = Typescript_ListOfPrefix,
			@removePrefix = 1,
			@modelNamespace = null
		FROM build.Settings
		WHERE AppPackageID = @AppPackageID;
	END

	-- c# models?
	IF @FrontendGenerator = 'csharp'
		AND EXISTS(SELECT * FROM build.Settings WHERE AppPackageID = @AppPackageID AND CSharp_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateCSharpModels @AppPackageID, @AppPackageVersion, 'SELECT';

		-- which database, schema and prefix?
		SELECT 
			@listOfSchema = CSharp_ListOfSchema,
			@listOfPrefix = CSharp_ListOfPrefix,
			@removePrefix = CSharp_RemovePrefix,
			@modelNamespace = CSharp_ModelNamespace
		FROM build.Settings
		WHERE AppPackageID = @AppPackageID;
	END

	-- build watcher command 
	DECLARE @max_modify_date datetime = IsNull((SELECT MAX(modify_date)
												FROM build.TEMP_Procedures 
												WHERE appPackageID = @AppPackageID 
													AND generator = @FrontendGenerator),
												GetDate());


	WITH dbList AS 
	(
		SELECT DatabaseName
		FROM build.TEMP_Procedures
		WHERE appPackageID = @AppPackageID 
			AND generator = @FrontendGenerator
		GROUP BY databaseName
	)
	SELECT 
		@WatcherCommand = N'
	-- START WATCHER COMMAND !!!
	WITH rawData AS
	(
		SELECT
			''' + @databaseName + N''' as databaseName,
			pf.appPackageID,
			pf.generator,
			' + ISNULL(N'''' + @modelNamespace + N'''', N'Convert(sysname, null)') + N' as modelNamespace,
			p.[object_id] as objectID,
			s.[name] as schemaName,
			p.[name],
			p.modify_date,
			[' + DB_NAME(DB_ID()) + N'].build.GetModelNameFromProcName(p.[name], ' + Convert(nchar(1), @removePrefix) + N') as modelName,
			[' + DB_NAME(DB_ID()) + N'].build.GetVersionFromProcName(p.[name]) as procVersion,
			Convert(bit, 0) AS IsExternal
		FROM [' + @databaseName + N'].sys.procedures p
			INNER JOIN [' + @databaseName + N'].sys.schemas s ON s.[schema_id] = p.[schema_id]
			INNER JOIN [' + DB_NAME(DB_ID()) + N'].build.TEMP_Prefixes pf ON p.[name] LIKE (pf.prefix + ''%'') COLLATE DATABASE_DEFAULT
			INNER JOIN [' + DB_NAME(DB_ID()) + N'].build.TEMP_Schemas ps ON p.[schema_id] = ps.schemaID AND ps.appPackageID = pf.appPackageID COLLATE DATABASE_DEFAULT AND ps.generator = pf.generator COLLATE DATABASE_DEFAULT
		WHERE pf.appPackageID = ''' + REPLACE(@AppPackageID, N'''', N'''''') + N'''
			AND pf.generator = ''' + @FrontendGenerator + N'''' + 
			
		STRING_AGG('

		UNION ALL

		SELECT tp.databaseName, tp.appPackageID, tp.generator, tp.modelNamespace, tp.objectID, tp.schemaName, tp.[name], 
			p.modify_date, 
			[' + DB_NAME(DB_ID()) + N'].build.GetModelNameFromProcName(p.[name], 1) as modelName,
			[' + DB_NAME(DB_ID()) + N'].build.GetVersionFromProcName(p.[name]) as procVersion,
			IsExternal
		FROM [' + DB_NAME(DB_ID()) + N'].build.TEMP_Procedures tp
			INNER JOIN [' + DatabaseName + N'].sys.procedures p ON p.[object_id] = tp.objectID
		WHERE tp.appPackageID = ''' + REPLACE(@AppPackageID, N'''', N'''''') + N'''
			AND tp.generator = ''' + @FrontendGenerator + N'''
			AND tp.isExternal = 1', '') + '
	), validProcs AS
	(
		SELECT 
			databaseName,
			appPackageID,
			generator,
			modelNamespace,
			objectID, 
			schemaName, 
			[name], 
			modelName, 
			procVersion, 
			modify_date,
			ROW_NUMBER() OVER(PARTITION BY schemaName, modelName ORDER BY [' + DB_NAME(DB_ID()) + N'].build.VersionToInt(procVersion) DESC) AS latestIsOne
		FROM rawData
		WHERE modelName IS NOT NULL
			AND (IsExternal = 1
				OR [' + DB_NAME(DB_ID()) + N'].build.VersionToInt(procVersion) <= ' + Convert(nvarchar(20), LeveroModels.build.VersionToInt(@AppPackageVersion)) + N')	-- V' + @AppPackageVersion + N'
	)
	SELECT 
		p.databaseName COLLATE DATABASE_DEFAULT,
		p.schemaName COLLATE DATABASE_DEFAULT, 
		p.[name] COLLATE DATABASE_DEFAULT,
		p.modelNamespace COLLATE DATABASE_DEFAULT,
		p.modelName COLLATE DATABASE_DEFAULT,
		p.modify_date,
		''SELECT'' AS OutputType,
		p.objectID,
		''' + @FrontendGenerator + N''' as generator
	FROM validProcs p
		LEFT OUTER JOIN [' + DB_NAME(DB_ID()) + N'].build.TEMP_Procedures t ON t.modelName = p.modelName AND t.appPackageID = p.appPackageID AND t.generator = p.generator
	WHERE p.latestIsOne = 1 
		AND (p.modify_date > ''' + Convert(nvarchar(30), @max_modify_date, 126) + N'''	-- procedure has been updated
			OR t.[name] IS NULL							-- procedure has been created
			OR t.objectID <> p.objectID)				-- procedure has been deleted and an older procedure exists

	UNION ALL

	-- for all deleted procedures we need to select all existing rows in TEMP_Procedures with non-existing procedures
	SELECT 
		t.databaseName,
		t.schemaName, 
		t.[name],
		t.modelNamespace,
		t.modelName,
		t.modify_date,
		''SELECT'' AS OutputType,
		t.objectID,
		''' + @FrontendGenerator + N''' as generator
	FROM validProcs p
		RIGHT OUTER JOIN [' + DB_NAME(DB_ID()) + N'].build.TEMP_Procedures t ON t.modelName = p.modelName AND t.appPackageID = p.appPackageID AND t.generator = p.generator
	WHERE p.latestIsOne IS NULL 
		AND t.appPackageID = ''' + REPLACE(@AppPackageID, N'''', N'''''') + N'''
		AND t.generator = ''' + @FrontendGenerator + N'''
	-- END WATCHER COMMAND !!!'
	FROM dbList;

	-- Tests
	-- DECLARE @WatcherCommand nvarchar(max);
	-- EXEC build.GenerateModels 'barcode', '6.0', 'csharp', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels 'ils', '4.7', 'csharp', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels 'levero-trans', '1', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels 'po-shop', '2', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels 'fire-connect', '1', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels 'fire-services', '2.1', 'csharp', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels 'te-datapool', '1.4', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels 'po-smart', '1.5', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
END
GO
