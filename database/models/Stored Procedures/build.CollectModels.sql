SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-03-05
-- Description:	Collects all procedures from target database
-- =============================================
CREATE PROCEDURE [build].[CollectModels]
	@Generator					varchar(15),
	@DatabaseName				sysname,
	@AppPackageID				varchar(15),
	@AppPackageVersion			varchar(10),
	@ListOfSchema				varchar(100),
	@ListOfPrefix				varchar(100),
	@RemovePrefix				bit
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @AppVersionComparable int = build.VersionToInt(@AppPackageVersion);

	IF NOT EXISTS(SELECT * FROM master.sys.databases WHERE [name] = @DatabaseName)
	BEGIN
		RAISERROR('Database with name %s not found!', 16, 1, @DatabaseName);
		RETURN;
	END

	-- first clear all TEMP_ tables
	DELETE FROM build.TEMP_Procedures WHERE appPackageID = @AppPackageID AND generator = @Generator;
	DELETE FROM build.TEMP_Prefixes WHERE appPackageID = @AppPackageID AND generator = @Generator;
	DELETE FROM build.TEMP_Schemas WHERE appPackageID = @AppPackageID AND generator = @Generator;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load settings FOR ACTIONS
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @sql nvarchar(max), @Value varchar(max);

	PRINT '------------------------------------------------------------------------------';
	PRINT 'generate ' + UPPER(@Generator) + ' of app package ' + @AppPackageID + ' version ' + @AppPackageVersion + ' in database ' + @DatabaseName;
	PRINT '------------------------------------------------------------------------------';

	-- get prefix from settings
	INSERT INTO build.TEMP_Prefixes (appPackageID, generator, prefix)
	SELECT @AppPackageID, @Generator, Item 
	FROM build.Split(REPLACE(@ListOfPrefix, ' ', ''), ',');

	-- get schema list from settings
	INSERT INTO build.TEMP_Schemas(appPackageID, generator, schemaName)
	SELECT @AppPackageID, @Generator, Item
	FROM build.Split(REPLACE(@ListOfSchema, ' ', ''), ',');

	-- check schemas
	SET @sql = N'
	UPDATE build.TEMP_Schemas
	SET schemaID = s.[schema_id]
	FROM build.TEMP_Schemas t
		INNER JOIN [' + @DatabaseName + N'].sys.schemas s ON s.[name] = t.schemaName COLLATE DATABASE_DEFAULT
	WHERE t.appPackageID = ''' + REPLACE(@AppPackageID, '''', '''''') + N'''
		AND t.generator = ''' + REPLACE(@Generator, '''', '''''') + N''';
	IF EXISTS(SELECT * FROM build.TEMP_Schemas 
			  WHERE appPackageID = ''' + REPLACE(@AppPackageID, '''', '''''') + N''' 
				  AND generator = ''' + REPLACE(@Generator, '''', '''''') + N''' AND schemaID IS NULL)
	BEGIN
		RAISERROR(''one or more schemas in database ' + @DatabaseName + N' not found!'', 16, 1);
		RETURN;
	END';
	--PRINT @sql;
	EXEC sys.sp_executesql @sql;

	-- which schemas are needed
	DECLARE @SchemaIDs nvarchar(max) = null;
	SELECT @SchemaIDs = IsNull(@SchemaIDs + ', ', '') + Convert(varchar, schemaID)
	FROM build.TEMP_Schemas
	WHERE appPackageID = @AppPackageID 
		AND generator = @Generator;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load all procedures with given schemas and prefix
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @Procedures TABLE (objectID int NOT NULL, modify_date datetime not null, schemaName sysname NOT NULL, name sysname NOT NULL, modelName sysname NOT NULL, procVersion varchar(10) NOT NULL);
	WHILE 1 = 1
	BEGIN
		SELECT TOP (1) @Value = prefix FROM build.TEMP_Prefixes WHERE appPackageID = @AppPackageID AND generator = @Generator AND done = 0;
		IF @@ROWCOUNT = 0
			BREAK;
		ELSE
		BEGIN	
			UPDATE build.TEMP_Prefixes SET done = 1 WHERE appPackageID = @AppPackageID AND generator = @Generator AND prefix = @Value;
			SET @sql = N'USE [' + @DatabaseName + N'];
			WITH rawData AS
			(
				SELECT 
					SCHEMA_NAME(p.schema_id) AS schemaName, 
					p.[object_id] as objectID, 
					p.[name], 
					[' + DB_NAME() + N'].build.GetModelNameFromProcName(p.[name], ' + Convert(varchar, IsNull(@RemovePrefix, 0)) + N') AS modelName, 
					[' + DB_NAME() + N'].build.GetVersionFromProcName(p.[name]) as procVersion,
					p.modify_date
				FROM sys.procedures p
				WHERE p.[name] LIKE (''' + REPLACE(@Value, '_', '~_') + N'%'') ESCAPE ''~''
					AND p.[schema_id] IN (' + @SchemaIDs + N')
			), validProcs AS
			(
				SELECT 
					objectID, 
					schemaName, 
					[name], 
					modelName, 
					procVersion, 
					modify_date,
					ROW_NUMBER() OVER(PARTITION BY schemaName, modelName ORDER BY [' + DB_NAME() + N'].build.VersionToInt(procVersion) DESC) AS latestIsOne
				FROM rawData
				WHERE modelName IS NOT NULL
					AND [' + DB_NAME() + N'].build.VersionToInt(procVersion) <= ' + Convert(nvarchar, @AppVersionComparable) + N'
			)
			INSERT INTO [' + DB_NAME() + N'].build.TEMP_Procedures(databaseName, appPackageID, generator, objectID, modify_date, schemaName, name, modelName, procVersion)
			SELECT 
				''' + REPLACE(@DatabaseName, '''', '''''') + N''', 
				''' + REPLACE(@AppPackageID, '''', '''''') + N''',
				''' + REPLACE(@Generator, '''', '''''') + N''',
				objectID, 
				modify_date,
				schemaName, 
				[name], 
				modelName, 
				procVersion
			FROM validProcs
			WHERE latestIsOne = 1
			ORDER BY modelName;';
			--PRINT @sql;
			EXEC sys.sp_executesql @sql;
		END
	END
	DECLARE @count int = (SELECT COUNT(*) FROM build.TEMP_Procedures WHERE appPackageID = @AppPackageID AND generator = @Generator);
	PRINT 'found ' + CONVERT(varchar, @count) + ' procedure(s)';

	-- parse all procedures
	DECLARE @objectToParse int;
	SELECT @objectToParse = MIN(objectID)
	FROM build.TEMP_Procedures
	WHERE appPackageID = @AppPackageID 
		AND generator = @Generator
		AND done = 0;

	WHILE @objectToParse IS NOT NULL
	BEGIN
		-- parse
		EXEC build.ParseProcedure @AppPackageID, @generator, @objectToParse, @RemovePrefix;

		-- set to done
		UPDATE build.TEMP_Procedures
		SET done = 1
		WHERE appPackageID = @AppPackageID 
			AND generator = @Generator
			AND objectID = @objectToParse;

		-- get next
		SELECT @objectToParse = MIN(objectID)
		FROM build.TEMP_Procedures
		WHERE appPackageID = @AppPackageID 
			AND generator = @Generator
			AND done = 0;
	END

	-- check duplicates
	PRINT 'check for duplicate model names...';
	IF EXISTS(SELECT modelName, COUNT(*) 
			  FROM build.TEMP_Procedures 
			  WHERE appPackageID = @AppPackageID AND generator = @Generator 
			  GROUP BY modelName 
			  HAVING COUNT(*) > 1)
	BEGIN
		RAISERROR('There are procedures from different schemas but with the same name!', 16, 1);
		RETURN;
	END

	-- reset done
	UPDATE build.TEMP_Procedures
	SET done = 0
	WHERE appPackageID = @AppPackageID 
		AND generator = @Generator;

	PRINT 'finished';

	-- Tests
	-- EXEC build.CollectModels 'actions', 'FuhrparkTest', 'levero-trans', '2', 'dbo', 'V3_', 1;
	-- EXEC build.CollectModels 'actions', 'Pumpenoase', 'ils', '99', 'dbo', 'ACT_', 1;
	-- EXEC build.CollectModels 'actions', 'LeveroApps', 'te-capex-fc', '2', 'dbo,roles,capex', 'ACT_', 1;
	-- EXEC build.CollectModels 'typescript', 'LeveroApps', 'te-datapool', '1.4', 'dbo,roles,db', 'ACTF_', 1;
END
GO
