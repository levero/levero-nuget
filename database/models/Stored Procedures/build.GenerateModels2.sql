SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-03-11
-- Description:	Creates models for a specific app package and version
-- =============================================
CREATE PROCEDURE [build].[GenerateModels2] 
	@AppPackageID			varchar(15), 
	@AppPackageVersion		varchar(10),
	@FrontendGenerator		varchar(15) = NULL,
	@WatcherCommand			nvarchar(max) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	-- constants
	DECLARE @ModelsDB sysname = QUOTENAME(DB_NAME());

	SET @FrontendGenerator = ISNULL(LOWER(@FrontendGenerator), 'typescript');
	IF @FrontendGenerator NOT IN ('csharp', 'typescript')
	BEGIN
		RAISERROR('Parameter @FrontendGenerator must be charp or typescript!', 16, 1);
		RETURN;
	END

	-- which database 
	DECLARE @appID varchar(15), @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max), @removePrefix bit, @modelNamespace nvarchar(max);
	DECLARE @sql nvarchar(max), @models nvarchar(max);
	SELECT
		@appID = AppID, 
		@databaseName = DatabaseName
	FROM build.Settings2
	WHERE AppPackageID = @AppPackageID;

	IF @@ROWCOUNT = 0
	BEGIN
		RAISERROR('No app package with ID %s found!', 16, 1, @AppPackageID);
		RETURN;
	END

	-- prepare temp tables
	DELETE FROM build.TEMP_Procedures2 WHERE appPackageID = @AppPackageID;
	DELETE FROM build.TEMP_Filter2 WHERE appPackageID = @AppPackageID;

	-- backend actions?
	IF EXISTS(SELECT * FROM build.Settings2 WHERE AppPackageID = @AppPackageID AND BackendActions_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateBackendActionModels2 @AppPackageID, @AppPackageVersion, @models OUTPUT;

		-- and save them into settings table of target database
		DECLARE @SettingInstance nvarchar(max) = @AppPackageID + '|' + @AppPackageVersion;
		SET @sql = N'EXEC [' + @databaseName + N'].dbo.SYS_Settings_SaveSystem @AppID, ''Backend-Actions'', @SettingInstance, ''s:max'', @models;';
		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
			@AppID = @appID, @SettingInstance = @SettingInstance, @models = @models;

-- LEGACY: remove after switching all backends to V5
--		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
--			@AppID = @appID, @SettingInstance = @AppPackageVersion, @models = @models;
	END

	-- backend reports?
	IF EXISTS(SELECT * FROM build.Settings2 WHERE AppPackageID = @AppPackageID AND BackendReports_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateBackendReportModels2 @AppPackageID, @AppPackageVersion, @models OUTPUT;

		-- and save them into settings table of target database
		SET @SettingInstance = @AppPackageID + '|' + @AppPackageVersion;
		SET @sql = N'EXEC [' + @databaseName + N'].dbo.SYS_Settings_SaveSystem @AppID, ''Backend-Reports'', @SettingInstance, ''s:max'', @models;';
		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
			@AppID = @appID, @SettingInstance = @SettingInstance, @models = @models;

-- LEGACY: remove after switching all backends to V5
--		EXEC sys.sp_executesql @sql, N'@AppID varchar(15), @SettingInstance nvarchar(max), @models nvarchar(max)', 
--			@AppID = @appID, @SettingInstance = @AppPackageVersion, @models = @models;
	END

	-- typescript models?
	IF @FrontendGenerator = 'typescript'
		AND EXISTS(SELECT * FROM build.Settings2 WHERE AppPackageID = @AppPackageID AND Typescript_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateTypescriptModels2 @AppPackageID, @AppPackageVersion;

		-- which database, schema and prefix?
		SELECT 
			@listOfSchema = Typescript_ListOfSchema,
			@listOfPrefix = Typescript_ListOfPrefix,
			@removePrefix = 1,
			@modelNamespace = NULL
		FROM build.Settings2
		WHERE AppPackageID = @AppPackageID;
	END

	-- c# models?
	IF @FrontendGenerator = 'csharp'
		AND EXISTS(SELECT * FROM build.Settings2 WHERE AppPackageID = @AppPackageID AND CSharp_Generate = 1)
	BEGIN
		-- generate models
		EXEC build.GenerateCSharpModels2 @AppPackageID, @AppPackageVersion, 'SELECT';

		-- which database, schema and prefix?
		SELECT 
			@listOfSchema = CSharp_ListOfSchema,
			@listOfPrefix = CSharp_ListOfPrefix,
			@removePrefix = CSharp_RemovePrefix,
			@modelNamespace = CSharp_ModelNamespace
		FROM build.Settings2
		WHERE AppPackageID = @AppPackageID;
	END

	-- build watcher command 
	DECLARE @max_modify_date datetime = IsNull((SELECT MAX(modify_date)
												FROM build.TEMP_Procedures2 
												WHERE appPackageID = @AppPackageID 
													AND generator = @FrontendGenerator),
												GetDate());


	WITH dbList AS 
	(
		SELECT DatabaseName
		FROM build.TEMP_Procedures2
		WHERE appPackageID = @AppPackageID 
			AND generator = @FrontendGenerator
		GROUP BY databaseName
	)
	SELECT 
		@WatcherCommand = N'
	-- START WATCHER COMMAND !!!
	WITH rawData AS
	(
		SELECT
			ps.databaseName,
			ps.appPackageID,
			ps.modulID,
			ps.generator,
			' + ISNULL(N'''' + @modelNamespace + N'''', N'Convert(sysname, null)') + N' as modelNamespace,
			p.[object_id] as objectID,
			s.[name] as schemaName,
			p.[name],
			p.modify_date,
			' + @ModelsDB + N'.build.GetModelNameFromProcName(p.[name], ' + Convert(nchar(1), @removePrefix) + N') as modelName,
			' + @ModelsDB + N'.build.VersionToInt(' + @ModelsDB + N'.build.GetVersionFromProcName(p.[name])) as procVersion,
			' + @ModelsDB + N'.build.VersionToInt(ps.targetVersion) as targetVersion
		FROM [' + @databaseName + N'].sys.procedures p
			INNER JOIN [' + @databaseName + N'].sys.schemas s ON s.[schema_id] = p.[schema_id]
			INNER JOIN ' + @ModelsDB + N'.build.TEMP_Filter2 ps ON p.[schema_id] = ps.schemaID
			CROSS APPLY ' + @ModelsDB + N'.build.Split2(ps.listOfPrefix, '','', 1, 1) pf
		WHERE p.[name] LIKE (pf.Item + ''%'') COLLATE DATABASE_DEFAULT 
			AND ps.appPackageID = ''' + REPLACE(@AppPackageID, N'''', N'''''') + N'''
			AND ps.generator = ''' + @FrontendGenerator + N'''' + 
			
		STRING_AGG('

		UNION ALL

		SELECT 
			tp.databaseName, 
			tp.appPackageID, 
			tp.modulID,
			tp.generator, 
			tp.modelNamespace, 
			tp.objectID, 
			tp.schemaName, 
			tp.[name], 
			p.modify_date, 
			tp.modelName,
			' + @ModelsDB + N'.build.VersionToInt(procVersion),
			655294464
		FROM ' + @ModelsDB + N'.build.TEMP_Procedures2 tp
			INNER JOIN [' + DatabaseName + N'].sys.procedures p ON p.[object_id] = tp.objectID
		WHERE tp.appPackageID = ''' + REPLACE(@AppPackageID, N'''', N'''''') + N'''
			AND tp.generator = ''' + @FrontendGenerator + N'''
			AND tp.isExternal = 1', '') + '
	), validProcs AS
	(
		SELECT 
			databaseName,
			appPackageID,
			generator,
			modelNamespace,
			objectID, 
			schemaName, 
			[name], 
			modelName, 
			procVersion, 
			modify_date,
			ROW_NUMBER() OVER(PARTITION BY schemaName, modelName ORDER BY procVersion DESC) AS latestIsOne
		FROM rawData
		WHERE modelName IS NOT NULL
			AND procVersion <= targetVersion
	)
	SELECT 
		p.databaseName COLLATE DATABASE_DEFAULT,
		p.schemaName COLLATE DATABASE_DEFAULT, 
		p.[name] COLLATE DATABASE_DEFAULT,
		p.modelNamespace COLLATE DATABASE_DEFAULT,
		p.modelName COLLATE DATABASE_DEFAULT,
		p.modify_date,
		''SELECT'' AS OutputType,
		p.objectID,
		''' + @FrontendGenerator + N''' as generator
		--, t.objectID, t.[name], p.latestIsOne, p.procVersion, p.targetVersion
	FROM validProcs p
		LEFT OUTER JOIN ' + @ModelsDB + N'.build.TEMP_Procedures2 t ON t.databaseName = p.databaseName AND t.modelName = p.modelName AND t.appPackageID = p.appPackageID AND t.generator = p.generator
	WHERE p.latestIsOne = 1 
		AND (p.modify_date > ''' + Convert(nvarchar(30), @max_modify_date, 126) + N'''	-- procedure has been updated
			OR t.[name] IS NULL							-- procedure has been created
			OR t.objectID <> p.objectID)				-- procedure has been deleted and an older procedure exists

	UNION ALL

	-- for all deleted procedures we need to select all existing rows in TEMP_Procedures2 with non-existing procedures
	SELECT 
		t.databaseName,
		t.schemaName, 
		t.[name],
		t.modelNamespace,
		t.modelName,
		t.modify_date,
		''SELECT'' AS OutputType,
		t.objectID,
		''' + @FrontendGenerator + N''' as generator
		--, t.objectID, t.[name], p.latestIsOne, p.procVersion, p.targetVersion
	FROM validProcs p
		RIGHT OUTER JOIN ' + @ModelsDB + N'.build.TEMP_Procedures2 t ON t.databaseName = p.databaseName AND t.modelName = p.modelName AND t.appPackageID = p.appPackageID AND t.generator = p.generator
	WHERE p.latestIsOne IS NULL 
		AND t.appPackageID = ''' + REPLACE(@AppPackageID, N'''', N'''''') + N'''
		AND t.generator = ''' + @FrontendGenerator + N'''
	-- END WATCHER COMMAND !!!'
	FROM dbList;

	-- Tests
	-- DECLARE @WatcherCommand nvarchar(max);
	-- EXEC build.GenerateModels2 'barcode', '6.0', 'csharp', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'ils', '4.7', 'csharp', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'levero-trans', '1', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'po-shop', '2', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'fire-connect', '1', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'fire-services', '2.1', 'csharp', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'te-datapool', '1.4', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'po-smart', '1.5', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'gwk-smart', '1.5', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
	-- EXEC build.GenerateModels2 'easypfand', '1.1', 'typescript', @WatcherCommand OUTPUT; PRINT @WatcherCommand;
END
GO
