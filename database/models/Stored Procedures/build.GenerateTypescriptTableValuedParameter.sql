SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-22
-- Description:	Generates a frontend model description for a table type parameter
-- =============================================
CREATE PROCEDURE [build].[GenerateTypescriptTableValuedParameter]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@objectID					int,				-- of procedure!
	@paramName					sysname,
	@tableTypeName				sysname,
	@frontendModelName			sysname,
	@frontendModelDefinition	nvarchar(max) OUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @procModelName nvarchar(max); 
	SELECT
		@procModelName = modelName
	FROM build.TEMP_Procedures
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for frontend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @frontend_tableType nvarchar(max) =
'/* ***************************************************************************************************************/
/** Request {TT} */
export class {NAME} extends BaseTools {{CONSTANTS}

  constructor({COLUMNS}) {
    super();{PREPARE}
  }
}

';
    DECLARE @frontend_constMaxLen nvarchar(max) = '
  /** param: {COLNAME} {SQLTYPE} */
  public static readonly {COLNAME}_MaxLength = {MAXLEN};';
    DECLARE @frontend_columns nvarchar(max) = '
    /** sql type: {SQLTYPE} */
    public {COLNAME}: {COLTYPE},';
    DECLARE @frontend_prepare nvarchar(max) = '
    this.{COLNAME} = this._tools.{PREPARE}(this.{COLNAME}, true);';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load columns
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @columnID int = 1, @columns nvarchar(max) = '', @colPrepare nvarchar(max) = '', @colConstants nvarchar(max) = '';

	WHILE @columnID > 0
	BEGIN
		DECLARE @colName sysname, @colIsNullable bit, @colIsReadOnly bit, @sqlType sysname, @colDef nvarchar(max) = ' ';
		-- search for column
		SELECT TOP 1
			@colName = colName, 
			@sqlType = sqlType,
			@colDef = colDefinition,
			@colIsNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END,
			@colIsReadOnly = CASE 
				WHEN colDefinition LIKE '% computed %' THEN 1 
				WHEN colDefinition LIKE '% identity %' THEN 1 
				ELSE 0
			END
		FROM build.TEMP_Columns
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND resultset = @paramName
			AND colIndex = @columnID - 1;

		IF @@ROWCOUNT = 0
			SET @columnID = -1;
		ELSE IF @colIsReadOnly = 1
		BEGIN
			--PRINT '        -> found ' + Convert(varchar, @columnID) + '. column ' + @colName + ' with type ' + @sqlType + ' and READONLY !!!';
			DECLARE @dummy int;
		END
		ELSE
		BEGIN
			--PRINT '        -> found ' + Convert(varchar, @columnID) + '. column ' + @colName + ' with type ' + @sqlType;

			-- regular parameter
			SET @columns += REPLACE(REPLACE(REPLACE(@frontend_columns, 
				'{COLNAME}', @colName), 
				'{COLTYPE}', build.GetParamTypeForFrontend(@sqlType)), 
				'{SQLTYPE}', @sqlType);
			SET @colPrepare += ISNULL(REPLACE(REPLACE(@frontend_prepare, 
				'{COLNAME}', @colName), 
				'{PREPARE}', build.GetParamPrepareForFrontend(@sqlType)), '');

			-- is this parameter a string?
			IF @sqlType LIKE '%char([0-9]%'
				SET @colConstants += REPLACE(REPLACE(REPLACE(@frontend_constMaxLen,
				'{COLNAME}', @colName), 
				'{SQLTYPE}', @sqlType),
				'{MAXLEN}', REPLACE(SUBSTRING(@sqlType, CHARINDEX('(', @sqlType) + 1, 999), ')', ''));

		END
		SET @columnID += 1;
	END;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build together
	---------------------------------------------------------------------------------------------------------------------------------------------
	--PRINT '@frontend_tableType: ' + IsNull(@frontend_tableType, '<null>');
	--PRINT '@tableTypeName: ' + IsNull(@tableTypeName, '<null>');
	--PRINT '@frontendModelName: ' + IsNull(@frontendModelName, '<null>');
	--PRINT '@columns: ' + IsNull(@columns, '<null>');
	--PRINT '@colPrepare: ' + IsNull(@colPrepare, '<null>');
	SET @frontendModelDefinition = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_tableType, 
		'{TT}', @tableTypeName), 
		'{NAME}', @frontendModelName), 
		'{COLUMNS}', SUBSTRING(@columns, 1, LEN(@columns) - 1)), 
		'{PREPARE}', @colPrepare),
		'{CONSTANTS}', @colConstants);

	--PRINT '----------------------------- F R O N T E N D -----------------------------';
	--PRINT Convert(varchar, LEN(@frontendModelDefinition)) + ' characters';
	--PRINT @frontendModelDefinition;
	--PRINT '---------------------------------------------------------------------------';

	-- Tests
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptTableType 'te-capex-fc', 'typescript', 2046630334, '@Risks', '[ehsra].[TT_SaveRisk_V1]', 'SaveRiskAnalysis_Risks', @f OUT; PRINT @f;
END
GO
