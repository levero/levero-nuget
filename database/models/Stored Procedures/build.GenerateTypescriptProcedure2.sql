SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-24
-- Description:	Generates a frontend model description for one procedure
-- =============================================
CREATE PROCEDURE [build].[GenerateTypescriptProcedure2]
	@appPackageID				VARCHAR(15),
	@generator					VARCHAR(15),
	@objectID					INT,
	@frontendModelDefinition	NVARCHAR(MAX) OUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @schemaName sysname, @name sysname, @modelName sysname, @modulIDprefix sysname;
	SELECT
		@schemaName = schemaName,
		@name = [name],
		@modelName = modelName,
		@modulIDprefix = IsNull(NullIf(modulID, '') + '|', '')
	FROM build.TEMP_Procedures2
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for frontend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @frontend_request nvarchar(max) =
'/* ***************************************************************************************************************/
/** Request {SP} */
export class {SPCLASS}Request extends ActionRequest {{CONSTANTS}

  constructor({PARAMS}
    actionContext?: ActionContext) {
    super(''{SP}'', it => new {SPCLASS}Response(it), actionContext);{PREPARE}
  }
}

';
    DECLARE @frontend_constMaxLen nvarchar(max) = '
  /** param: {PARAMNAME} {SQLTYPE} */
  public static readonly {PARAMNAME}_MaxLength = {MAXLEN};';
    DECLARE @frontend_param nvarchar(max) = '
    /** sql type: {SQLTYPE}, nullable: {NULLABLE}{SQL_DEFAULT} */
    public {PARAMNAME}: {PARAMTYPE}{DEFAULT},';
    DECLARE @frontend_prepare nvarchar(max) = '
    this.{PARAMNAME} = this._tools.{PREPARE}(this.{PARAMNAME}, true);';
    DECLARE @frontend_prepareTT nvarchar(max) = '
    this.{PARAMNAME} = this.{PARAMNAME} || [];';
    DECLARE @frontend_response_interface nvarchar(max) =
'/* ***************************************************************************************************************/
/** Interface Response {SP} */
export interface {SPCLASS}Result {{TABLES}
}

';
    DECLARE @frontend_response_implement_interface nvarchar(max)= ' implements {SPCLASS}Result';
    DECLARE @frontend_response nvarchar(max) =
'/* ***************************************************************************************************************/
/** Response {SP} */
export class {SPCLASS}Response extends ActionResponse{INTERFACE} {{TABLES}

  constructor(data: any) {
    super();
    // generate all tables (or single rows){CONVERTER}
    // calculate columns (not implemented yet)
  }
}

';
    DECLARE @frontend_response_table_01 nvarchar(max) = '
  /** {TABINDEX}. resultset, cardinality={CARDINAL} */
  {TABNAME}: {SPCLASS}_{TABNAME};';
    DECLARE @frontend_response_table_0n nvarchar(max) = '
  /** {TABINDEX}. resultset, cardinality={CARDINAL} */
  {TABNAME}: {SPCLASS}_{TABNAME}[];';
    DECLARE @frontend_response_converter_01 nvarchar(max) = '
    this.{TABNAME} = (data.{TABNAME} ?? []).map((it: any) => new {SPCLASS}_{TABNAME}(it{SUBTABLES}, this._tools))[0] ?? null;';
    DECLARE @frontend_response_converter_0n nvarchar(max) = '
    this.{TABNAME} = (data.{TABNAME} ?? []).map((it: any) => new {SPCLASS}_{TABNAME}(it{SUBTABLES}, this._tools));';
    DECLARE @frontend_table nvarchar(max) =
'/** {SP}: Klasse vom Ergebnis {TABNAME} ({TABINDEX}. Tabelle) */
export class {SPCLASS}_{TABNAME} {{CONSTANTS}

  constructor(data: any = {}{SUBTABLES}, protected tools: GeneratorTools) {{INIT}
  }{PARAMS}
}

';
    DECLARE @frontend_table_constMaxLen nvarchar(max) = '
  /** param: {COLNAME} {SQLTYPE} */
  public static readonly {COLNAME}_MaxLength = {MAXLEN};';
    DECLARE @frontend_table_init_prepare nvarchar(max) = '
    this.{PARAMNAME} = this.tools.{PREPARE}(data.{PARAMNAME});';
    DECLARE @frontend_table_init_default nvarchar(max) = '
    this.{PARAMNAME} = data.{PARAMNAME} === undefined ? {DEFAULT} : data.{PARAMNAME};';
    DECLARE @frontend_table_row_class nvarchar(max) = '
  /** sql type: {SQLTYPE}, nullable: {NULLABLE} */
  public {PARAMNAME}: {PARAMTYPE};';
    DECLARE @frontend_table_row_class_format nvarchar(max) = '
  /** sql type: {SQLTYPE}, formatter for {PARAMNAME}, format: {FORMATARGS} */
  public get {PARAMNAMEFORMAT}(): string {
    return this.tools.{FORMATTER}(this.{PARAMNAME}{COMMA}{FORMATARGS});
  }';
    DECLARE @frontend_init nvarchar(max) = '
    this.{PARAMNAME} = {DEFAULT};';


    DECLARE @frontend_relation_param_def NVARCHAR(max) = ', {CHILDPARAM}: {SPCLASS}_{CHILD}[]';
    DECLARE @frontend_relation_param_call NVARCHAR(max) = ', this.{CHILD}';
    DECLARE @frontend_relation_filter NVARCHAR(max) = '
    this.{CHILD} = {CHILDPARAM}.filter(it => {COMPARE});';
    DECLARE @frontend_relation_property NVARCHAR(max) = '
  /** all sub table rows "{CHILD}" of this parent table row "{PARENT}" */
  public {CHILD}: {SPCLASS}_{CHILD}[];';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	-- PRINT 'building ' + @modelName + ' from ' + @schemaName + '.' + @name + ' - ' + CONVERT(VARCHAR(MAX), @objectID);

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- get parameters of procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	-- reset variables
	DECLARE @parameterID int, @modelParams nvarchar(max) = '', @modelPrepare nvarchar(max) = '', @frontendTTmodels nvarchar(max) = '', @modelConstants NVARCHAR(MAX) = '';
    DECLARE @colIsNullable BIT, @colDefault NVARCHAR(max), @colTranslatedDefault NVARCHAR(max);
	DECLARE @tableTypes TABLE (tableTypeID int NOT NULL, paramName sysname NOT NULL);
	SET @parameterID = 1;
	WHILE @parameterID > 0
	BEGIN
		DECLARE @paramName sysname = null, @paramSqlType sysname, @isOutput bit, @tableTypeID int;
		DECLARE @hasUser bit = 0, @hasAppID bit = 0;
		SELECT 
			@paramName = colName,
			@paramSqlType = sqlType,
			@tableTypeID = tableTypeObjectID,
			@isOutput = CASE WHEN colDefinition LIKE '% out %' THEN 1 ELSE 0 END,
            @colIsNullable = isNullable,
            @colDefault = defaultValue,
            @colTranslatedDefault = CASE WHEN useDefaultValue = 1 THEN translatedDefaultValue END
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultset = '@'
			AND colIndex = @parameterID - 1;

		--SET @sql = N'
		--SELECT 
		--	@paramName = SUBSTRING(p.name, 2, 999), 
		--	@type = build.GetSqlDataType(t.[name], s.[name], p.max_length, p.[precision], p.scale), 
		--	@isOutput = p.is_output,
		--	@tableTypeID = tt.type_table_object_id
		--FROM [' + @databaseName + N'].sys.parameters p 
		--	INNER JOIN [' + @databaseName + N'].sys.types t ON t.user_type_id = p.user_type_id
		--	INNER JOIN [' + @databaseName + N'].sys.schemas s ON s.[schema_id] = t.[schema_id]
		--	LEFT OUTER JOIN [' + @databaseName + N'].sys.table_types tt ON tt.user_type_id = p.user_type_id
		--WHERE p.[object_id] = ' + Convert(nvarchar, @procedureObjectID) + N' AND p.parameter_id = ' + Convert(nvarchar, @parameterID);
		--EXEC sys.sp_executesql @sql, N'@paramName sysname OUTPUT, @type sysname OUTPUT, @isOutput bit OUTPUT, @tableTypeID int OUTPUT',
		--	@paramName = @paramName OUTPUT, @type = @type OUTPUT, @isOutput = @isOutput OUTPUT, @tableTypeID = @tableTypeID OUTPUT;

		IF @paramName IS NULL
			SET @parameterID = 0;
		ELSE
		BEGIN
			--PRINT '    -> found ' + Convert(varchar, @parameterID) + '. parameter ' + @paramName + ' with type ' + @paramSqlType;

			-- system parameter?
			IF @paramName LIKE '~_%' ESCAPE '~'
			BEGIN
				IF @paramName = '_UserID'
				BEGIN
					SET @hasUser = 1;
					IF @paramSqlType <> 'varchar(10)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(10))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_LogonUserID'
				BEGIN
					IF @paramSqlType <> 'varchar(10)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(10))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_User'		-- OBSOLETE - soll durch _UserID ersetzt werden
				BEGIN
					SET @hasUser = 1;
					IF @paramSqlType <> 'nvarchar(30)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(30))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppID'
				BEGIN
					SET @hasAppID = 1;
					IF @paramSqlType <> 'varchar(15)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(15))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
                ELSE IF @paramName = '_TenantID'
				BEGIN
					SET @hasUser = 1;
					IF @paramSqlType <> 'varchar(15)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(15))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_EnvironmentID'
				BEGIN
					IF @paramSqlType <> 'nvarchar(82)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(82))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppEnvironmentID'
				BEGIN
					IF @paramSqlType <> 'nvarchar(82)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(82))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_ClientIP'
				BEGIN
					IF @paramSqlType <> 'varchar(48)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(48))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE
				BEGIN
					RAISERROR('Procedure %s.%s has an unknown system parameter "%s"!', 16, 1, @schemaName, @name, @paramName);
					RETURN;
				END
			END
			ELSE IF @tableTypeID IS NOT NULL
			BEGIN
				-- build model for table type
				DECLARE @frontendTTname sysname = REPLACE(@modelName, '_', '') + '$' + @paramName;
				IF NOT EXISTS(SELECT * FROM @tableTypes WHERE tableTypeID = @tableTypeID AND paramName = @paramName)
				BEGIN
					DECLARE @resultset sysname = '@' + @paramName, @frontendTTmodel nvarchar(max);
					EXEC build.GenerateTypescriptTableValuedParameter2 @appPackageID, @generator, @objectID, @resultset, @paramSqlType, @frontendTTname, @frontendTTmodel OUT;

					INSERT INTO @tableTypes (tableTypeID, paramName) VALUES (@tableTypeID, @paramName);
				END

				-- extend frontend
				SET @frontendTTmodels += @frontendTTmodel;

				-- table type parameter
				SET @modelParams += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_param, 
					'{PARAMNAME}', @paramName), 
					'{PARAMTYPE}', @frontendTTname + '[]'),
					'{SQLTYPE}', @paramSqlType + ', cardinality: [0..n]'),
					'{SQL_DEFAULT}', ISNULL(', default: ' + @colDefault, '')),
					'{DEFAULT}', ISNULL(' = ' + build.GenerateDefaultValueToTypeScript(@paramSqlType, @colIsNullable, @colTranslatedDefault), '')),
					'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN 'true' ELSE 'false' END);
					
				SET @modelPrepare += REPLACE(@frontend_prepareTT, 
					'{PARAMNAME}', @paramName);
			END
			ELSE 
			BEGIN
				-- regular parameter
				SET @modelParams += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_param, 
					'{PARAMNAME}', @paramName), 
					'{PARAMTYPE}', build.GetParamTypeForFrontend(@paramSqlType)),
					'{SQLTYPE}', @paramSqlType),
					'{SQL_DEFAULT}', ISNULL(', default: ' + @colDefault, '')),
					'{DEFAULT}', ISNULL(' = ' + build.GenerateDefaultValueToTypeScript(@paramSqlType, @colIsNullable, @colTranslatedDefault), '')),
					'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN 'true' ELSE 'false' END);	-- NATIVELY COMPILED PROCEDURES (starting at Sql Server 2014) may have NOT NULL restriction!!!
				SET @modelPrepare += ISNULL(REPLACE(REPLACE(@frontend_prepare, 
					'{PARAMNAME}', @paramName), 
					'{PREPARE}', build.GetParamPrepareForFrontend(@paramSqlType)), '');

				-- is this parameter a string?
				IF @paramSqlType LIKE '%char([0-9]%'
					SET @modelConstants += REPLACE(REPLACE(REPLACE(@frontend_constMaxLen,
					'{PARAMNAME}', @paramName), 
					'{SQLTYPE}', @paramSqlType),
					'{MAXLEN}', REPLACE(SUBSTRING(@paramSqlType, CHARINDEX('(', @paramSqlType) + 1, 999), ')', ''));
			END

			SET @parameterID += 1;
		END
	END;
	--PRINT ' @modelParams = ' + IsNull(@modelParams, '<null>');
	--PRINT ' @modelPrepare = ' + IsNull(@modelPrepare, '<null>');

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- read schema info (resultsets and fields)
	---------------------------------------------------------------------------------------------------------------------------------------------

	DECLARE @tableIndex int = 1, @tableCount int = 0, @modelsTablesCode nvarchar(max) = '';
	DECLARE @modelTables nvarchar(max) = '', @modelConverter nvarchar(max) = '';
	DECLARE @modelsTableIntf nvarchar(max) = '', @modelsTableImpl nvarchar(max) = '';
	WHILE @tableIndex > 0
	BEGIN
		DECLARE @tableName sysname = null, @tableDef nvarchar(max);

		-- search for table
		SELECT 
			@tableName = resultset,
			@tableDef = colDefinition
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID 
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultsetIndex = @tableIndex
			AND colIndex = -1;

		IF @@ROWCOUNT = 0
			SET @tableIndex = 0;
		ELSE
		BEGIN
			-- table found
			SET @tableCount += 1;
			--SET @tableTypeTRow = 'T' + @tableName;
			--SET @tableTypeTResult = @tableTypeTRow;
			--SET @tableTypeRow = @modelName + '__' + @tableName;
			--SET @tableTypeResult = @tableTypeRow;

			--PRINT '    -> found table "' + @tableName + '" with options [' + RTRIM(LTRIM(@tableDef)) + ']';


            -- search for relations 
            DECLARE @modelsTableSubTablesParametersDefinition nvarchar(max) = '', @modelsTableSubTablesParametersCall nvarchar(max) = '';
            DECLARE @modelsTableSubTablesFilter nvarchar(max) = '', @modelsTableSubTablesProperties nvarchar(max) = '';

            SELECT 
                @modelsTableSubTablesParametersDefinition += REPLACE(REPLACE(REPLACE(@frontend_relation_param_def,
                    '{SPCLASS}', REPLACE(@modelName, '_', '')),
                    '{CHILD}', tr.childTable),
                    '{CHILDPARAM}', LOWER(SUBSTRING(tr.childTable, 1, 1)) + SUBSTRING(tr.childTable, 2, 999)),
                @modelsTableSubTablesParametersCall += REPLACE(@frontend_relation_param_call,
                    '{CHILD}', tr.childTable),
                @modelsTableSubTablesFilter += REPLACE(REPLACE(REPLACE(@frontend_relation_filter,
                    '{CHILD}', tr.childTable),
                    '{CHILDPARAM}', LOWER(SUBSTRING(tr.childTable, 1, 1)) + SUBSTRING(tr.childTable, 2, 999)),
                    '{COMPARE}', (SELECT STRING_AGG('it.' + c.Item + ' === this.' + p.Item, ' && ')
                                  FROM build.Split2(tr.parentColumns, ',', 1, 1) p
                                      INNER JOIN build.Split2(tr.childColumns, ',', 1, 1) c ON c.ItemIndex = p.ItemIndex)),
                @modelsTableSubTablesProperties += REPLACE(REPLACE(REPLACE(@frontend_relation_property,
                    '{SPCLASS}', REPLACE(@modelName, '_', '')),
                    '{CHILD}', tr.childTable),
                    '{PARENT}', tr.parentTable)
            FROM build.TEMP_Relations2 tr
            WHERE tr.appPackageID = @appPackageID
                AND tr.generator = @generator
                AND tr.objectID = @objectID
                AND tr.parentTable = @tableName;

			-- single row?
			DECLARE @isSingleRow bit = CASE WHEN @tableDef like '% ~[0..1~] %' ESCAPE '~' THEN 1 ELSE 0 END;
			IF @isSingleRow = 1
			BEGIN
				--INSERT INTO @Fields (TableName, FieldName, FieldType, IsParent, IsChild, Options)
				--VALUES (@tableName, '', REPLACE(@modelName, '_', '') + '_' + @tableName, 0, 0, RTRIM(LTRIM(@tableDef)));

				-- table has only zero or 1 row !!
				SET @modelTables += REPLACE(REPLACE(REPLACE(REPLACE(@frontend_response_table_01,
					'{TABNAME}', @tableName),
					'{SPCLASS}', REPLACE(@modelName, '_', '')),
					'{TABINDEX}', Convert(varchar, @tableIndex)),
					'{CARDINAL}', '[0..1]');
				SET @modelConverter = REPLACE(REPLACE(REPLACE(@frontend_response_converter_01,
					'{TABNAME}', @tableName),
					'{SPCLASS}', REPLACE(@modelName, '_', '')),
                    '{SUBTABLES}', @modelsTableSubTablesParametersCall) + @modelConverter; -- add in reversed order
			END
			ELSE
			BEGIN
				--INSERT INTO @Fields (TableName, FieldName, FieldType, IsParent, IsChild, Options)
				--VALUES (@tableName, '', REPLACE(@modelName, '_', '') + '_' + @tableName + '[]', 0, 0, RTRIM(LTRIM(@tableDef)));

				-- table has more than 1 rows
				SET @modelTables += REPLACE(REPLACE(REPLACE(REPLACE(@frontend_response_table_0n,
					'{TABNAME}', @tableName),
					'{SPCLASS}', REPLACE(@modelName, '_', '')),
					'{TABINDEX}', Convert(varchar, @tableIndex)),
					'{CARDINAL}', '[0..n]');
				SET @modelConverter = REPLACE(REPLACE(REPLACE(@frontend_response_converter_0n,
					'{TABNAME}', @tableName),
					'{SPCLASS}', REPLACE(@modelName, '_', '')),
                    '{SUBTABLES}', @modelsTableSubTablesParametersCall) + @modelConverter; -- add in reversed order
			END

			-- search for columns
			DECLARE @colIndex int = 1;
			DECLARE @modelsTableRowClass nvarchar(max) = '', @modelsTableRowImpl nvarchar(max) = '', @modelsTableConstants nvarchar(max) = ''; 

			WHILE @colIndex > 0
			BEGIN
				DECLARE @colDef nvarchar(max), @colName sysname, @colSqlType sysname;
				SELECT 
					@colName = colName,
					@colSqlType = sqlType,
					@colDef = colDefinition
				FROM build.TEMP_Columns2
				WHERE appPackageID = @appPackageID 
					AND generator = @generator 
					AND objectID = @objectID 
					AND resultsetIndex = @tableIndex
					AND colIndex = @colIndex - 1
					AND colType = 'C';

				IF @@ROWCOUNT = 0
					SET @colIndex = 0;
				ELSE
				BEGIN
					-- column found
					--PRINT '        -> found ' + Convert(varchar, @colIndex) + '. column "' + @colName + '" with type "' + @colSqlType + '" and options [' + RTRIM(LTRIM(@colDef)) + ']';

					-- build class and implementation
					SET @modelsTableRowClass += REPLACE(REPLACE(REPLACE(REPLACE(@frontend_table_row_class,
						'{PARAMNAME}', @colName),
						'{NULLABLE}', CASE WHEN @colDef LIKE '% null %' THEN 'true' ELSE 'false' END),
						'{PARAMTYPE}', build.GetParamTypeForFrontend(@colSqlType)),
						'{SQLTYPE}', @colSqlType);
					SET @modelsTableRowImpl += REPLACE(REPLACE(REPLACE(CASE WHEN build.GetParamPrepareForFrontend(@colSqlType) IS NULL THEN @frontend_table_init_default ELSE @frontend_table_init_prepare END,
						'{PARAMNAME}', @colName),
						'{PREPARE}', ISNULL(build.GetParamPrepareForFrontend(@colSqlType), '')),
						'{DEFAULT}', CASE WHEN @colDef LIKE '% null %' THEN 'null' ELSE build.GetParamDefaultValueForFrontend(@colSqlType) END);
					
					-- is this parameter a string?
					IF @colSqlType LIKE '%char([0-9]%'
						SET @modelsTableConstants += REPLACE(REPLACE(REPLACE(@frontend_table_constMaxLen,
						'{COLNAME}', @colName), 
						'{SQLTYPE}', @colSqlType),
						'{MAXLEN}', REPLACE(SUBSTRING(@colSqlType, CHARINDEX('(', @colSqlType) + 1, 999), ')', ''));

					DECLARE @formatDef nvarchar(max), @formatName sysname, @formatArgs nvarchar(max);
					DECLARE @formatID int = 1;
					WHILE @formatID > 0
					BEGIN
						WITH source AS
						(
							SELECT *, 
								ROW_NUMBER() OVER(ORDER BY colName ASC) AS RowID
							FROM build.TEMP_Columns2
							WHERE appPackageID = @appPackageID 
								AND generator = @generator 
								AND objectID = @objectID 
								AND resultsetIndex = @tableIndex
								AND colIndex = @colIndex - 1
								AND colType = 'F'
						)
						SELECT 
							@formatName = colName,
							@formatArgs = sqlType,
							@formatDef = colDefinition
						FROM source
						WHERE RowID = @formatID;

						IF @@ROWCOUNT = 0
							SET @formatID = 0;
						ELSE
						BEGIN
							--PRINT '          -> found format-getter "' + @formatName + '" and args [' + RTRIM(LTRIM(@formatArgs)) + ']';

							-- build class and implementation
							SET @modelsTableRowClass += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_table_row_class_format,
								'{PARAMNAME}', @colName),
								'{PARAMNAMEFORMAT}', @formatName),
								'{PARAMTYPE}', build.GetParamTypeForFrontend(@colSqlType)),
								'{SQLTYPE}', @colSqlType),
								'{FORMATTER}', build.GetFormatterForFrontend(@colSqlType)),
								'{FORMATARGS}', @formatArgs),
								'{COMMA}', CASE WHEN Len(@formatArgs) > 0 THEN ', ' ELSE '' END);

							SET @formatID += 1;
						END

					END

					-- search for next column
					SET @colIndex += 1;
				END
			END

			-- TODO: search for calculated fields? (to be continued :-)

			SET @modelsTablesCode += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_table,
				'{SP}', @modelName),
				'{SPCLASS}', REPLACE(@modelName, '_', '')),
				'{TABNAME}', @tableName),
				'{TABINDEX}', Convert(varchar, @tableIndex)),
				'{PARAMS}', @modelsTableRowClass + @modelsTableSubTablesProperties),
                '{SUBTABLES}', @modelsTableSubTablesParametersDefinition),
				'{INIT}', @modelsTableRowImpl + @modelsTableSubTablesFilter),
				'{CONSTANTS}', @modelsTableConstants);			

			-- search for next table		
			SET @tableIndex += 1;
		END
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- generate code
	---------------------------------------------------------------------------------------------------------------------------------------------
	SET @FrontendModelDefinition = @frontendTTmodels; 

	SET @FrontendModelDefinition += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_request, 
		'{SP}', @modulIDprefix + @modelName), 
		'{SPCLASS}', REPLACE(@modelName, '_', '')), 
		'{PARAMS}', @modelParams), 
		'{PREPARE}', @modelPrepare),
		'{CONSTANTS}', @modelConstants);

    IF @modelTables <> ''
        SET @FrontendModelDefinition += REPLACE(REPLACE(REPLACE(@frontend_response_interface,
		    '{SP}', @modulIDprefix + @modelName), 
		    '{SPCLASS}', REPLACE(@modelName, '_', '')), 
		    '{TABLES}', @modelTables);

	SET @FrontendModelDefinition += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_response,
		'{SP}', @modulIDprefix + @modelName), 
        '{INTERFACE}', CASE WHEN @modelTables <> '' THEN @frontend_response_implement_interface ELSE '' END),
		'{SPCLASS}', REPLACE(@modelName, '_', '')), 
		'{TABLES}', @modelTables), 
		'{CONVERTER}', @modelConverter);

	SET @FrontendModelDefinition += @modelsTablesCode;

	-- for debugging
	--PRINT '----------------------------- F R O N T E N D -----------------------------';
	--PRINT Convert(varchar, LEN(@FrontendModelDefinition)) + ' characters';
	--PRINT @FrontendModelDefinition;
	--PRINT '---------------------------------------------------------------------------';

	-- Tests
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptProcedure2 'te-capex-fc', 'typescript', 2015346244, @f OUT; PRINT @f;
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptProcedure2 'te-datapool', 'typescript', 1753109336, @f OUT; PRINT @f;  -- table valued parameter
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptProcedure2 'te-datapool', 'typescript', 1689109108, @f OUT; PRINT @f;  -- response tables
END
GO
