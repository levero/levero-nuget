SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Generates all models
-- =============================================
CREATE PROCEDURE [build].[GenerateCSharpModels]
	@AppPackageID				varchar(15),
	@AppPackageVersion			varchar(10),
	@OutputType					varchar(10) = 'SELECT'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @AppVersionComparable int = build.VersionToInt(@AppPackageVersion);
	DECLARE @generator varchar(15) = 'csharp';
    DECLARE @flagIGNORE_C# sysname = 'IGNORE_C#';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- collect procedures
	---------------------------------------------------------------------------------------------------------------------------------------------
	
	-- which database, schema and prefix?
	DECLARE @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max), @removePrefix bit, @modelNamespace nvarchar(max);
	DECLARE @mode varchar(max);
	SELECT 
		@databaseName = DatabaseName,
		@listOfSchema = CSharp_ListOfSchema,
		@listOfPrefix = CSharp_ListOfPrefix,
		@removePrefix = CSharp_RemovePrefix,
		@modelNamespace = CSharp_ModelNamespace,
		@mode = CSharp_Mode
	FROM build.Settings
	WHERE AppPackageID = @AppPackageID;
	
	-- get models and fill table build.TEMP_Procedures 
	EXEC build.CollectModels @generator, @databaseName, @AppPackageID, @AppPackageVersion, @listOfSchema, @listOfPrefix, @removePrefix;
	
	-- set model namespace
	UPDATE build.TEMP_Procedures
	SET modelNamespace = @modelNamespace,
		done = 0
	WHERE appPackageID = @AppPackageID
		AND generator = @generator;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- run through all procedures and build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @objectID int, @isExtern bit;

	WHILE EXISTS(SELECT * FROM build.TEMP_Procedures WHERE appPackageID = @AppPackageID AND generator = @generator AND done = 0)
	BEGIN
		-- next procedure
		SELECT TOP 1
			@objectID = objectID,
			@isExtern = isExternal
		FROM build.TEMP_Procedures 
		WHERE appPackageID = @AppPackageID 
			AND generator = @generator
			AND done = 0
		ORDER BY modelName;

		-- mark as done
		UPDATE build.TEMP_Procedures 
		SET done = 1
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND objectID = @objectID;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- build procedure
		---------------------------------------------------------------------------------------------------------------------------------------------
        IF NOT EXISTS(SELECT * FROM build.TEMP_Flags2 tf WHERE tf.appPackageID = @AppPackageID AND tf.generator = @generator AND tf.objectID = @objectID AND tf.flag = @flagIGNORE_C#)
        BEGIN
		    IF @mode = 'sql' OR @mode like 'sql:%'
		    BEGIN
			    EXEC build.GenerateCSharpProcedure_Sql @AppPackageID, @generator, @mode, @objectID, @isExtern, @OutputType;
		    END
		    ELSE IF @mode = 'backendhub' OR @mode like 'backendhub:%'
		    BEGIN
			    IF @isExtern = 0
				    EXEC build.GenerateCSharpProcedure_BackendHub @AppPackageID, @generator, @mode, @objectID, @OutputType;
		    END
		    ELSE
		    BEGIN
			    RAISERROR('CSharp mode %s is not allowed!', 16, 1, @mode);
			    RETURN;
    		END
		END
	END

	-- Tests
	-- EXEC build.GenerateCSharpModels 'rlh-wt-gutschei', '2', 'PRINT';
	-- EXEC build.GenerateCSharpModels 'ils', '2', 'PRINT';
	-- EXEC build.GenerateCSharpModels 'fire', '2', 'PRINT';
END
GO
