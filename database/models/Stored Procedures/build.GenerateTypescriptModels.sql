SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-11
-- Description:	Generates a frontend model description
-- =============================================
CREATE PROCEDURE [build].[GenerateTypescriptModels]
	@AppPackageID				varchar(15),
	@AppPackageVersion			varchar(10)
AS
BEGIN
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for frontend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @generator varchar(15) = 'typescript';
	DECLARE @frontend_header nvarchar(max) = '// tslint:disable:class-name
// tslint:disable:max-line-length

import { ActionRequest, ActionResponse, ActionContext, BaseTools, GeneratorTools, from } from ''[[[FRONTENDPROJECT]]]/libs/levero-core/src/public_api'';

';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- collect procedures
	---------------------------------------------------------------------------------------------------------------------------------------------
	
	-- which database, schema and prefix?
	DECLARE @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max);
	SELECT 
		@databaseName = DatabaseName,
		@listOfSchema = Typescript_ListOfSchema,
		@listOfPrefix = Typescript_ListOfPrefix
	FROM build.Settings
	WHERE AppPackageID = @AppPackageID;
	
	-- get models and fill table build.TEMP_Procedures 
	EXEC build.CollectModels @Generator, @databaseName, @AppPackageID, @AppPackageVersion, @listOfSchema, @listOfPrefix, 1;
	---------------------------------------------------------------------------------------------------------------------------------------------
	-- run through all procedures and build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @objectID int, @schemaName sysname;

	WHILE EXISTS(SELECT * FROM build.TEMP_Procedures WHERE appPackageID = @AppPackageID AND generator = @generator AND isExternal = 0 AND done = 0)
	BEGIN
		-- next procedure
		SELECT TOP 1 
			@objectID = objectID,
			@schemaName = schemaName
		FROM build.TEMP_Procedures
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND done = 0
			AND isExternal = 0
		ORDER BY modelName;

		-- mark as done
		UPDATE build.TEMP_Procedures 
		SET done = 1
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND objectID = @objectID;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- build procedure
		---------------------------------------------------------------------------------------------------------------------------------------------
		DECLARE @frontendDef nvarchar(max);
		EXEC build.GenerateTypescriptProcedure @AppPackageID, @generator, @objectID, @frontendDef OUT;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- save code
		---------------------------------------------------------------------------------------------------------------------------------------------
		UPDATE build.TEMP_Schemas
		SET frontendModels = IsNull(frontendModels, @frontend_header) + @frontendDef
		WHERE appPackageID = @AppPackageID 
			AND generator = @Generator 
			AND schemaName = @schemaName;
	END;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- return frontend models
	---------------------------------------------------------------------------------------------------------------------------------------------
	WITH youngest AS
	(
		SELECT 
			schemaName,
			MAX(modify_date) AS modify_date
		FROM build.TEMP_Procedures
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND isExternal = 0
		GROUP BY schemaName
	)
	SELECT 
		s.schemaName AS SchemaName,
		ISNULL(s.frontendModels, '') AS FrontEndModelsContent,
		youngest.modify_date
	FROM build.TEMP_Schemas s
		LEFT OUTER JOIN youngest ON youngest.schemaName = s.schemaName
	WHERE appPackageID = @AppPackageID 
		AND generator = @Generator
	ORDER BY schemaName;

	-- Tests
	-- EXEC build.GenerateTypescriptModels 'po-shop', 0.11;
END
GO
