SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-11
-- Description:	Generates a frontend model description
-- =============================================
CREATE PROCEDURE [build].[GenerateTypescriptModels2]
	@AppPackageID				VARCHAR(15),
	@AppPackageVersion			VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for frontend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @True	BIT = 1;
	DECLARE @False	BIT = 0;
	DECLARE @generator VARCHAR(15) = 'typescript';
	DECLARE @generatorReports VARCHAR(15) = NULL;
	DECLARE @frontend_header nvarchar(max) = '// tslint:disable:class-name
// tslint:disable:max-line-length

import { ActionRequest, ActionResponse, ActionContext, LeveroToolsService, ServiceLocator, GeneratorTools, base64String } from ''[[[FRONTENDPROJECT]]]/libs/levero-core/src/public_api'';

';
	DECLARE @frontend_headerReports nvarchar(max) = '// tslint:disable:class-name
// tslint:disable:max-line-length

import { LeveroToolsService, ServiceLocator } from ''[[[FRONTENDPROJECT]]]/libs/levero-core/src/public_api'';

';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- collect procedures
	---------------------------------------------------------------------------------------------------------------------------------------------
	
	-- which database, schema and prefix?
	DECLARE @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max), @listOfModules nvarchar(max);
	SELECT 
		@databaseName = DatabaseName,
		@listOfSchema = Typescript_ListOfSchema,
		@listOfPrefix = Typescript_ListOfPrefix,
		@listOfModules = Typescript_ListOfModules,
        @generatorReports = CASE WHEN Typescript_IncludeReports = @True THEN 'reports' END
	FROM build.Settings2
	WHERE AppPackageID = @AppPackageID;

	-- get models and fill table build.TEMP_Procedures2 
	EXEC build.CollectModels2 @Generator, @AppPackageID, '', @databaseName, @AppPackageVersion, @listOfSchema, @listOfPrefix, @True;

	-- get modules 
	DECLARE @Modules TABLE(
		ModulID varchar(15) PRIMARY KEY,
		IsDone bit DEFAULT 0
	);
	INSERT INTO @Modules (ModulID)
	SELECT Item
	FROM build.Split2(@listOfModules, ',', @True, @True);

	DECLARE @ModulID varchar(15), @ModulDb sysname, @ModulVersion varchar(10);
	WHILE EXISTS(SELECT * FROM @Modules WHERE IsDone = 0)
	BEGIN
		SELECT TOP(1) 
			@ModulID = m.ModulID,
			@ModulDb = IsNull(NullIf(s.DatabaseName, '*'), @databaseName),
			@ModulVersion = IsNull(NullIf(s.ModulVersion, '*'), @AppPackageVersion),
			@listOfSchema = s.Typescript_ListOfSchema,
			@listOfPrefix = s.Typescript_ListOfPrefix,
			@listOfModules = s.Typescript_ListOfModules
		FROM @Modules m
			LEFT OUTER JOIN build.Settings2 s ON s.AppID = '*' AND s.AppPackageID = m.ModulID
		WHERE IsDone = 0;
		UPDATE @Modules 
		SET IsDone = 1
		WHERE ModulID = @ModulID;

		-- let modules be recursive
		INSERT INTO @Modules (ModulID)
		SELECT s.Item
		FROM build.Split2(@listOfModules, ',', @True, @True) s
		WHERE s.Item NOT IN (SELECT ModulID FROM @Modules);

		-- get models and fill table build.TEMP_Procedures2 
		EXEC build.CollectModels2 @Generator, @AppPackageID, @ModulID, @ModulDb, @ModulVersion, @listOfSchema, @listOfPrefix, @True;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- check duplicates
	---------------------------------------------------------------------------------------------------------------------------------------------
	PRINT 'check for duplicate model names partitioned by modul...';
	DECLARE @duplicates NVARCHAR(max) = NULL;
	SELECT @duplicates = STRING_AGG(hint, ', ')
	FROM (SELECT modelName + ' (' + STRING_AGG(databaseName + '.' + schemaName + '.' + [name], ', ') WITHIN GROUP(ORDER BY databaseName, schemaName, [name]) + ')' AS hint
		  FROM build.TEMP_Procedures2 
		  WHERE appPackageID = @AppPackageID AND generator IN (@Generator, @generatorReports) AND IsExternal = 0
		  GROUP BY modelName, modulID
		  HAVING COUNT(*) > 1) t;
	IF @duplicates IS NOT NULL
	BEGIN
		RAISERROR('There are procedures with the same model name per modul: %s', 16, 1, @duplicates);
		RETURN;
	END

	PRINT 'check for duplicate model names partitioned by database...';
	SELECT @duplicates = STRING_AGG(hint, ', ')
	FROM (SELECT modelName + ' (' + STRING_AGG(databaseName + '.' + schemaName + '.' + [name], ', ') WITHIN GROUP(ORDER BY databaseName, schemaName, [name]) + ')' AS hint
		  FROM build.TEMP_Procedures2 
		  WHERE appPackageID = @AppPackageID AND generator IN (@Generator, @generatorReports)
		  GROUP BY modelName, databaseName, generator
		  HAVING COUNT(*) > 1) t;
	IF @duplicates IS NOT NULL
	BEGIN
		RAISERROR('There are procedures with the same model name per database: %s', 16, 1, @duplicates);
		RETURN;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- run through all procedures and build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @objectID int, @schemaName sysname;

	WHILE EXISTS(SELECT * FROM build.TEMP_Procedures2 WHERE appPackageID = @AppPackageID AND generator = @generator AND isExternal = 0 AND done = 0)
	BEGIN
		-- next procedure
		SELECT TOP 1 
			@modulID = modulID,
			@objectID = objectID,
			@schemaName = schemaName
		FROM build.TEMP_Procedures2
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND done = 0
			AND isExternal = 0
		ORDER BY modelName;

		-- mark as done
		UPDATE build.TEMP_Procedures2 
		SET done = 1
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND objectID = @objectID;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- build procedure
		---------------------------------------------------------------------------------------------------------------------------------------------
		DECLARE @frontendDef nvarchar(max);
		EXEC build.GenerateTypescriptProcedure2 @AppPackageID, @generator, @objectID, @frontendDef OUT;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- save code
		---------------------------------------------------------------------------------------------------------------------------------------------
		UPDATE build.TEMP_Filter2
		SET frontendModels = IsNull(frontendModels, @frontend_header) + @frontendDef
		WHERE appPackageID = @appPackageID
			AND modulID = @modulID
			AND generator = @Generator 
			AND schemaName = @schemaName;
	END;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- run through all procedures and build models for REPORTS
	---------------------------------------------------------------------------------------------------------------------------------------------
    IF @generatorReports IS NOT NULL
        UPDATE build.TEMP_Procedures2
        SET done = 0
        WHERE appPackageID = @AppPackageID AND generator = @generatorReports AND isExternal = 0;

	WHILE EXISTS(SELECT * FROM build.TEMP_Procedures2 WHERE appPackageID = @AppPackageID AND generator = @generatorReports AND isExternal = 0 AND done = 0)
	BEGIN
		-- next procedure
		SELECT TOP 1 
			@modulID = modulID,
			@objectID = objectID,
			@schemaName = schemaName
		FROM build.TEMP_Procedures2
		WHERE appPackageID = @AppPackageID
			AND generator = @generatorReports
			AND done = 0
			AND isExternal = 0
		ORDER BY modelName;

		-- mark as done
		UPDATE build.TEMP_Procedures2 
		SET done = 1
		WHERE appPackageID = @AppPackageID
			AND generator = @generatorReports
			AND objectID = @objectID;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- build procedure
		---------------------------------------------------------------------------------------------------------------------------------------------
		EXEC build.GenerateTypescriptReportsProcedure2 @AppPackageID, @generatorReports, @objectID, @frontendDef OUT;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- save code
		---------------------------------------------------------------------------------------------------------------------------------------------
		UPDATE build.TEMP_Filter2
		SET frontendModels = IsNull(frontendModels, @frontend_headerReports) + @frontendDef
		WHERE appPackageID = @appPackageID
			AND modulID = @modulID
			AND generator = @generatorReports 
			AND schemaName = @schemaName;
	END;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- return frontend models
	---------------------------------------------------------------------------------------------------------------------------------------------
	WITH youngest AS
	(
		SELECT 
			schemaName,
			modulID,
			MAX(modify_date) AS modify_date
		FROM build.TEMP_Procedures2
		WHERE appPackageID = @AppPackageID
			AND generator IN (@generator, @generatorReports)
			AND isExternal = 0				-- TODO: vielleicht muss der Filter weg?? wenn die Externe SP geändert wird, aber die interne nicht, wird modify_date nicht aktualisiert
		GROUP BY schemaName, modulID
	)
	SELECT 
		Lower(IsNull(NullIf(f.modulID, '') + '_', '') + f.schemaName + 
        CASE WHEN f.generator = @generatorReports THEN '_reports' ELSE '' END) AS SchemaName,
		ISNULL(f.frontendModels, '') AS FrontEndModelsContent,
		youngest.modify_date
	FROM build.TEMP_Filter2 f
		LEFT OUTER JOIN youngest ON youngest.schemaName = f.schemaName AND youngest.modulID = f.modulID
	WHERE appPackageID = @AppPackageID 
		AND generator IN (@generator, @generatorReports)
        AND Len(f.frontendModels) > 0
	ORDER BY schemaName;

	-- Tests
	-- EXEC build.GenerateTypescriptModels2 'po-shop', 0.11;
END
GO
