SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-09-29
-- Description:	Generates models (call, request and response tables) for one procedure
-- =============================================
CREATE PROCEDURE [build].[GenerateCSharpProcedure_BackendHub]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@csharp_mode				varchar(max),
	@objectID					int,
	@OutputType					varchar(10) = 'SELECT'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @schemaName sysname, @name sysname, @modelNamespace sysname, @modelNameProc sysname, @modelNameTable sysname, @modify_date datetime;
	SELECT
		@schemaName = schemaName,
		@name = [name],
		@modelNamespace = IsNull(modelNamespace, 'LeveroModels.DefaultNamespace'),
		@modelNameProc = modelName,
		@modify_date = modify_date
	FROM build.TEMP_Procedures
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @ttProc nvarchar(max) = 'using Levero.Persistence.BackendHub;
using LeveroApps.DeviceService.Backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable IDE0017 // Simplify object initialization
#pragma warning disable IDE0022 // Use expression body for methods
#pragma warning disable IDE0049 // Simplify Names
#pragma warning disable IDE0063 // Use simple ''using'' statement

namespace {MODELNAMESPACE}
{
    public static partial class {MODELNAME}Extensions
    {
        public static async {TASKRESULTTYPES} {MODELNAME}(
            this IBackendHub connection{CSPARAMSLIST})
        {
            var transport = global::{MODELNAMESPACE}.{MODELNAME}.Prepare({PARAMSCALL});

            var result = await connection.doAction(transport);
            if (result == null)
                throw new Exception("Action {MODELNAME} failed: no result returned (possible timeout)");
            if (result.error != null)
                throw new Exception("Action {MODELNAME} failed: " + result.error.message +
                    "\r\nDetails: " + result.error.details);
            if (result.response == null || result.response.Length < 1)
                throw new Exception("Action {MODELNAME} failed: no response returned");
{EXTENSIONRETURN}        }
    }

    public partial class {MODELNAME}
    {
        public static ActionTransport Prepare({PARAMSLIST})
        {
            var transport = new ActionTransport();
            transport.action = "{MODELNAME}";{ADDPARAMS}
            return transport;
        }
{PROCESSRESULT}    }
}';
	DECLARE @ttExtensionReturn nvarchar(max) = '
            return global::{MODELNAMESPACE}.{MODELNAME}.ProcessResult(result.response[0]);
';
DECLARE @ttProcessResults nvarchar(max) = '
        public static {RESULTTYPES} ProcessResult(ActionResponseAction? response)
        {
            const string constProcedureName = "{SCHEMANAME}.{PROCNAME}";
{EXECUTEBLOCK}
        }
';
	DECLARE @ttProcParam nvarchar(max) = '{PARAMTYPE} {PARAMNAMECC}';
	DECLARE @ttProcParamCall nvarchar(max) = '{PARAMNAMECC}';
	DECLARE @ttProcAddParam nvarchar(max) = '
            transport.data.Add("{PARAMNAME}", {PARAMNAMECC});';

	DECLARE @ttProcExecuteNoResult nvarchar(max) = ''
	DECLARE @ttProcExecuteReader nvarchar(max) = '{READTABLES}
            return {RETURNVALUE};';

	DECLARE @ttProcReadTable nvarchar(max) = '
            // {TABLENUMBER}. table ChangedAlarms
            const string constTableName{TABLENUMBER} = "{TABLENAME}";
            var table{TABLENUMBER} = response?.data?.TryGetValue(constTableName{TABLENUMBER}, null!) 
                ?? throw new Exception($"Table ''{constTableName{TABLENUMBER}}'' of procedure {constProcedureName} was not found!");
            var list{TABLENUMBER} = new List<{TABLETYPE}>();
            foreach (var row in table{TABLENUMBER})
            {
                var item{TABLENUMBER} = new {TABLETYPE}();

{COLBLOCK}
                if (item{TABLENUMBER} is Levero.Persistence.DAL.IAfterInit iai)
                    iai.AfterInit();
                list{TABLENUMBER}.Add(item{TABLENUMBER});
            }';

	DECLARE @ttProcReadTableColNull nvarchar(max) = '                // column {COLNAME} of type {COLSQLTYPE} and option {OPTIONS}
                item{TABLENUMBER}.{FIELDPREFIX}{COLNAME} = row!.GetNullable{COLTYPE}("{COLNAME}", constTableName{TABLENUMBER}, constProcedureName);
';
	DECLARE @ttProcReadTableColNotNull nvarchar(max) = '                // column {COLNAME} of type {COLSQLTYPE} and option {OPTIONS}
                item{TABLENUMBER}.{FIELDPREFIX}{COLNAME} = row!.Get{COLTYPE}("{COLNAME}", constTableName{TABLENUMBER}, constProcedureName);
';
	DECLARE @ttProcReadTablesReturn nvarchar(max) = 'list{TABLENUMBER}.{ARRAYorFIRSTORDEFAULT}()';


	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	IF @OutputType = 'PRINT'
		PRINT 'building ' + @modelNameProc + ' from ' + @schemaName + '.' + @name;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- get parameters of procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @hasClient bit = 0, @parameterID int;
	DECLARE @CodeParams nvarchar(max) = '', @CodeParamsCall nvarchar(max) = '', @CodeAddParams nvarchar(max) = '', @paramNameCamelCase sysname;
	DECLARE @tableTypes TABLE (tableTypeID int NOT NULL);
	SET @parameterID = 1;
	WHILE @parameterID > 0
	BEGIN
		DECLARE @paramName sysname = NULL, @paramValue sysname, @paramType sysname, @paramSqlType sysname;
		DECLARE @isNullable bit, @isOutput bit, @tableTypeID int, @paramTableTypeName nvarchar(max) = '';
		SELECT 
			@paramName = colName,
			@paramSqlType = sqlType,
			@tableTypeID = tableTypeObjectID,
			@isOutput = CASE WHEN colDefinition LIKE '% out %' THEN 1 ELSE 0 END,
			@isNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END
		FROM build.TEMP_Columns
		WHERE appPackageID = @appPackageID
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultset = '@' 
			AND colIndex = @parameterID - 1;

		IF @paramName IS NULL
			SET @parameterID = 0;
		ELSE
		BEGIN
			SELECT @paramType = CASE WHEN @isNullable = 1 THEN NullableType ELSE BaseType END 
			FROM build.ConvertToNetType(@paramSqlType);
			IF @OutputType = 'PRINT'
				PRINT '    -> found ' + Convert(varchar, @parameterID) + '. parameter ' + @paramName + ' with type ' + @paramSqlType;

			SET @paramNameCamelCase = LOWER(SUBSTRING(@paramName, 1, 1)) + SUBSTRING(@paramName, 2, 999);
			SET @paramValue = @paramNameCamelCase;
			-- system parameter?
			IF @paramName LIKE '~_%' ESCAPE '~'
			BEGIN
				SET @paramNameCamelCase = LOWER(SUBSTRING(@paramName, 1, 2)) + SUBSTRING(@paramName, 3, 999);
				IF @paramName = '_ClientID'
				BEGIN
					SET @hasClient = 1;
					SET @paramValue = 'Environment.MachineName';
					IF @paramSqlType <> 'varchar(20)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(20))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE
				BEGIN
					RAISERROR('Procedure %s.%s has an unknown system parameter "%s"!', 16, 1, @schemaName, @name, @paramName);
					RETURN;
				END
			END
			ELSE IF @tableTypeID IS NOT NULL
			BEGIN
				-- build model for table type
				IF NOT EXISTS(SELECT * FROM @tableTypes WHERE tableTypeID = @tableTypeID)
				BEGIN
					DECLARE @resultset sysname = '@' + @paramName;
					--PRINT 'EXEC build.GenerateCSharpTableValuedParameter ''' + @appPackageID + ''', ''' + @generator + ''', ' + Convert(varchar, @objectID) + ', ''' + @resultset + ''', ''' + @paramType + ''', ''' + @OutputType + ''';';
					EXEC build.GenerateCSharpTableValuedParameter @appPackageID, @generator, @csharp_mode, @objectID, @resultset, @paramType, @OutputType;

					INSERT INTO @tableTypes (tableTypeID) VALUES (@tableTypeID);
				END

				-- make full qualified name
				SET @paramType = @modelNameProc + N'__' + @paramType;

				-- add converter for datatable
				--SET @paramValue = @paramType + N'.ConvertToTable(' + @paramNameCamelCase + ')';
				SET @paramValue = @paramNameCamelCase + ' /* MUSS HIER KONVERTIERT WERDEN? */';
				SET @paramTableTypeName = N', "' + @paramSqlType + N'"';

				-- make array
				SET @paramType = @paramType + N'[]';
			END

			-- params list (no system parameters!)
			IF @paramName NOT LIKE '~_%' ESCAPE '~'
			BEGIN
				IF LEN(@CodeParams) > 0
					SET @CodeParams += ', ';
				SET @CodeParams += REPLACE(REPLACE(@ttProcParam, 
					'{PARAMNAMECC}', @paramNameCamelCase),
					'{PARAMTYPE}', @paramType + N' /* ' + @paramSqlType + N' */');
				IF LEN(@CodeParamsCall) > 0
					SET @CodeParamsCall += ', ';
				SET @CodeParamsCall += REPLACE(@ttProcParamCall, '{PARAMNAMECC}', @paramNameCamelCase);
			END

			-- add param value to db command
			SET @CodeAddParams += REPLACE(REPLACE(REPLACE(@ttProcAddParam, 
				'{PARAMNAME}', @paramName),
				'{PARAMNAMECC}', @paramNameCamelCase),
				'{PARAMVALUE}', @paramValue);

			SET @parameterID += 1;
		END
	END;
	--PRINT ' @CodeParams = ' + IsNull(@CodeParams, '<null>');
	--PRINT ' @CodeParamsCall = ' + IsNull(@CodeParamsCall, '<null>');
	--PRINT ' @CodeAddParams = ' + IsNull(@CodeAddParams, '<null>');

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- read schema info (resultsets and fields)
	---------------------------------------------------------------------------------------------------------------------------------------------

	DECLARE @tableIndex int = 1, @tableCount int = 0;
	DECLARE @CodeReturnList nvarchar(max) = null, @CodeReturnValues nvarchar(max) = null;
	DECLARE @CodeReadTable nvarchar(max) = '';
	WHILE @tableIndex > 0
	BEGIN
		DECLARE @tableName sysname = null, @tableDef nvarchar(max), @tableTypeRow nvarchar(max), @tableTypeResult nvarchar(max);
		DECLARE @CodeContent nvarchar(max) = '', @CodeEqualAll nvarchar(max) = '', @CodeEqualPK nvarchar(max) = '';
		DECLARE @CodeIntf nvarchar(max) = '', @CodeToString nvarchar(max) = '', @CodeHashAll nvarchar(max) = null, @CodeHashPK nvarchar(max) = null;

		-- search for table
		SELECT 
			@tableName = resultset,
			@tableDef = colDefinition,
			@modelNameTable = IsNull(NullIf(colName, ''), @modelNameProc)
		FROM build.TEMP_Columns 
		WHERE appPackageID = @appPackageID 
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultsetIndex = @tableIndex
			AND colIndex = -1;
			
		IF @@ROWCOUNT = 0
			SET @tableIndex = 0;
		ELSE
		BEGIN
			-- table found
			SET @tableCount += 1;
			SET @tableTypeRow = @modelNameTable + '__' + @tableName;
			SET @tableTypeResult = @tableTypeRow;

			IF @OutputType = 'PRINT'
				PRINT '    -> found table "' + @modelNameTable + '__' + @tableName + '" with options [' + RTRIM(LTRIM(@tableDef)) + ']';

			-- custom dto?
			DECLARE @isCustomDto bit = 0;
			IF @tableDef like '% c#:%'
			BEGIN
				SET @tableTypeRow = SUBSTRING(@tableDef, CHARINDEX(' c#:', @tableDef) + 4, 999);
				SET @tableTypeRow = SUBSTRING(@tableTypeRow, 1, CHARINDEX(' ', @tableTypeRow) - 1);
				SET @tableTypeResult = @tableTypeRow;
				SET @isCustomDto = 1;
			END

			-- single row?
			DECLARE @isSingleRow bit = CASE WHEN @tableDef like '% ~[0..1~] %' ESCAPE '~' THEN 1 ELSE 0 END;
			IF @isSingleRow = 1
				SET @tableTypeResult += '?';
			ELSE
				SET @tableTypeResult += '[]';

			--PRINT ' @tableTypeRow = ' + @tableTypeRow;
			--PRINT ' @tableTypeResult = ' + @tableTypeResult;

			-- search for columns
			DECLARE @colIndex int = 1;
			DECLARE @CodeReadColumns nvarchar(max) = '';

			WHILE @colIndex > 0
			BEGIN
				DECLARE @colDef nvarchar(max), @colName sysname, @colSqlType sysname;
				SELECT 
					@colName = colName,
					@colSqlType = sqlType,
					@colDef = colDefinition
				FROM build.TEMP_Columns 
				WHERE appPackageID = @appPackageID 
					AND generator = @generator 
					AND objectID = @objectID 
					AND resultsetIndex = @tableIndex
					AND colIndex = @colIndex - 1;

				IF @@ROWCOUNT = 0
					SET @colIndex = 0;
				ELSE
				BEGIN
					-- column found
					DECLARE @colType sysname, @IsBaseTypeNullable bit, @colReader sysname, @colMaxLength int;
					SELECT @colType = CASE WHEN @colDef LIKE '% null %' THEN NullableType ELSE BaseType END,
						@IsBaseTypeNullable = IsBaseTypeNullable,
						@colReader = ReaderMethod,
						@colMaxLength = Length
					FROM build.ConvertToNetType(@colSqlType);
					IF @OutputType = 'PRINT'
						PRINT '        -> found ' + Convert(varchar, @colIndex) + '. column "' + @colName + '" with type "' + @colType + '" and options [' + RTRIM(LTRIM(@colDef)) + ']';

					-- build class and implementation
					SET @CodeReadColumns += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
						CASE WHEN @colDef LIKE '% null %' THEN @ttProcReadTableColNull ELSE @ttProcReadTableColNotNull END,
						'{TABLENUMBER}', @tableIndex),
						'{FIELDPREFIX}', CASE WHEN @isCustomDto = 1 THEN '' ELSE '_' END),
						'{COLNAME}', @colName),
						'{COLTYPE}', REPLACE(@colType, '?', '')),
						'{CONVERTER}', @colReader),
						'{TABLENAME}', @tableName),
						'{SCHEMANAME}', @schemaName),
						'{PROCNAME}', @name),
						'{COLSQLTYPE}', @colSqlType),
						'{OPTIONS}', @colDef);
					--PRINT ' @CodeReadColumns = ' + @CodeReadColumns;

					-- search for next column
					SET @colIndex += 1;
				END
			END

			-- build model for table type
			IF @isCustomDto = 0
				EXEC build.GenerateCSharpTable @appPackageID, @generator, @csharp_mode, @objectID, @tableIndex, @tableName, @OutputType;

			--------------------------------------------------------------------------------
			-- TABLE BLOCK
			SET @CodeReadTable += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProcReadTable,
				'{TABLENUMBER}', @tableIndex),
				'{TABLENAME}', @tableName),
				'{TABLETYPE}', @tableTypeRow),
				'{SCHEMANAME}', @schemaName),
				'{PROCNAME}', @name),
				'{COLBLOCK}', @CodeReadColumns);

			SET @CodeReturnList = IsNull(@CodeReturnList + ', ', '') + @tableTypeResult;

			SET @CodeReturnValues = IsNull(@CodeReturnValues + ', ', '') + REPLACE(REPLACE(@ttProcReadTablesReturn,
				'{TABLENUMBER}', @tableIndex),
				'{ARRAYorFIRSTORDEFAULT}', CASE WHEN @isSingleRow = 1 THEN 'FirstOrDefault' ELSE 'ToArray' END);
	
			-- search for next table		
			SET @tableIndex += 1;
		END
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- generate code
	---------------------------------------------------------------------------------------------------------------------------------------------

	DECLARE @CodeProc nvarchar(max), @CodeExecute nvarchar(max);
	IF @CodeReadTable = ''
	BEGIN
		SET @CodeReturnList = 'void'; 
		SET @CodeExecute = @ttProcExecuteNoResult;
	END
	ELSE
	BEGIN
		IF @tableCount > 1
		BEGIN
			SET @CodeReturnList = '(' + @CodeReturnList + ')';
			SET @CodeReturnValues = '(' + @CodeReturnValues + ')';
		END
		SET @CodeExecute = REPLACE(REPLACE(@ttProcExecuteReader, 
			'{READTABLES}', @CodeReadTable), 
			'{RETURNVALUE}', @CodeReturnValues);
	END

	--PRINT ' @modelNamespace = ' + IsNull(@modelNamespace, '<null>');
	--PRINT ' @CodeTypeTWhere = ' + IsNull(@CodeTypeTWhere, '<null>');
	--PRINT ' @CodeTypeTDefs = ' + IsNull(@CodeTypeTDefs, '<null>');
	--PRINT ' @modelNameProc = ' + IsNull(@modelNameProc, '<null>');
	--PRINT ' @modelNameTable = ' + IsNull(@modelNameTable, '<null>');
	--PRINT ' @CodeParams = ' + IsNull(@CodeParams, '<null>');
	--PRINT ' @schemaName = ' + IsNull(@schemaName, '<null>');
	--PRINT ' @name = ' + IsNull(@name, '<null>');
	--PRINT ' @CodeAddParams = ' + IsNull(@CodeAddParams, '<null>');
	--PRINT ' @CodeExecute = ' + IsNull(@CodeExecute, '<null>');
	
	DECLARE @CodeExtensionReturn nvarchar(max) = REPLACE(REPLACE(@ttExtensionReturn,
		'{MODELNAMESPACE}', @modelNamespace),
		'{MODELNAME}', @modelNameProc);

	DECLARE @CodeProcessResults nvarchar(max) = REPLACE(REPLACE(REPLACE(REPLACE(@ttProcessResults,
		'{RESULTTYPES}', @CodeReturnList), 
		'{SCHEMANAME}', @schemaName),
		'{PROCNAME}', @name),
		'{EXECUTEBLOCK}', @CodeExecute);

	SET @CodeProc = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProc, 
		'{MODELNAMESPACE}', @modelNamespace),
		'{TASKRESULTTYPES}', CASE WHEN @CodeReturnList = 'void' THEN 'Task' ELSE 'Task<' + @CodeReturnList + '>' END), 
		'{EXTENSIONRETURN}', CASE WHEN @CodeReturnList = 'void' THEN '' ELSE @CodeExtensionReturn END), 
		'{MODELNAME}', @modelNameProc),
		'{CSPARAMSLIST}', CASE WHEN Len(@CodeParams) > 0 THEN ', ' + @CodeParams ELSE '' END),
		'{PARAMSLIST}', @CodeParams),
		'{PARAMSCALL}', @CodeParamsCall),
		'{SCHEMANAME}', @schemaName),
		'{PROCNAME}', @name),
		'{ADDPARAMS}', @CodeAddParams),
		'{PROCESSRESULT}', CASE WHEN @CodeReturnList = 'void' THEN '' ELSE @CodeProcessResults END);


	-- for debugging
	IF @OutputType = 'PRINT'
	BEGIN
		PRINT '--------------------------- C O D E   P R O C -----------------------------';
		PRINT Convert(varchar, LEN(@CodeProc)) + ' characters';
		PRINT @CodeProc;
		PRINT '---------------------------------------------------------------------------';
	END
	ELSE
		SELECT 
			@modelNameProc + '.schema.cs' AS [FileName],
			@CodeProc AS Content,
			@modify_date AS modify_date;

	-- Tests
	-- EXEC build.GenerateCSharpProcedure_BackendHub 'ils', 'csharp', 'backendhub', 33487248, 'PRINT';
	-- EXEC build.GenerateCSharpProcedure_BackendHub 'ils', 'csharp', 'backendhub', 641489414, 'PRINT';
	-- EXEC build.GenerateCSharpProcedure_BackendHub 'fire', 'csharp', 'backendhub', 1044198770, 'PRINT';
END
GO
