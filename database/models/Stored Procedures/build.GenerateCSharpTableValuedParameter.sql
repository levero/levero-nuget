SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Generates a model for a table type parameter
-- =============================================
CREATE PROCEDURE [build].[GenerateCSharpTableValuedParameter]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@csharp_mode				varchar(max),
	@objectID					int,
	@paramName					sysname,
	@modelName					sysname,
	@OutputType					varchar(10) = 'SELECT'
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @modeNULLABLE bit = 0;

	-- generator mode nullable?
	IF (@csharp_mode + ',') LIKE '%[:,]nullable,%'
		SET @modeNULLABLE = 1;

	DECLARE @procModelNamespace nvarchar(max), @procModelName nvarchar(max), @modify_date datetime; 
	SELECT
		@procModelNamespace = IsNull(modelNamespace, 'LeveroModels.DefaultNamespace'),
		@procModelName = modelName,
		@modify_date = modify_date
	FROM build.TEMP_Procedures
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for table types
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @ttTable nvarchar(max) = 'using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

#pragma warning disable IDE0079 // Remove unnecessary suppression
#pragma warning disable IDE0016 // Use ''throw'' expression
#pragma warning disable IDE0047 // Remove unnecessary parentheses
#pragma warning disable IDE0049 // Simplify Names
#pragma warning disable IDE0051 // Remove unused private members

namespace {MODELNAMESPACE}
{
    /// <summary>
    /// This class is of type {TABLETYPE} and is used for argument {PARAMNAME} in procedure {PROCNAME}
    /// </summary>
    public partial class {MODELNAME}__{TABLENAME}
    {
        private const string NOT_INITIALIZED = "<not initialized>";

{CONTENT}        public override string ToString()
            => $"{MODELNAME}__{TABLENAME}:{TOSTRING}";
    }
}';

	DECLARE @ttTableProperty nvarchar(max) = '        internal {COLTYPE} _{COLNAME}{DEFAULT};

        /// <summary>
		/// Field {COLNAME} with sql type {SQLTYPE} and options [{OPTIONS}]
		/// </summary>
        public {COLTYPE} {COLNAME}
        {
            get => this._{COLNAME};
            set
            {
                if (this._{COLNAME} == value)
                    return;
{NULLCHECK}{LENCHECK}                this._{COLNAME} = value;
            }
        }

';
	DECLARE @ttTablePropertyIdentity nvarchar(max) = '        // you can (if key: must) implement following code for this identity column in partial class:
        /// <summary>
		/// Field {COLNAME} with sql type {SQLTYPE} and options [{OPTIONS}]
		/// </summary>
        //public {COLTYPE} {COLNAME} { get; } = GetNextIdentityValue_for_{COLNAME}();

        // you can implement following code for this computed key column in partial class:
        // private static {COLTYPE} GetNextIdentityValue_for_{COLNAME}()
        //    =>  ... do calculation ...;

';
	DECLARE @ttTablePropertyComputed nvarchar(max) = '        // you can (if key: must) implement following code for this computed column in partial class:
	    /// <summary>
		/// Field {COLNAME} with sql type {SQLTYPE} and options [{OPTIONS}]
		/// </summary>
        //public {COLTYPE} {COLNAME} 
		//	=> ... do calculation ...;

';
	DECLARE @ttTableNullCheck nvarchar(max) = '                if (value == null)
                    throw new Exception("Property {MODELNAME}__{TABLENAME}.{COLNAME} must not be null!");
';
	DECLARE @ttTableLenCheck nvarchar(max) = '                if (value.Length > {MAXLEN})
                    throw new Exception("Property {MODELNAME}__{TABLENAME}.{COLNAME} may only be {MAXLEN} character(s) long!");
';
	DECLARE @ttTableLenCheckNullable nvarchar(max) = '                if (value != null && value.Length > {MAXLEN})
                    throw new Exception("Property {MODELNAME}__{TABLENAME}.{COLNAME} may only be {MAXLEN} character(s) long!");
';
	DECLARE @ttTableEqualityIntf nvarchar(max) = ', IEquatable<{MODELNAME}__{TABLENAME}>';
	DECLARE @ttTableEqualityImpl nvarchar(max) = '        public bool Equals({MODELNAME}__{TABLENAME}? other)
        {
			// if computed/identity columns are used as keys you have to implement them in partial classes.
			// in this case there you can find a template in code above.
			if (other == null) return false;
{COMPARE}            return true;
        }

        public override bool Equals(object? obj)
            => obj is {MODELNAME}__{TABLENAME} other && this.Equals(other);
		
        public override int GetHashCode()
			=> {HASH};

';
	DECLARE @ttTableEqualityCompareNullable nvarchar(max) = '            if (this.{COLNAME} == null ^ other.{COLNAME} == null) return false;
            if (this.{COLNAME} != null && !this.{COLNAME}.Equals(other.{COLNAME})) return false;
';
	DECLARE @ttTableEqualityCompare nvarchar(max) = '            if (!this.{COLNAME}.Equals(other.{COLNAME})) return false;
';
	DECLARE @ttTableEqualityHash nvarchar(max) = 'this.{COLNAME}.GetHashCode()';
	DECLARE @ttTableEqualityHashNullable nvarchar(max) = '(this.{COLNAME}?.GetHashCode() ?? 0)';
	DECLARE @ttTableEqualityNextHash nvarchar(max) = '
                ^ ';
	DECLARE @ttTableToString nvarchar(max) = '\r\n  {COLNAME}={this.{COLNAME}{NULLABLE}}';

	DECLARE @ttTableInitMetaData1 nVarchar(MAX) = '            new SqlMetaData("{COLNAME}", SqlDbType.{SQLDBTYPE}, {USEDEFAULT}, {ISKEY}, System.Data.SqlClient.SortOrder.{SORTDIRECTION}, {SORTORDER}),
';
	DECLARE @ttTableInitMetaData2 nVarchar(MAX) = '            new SqlMetaData("{COLNAME}", SqlDbType.{SQLDBTYPE}, {MAXLEN}, {USEDEFAULT}, {ISKEY}, System.Data.SqlClient.SortOrder.{SORTDIRECTION}, {SORTORDER}),
';
	DECLARE @ttTableInitMetaData3 nVarchar(MAX) = '            new SqlMetaData("{COLNAME}", SqlDbType.{SQLDBTYPE}, {PRECISION}, {SCALE}, {USEDEFAULT}, {ISKEY}, System.Data.SqlClient.SortOrder.{SORTDIRECTION}, {SORTORDER}),
';
	DECLARE @ttTableConvertSort nVarchar(MAX) = '                .{METHOD}(it => it.{COLNAME})
';
	DECLARE @ttTableConvertSetValue nVarchar(MAX) = '                    row.{SETMETHOD}({COLINDEX}, it.{COLNAME});
';
	DECLARE @ttTableConvertSetValueNullableValTyp nVarchar(MAX) = '                    if (it.{COLNAME} != null)
                        row.{SETMETHOD}({COLINDEX}, it.{COLNAME}.Value);
					else
						row.SetDBNull({COLINDEX});
';
	DECLARE @ttTableConvertSetValueNullableRefTyp nVarchar(MAX) = '                    if (it.{COLNAME} != null)
                        row.{SETMETHOD}({COLINDEX}, it.{COLNAME});
					else
						row.SetDBNull({COLINDEX});
';

	DECLARE @ttTableConvertToTable nvarchar(max) = '		private static readonly SqlMetaData[] meta = 
        {
{INITMETADATA}        };

        internal static SqlDataRecord[]{NULLABLE} ConvertToTable({MODELNAME}__{TABLENAME}[]? rows)
        {
			if (rows == null || rows.Length == 0)
				return null;
            return rows
{ORDERBYKEY}                .Select(it =>
                {
                    var row = new SqlDataRecord(meta);
{SETVALUES}                    return row;
                })
                .ToArray();
        }

		internal static DataTable ConvertToDataTable({MODELNAME}__{TABLENAME}[]? rows)
		{
			// this is old converter which is not used in automatic conversion. can be used for manually conversion.
            // prepare DataTable
            var table = new DataTable();
{PREPARECOLS}
            // fill DataTable
			if (rows == null) return table;
            foreach (var row in rows)
            {
                var dataRow = table.NewRow();
{FILLCOLS}                table.Rows.Add(dataRow);
            }

			return table;
		}

';
	DECLARE @ttTablePrepareColumns nvarchar(max) = '            table.Columns.Add("{COLNAME}", typeof({COLTYPE})).AllowDBNull = {NULLABLE};
';
	DECLARE @ttTableFillColumns nvarchar(max) = '                dataRow["{COLNAME}"] = row.{COLNAME}{NULLABLE};
';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load columns
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @columnID int = 1, @CodeContent nvarchar(max) = '', @CodeEqualPK nvarchar(max) = '';
	DECLARE @CodeConverterPrepare nvarchar(max) = '', @CodeConverterFill nvarchar(max) = '';
	DECLARE @CodeIntf nvarchar(max) = '', @CodeToString nvarchar(max) = '', @CodeHashPK nvarchar(max) = null;
	DECLARE @tableTypeName NVARCHAR(MAX), @procName NVARCHAR(MAX);
	DECLARE @CodeInitMetaData NVARCHAR(MAX) = '';
	DECLARE @CodeOrderByKey NVARCHAR(MAX) = '', @keySortOrder int = -1;
	DECLARE @CodeSetValues NVARCHAR(MAX) = '';

	SELECT 
		@procName = schemaName + '.' + [name]
	FROM build.TEMP_Procedures
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID;

	SELECT TOP 1
		@tableTypeName = sqlType
	FROM build.TEMP_Columns
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID
		AND resultset = '@'
		AND colName = SUBSTRING(@paramName, 2, 999);

	DECLARE @colName sysname, @colIsNullable bit, @sqlType sysname, @colDef nvarchar(max);
	WHILE @columnID > 0
	BEGIN
		SET @colName = NULL;
		SET @colDef = ' ';

		-- search for column
		SELECT TOP 1
			@colName = colName, 
			@sqlType = sqlType,
			@colDef = colDefinition,
			@colIsNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END
		FROM build.TEMP_Columns
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND resultset = @paramName
			AND colIndex = @columnID - 1;

		IF @colName IS NULL
			SET @columnID = -1;
		ELSE
		BEGIN
			DECLARE @netType sysname, @netBaseType sysname, @isBaseTypeNullable bit, @colMaxLen sysname, @colMetaMaxLen sysname, @colPrecision int, @colScale int;
			DECLARE @colSqlDbType sysname, @colSetMethod sysname;
			SELECT @netType = CASE WHEN @colIsNullable = 1 THEN NullableType ELSE BaseType END,
				@netBaseType = BaseType,
				@isBaseTypeNullable = IsBaseTypeNullable,
				@colMaxLen = [Length],
				@colMetaMaxLen = Convert(nVarchar, [Length]),
				@colPrecision = [Precision],
				@colScale = Scale,
				@colSqlDbType = SqlDbType,
				@colSetMethod = SetMethod
			FROM build.ConvertToNetType(@sqlType);
			--PRINT '        -> found ' + Convert(varchar, @columnID) + '. column ' + @colName + ' with type ' + @sqlType + ' and options [' + RTRIM(LTRIM(@colDef)) + ']';
			
			DECLARE @isComputed bit = CASE WHEN @colDef LIKE '% computed %' THEN 1 ELSE 0 END;
			DECLARE @isIdentity bit = CASE WHEN @colDef LIKE '% identity %' THEN 1 ELSE 0 END;
			DECLARE @isKey bit = CASE WHEN @colDef LIKE '% key %' THEN 1 ELSE 0 END;
			DECLARE @isReadonly bit = CASE WHEN @isComputed = 1 THEN 1 WHEN @isIdentity = 1 THEN 1 ELSE 0 END;

			-- prepare ToString
			IF @isReadonly = 0
				SET @CodeToString += REPLACE(REPLACE(@ttTableToString, 
					'{COLNAME}', @colName), 
					'{NULLABLE}', CASE WHEN @ColDef LIKE '% null %' THEN '?.ToString() ?? "<null>"' ELSE '' END);

			-- prepare table converter - prepare cols
			IF @isReadonly = 0
				SET @CodeConverterPrepare += REPLACE(REPLACE(REPLACE(@ttTablePrepareColumns,
					'{COLNAME}', @colName),
					'{COLTYPE}', @netBaseType),
					'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN 'true' ELSE 'false' END);

			-- prepare table converter - fill cols
			IF @isReadonly = 0
				SET @CodeConverterFill += REPLACE(REPLACE(@ttTableFillColumns,
					'{COLNAME}', @colName),
					'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN ' ?? (object)DBNull.Value' ELSE '' END);

			-- hash
			IF @isKey = 1
				SET @CodeHashPK = IsNull(@CodeHashPK + @ttTableEqualityNextHash, '') + 
					REPLACE(CASE WHEN @colIsNullable = 1 OR @isBaseTypeNullable = 1 THEN @ttTableEqualityHashNullable ELSE @ttTableEqualityHash END, '{COLNAME}', @colName);

			-- equality
			IF @isKey = 1
				SET @CodeEqualPK += REPLACE(CASE WHEN @colIsNullable = 1 OR @isBaseTypeNullable = 1 THEN @ttTableEqualityCompareNullable ELSE @ttTableEqualityCompare END, '{COLNAME}', @colName);

			-- key sort order
			IF @isKey = 1
				SET @keySortOrder += 1;

			-- prepare max length
			IF @colMetaMaxLen IS NULL AND (@colSqlDbType LIKE '%char' OR @colSqlDbType LIKE '%binary' OR @colSqlDbType IN ('text', 'image', 'ntext'))
				SET @colMetaMaxLen = 'SqlMetaData.Max';

			-- converter - init metadata
			--IF @
			SET @CodeInitMetaData += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
				CASE 
					WHEN @colScale IS NOT NULL AND @colPrecision IS NOT NULL THEN @ttTableInitMetaData3 
					WHEN @colMetaMaxLen IS NOT NULL THEN @ttTableInitMetaData2 
					ELSE @ttTableInitMetaData1 
				END,
				'{COLNAME}', @colName), 
				'{SQLDBTYPE}', IsNull(@colSqlDbType, '<not implemented>')), 
				'{MAXLEN}', IsNull(@colMetaMaxLen, '??')), 
				'{PRECISION}', IsNull(Convert(varchar, @colPrecision), '??')), 
				'{SCALE}', IsNull(Convert(varchar, @colScale), '??')), 
				'{USEDEFAULT}', CASE WHEN @isReadonly = 1 THEN 'true' ELSE 'false' END), 
				'{ISKEY}', CASE WHEN @isKey = 1 THEN 'true' ELSE 'false' END), 
				'{SORTDIRECTION}', CASE WHEN @isKey = 1 THEN 'Ascending' ELSE 'Unspecified' END), 
				'{SORTORDER}', CASE WHEN @isKey = 1 THEN Convert(varchar, @keySortOrder) ELSE '-1' END);

			-- converter - order
			IF @isKey = 1
				SET @CodeOrderByKey += REPLACE(REPLACE(@ttTableConvertSort,
					'{METHOD}', CASE WHEN @keySortOrder = 0 THEN 'OrderBy' ELSE 'ThenBy' END),
					'{COLNAME}', @colName);

			-- converter - set value (if char -> add ToString())
			IF @isReadonly = 0
				SET @CodeSetValues += REPLACE(REPLACE(REPLACE(
					CASE 
						WHEN @colIsNullable = 1 AND @isBaseTypeNullable = 1 THEN @ttTableConvertSetValueNullableRefTyp 
						WHEN @colIsNullable = 1 THEN @ttTableConvertSetValueNullableValTyp 
						ELSE @ttTableConvertSetValue 
					END,
					'{SETMETHOD}', IsNull(@colSetMethod, '<undfined set method>')), 
					'{COLNAME}', @colName + CASE WHEN @netType = 'char' THEN '.ToString()' ELSE '' END), 
					'{COLINDEX}', @columnID - 1);

			-- regular parameter
			SET @CodeContent += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
				CASE 
					WHEN @isIdentity = 1 THEN @ttTablePropertyIdentity 
					WHEN @isComputed = 1 THEN @ttTablePropertyComputed 
					ELSE @ttTableProperty 
				END,
				'{NULLCHECK}', CASE WHEN @colIsNullable = 0 AND @isBaseTypeNullable = 1 THEN @ttTableNullCheck ELSE '' END),	-- must be before TABLENAME and COLNAME
				'{LENCHECK}', CASE 
								WHEN @colMaxLen IS NULL THEN '' 
								WHEN @colIsNullable = 0 AND @isBaseTypeNullable = 1 THEN @ttTableLenCheck 
								ELSE @ttTableLenCheckNullable 
								END),
				'{MAXLEN}', IsNull(Convert(varchar, @colMaxLen), '')),
				'{MODELNAME}', @procModelName), 
				'{TABLENAME}', @modelName), 
				'{COLNAME}', @colName), 
				'{COLTYPE}', @netType), 
				'{SQLTYPE}', @sqlType), 
				'{OPTIONS}', RTRIM(LTRIM(@colDef))),
				'{DEFAULT}', CASE WHEN @netType = 'String' THEN ' = NOT_INITIALIZED' ELSE '' END);

		END
		SET @columnID += 1;
	END;

	-- convert to table
	IF @csharp_mode like 'sql%'
		SET @CodeContent += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttTableConvertToTable,
			'{MODELNAME}', @procModelName), 
			'{TABLENAME}', @modelName), 
			'{PREPARECOLS}', @CodeConverterPrepare), 
			'{FILLCOLS}', @CodeConverterFill),
			'{INITMETADATA}', @CodeInitMetaData),
			'{ORDERBYKEY}', @CodeOrderByKey),
			'{SETVALUES}', @CodeSetValues),
			'{NULLABLE}', CASE WHEN @modeNULLABLE = 1 THEN '?' ELSE '' END);

	-- equality
	IF Len(@CodeEqualPK) > 0
	BEGIN
		SET @CodeIntf += REPLACE(REPLACE(@ttTableEqualityIntf, '{MODELNAME}', @procModelName), '{TABLENAME}', @modelName);
		SET @CodeContent += REPLACE(REPLACE(REPLACE(REPLACE(@ttTableEqualityImpl, 
			'{MODELNAME}', @procModelName), 
			'{TABLENAME}', @modelName), 
			'{COMPARE}', @CodeEqualPK), 
			'{HASH}', @CodeHashPK);
	END		

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build together
	---------------------------------------------------------------------------------------------------------------------------------------------

	DECLARE @table nvarchar(max);
	SET @table = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttTable, 
		'{MODELNAMESPACE}', @procModelNamespace), 
		'{MODELNAME}', @procModelName), 
		'{TABLENAME}', @modelName), 
		'{INTERFACES}', @CodeIntf), 
		'{CONTENT}', @CodeContent), 
		'{TOSTRING}', @CodeToString), 
		'{TABLETYPE}', @tableTypeName), 
		'{PARAMNAME}', @paramName), 
		'{PROCNAME}', @procName);

	IF @OutputType = 'PRINT'
	BEGIN
		PRINT '------------------------ P A R A M   T A B L E ------------------------------';
		PRINT Convert(varchar, LEN(@table)) + ' characters';
		PRINT @table;
		PRINT '-----------------------------------------------------------------------------';
	END 
	ELSE 
		SELECT 
			@procModelName + '__' + @modelName + '.schema.cs' AS [FileName],
			@table AS Content,
			@modify_date AS modify_date;

	-- Tests
	-- EXEC build.GenerateCSharpParamTable 'ils', 'csharp', 1042154808, '@ArtikelIDs', 'ArtikelIDs', 'PRINT';
	-- EXEC build.GenerateCSharpParamTable 'fire-connect', 'csharp', 866818150, '@Members', 'Members', 'PRINT';
END
GO
