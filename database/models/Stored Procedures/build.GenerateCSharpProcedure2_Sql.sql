SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2020-12-05
-- Description:	Generates models (call, request and response tables) for one procedure
-- =============================================
CREATE PROCEDURE [build].[GenerateCSharpProcedure2_Sql]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@csharp_mode				varchar(max),
	@objectID					int,
	@isExternal					bit,
	@OutputType					varchar(10) = 'SELECT'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @modeNULLABLE bit = 0;
	DECLARE @modeASYNC_X bit = 0;

	-- generator mode nullable?
	IF (@csharp_mode + ',') LIKE '%[:,]nullable,%'
		SET @modeNULLABLE = 1;
	-- generator mode async?
	IF (@csharp_mode + ',') LIKE '%[:,]async,%'
		SET @modeASYNC_X = 1;

	DECLARE @schemaName sysname, @name sysname, @modelNamespace sysname, @modelNameProc sysname, @modelNameTable sysname, @modify_date datetime;
	SELECT
		@schemaName = schemaName,
		@name = [name],
		@modelNamespace = IsNull(modelNamespace, 'LeveroModels.DefaultNamespace') + IsNull('.' + NullIf(modulID, ''), ''),
		@modelNameProc = modelName,
		@modify_date = modify_date
	FROM build.TEMP_Procedures2
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @ttPrefix nvarchar(max) = 'using Levero.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;{USINGTASKS}

#pragma warning disable IDE0079 // Remove unnecessary suppression
#pragma warning disable IDE0022 // Use expression body for methods
#pragma warning disable IDE0049 // Simplify Names
#pragma warning disable IDE0063 // Use simple ''using'' statement

namespace {MODELNAMESPACE}
{
    public static partial class dbo
    {';
DECLARE @ttProc nvarchar(max) = '
{DEFAULT}        public static {TRESULTTYPES} {MODELNAME}{TDEFS}(this IUnitOfWork unit{PARAMSLIST})
{TWHERE}        {
            using var cmd = unit.PrepareProcedure("{SCHEMANAME}", "{PROCNAME}");
            cmd.CommandType = CommandType.StoredProcedure;{ADDPARAMS}

{EXECUTEBLOCK}
{GETOUTPUTPARAMS}        }
';
	DECLARE @ttPostfix nvarchar(max) = '    }
}';
	DECLARE @ttProcUsingTasks nVarchar(MAX) = '
using System.Threading.Tasks;'
	DECLARE @ttProcDefaultSync nvarchar(max) = '        public static {RESULTTYPES} {MODELNAME}(this IUnitOfWork unit{PARAMSLIST})
            => {MODELNAME}{TUSAGE}(unit{PARAMSCALL});

';
	DECLARE @ttProcDefaultAsync nvarchar(max) = '        public static Task<{RESULTTYPES}> {MODELNAME}Async(this IUnitOfWork unit{PARAMSLIST})
            => {MODELNAME}Async{TUSAGE}(unit{PARAMSCALL});

';

	DECLARE @ttProcParam nvarchar(max) = ', {QUALIFIER}{PARAMTYPE} {PARAMNAME}';
	DECLARE @ttProcParamCall nvarchar(max) = ', {QUALIFIER}{PARAMNAME}';
	DECLARE @ttProcAddParam nvarchar(max) = '
            {OUTPUTVAR}cmd.Add{PARAMDIRECTION}Parameter("{PARAMNAME}", {QUALIFIER}{PARAMVALUE}{MOREPARAMS});';

	DECLARE @ttProcTWhere nvarchar(max) = '            where {TTYPE}: {TYPE}, new()
';

	DECLARE @ttProcExecuteNoResultSync nvarchar(max) = '                cmd.ExecuteNonQuery();'
	DECLARE @ttProcExecuteNoResultAsync nvarchar(max) = '                await cmd.ExecuteNonQueryAsync();'
	DECLARE @ttProcExecuteReaderSync nvarchar(max) = '            using var reader = cmd.ExecuteReader();

{READTABLES}            return {RETURNVALUE};';

	DECLARE @ttProcExecuteReaderAsync nvarchar(max) = '            using var reader = await cmd.ExecuteReaderAsync();

{READTABLES}            return {RETURNVALUE};';

	DECLARE @ttProcGetOutputParam nvarchar(max) = '                param{PARAMNAME}.ApplyValue(out {PARAMNAMECC});
';
	DECLARE @ttProcReadTableSync nvarchar(max) = '            // {TABLENUMBER}. table {TABLENAME}
{NEXTTABLE}			var list{TABLENUMBER} = new List<{TTABLETYPE}>();
{ORDINALS}            while (reader.Read())
            {
				var item = new {TTABLETYPE}();

{COLBLOCK}
				if (item is Levero.Persistence.DAL.IAfterInit iai)
					iai.AfterInit();
				list{TABLENUMBER}.Add(item);
            }

';

	DECLARE @ttProcReadTableAsync nvarchar(max) = '            // {TABLENUMBER}. table {TABLENAME}
{NEXTTABLE}			var list{TABLENUMBER} = new List<{TTABLETYPE}>();
{ORDINALS}            while (await reader.ReadAsync())
            {
				var item = new {TTABLETYPE}();

{COLBLOCK}
				if (item is Levero.Persistence.DAL.IAfterInit iai)
					iai.AfterInit();
				list{TABLENUMBER}.Add(item);
            }

';

	DECLARE @ttProcReadTableOrdinals nvarchar(max) = '            var col{TABLENUMBER}_{COLNAME} = reader.GetOrdinal("{COLNAME}");
';

	DECLARE @ttProcReadTableNextSync nvarchar(max) = '            if (!reader.NextResult())
                throw new Exception("Missing {TABLENUMBER}. table ''{TABLENAME}'' from procedure {SCHEMANAME}.{PROCNAME}!");
';
	DECLARE @ttProcReadTableNextAsync nvarchar(max) = '            if (!await reader.NextResultAsync())
                throw new Exception("Missing {TABLENUMBER}. table ''{TABLENAME}'' from procedure {SCHEMANAME}.{PROCNAME}!");
';
	DECLARE @ttProcReadTableColNull nvarchar(max) = '                // column {COLNAME} of type {COLSQLTYPE} and option {OPTIONS}
                item.{FIELDPREFIX}{COLNAME} = reader.IsDBNull(col{TABLENUMBER}_{COLNAME}) ? ({COLTYPE})null : reader.{CONVERTER}(col{TABLENUMBER}_{COLNAME});
';
	DECLARE @ttProcReadTableColNotNull nvarchar(max) = '                // column {COLNAME} of type {COLSQLTYPE} and option {OPTIONS}
                if (reader.IsDBNull(col{TABLENUMBER}_{COLNAME}))
					throw new Exception("Value of column ''{COLNAME}'' in {TABLENUMBER}. table ''{TABLENAME}'' of procedure {SCHEMANAME}.{PROCNAME} must not be null!");
                item.{FIELDPREFIX}{COLNAME} = reader.{CONVERTER}(col{TABLENUMBER}_{COLNAME});
';
	DECLARE @ttProcReadTablesReturn nvarchar(max) = 'list{TABLENUMBER}.{ARRAYorFIRSTORDEFAULT}()';


	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	IF @OutputType = 'PRINT'
		PRINT 'building ' + @modelNameProc + ' from ' + @schemaName + '.' + @name;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- get parameters of procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @hasClient bit = 0, @parameterID int;
	DECLARE @CodeParams nvarchar(max) = '', @CodeParamsCall nvarchar(max) = '', @CodeAddParams nvarchar(max) = '', @CodeOutputParams nvarchar(max) = '', @paramNameCamelCase sysname;
	DECLARE @tableTypes TABLE (tableTypeID int NOT NULL);
	SET @parameterID = 1;
	WHILE @parameterID > 0
	BEGIN
		DECLARE @paramName sysname = NULL, @paramValue sysname, @paramType sysname, @paramSqlType sysname, @paramTypeSize int;
		DECLARE @isNullable bit, @isOutput bit, @tableTypeID int, @paramTableTypeName nvarchar(max) = '';
		SELECT 
			@paramName = colName,
			@paramSqlType = sqlType,
			@tableTypeID = tableTypeObjectID,
			@isOutput = CASE WHEN colDefinition LIKE '% out %' THEN 1 ELSE 0 END,
			@isNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultset = '@' 
			AND colIndex = @parameterID - 1;

		IF @paramName IS NULL
			SET @parameterID = 0;
		ELSE
		BEGIN
			SELECT 
				@paramType = CASE WHEN @isNullable = 1 THEN NullableType ELSE BaseType END,
				@paramTypeSize = SizeInBytes
			FROM build.ConvertToNetType(@paramSqlType);
			IF @OutputType = 'PRINT'
				PRINT '    -> found ' + Convert(varchar, @parameterID) + '. parameter ' + @paramName + ' with type ' + @paramSqlType;

			SET @paramNameCamelCase = LOWER(SUBSTRING(@paramName, 1, 1)) + SUBSTRING(@paramName, 2, 999);
			SET @paramValue = @paramNameCamelCase;
			-- system parameter?
			IF @paramName LIKE '~_%' ESCAPE '~'
			BEGIN
				SET @paramNameCamelCase = LOWER(SUBSTRING(@paramName, 1, 2)) + SUBSTRING(@paramName, 3, 999);
				IF @paramName = '_ClientID'
				BEGIN
					SET @hasClient = 1;
					SET @paramValue = 'Environment.MachineName';
					IF @paramSqlType <> 'varchar(20)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(20))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE
				BEGIN
					RAISERROR('Procedure %s.%s has an unknown system parameter "%s"!', 16, 1, @schemaName, @name, @paramName);
					RETURN;
				END
			END
			ELSE IF @tableTypeID IS NOT NULL
			BEGIN
				-- build model for table type
				IF NOT EXISTS(SELECT * FROM @tableTypes WHERE tableTypeID = @tableTypeID) AND @isExternal = 0
				BEGIN
					DECLARE @resultset sysname = '@' + @paramName;
					--PRINT 'EXEC build.GenerateCSharpTableValuedParameter2 ''' + @appPackageID + ''', ''' + @generator + ''', ' + Convert(varchar, @objectID) + ', ''' + @resultset + ''', ''' + @paramType + ''', ''' + @OutputType + ''';';
					EXEC build.GenerateCSharpTableValuedParameter2 @appPackageID, @generator, @csharp_mode, @objectID, @resultset, @paramType, @OutputType;

					INSERT INTO @tableTypes (tableTypeID) VALUES (@tableTypeID);
				END

				-- make full qualified name
				SET @paramType = @modelNameProc + N'__' + @paramType;

				-- add converter for datatable
				SET @paramValue = @paramType + N'.ConvertToTable(' + @paramNameCamelCase + ')';
				SET @paramTableTypeName = N', "' + @paramSqlType + N'"';

				-- make array
				SET @paramType = @paramType + N'[]';
			END

			-- params list (no system parameters!)
			IF @paramName NOT LIKE '~_%' ESCAPE '~'
			BEGIN
				SET @CodeParams += REPLACE(REPLACE(REPLACE(@ttProcParam, 
					'{QUALIFIER}', CASE WHEN @isOutput = 1 THEN 'out ' ELSE '' END),
					'{PARAMNAME}', @paramNameCamelCase),
					'{PARAMTYPE}', @paramType + N' /* ' + @paramSqlType + N' */');
				SET @CodeParamsCall += REPLACE(REPLACE(@ttProcParamCall, 
					'{QUALIFIER}', CASE WHEN @isOutput = 1 THEN 'out ' ELSE '' END),
					'{PARAMNAME}', @paramNameCamelCase);
			END

			-- add param value to db command
            --  {OUTPUTVAR}cmd.Add{PARAMDIRECTION}Parameter("{PARAMNAME}", {QUALIFIER}{PARAMVALUE}{MOREPARAMS});
			SET @CodeAddParams += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProcAddParam, 
				'{OUTPUTVAR}', CASE WHEN @isOutput = 1 THEN 'var param' + @paramName + ' = ' ELSE '' END),
				'{PARAMDIRECTION}', CASE 
										WHEN @tableTypeID IS NOT NULL THEN 'Table' 
										WHEN @isOutput = 1 THEN 'Output' 
										ELSE 'Input' 
									END),
				'{PARAMNAME}', @paramName),
				'{QUALIFIER}', CASE WHEN @isOutput = 1 THEN 'out ' ELSE '' END),
				'{PARAMVALUE}', @paramValue),
				'{MOREPARAMS}', CASE WHEN @isOutput = 1 THEN IsNull(', ' + CONVERT(nvarchar(max), @paramTypeSize), '') ELSE @paramTableTypeName END);

			-- return output variables
			IF @isOutput = 1
				SET @CodeOutputParams += REPLACE(REPLACE(@ttProcGetOutputParam,
					'{PARAMNAME}', @paramName),
					'{PARAMNAMECC}', @paramNameCamelCase);

			SET @parameterID += 1;
		END
	END;
	--PRINT ' @CodeParams = ' + IsNull(@CodeParams, '<null>');
	--PRINT ' @CodeParamsCall = ' + IsNull(@CodeParamsCall, '<null>');
	--PRINT ' @CodeAddParams = ' + IsNull(@CodeAddParams, '<null>');

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- read schema info (resultsets and fields)
	---------------------------------------------------------------------------------------------------------------------------------------------

	DECLARE @tableIndex int = 1, @tableCount int = 0, @prevTableName sysname = NULL;
	DECLARE @CodeReturnValues nvarchar(max) = null, @CodeTypeTWhere nvarchar(max) = '';
	DECLARE @CodeReadTableSync nvarchar(max) = '', @CodeReturnListSync nvarchar(max) = null, @CodeTReturnListSync nvarchar(max) = null;
	DECLARE @CodeReadTableAsync nvarchar(max) = '', @CodeReturnListAsync nvarchar(max) = null, @CodeTReturnListAsync nvarchar(max) = null;
	WHILE @tableIndex > 0
	BEGIN
		DECLARE @tableName sysname = null, @tableDef nvarchar(max), @tableTypeRow nvarchar(max), @tableTypeTRow nvarchar(max);
		DECLARE @tableTypeResult nvarchar(max), @tableTypeTResult nvarchar(max);
		DECLARE @CodeTypeTDefs nvarchar(max), @CodeTypeTUsage nvarchar(max);
		DECLARE @CodeProcOrdinals nvarchar(max) = '', @CodeContent nvarchar(max) = '', @CodeEqualAll nvarchar(max) = '', @CodeEqualPK nvarchar(max) = '';
		DECLARE @CodeIntf nvarchar(max) = '', @CodeToString nvarchar(max) = '', @CodeHashAll nvarchar(max) = null, @CodeHashPK nvarchar(max) = null;

		-- search for table
		SELECT 
			@tableName = resultset,
			@tableDef = colDefinition,
			@modelNameTable = IsNull(NullIf(colName, ''), @modelNameProc)
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID 
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultsetIndex = @tableIndex
			AND colIndex = -1;
			
		IF @@ROWCOUNT = 0
			SET @tableIndex = 0;
		ELSE
		BEGIN
			-- table found
			SET @tableCount += 1;
			SET @tableTypeTRow = 'T' + @tableName;
			SET @tableTypeTResult = @tableTypeTRow;
			SET @tableTypeRow = @modelNameTable + '__' + @tableName;
			SET @tableTypeResult = @tableTypeRow;

			IF @OutputType = 'PRINT'
				PRINT '    -> found table "' + @modelNameTable + '__' + @tableName + '" with options [' + RTRIM(LTRIM(@tableDef)) + ']';

			-- custom dto?
			DECLARE @isCustomDto bit = 0;
			IF @tableDef like '% c#:%'
			BEGIN
				SET @tableTypeRow = SUBSTRING(@tableDef, CHARINDEX(' c#:', @tableDef) + 4, 999);
				SET @tableTypeRow = SUBSTRING(@tableTypeRow, 1, CHARINDEX(' ', @tableTypeRow) - 1);
				SET @tableTypeResult = @tableTypeRow;
				SET @isCustomDto = 1;
			END

			SET @CodeTypeTWhere += REPLACE(REPLACE(@ttProcTWhere, '{TTYPE}', @tableTypeTRow), '{TYPE}', @tableTypeRow);
			SET @CodeTypeTDefs = IsNull(@CodeTypeTDefs + ', ', '') + @tableTypeTRow;
			SET @CodeTypeTUsage = IsNull(@CodeTypeTUsage + ', ', '') + @tableTypeRow;

			--PRINT ' @CodeTypeTWhere = ' + @CodeTypeTWhere;
			--PRINT ' @CodeTypeTDefs = ' + @CodeTypeTDefs;
			--PRINT ' @CodeTypeTUsage = ' + @CodeTypeTUsage;

			-- single row? any row? multiple rows?
			DECLARE @tableTypePostfix varchar(5) = 
				CASE 
					WHEN @tableDef like '% ~[0..1~] %' ESCAPE '~' AND @modeNULLABLE = 1 THEN '?'	-- 0 or 1 rows
					WHEN @tableDef like '% ~[0..1~] %' ESCAPE '~' AND @modeNULLABLE = 0 THEN ''		-- 0 or 1 rows
					WHEN @tableDef like '% ~[1..1~] %' ESCAPE '~' OR
						 @tableDef like '% ~[1~] %' ESCAPE '~' THEN ''		-- exact 1 row
					ELSE '[]'												-- multiple rows (array)
				END;
			DECLARE @tableTypeConvertMethod varchar(25) = 
				CASE 
					WHEN @tableDef like '% ~[0..1~] %' ESCAPE '~' THEN 'FirstOrDefault'	-- 0 or 1 rows
					WHEN @tableDef like '% ~[1..1~] %' ESCAPE '~' OR
						 @tableDef like '% ~[1~] %' ESCAPE '~' THEN 'First'				-- exact 1 row
					ELSE 'ToArray'														-- multiple rows (array)
				END;
			SET @tableTypeResult += @tableTypePostfix;
			SET @tableTypeTResult += @tableTypePostfix;

			--PRINT ' @tableTypeTRow = ' + @tableTypeTRow;
			--PRINT ' @tableTypeTResult = ' + @tableTypeTResult;
			--PRINT ' @tableTypeRow = ' + @tableTypeRow;
			--PRINT ' @tableTypeResult = ' + @tableTypeResult;

			-- search for columns
			DECLARE @colIndex int = 1;
			DECLARE @CodeReadColumns nvarchar(max) = '';

			WHILE @colIndex > 0
			BEGIN
				DECLARE @colDef nvarchar(max), @colName sysname, @colSqlType sysname;
				SELECT 
					@colName = colName,
					@colSqlType = sqlType,
					@colDef = colDefinition
				FROM build.TEMP_Columns2
				WHERE appPackageID = @appPackageID 
					AND generator = @generator 
					AND objectID = @objectID 
					AND resultsetIndex = @tableIndex
					AND colIndex = @colIndex - 1;

				IF @@ROWCOUNT = 0
					SET @colIndex = 0;
				ELSE
				BEGIN
					-- column found
					DECLARE @colType sysname, @IsBaseTypeNullable bit, @colReader sysname, @colMaxLength int;
					SELECT @colType = CASE WHEN @colDef LIKE '% null %' THEN NullableType ELSE BaseType END,
						@IsBaseTypeNullable = IsBaseTypeNullable,
						@colReader = ReaderMethod,
						@colMaxLength = Length
					FROM build.ConvertToNetType(@colSqlType);
					IF @OutputType = 'PRINT'
						PRINT '        -> found ' + Convert(varchar, @colIndex) + '. column "' + @colName + '" with type "' + @colType + '" and options [' + RTRIM(LTRIM(@colDef)) + ']';

					-- build class and implementation
					SET @CodeProcOrdinals += REPLACE(REPLACE(REPLACE(@ttProcReadTableOrdinals,
						'{TABLENUMBER}', @tableIndex),
						'{TABLENAME}', @tableName),
						'{COLNAME}', @colName);
					SET @CodeReadColumns += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
						CASE WHEN @colDef LIKE '% null %' THEN @ttProcReadTableColNull ELSE @ttProcReadTableColNotNull END,
						'{TABLENUMBER}', @tableIndex),
						'{FIELDPREFIX}', CASE WHEN @isCustomDto = 1 THEN '' ELSE '_' END),
						'{COLNAME}', @colName),
						'{COLTYPE}', @colType),
						'{CONVERTER}', @colReader),
						'{TABLENAME}', @tableName),
						'{SCHEMANAME}', @schemaName),
						'{PROCNAME}', @name),
						'{COLSQLTYPE}', @colSqlType),
						'{OPTIONS}', @colDef);
					--PRINT ' @CodeProcOrdinals = ' + @CodeProcOrdinals;
					--PRINT ' @CodeReadColumns = ' + @CodeReadColumns;

					-- search for next column
					SET @colIndex += 1;
				END
			END

			-- build model for table type
			IF @isCustomDto = 0
				EXEC build.GenerateCSharpTable2 @appPackageID, @generator, @csharp_mode, @objectID, @tableIndex, @tableName, @OutputType;

			--------------------------------------------------------------------------------
			-- TABLE BLOCK
			-- add call to NextResult()
			DECLARE @CodeReadNextTableSync nvarchar(max) = '', @CodeReadNextTableAsync nvarchar(max) = '';
			if @tableIndex > 1
			BEGIN
				SET @CodeReadNextTableSync = REPLACE(REPLACE(REPLACE(REPLACE(@ttProcReadTableNextSync,
					'{TABLENUMBER}', @tableIndex),
					'{TABLENAME}', @tableName),
					'{SCHEMANAME}', @schemaName),
					'{PROCNAME}', @name);
				SET @CodeReadNextTableAsync = REPLACE(REPLACE(REPLACE(REPLACE(@ttProcReadTableNextAsync,
					'{TABLENUMBER}', @tableIndex),
					'{TABLENAME}', @tableName),
					'{SCHEMANAME}', @schemaName),
					'{PROCNAME}', @name);
			END

			SET @CodeReadTableSync += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProcReadTableSync,
				'{TABLENUMBER}', @tableIndex),
				'{TABLENAME}', @tableName),
				'{SCHEMANAME}', @schemaName),
				'{PROCNAME}', @name),
				'{NEXTTABLE}', @CodeReadNextTableSync),
				'{TTABLETYPE}', @tableTypeTRow),
				'{ORDINALS}', @CodeProcOrdinals),
				'{COLBLOCK}', @CodeReadColumns);
			SET @CodeReadTableAsync += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProcReadTableAsync,
				'{TABLENUMBER}', @tableIndex),
				'{TABLENAME}', @tableName),
				'{SCHEMANAME}', @schemaName),
				'{PROCNAME}', @name),
				'{NEXTTABLE}', @CodeReadNextTableAsync),
				'{TTABLETYPE}', @tableTypeTRow),
				'{ORDINALS}', @CodeProcOrdinals),
				'{COLBLOCK}', @CodeReadColumns);

			SET @CodeReturnListSync = IsNull(@CodeReturnListSync + IsNull(' ' + @prevTableName, '') + ', ', '') + @tableTypeResult;
			SET @CodeReturnListAsync = IsNull(@CodeReturnListAsync + IsNull(' ' + @prevTableName, '') + ', ', '') + @tableTypeResult;
			SET @CodeTReturnListSync = IsNull(@CodeTReturnListSync + IsNull(' ' + @prevTableName, '') + ', ', '') + @tableTypeTResult;
			SET @CodeTReturnListAsync = IsNull(@CodeTReturnListAsync + IsNull(' ' + @prevTableName, '') + ', ', '') + @tableTypeTResult;

			SET @CodeReturnValues = IsNull(@CodeReturnValues + ', ', '') + REPLACE(REPLACE(@ttProcReadTablesReturn,
				'{TABLENUMBER}', @tableIndex),
				'{ARRAYorFIRSTORDEFAULT}', @tableTypeConvertMethod);

			SET @prevTableName = @tableName;
	
			-- search for next table		
			SET @tableIndex += 1;
		END
	END
	IF @tableCount > 1
	BEGIN
		SET @CodeReturnListSync += IsNull(' ' + @prevTableName, '');
		SET @CodeReturnListAsync += IsNull(' ' + @prevTableName, '');
		SET @CodeTReturnListSync += IsNull(' ' + @prevTableName, '');
		SET @CodeTReturnListAsync += IsNull(' ' + @prevTableName, '');
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- generate code
	---------------------------------------------------------------------------------------------------------------------------------------------

	IF @tableCount > 1
	BEGIN
		SET @CodeReturnListSync = '(' + @CodeReturnListSync + ')';
		SET @CodeReturnListAsync = '(' + @CodeReturnListAsync + ')';
		SET @CodeTReturnListSync = '(' + @CodeTReturnListSync + ')';
		SET @CodeTReturnListAsync = '(' + @CodeTReturnListAsync + ')';
		SET @CodeReturnValues = '(' + @CodeReturnValues + ')';
	END

	DECLARE @CodeProcDefaultSync nvarchar(max) = '', @CodeProcDefaultAsync nvarchar(max) = '';
	IF @CodeTypeTUsage IS NOT NULL
	BEGIN
		SET @CodeProcDefaultSync = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProcDefaultSync, 
			'{RESULTTYPES}', @CodeReturnListSync), 
			'{TUSAGE}', '<' + @CodeTypeTUsage + '>'), 
			'{PARAMSCALL}', @CodeParamsCall), 
			'{MODELNAME}', @modelNameProc),
			'{PARAMSLIST}', @CodeParams);
		SET @CodeProcDefaultAsync = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProcDefaultAsync, 
			'{RESULTTYPES}', @CodeReturnListAsync), 
			'{TUSAGE}', '<' + @CodeTypeTUsage + '>'), 
			'{PARAMSCALL}', @CodeParamsCall), 
			'{MODELNAME}', @modelNameProc),
			'{PARAMSLIST}', @CodeParams);
	END

	DECLARE @CodeExecuteSync nvarchar(max), @CodeExecuteAsync nvarchar(max);
	-- Sync
	IF @CodeReadTableSync = ''
	BEGIN
		SET @CodeReturnListSync = 'void'; 
		SET @CodeTReturnListSync = 'void'; 
		SET @CodeExecuteSync = @ttProcExecuteNoResultSync;
	END
	ELSE
	BEGIN
		SET @CodeExecuteSync = REPLACE(REPLACE(@ttProcExecuteReaderSync, 
			'{READTABLES}', @CodeReadTableSync), 
			'{RETURNVALUE}', @CodeReturnValues);
	END 
	-- Async
	IF @CodeReadTableAsync = ''
	BEGIN
		SET @CodeReturnListAsync = 'Task'; 
		SET @CodeTReturnListAsync = 'async Task'; 
		SET @CodeExecuteAsync = @ttProcExecuteNoResultAsync;
	END
	ELSE
	BEGIN
		SET @CodeExecuteAsync = REPLACE(REPLACE(@ttProcExecuteReaderAsync, 
			'{READTABLES}', @CodeReadTableAsync), 
			'{RETURNVALUE}', @CodeReturnValues);
		SET @CodeReturnListAsync = 'Task<' + @CodeReturnListAsync + '>';
		SET @CodeTReturnListAsync = 'async Task<' + @CodeTReturnListAsync + '>';
	END


	--PRINT ' @modelNamespace = ' + IsNull(@modelNamespace, '<null>');
	--PRINT ' @CodeTReturnList = ' + IsNull(@CodeTReturnList, '<null>');
	--PRINT ' @CodeTypeTWhere = ' + IsNull(@CodeTypeTWhere, '<null>');
	--PRINT ' @CodeProcDefault = ' + IsNull(@CodeProcDefault, '<null>');
	--PRINT ' @CodeTypeTDefs = ' + IsNull(@CodeTypeTDefs, '<null>');
	--PRINT ' @modelName = ' + IsNull(@modelName, '<null>');
	--PRINT ' @CodeParams = ' + IsNull(@CodeParams, '<null>');
	--PRINT ' @schemaName = ' + IsNull(@schemaName, '<null>');
	--PRINT ' @name = ' + IsNull(@name, '<null>');
	--PRINT ' @CodeAddParams = ' + IsNull(@CodeAddParams, '<null>');
	--PRINT ' @CodeExecute = ' + IsNull(@CodeExecute, '<null>');

	DECLARE @CodeProc nvarchar(max) ;
	SET @CodeProc = REPLACE(REPLACE(@ttPrefix, 
		'{USINGTASKS}', @ttProcUsingTasks),
		'{MODELNAMESPACE}', @modelNamespace);

	SET @CodeProc += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProc, 
		'{TRESULTTYPES}', @CodeTReturnListSync), 
		'{TWHERE}', @CodeTypeTWhere), 
		'{DEFAULT}', @CodeProcDefaultSync), 
		'{TDEFS}', IsNull('<' + @CodeTypeTDefs + '>', '')), 
		'{MODELNAME}', @modelNameProc),
		'{PARAMSLIST}', @CodeParams),
		'{SCHEMANAME}', @schemaName),
		'{PROCNAME}', @name),
		'{ADDPARAMS}', @CodeAddParams),
		'{EXECUTEBLOCK}', @CodeExecuteSync),
		'{GETOUTPUTPARAMS}', @CodeOutputParams);

	IF @modeASYNC_X = 1
		SET @CodeProc += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttProc, 
			'{USINGTASKS}', @ttProcUsingTasks),
			'{MODELNAMESPACE}', @modelNamespace),
			'{TRESULTTYPES}', @CodeTReturnListAsync), 
			'{TWHERE}', @CodeTypeTWhere), 
			'{DEFAULT}', @CodeProcDefaultAsync), 
			'{TDEFS}', IsNull('<' + @CodeTypeTDefs + '>', '')), 
			'{MODELNAME}', @modelNameProc + 'Async'),
			'{PARAMSLIST}', @CodeParams),
			'{SCHEMANAME}', @schemaName),
			'{PROCNAME}', @name),
			'{ADDPARAMS}', @CodeAddParams),
			'{EXECUTEBLOCK}', @CodeExecuteAsync),
			'{GETOUTPUTPARAMS}', @CodeOutputParams);

	SET @CodeProc += @ttPostfix;

	-- for debugging
	IF @OutputType = 'PRINT'
	BEGIN
		PRINT '--------------------------- C O D E   P R O C -----------------------------';
		PRINT Convert(varchar, LEN(@CodeProc)) + ' characters';
		PRINT @CodeProc;
		PRINT '---------------------------------------------------------------------------';
	END
	ELSE
		SELECT 
			@modelNameProc + '.schema.cs' AS [FileName],
			@CodeProc AS Content,
			@modify_date AS modify_date
		WHERE @isExternal = 0;

	-- Tests
	-- EXEC build.GenerateCSharpProcedure2_Sql 'ils', 'csharp', 'sql:async,null', 33487248, 'PRINT';
	-- EXEC build.GenerateCSharpProcedure2_Sql 'ils', 'csharp', 'sql:async,null', 641489414, 'PRINT';
	-- EXEC build.GenerateCSharpProcedure2_Sql 'ils', 'csharp', 'sql:async,null', 1000077232, 'PRINT';	 -- output params
END
GO
