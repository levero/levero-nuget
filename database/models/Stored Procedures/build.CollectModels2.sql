SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-03-05
-- Description:	Collects all procedures from target database
-- =============================================
CREATE PROCEDURE [build].[CollectModels2]
	@Generator					varchar(15),
	@AppPackageID				varchar(15),
	@ModulID					varchar(15),
	@DatabaseName				sysname,
	@AppPackageVersion			varchar(10),
	@ListOfSchema				varchar(100),
	@ListOfPrefix				varchar(100),
	@RemovePrefix				bit
AS
BEGIN
	SET NOCOUNT ON;

	-- constants
	DECLARE @True			bit = 1;
	DECLARE @False			bit = 0;
	DECLARE @ModelDB		sysname = QUOTENAME(DB_NAME());
	DECLARE @AppVersionComparable int = build.VersionToInt(@AppPackageVersion);

	-- variables
	DECLARE @schemaName		sysname;
	DECLARE @sql			nvarchar(max);
	DECLARE @schemaID		int;


	IF NOT EXISTS(SELECT * FROM master.sys.databases WHERE [name] = @DatabaseName)
	BEGIN
		RAISERROR('Database with name %s not found!', 16, 1, @DatabaseName);
		RETURN;
	END

	SET @ModulID = IsNull(@ModulID, '');

	-- first clear all TEMP_ tables
	IF @ModulID = ''
	BEGIN
		DELETE FROM build.TEMP_Filter2 WHERE appPackageID = @AppPackageID AND generator = @Generator;
		DELETE FROM build.TEMP_Procedures2 WHERE appPackageID = @AppPackageID AND generator = @Generator;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load settings FOR ACTIONS
	---------------------------------------------------------------------------------------------------------------------------------------------

	PRINT '------------------------------------------------------------------------------';
	PRINT 'generate ' + UPPER(@Generator) + ' of app package ' + @AppPackageID + IsNull('/' + NullIf(@ModulID, ''), '') + 
		' version ' + @AppPackageVersion + ' in ' + @DatabaseName + '.(' + @ListOfSchema + ').(' + @ListOfPrefix + ')...';
	PRINT '------------------------------------------------------------------------------';

	-- get prefix and schema from settings
	INSERT INTO build.TEMP_Filter2 (appPackageID, modulID, generator, databaseName, schemaName, listOfPrefix, targetVersion)
	SELECT @AppPackageID, @ModulID, @Generator, @DatabaseName, s.Item, @ListOfPrefix, @AppPackageVersion
	FROM build.Split2(@ListOfSchema, ',', @True, @True) s;

	-- check schemas
	SET @sql = N'
	UPDATE t
	SET schemaID = s.[schema_id]
	FROM build.TEMP_Filter2 t
		INNER JOIN [' + @DatabaseName + N'].sys.schemas s ON s.[name] = t.schemaName COLLATE DATABASE_DEFAULT
	WHERE t.appPackageID = ''' + @AppPackageID + N'''
		AND t.modulID = ''' + @ModulID + N'''
		AND t.generator = ''' + @Generator + N''';
	IF EXISTS(SELECT * FROM build.TEMP_Filter2 
			  WHERE appPackageID = ''' + @AppPackageID + N''' 
				  AND modulID = ''' + @ModulID + N''' 
				  AND generator = ''' + @Generator + N''' AND schemaID IS NULL)
	BEGIN
		RAISERROR(''one or more schemas in database ' + @DatabaseName + N' not found!'', 16, 1);
		RETURN;
	END';
	--PRINT @sql;
	EXEC sys.sp_executesql @sql;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load all procedures with given schemas and prefix
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @Procedures TABLE (objectID int NOT NULL, modify_date datetime not null, schemaName sysname NOT NULL, name sysname NOT NULL, modelName sysname NOT NULL, procVersion varchar(10) NOT NULL);
	WHILE 1 = 1
	BEGIN
		SELECT TOP (1) @schemaID = schemaID, @schemaName = schemaNAme 
		FROM build.TEMP_Filter2 
		WHERE appPackageID = @AppPackageID AND modulID = @ModulID AND generator = @Generator AND done = 0;
		IF @@ROWCOUNT = 0
			BREAK;
		ELSE
		BEGIN	
			UPDATE build.TEMP_Filter2 
			SET done = 1 
			WHERE appPackageID = @AppPackageID AND modulID = @ModulID AND generator = @Generator AND schemaID = @schemaID;

			PRINT ' > collect ' + @schemaName + '[' + Format(@schemaID, '0') + '].' + @ListOfPrefix + '...';
			SET @sql = N'USE [' + @DatabaseName + N'];
			WITH rawData AS
			(
				SELECT 
					SCHEMA_NAME(p.schema_id) AS schemaName, 
					p.[object_id] as objectID, 
					p.[name], 
					' + @ModelDB + N'.build.GetModelNameFromProcName(p.[name], ' + Convert(varchar, IsNull(@RemovePrefix, 0)) + N') AS modelName, 
					' + @ModelDB + N'.build.GetVersionFromProcName(p.[name]) as procVersion,
					p.modify_date
				FROM sys.procedures p
				WHERE (' + (
					SELECT 
						STRING_AGG(N'p.[name] LIKE (''' + REPLACE(Item, '_', '~_') + N'%'') ESCAPE ''~''', N'
					OR ')
					FROM build.Split2(@ListOfPrefix, ',', @True, @False)) + N')
					AND p.[schema_id] = ' + Format(@SchemaID, '0') + N'
			), validProcs AS
			(
				SELECT 
					objectID, 
					schemaName, 
					[name], 
					modelName, 
					procVersion, 
					modify_date,
					ROW_NUMBER() OVER(PARTITION BY schemaName, modelName ORDER BY ' + @ModelDB + N'.build.VersionToInt(procVersion) DESC) AS latestIsOne
				FROM rawData
				WHERE modelName IS NOT NULL
					AND ' + @ModelDB + N'.build.VersionToInt(procVersion) <= ' + Convert(nvarchar, @AppVersionComparable) + N'
			)
			INSERT INTO ' + @ModelDB + N'.build.TEMP_Procedures2 (databaseName, appPackageID, modulID, generator, objectID, modify_date, schemaName, name, modelName, procVersion)
			SELECT 
				''' + REPLACE(@DatabaseName, '''', '''''') + N''', 
				''' + REPLACE(@AppPackageID, '''', '''''') + N''',
				''' + REPLACE(@ModulID, '''', '''''') + N''',
				''' + REPLACE(@Generator, '''', '''''') + N''',
				objectID, 
				modify_date,
				schemaName, 
				[name], 
				modelName, 
				procVersion
			FROM validProcs
			WHERE latestIsOne = 1
			ORDER BY modelName;';
			--PRINT @sql;
			EXEC sys.sp_executesql @sql;
		END
	END
	DECLARE @count int = (SELECT COUNT(*) FROM build.TEMP_Procedures2 WHERE appPackageID = @AppPackageID AND modulID = @ModulID AND generator = @Generator);
	PRINT 'found ' + CONVERT(varchar, @count) + ' procedure(s)';

	-- parse all procedures
	DECLARE @objectToParse INT, @objectName sysname;
	SELECT TOP(1)
        @objectToParse = p.objectID,
        @objectName = p.databaseName + '.' + p.schemaName + '.' + p.[name]
	FROM build.TEMP_Procedures2 p
	WHERE p.appPackageID = @AppPackageID 
		AND p.modulID = @ModulID 
		AND p.generator = @Generator
		AND p.done = 0
    ORDER BY p.[name];

	WHILE @objectToParse IS NOT NULL
	BEGIN
		-- parse
        BEGIN TRY
		    EXEC build.ParseProcedure2 @AppPackageID, @generator, @objectToParse, @RemovePrefix;
        END TRY
        BEGIN CATCH
            PRINT 'Failed to parse ' + @objectName + ': ' + ERROR_MESSAGE();
            PRINT 'EXEC build.ParseProcedure2 @appPackageID=''' + @AppPackageID + ''', @generator=''' + @generator + 
                ''', @objectID=' + FORMAT(@objectToParse, '0') + ', @removePrefix=' + FORMAT(Convert(INT, @RemovePrefix), '0') + ', @debug=1;';
            THROW;
        END CATCH

		-- set to done
		UPDATE build.TEMP_Procedures2
		SET done = 1
		WHERE appPackageID = @AppPackageID 
			AND modulID = @ModulID 
			AND generator = @Generator
			AND objectID = @objectToParse;

		-- get next
        SET @objectToParse = NULL;
	    SELECT TOP(1)
            @objectToParse = p.objectID,
            @objectName = p.databaseName + '.' + p.schemaName + '.' + p.[name]
	    FROM build.TEMP_Procedures2 p
	    WHERE p.appPackageID = @AppPackageID 
		    AND p.modulID = @ModulID 
		    AND p.generator = @Generator
		    AND p.done = 0
        ORDER BY p.[name];
	END

	-- reset done
	UPDATE build.TEMP_Procedures2
	SET done = 0
	WHERE appPackageID = @AppPackageID 
		AND modulID = @ModulID
		AND generator = @Generator;

	--PRINT 'finished';

	-- Tests
	-- EXEC build.CollectModels2 'actions', 'FuhrparkTest', 'levero-trans', '2', 'dbo', 'V3_', '', 1;
	-- EXEC build.CollectModels2 'actions', 'Pumpenoase', 'ils', '99', 'dbo', 'ACT_', '', 1;
	-- EXEC build.CollectModels2 'actions', 'LeveroApps', 'te-capex-fc', '2', 'dbo,roles,capex', 'ACT_', '', 1;
	-- EXEC build.CollectModels2 'typescript', 'LeveroApps', 'te-datapool', '1.4', 'dbo,roles,db', 'ACT_', '', 1;
	-- EXEC build.CollectModels2 'typescript', 'LeveroAppsKammerer', 'test', '0.22', 'dbo', 'ACT_', 'roles', 1;
END
GO
