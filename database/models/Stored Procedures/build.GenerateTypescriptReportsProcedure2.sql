SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-24
-- Description:	Generates a frontend model description for one procedure
-- =============================================
CREATE PROCEDURE [build].[GenerateTypescriptReportsProcedure2]
	@appPackageID				VARCHAR(15),
	@generator					VARCHAR(15),
	@objectID					INT,
	@frontendModelDefinition	NVARCHAR(MAX) OUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @schemaName sysname, @name sysname, @modelName sysname, @modulIDprefix sysname;
	SELECT
		@schemaName = schemaName,
		@name = [name],
		@modelName = modelName,
		@modulIDprefix = ISNULL(NULLIF(modulID, '') + '|', '')
	FROM build.TEMP_Procedures2
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for frontend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @frontend_request NVARCHAR(MAX) =
'/* ***************************************************************************************************************/
/** Request Parameters of Report {SP} */
export class {SPCLASS} {{CONSTANTS}

  constructor({PARAMS}) {

    const _tools = ServiceLocator.injector.get(LeveroToolsService);
    {PREPARE}
  }
}

';
    DECLARE @frontend_constMaxLen nvarchar(max) = '
  /** param: {PARAMNAME} {SQLTYPE} */
  public static readonly {PARAMNAME}_MaxLength = {MAXLEN};';
    DECLARE @frontend_param nvarchar(max) = ',
    /** sql type: {SQLTYPE}, nullable: {NULLABLE}{SQL_DEFAULT} */
    public {PARAMNAME}: {PARAMTYPE}{DEFAULT}';
    DECLARE @frontend_prepare nvarchar(max) = '
    this.{PARAMNAME} = _tools.{PREPARE}(this.{PARAMNAME}, true);';
    DECLARE @frontend_prepareTT nvarchar(max) = '
    this.{PARAMNAME} = this.{PARAMNAME} || [];';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	-- PRINT 'building ' + @modelName + ' from ' + @schemaName + '.' + @name + ' - ' + CONVERT(VARCHAR(MAX), @objectID);

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- get parameters of procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	-- reset variables
	DECLARE @parameterID int, @modelParams nvarchar(max) = '', @modelPrepare nvarchar(max) = '', @frontendTTmodels nvarchar(max) = '', @modelConstants NVARCHAR(MAX) = '';
	DECLARE @tableTypes TABLE (tableTypeID int NOT NULL, paramName sysname NOT NULL);
    DECLARE @isfirstParameter BIT = 1, @colIsNullable BIT, @colDefault NVARCHAR(max), @colTranslatedDefault NVARCHAR(max);
	SET @parameterID = 1;
	WHILE @parameterID > 0
	BEGIN
		DECLARE @paramName sysname = null, @paramSqlType sysname, @isOutput bit, @tableTypeID int;
		DECLARE @hasUser bit = 0, @hasAppID bit = 0;
		SELECT 
			@paramName = colName,
			@paramSqlType = sqlType,
			@tableTypeID = tableTypeObjectID,
			@isOutput = CASE WHEN colDefinition LIKE '% out %' THEN 1 ELSE 0 END,
            @colIsNullable = isNullable,
            @colDefault = defaultValue,
            @colTranslatedDefault = CASE WHEN useDefaultValue = 1 THEN translatedDefaultValue END
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultset = '@' 
			AND colIndex = @parameterID - 1;

		--SET @sql = N'
		--SELECT 
		--	@paramName = SUBSTRING(p.name, 2, 999), 
		--	@type = build.GetSqlDataType(t.[name], s.[name], p.max_length, p.[precision], p.scale), 
		--	@isOutput = p.is_output,
		--	@tableTypeID = tt.type_table_object_id
		--FROM [' + @databaseName + N'].sys.parameters p 
		--	INNER JOIN [' + @databaseName + N'].sys.types t ON t.user_type_id = p.user_type_id
		--	INNER JOIN [' + @databaseName + N'].sys.schemas s ON s.[schema_id] = t.[schema_id]
		--	LEFT OUTER JOIN [' + @databaseName + N'].sys.table_types tt ON tt.user_type_id = p.user_type_id
		--WHERE p.[object_id] = ' + Convert(nvarchar, @procedureObjectID) + N' AND p.parameter_id = ' + Convert(nvarchar, @parameterID);
		--EXEC sys.sp_executesql @sql, N'@paramName sysname OUTPUT, @type sysname OUTPUT, @isOutput bit OUTPUT, @tableTypeID int OUTPUT',
		--	@paramName = @paramName OUTPUT, @type = @type OUTPUT, @isOutput = @isOutput OUTPUT, @tableTypeID = @tableTypeID OUTPUT;

		IF @paramName IS NULL
			SET @parameterID = 0;
		ELSE
		BEGIN
			--PRINT '    -> found ' + Convert(varchar, @parameterID) + '. parameter ' + @paramName + ' with type ' + @paramSqlType;

			-- system parameter?
			IF @paramName LIKE '~_%' ESCAPE '~'
			BEGIN
				IF @paramName = '_UserID'
				BEGIN
					SET @hasUser = 1;
					IF @paramSqlType <> 'varchar(10)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(10))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_LogonUserID'
				BEGIN
					IF @paramSqlType <> 'varchar(10)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(10))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_User'		-- OBSOLETE - soll durch _UserID ersetzt werden
				BEGIN
					SET @hasUser = 1;
					IF @paramSqlType <> 'nvarchar(30)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(30))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppID'
				BEGIN
					SET @hasAppID = 1;
					IF @paramSqlType <> 'varchar(15)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(15))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_EnvironmentID'
				BEGIN
					IF @paramSqlType <> 'nvarchar(82)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(82))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppEnvironmentID'
				BEGIN
					IF @paramSqlType <> 'nvarchar(82)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(82))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_ClientIP'
				BEGIN
					IF @paramSqlType <> 'varchar(48)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(48))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE
				BEGIN
					RAISERROR('Procedure %s.%s has an unknown system parameter "%s"!', 16, 1, @schemaName, @name, @paramName);
					RETURN;
				END
			END
			ELSE IF @tableTypeID IS NOT NULL
			BEGIN
				-- build model for table type
				DECLARE @frontendTTname sysname = 'Report' + REPLACE(@modelName, '_', '') + '$' + @paramName;
				IF NOT EXISTS(SELECT * FROM @tableTypes WHERE tableTypeID = @tableTypeID AND paramName = @paramName)
				BEGIN
					DECLARE @resultset sysname = '@' + @paramName, @frontendTTmodel nvarchar(max);
					EXEC build.GenerateTypescriptTableValuedParameter2 @appPackageID, @generator, @objectID, @resultset, @paramSqlType, @frontendTTname, @frontendTTmodel OUT;

					INSERT INTO @tableTypes (tableTypeID, paramName) VALUES (@tableTypeID, @paramName);
				END

				-- extend frontend
				SET @frontendTTmodels += @frontendTTmodel;

				-- table type parameter
				SET @modelParams += SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_param, 
					'{PARAMNAME}', @paramName), 
					'{PARAMTYPE}', @frontendTTname + '[]'),
					'{SQLTYPE}', @paramSqlType + ', cardinality: [0..n]'),
					'{SQL_DEFAULT}', ISNULL(', default: ' + @colDefault, '')),
					'{DEFAULT}', ISNULL(' = ' + build.GenerateDefaultValueToTypeScript(@paramSqlType, @colIsNullable, @colTranslatedDefault), '')),
					'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN 'true' ELSE 'false' END),
                    CASE WHEN @isfirstParameter = 1 THEN 2 ELSE 1 END, 9999);
					
				SET @modelPrepare += REPLACE(@frontend_prepareTT, 
					'{PARAMNAME}', @paramName);

                SET @isfirstParameter = 0;
			END
			ELSE 
			BEGIN
				-- regular parameter
				SET @modelParams += SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_param, 
					'{PARAMNAME}', @paramName), 
					'{PARAMTYPE}', build.GetParamTypeForFrontend(@paramSqlType)),
					'{SQLTYPE}', @paramSqlType),
					'{SQL_DEFAULT}', ISNULL(', default: ' + @colDefault, '')),
					'{DEFAULT}', ISNULL(' = ' + build.GenerateDefaultValueToTypeScript(@paramSqlType, @colIsNullable, @colTranslatedDefault), '')),
					'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN 'true' ELSE 'false' END),	-- NATIVELY COMPILED PROCEDURES (starting at Sql Server 2014) may have NOT NULL restriction!!!
                     CASE WHEN @isfirstParameter = 1 THEN 2 ELSE 1 END, 9999);
				SET @modelPrepare += ISNULL(REPLACE(REPLACE(@frontend_prepare, 
					'{PARAMNAME}', @paramName), 
					'{PREPARE}', build.GetParamPrepareForFrontend(@paramSqlType)), '');

				-- is this parameter a string?
				IF @paramSqlType LIKE '%char([0-9]%'
					SET @modelConstants += REPLACE(REPLACE(REPLACE(@frontend_constMaxLen,
					'{PARAMNAME}', @paramName), 
					'{SQLTYPE}', @paramSqlType),
					'{MAXLEN}', REPLACE(SUBSTRING(@paramSqlType, CHARINDEX('(', @paramSqlType) + 1, 999), ')', ''));

                SET @isfirstParameter = 0;
			END

			SET @parameterID += 1;
		END
	END;
	--PRINT ' @modelParams = ' + IsNull(@modelParams, '<null>');
	--PRINT ' @modelPrepare = ' + IsNull(@modelPrepare, '<null>');

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- generate code
	---------------------------------------------------------------------------------------------------------------------------------------------
	SET @FrontendModelDefinition = @frontendTTmodels; 

	SET @FrontendModelDefinition += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_request, 
		'{SP}', @modulIDprefix + @modelName), 
		'{SPCLASS}', 'Report' + REPLACE(@modelName, '_', '')),
		'{PARAMS}', @modelParams), 
		'{PREPARE}', @modelPrepare),
		'{CONSTANTS}', @modelConstants);

	--SET @FrontendModelDefinition += @modelsTablesCode;

	-- for debugging
	--PRINT '----------------------------- F R O N T E N D -----------------------------';
	--PRINT Convert(varchar, LEN(@FrontendModelDefinition)) + ' characters';
	--PRINT @FrontendModelDefinition;
	--PRINT '---------------------------------------------------------------------------';

	-- Tests
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptReportsProcedure2 'te-capex-fc', 'reports', 2015346244, @f OUT; PRINT @f;
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptReportsProcedure2 'te-datapool', 'reports', 1753109336, @f OUT; PRINT @f;  -- table valued parameter
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptReportsProcedure2 'te-datapool', 'reports', 1689109108, @f OUT; PRINT @f;  -- response tables
END
GO
