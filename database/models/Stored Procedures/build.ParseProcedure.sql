SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Generates models (call, request and response tables) for one procedure
-- =============================================
CREATE PROCEDURE [build].[ParseProcedure]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@objectID					int,
	@removePrefix				bit
AS
BEGIN
	SET NOCOUNT ON;

	SET @removePrefix = IsNull(@removePrefix, 0);

	DECLARE @databaseName sysname, @schemaName sysname, @name sysname, @modelName sysname, @modelNamespace sysname;
	SELECT 
		@databaseName = databaseName,
		@schemaName = schemaName,
		@name = [name],
		@modelName = modelName,
		@modelNamespace = modelNamespace
	FROM build.TEMP_Procedures
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID;
	IF @@ROWCOUNT = 0
	BEGIN
		RAISERROR('Procedure %d for app %s and generator %s not found!', 16, 1, @objectID, @appPackageID, @generator);
		RETURN;
	END

	-- clean up
	DELETE FROM build.TEMP_Columns
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID;

	DELETE FROM build.TEMP_Relations
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID;

	DELETE FROM build.TEMP_RoleChecks
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID;

	DELETE FROM build.TEMP_Flags
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- get parameters of procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @sql nvarchar(max);
	SET @sql = N'
		SELECT 
			''' + @appPackageID + N''' AS appID,
			''' + @generator + N''' AS generator,
			p.[object_id],
			0 AS resultsetIndex,
			N''@'' AS resultset,
			p.parameter_id - 1 AS colIndex,
			''P'' AS colType,
			SUBSTRING(p.[name], 2, 999) AS colName, 
			[' + DB_NAME(DB_ID()) + N'].build.GetSqlDataType(t.[name], s.[name], p.max_length, p.[precision], p.scale) AS sqlType,
			tt.type_table_object_id,
			N'' '' +
			CASE WHEN p.is_nullable = 1 THEN N''null '' ELSE N'''' END +
			CASE WHEN p.is_output = 1 THEN N''out '' ELSE N'''' END
			AS colDefinition
		FROM [' + @databaseName + N'].sys.parameters as p 
			INNER JOIN [' + @databaseName + N'].sys.types t ON t.user_type_id = p.user_type_id
			INNER JOIN [' + @databaseName + N'].sys.schemas s ON s.[schema_id] = t.[schema_id]
			LEFT OUTER JOIN [' + @databaseName + N'].sys.table_types tt ON tt.user_type_id = p.user_type_id
		WHERE p.[object_id] = ' + Convert(nvarchar, @objectID) + N';';
	INSERT INTO build.TEMP_Columns (appPackageID, generator, objectID, resultsetIndex, resultset, colIndex, colType, colName, sqlType, tableTypeObjectID, colDefinition)
	EXEC sys.sp_executesql @sql;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- get table types of parameters of procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @parameterID int = (
		SELECT MIN(colIndex) 
		FROM build.TEMP_Columns 
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND tableTypeObjectID IS NOT NULL);

	WHILE @parameterID IS NOT NULL
	BEGIN
		-- PRINT '@parameterID = ' + IsNull(Convert(nvarchar, @parameterID), '<null>');
		DECLARE @paramName sysname, @tableTypeID int;
		SELECT 
			@paramName = colName,
			@tableTypeID = tableTypeObjectID
		FROM build.TEMP_Columns 
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND colIndex = @parameterID
			AND tableTypeObjectID IS NOT NULL;
		--PRINT '@paramName = ' + IsNull(@paramName, '<null>');
		--PRINT '@tableTypeID = ' + IsNull(Convert(nvarchar, @tableTypeID), '<null>');

		-- run through table columns
		SET @sql = N'
			SELECT
				''' + @appPackageID + N''' AS appID,
				''' + @generator + N''' AS generator,
				' + CONVERT(nvarchar, @objectID) + N' AS objectID,
				0 AS resultsetIndex,
				''@' + @paramName + N''' AS resultset,
				c.column_id - 1 AS colIndex,
				''C'' AS colType,
				c.[name] AS colName, 
				[' + DB_NAME(DB_ID()) + N'].build.GetSqlDataType(t.[name], SCHEMA_NAME(t.[schema_id]), c.max_length, c.[precision], c.scale) AS sqlType,
				'' '' +
				CASE WHEN c.is_nullable = 1 THEN ''null '' ELSE '''' END +
				CASE WHEN c.is_computed = 1 THEN ''computed '' ELSE '''' END +
				CASE WHEN c.is_identity = 1 THEN ''identity '' ELSE '''' END +
				CASE WHEN ic.key_ordinal IS NOT NULL THEN ''key '' ELSE '''' END 
				AS colDefinition
			FROM [' + @databaseName + N'].sys.columns c
				INNER JOIN [' + @databaseName + N'].sys.types t ON t.user_type_id = c.user_type_id
				LEFT OUTER JOIN [' + @databaseName + N'].sys.indexes i ON i.[object_id] = c.[object_id] AND i.is_primary_key = 1
				LEFT OUTER JOIN [' + @databaseName + N'].sys.index_columns ic ON ic.[object_id] = i.[object_id] AND ic.index_id = i.index_id AND ic.column_id = c.column_id
			WHERE c.[object_id] = ' + Convert(nvarchar, @tableTypeID);
		--PRINT @sql;
		INSERT INTO build.TEMP_Columns (appPackageID, generator, objectID, resultsetIndex, resultset, colIndex, colType, colName, sqlType, colDefinition)
		EXEC sys.sp_executesql @sql;

		-- next table type
		SELECT @parameterID = MIN(colIndex) 
		FROM build.TEMP_Columns 
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND tableTypeObjectID IS NOT NULL 
			AND colIndex > @parameterID;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- read schema info (resultsets and fields)
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @procDefinition nvarchar(max);
	DECLARE @pos int, @pos2 int, @subDbName NVARCHAR(MAX), @subProcName nvarchar(max), @subSchemaName sysname, @subObjectID int = @objectID;

	-- load procedure and all called procedures (--§EXEC procname§)
	SET @sql = N'
		SELECT @procDefinition = [definition] 
		FROM [' + @databaseName + N'].sys.sql_modules
		WHERE [object_id] = ' + Convert(nvarchar, @objectID);
	EXEC sys.sp_executesql @sql, N'@procDefinition nvarchar(max) OUTPUT', @procDefinition = @procDefinition OUTPUT;

	-- remove all remarks
	SET @procDefinition = build.RemoveComments(@procDefinition);

	-- switch to model name
	Set @procDefinition = ' --§MODEL ' + @modelName + '§' + CHAR(10) + @procDefinition;

	-- find call marker
	SET @pos = CHARINDEX('--§EXEC', @procDefinition, 1)
	WHILE @pos > 0
	BEGIN
		-- end of call marker
		SET @pos2 = CHARINDEX('§', @procDefinition, @pos + 5);
		IF @pos2 > 0
		BEGIN
			SET @subProcName = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @pos + 8, @pos2 - @pos - 8), CHAR(9), ' ')));
			-- PRINT @subProcName;

			-- get database name 
			SET @subDbName = ISNULL(PARSENAME(@subProcName, 3), @databaseName);
			-- PRINT @subDbName;

			-- get schema name 
			SET @subSchemaName = IsNull(PARSENAME(@subProcName, 2), @schemaName);
			-- PRINT @subSchemaName;

			-- get procedure name 
			SET @subProcName = PARSENAME(@subProcName, 1);
			-- PRINT @subProcName;

			DECLARE @fqn sysname = QUOTENAME(@subDbName) + N'.' + QUOTENAME(@subSchemaName) + N'.' + QUOTENAME(@subProcName);
			-- PRINT '    -> found call to procedure ' + @fqn;

			-- get object id of called procedures
			SET @subObjectID = OBJECT_ID(@fqn);
			IF IsNull(@subObjectID, 0) = 0
			BEGIN
				SET @fqn = QUOTENAME(@subDbName) + N'.dbo.' + QUOTENAME(@subProcName);
				SET @subObjectID = OBJECT_ID(@fqn);
				IF IsNull(@subObjectID, 0) = 0
				BEGIN
					SET @subSchemaName += '|dbo';
					RAISERROR('Procedure %s.%s has a call to an unknown procedure %s.%s.%s!', 16, 1, @schemaName, @name, @subDbName, @subSchemaName, @subProcName);
					RETURN;
				END
				SET @subSchemaName = 'dbo';
				SET @fqn = QUOTENAME(@subDbName) + N'.' + QUOTENAME(@subSchemaName) + N'.' + QUOTENAME(@subProcName);
			END

			-- is this a synonym?
			DECLARE @baseReference nvarchar(max);
			DECLARE @subDbNameForLoadingSource sysname = @subDbName;
			DECLARE @subObjectIdForLoadingSource int = @subObjectID;

			SET @sql = N'
			SELECT @baseReference = base_object_name
			FROM ' + QUOTENAME(@subDbName) + N'.sys.synonyms
			WHERE [object_id] = ' + Convert(nvarchar, @subObjectID);
			-- PRINT @sql;
			EXEC sys.sp_executesql @sql, N'@baseReference nvarchar(max) OUTPUT', @baseReference = @baseReference OUTPUT;
			IF @baseReference is not null 
			BEGIN
				-- PRINT 'Sub-Procedure ' + @fqn + ' is a synonym for ' + @baseReference; 

				SET @subDbNameForLoadingSource = PARSENAME(@baseReference, 3);
				SET @baseReference = CASE WHEN @subDbNameForLoadingSource IS NULL THEN QUOTENAME(@subDbName) + N'.' ELSE N'' END + @baseReference;
				SET @subObjectIdForLoadingSource = OBJECT_ID(@baseReference);
				SET @subDbNameForLoadingSource = IsNull(@subDbNameForLoadingSource, @subDbName);
				IF IsNull(@subObjectID, 0) = 0
				BEGIN
					SET @subSchemaName += '|dbo';
					RAISERROR('Procedure %s.%s has a call to an unknown SYNONYM BASED procedure %s!', 16, 1, @schemaName, @name, @baseReference);
					RETURN;
				END

				SET @subDbName = @subDbNameForLoadingSource;
				SET @subObjectID = @subObjectIdForLoadingSource;
			END

			-- insert into referenced procedures if there is no entry in TEMP_Procedures
			IF NOT EXISTS(SELECT * FROM build.TEMP_Procedures WHERE appPackageID = @appPackageID AND generator = @generator AND objectID = @subObjectID)
			BEGIN
				SET @sql = N'
				WITH source AS
				(
					SELECT 
						@subDbName AS databaseName, 
						@appPackageID AS appPackageID, 
						@generator AS generator, 
						o.[object_id] AS objectID,  
						o.modify_date, 
						s.[name] AS schemaName, 
						o.[name], 
						' + QUOTENAME(Db_Name()) + N'.build.GetModelNameFromProcName(o.[name], ' + Convert(nVarchar, @removePrefix) + N') AS modelName, 
						@modelNamespace AS modelNamespace, 
						' + QUOTENAME(Db_Name()) + N'.build.GetVersionFromProcName(o.[name]) AS procVersion
					FROM ' + QUOTENAME(@subDbName) + N'.sys.objects o
						INNER JOIN ' + QUOTENAME(@subDbName) + N'.sys.schemas s ON s.schema_id = o.schema_id
					WHERE o.[object_id] = ' + Convert(nvarchar, @subObjectID) + N'
				) 
				MERGE INTO ' + QUOTENAME(Db_Name()) + N'.build.TEMP_Procedures t
				USING source s 
				ON s.appPackageID = t.appPackageID AND s.generator = t.generator AND s.objectID = t.objectID
				WHEN NOT MATCHED BY TARGET AND s.procVersion IS NOT NULL THEN
					INSERT (databaseName, appPackageID, generator, objectID, modify_date, schemaName, [name], modelName, modelNamespace, procVersion, isExternal)
					VALUES (s.databaseName, s.appPackageID, s.generator, s.objectID, s.modify_date, s.schemaName, s.[name], s.modelName, s.modelNamespace, s.procVersion, 1)
				;';
				-- PRINT @sql;
				EXEC sys.sp_executesql @sql, N'@subDbName sysname, @appPackageID varchar(15), @generator varchar(15), @modelNamespace sysname', 
					@subDbName = @subDbName, @appPackageID = @appPackageID, @generator = @generator, @modelNamespace = @modelNamespace;
			END

			-- Insert into procedure definition
			DECLARE @subProcDefinition nvarchar(max);

			SET @sql = N'
			SELECT @procDefinition = [definition] 
			FROM [' + @subDbNameForLoadingSource + N'].sys.sql_modules
			WHERE [object_id] = ' + Convert(nvarchar, @subObjectIDForLoadingSource);
			-- PRINT @sql;
			EXEC sys.sp_executesql @sql, N'@procDefinition nvarchar(max) OUTPUT', @procDefinition = @subProcDefinition OUTPUT;
			IF @subProcDefinition IS NULL
			BEGIN
				RAISERROR('Procedure %s.%s has a call to a procedure %s.%s.%s but model generator has no permission to read source code!', 
					16, 1, @schemaName, @name, @subDbName, @subSchemaName, @subProcName);
				RETURN;
			END

			-- remove all remarks
			SET @subProcDefinition = build.RemoveComments(@subProcDefinition);

			-- get new model name
			SELECT @modelName = modelName
			FROM build.TEMP_Procedures
			WHERE appPackageID = @appPackageID
				AND generator = @generator
				AND objectID = @subObjectID;
			-- fall back to original procedure name
			IF @@ROWCOUNT = 0
				SET @modelName = @subProcName;

			-- switch to new model name
			Set @subProcDefinition = ' --§MODEL ' + @modelName + '§' + CHAR(10) + @subProcDefinition + CHAR(10) + ' --§MODEL <-§' + CHAR(10);

			-- insert into original code
			SET @procDefinition = 
				SUBSTRING(@procDefinition, 1, @pos2) + CHAR(13) + CHAR(10) + 
				@subProcDefinition + 
				'	--§END ' + SUBSTRING(@procDefinition, @pos + 3, @pos2 - @pos) + CHAR(13) + CHAR(10) + 
				SUBSTRING(@procDefinition, @pos2 + 1, LEN(@procDefinition));

			SET @pos = CHARINDEX('--§EXEC', @procDefinition, @pos2);
		END
		ELSE 
			SET @pos = 0;
	END

	--PRINT SUBSTRING(@procdefinition, 1, 3000);
	--PRINT SUBSTRING(@procdefinition, 3001, 3000);
	--PRINT SUBSTRING(@procdefinition, 6001, 3000);
	--PRINT SUBSTRING(@procdefinition, 9001, 3000);

	-- replace all MODEL <- throw real names
	-- sadly this cannot be done in EXEC step, because recursivly calls makes remembering difficult -> first flatten and now replace
	DECLARE @modelNameStack TABLE(id int, modelName sysname);
	SET @pos = CHARINDEX('--§MODEL', @procDefinition, 1)
	WHILE @pos > 0
	BEGIN
		SET @pos2 = CHARINDEX('§', @procDefinition, @pos + 8);
		IF @pos2 > 0
		BEGIN
			SET @modelName = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @pos + 8, @pos2 - @pos - 8), CHAR(9), ' ')));

			IF @modelName <> '<-'
			BEGIN
				-- push on stack
				UPDATE @modelNameStack
				SET id = id + 1;
				INSERT INTO @modelNameStack VALUES (1, @modelName);
				--PRINT 'PUSH MODEL NAME [' + @modelName + '] at pos ' + Convert(varchar, @pos);
			END
			ELSE
			BEGIN
				-- pop from stack
				DELETE FROM @modelNameStack
				WHERE id = 1;
				UPDATE @modelNameStack 
				SET id = id - 1;
				SELECT @modelName = modelName
				FROM @modelNameStack 
				WHERE id = 1;
				--PRINT 'POP MODEL NAME [' + @modelName + '] at pos ' + Convert(varchar, @pos);

				-- insert instead of <- into @procDefinition
				SET @procDefinition = SUBSTRING(@procDefinition, 1, @pos + 8) 
					+ @modelName
					+ SUBSTRING(@procDefinition, @pos + 11, LEN(@procDefinition));
			END

			SET @pos = CHARINDEX('--§MODEL', @procDefinition, @pos + 8);
		END
		ELSE 
			SET @pos = 0;
	END

	--PRINT SUBSTRING(@procdefinition, 1, 3000);
	--PRINT SUBSTRING(@procdefinition, 3001, 3000);
	--PRINT SUBSTRING(@procdefinition, 6001, 3000);
	--PRINT SUBSTRING(@procdefinition, 9001, 3000);

	-- search for --§TABLE
	DECLARE @reverse nvarchar(max) = REVERSE(@procDefinition);
	DECLARE @tableIndex int = 0, @pos3 int, @tableName sysname, @tableDef nvarchar(max);
	SET @pos = CHARINDEX('--§TABLE', @procDefinition, 1);
	WHILE @pos > 0
	BEGIN
		-- end of table definition
		SET @pos2 = CHARINDEX('§', @procDefinition, @pos + 5);
		IF @pos2 > 0
		BEGIN
			-- table found
			SET @tableDef = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @pos + 9, @pos2 - @pos - 9), CHAR(9), ' ')));
			SET @pos3 = CHARINDEX(' ', @tableDef);
			SET @tableName = CASE WHEN @pos3 = 0 THEN @tableDef ELSE RTRIM(SUBSTRING(@tableDef, 1, @pos3)) END;
			SET @tableDef = ' ' + SUBSTRING(@tableDef, Len(@tableName) + 2, 9999) + ' ';

			SET @tableIndex += 1;

			-- search for latest model name
			DECLARE @posMN int = CHARINDEX('LEDOM§--', @reverse, LEN(@reverse) - @pos);
			IF @posMN > 0
			BEGIN
				SET @posMN = LEN(@reverse) - @posMN - 5;
				DECLARE @posMN2 int = CHARINDEX('§', @procDefinition, @posMN + 8);
				SET @modelName = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @posMN + 8, @posMN2 - @posMN - 8), CHAR(9), ' ')));
				--PRINT 'FOUND MODEL NAME [' + @modelName + '] FOR TABLE [' + @tableName + ']';
			END

			-- remember table
			INSERT INTO build.TEMP_Columns (appPackageID, generator, objectID, resultsetIndex, resultset, colIndex, colType, colName, sqlType, colDefinition)
			VALUES (@appPackageID, @generator, @objectID, @tableIndex, @tableName, -1, 'T', @modelName, '', @tableDef);
			--PRINT '    -> found table "' + @tableName + '" with options [' + RTRIM(LTRIM(@tableDef)) + ']';

			-- search for columns
			DECLARE @firstPos int = @pos2;
			DECLARE @lastPos int = CHARINDEX('--§TABLE', @procDefinition, @pos2);
			IF @lastPos = 0 
				SET @lastPos = LEN(@procDefinition);
			DECLARE @posCol int = CHARINDEX('--§COL', @procDefinition, @pos2);
			DECLARE @posFormat int = 0, @posFirst int;
			DECLARE @formatDef nvarchar(max), @formatName sysname, @formatArgs nvarchar(max);
			DECLARE @colIndex int = 0;
			WHILE (@posCol < @lastPos AND @posCol > 0) OR (@posFormat < @lastPos AND @posFormat > 0)
			BEGIN
				SET @posFirst = CASE 
					WHEN @posCol = 0 THEN @posFormat
					WHEN @posFormat = 0 THEN @posCol
					WHEN @posCol < @posFormat THEN @posCol
					ELSE @posFormat 
				END;
				-- end of col/format definition
				SET @pos2 = CHARINDEX('§', @procDefinition, @posFirst + 5);
				IF @pos2 > 0 AND @posFirst = @posCol
				BEGIN
					-- column found
					DECLARE @colDef nvarchar(max), @colName sysname, @colType sysname, @colSqlType sysname, @IsBaseTypeNullable bit;
					DECLARE @colReader sysname, @colMaxLength int;
					SET @colIndex += 1;
					SET @colDef = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @posCol + 7, @pos2 - @posCol - 7), CHAR(9), ' ')));
					SET @colName = SUBSTRING(@colDef, 1, CHARINDEX(' ', @colDef + ' ') - 1);
					SET @colDef = LTRIM(SUBSTRING(@colDef, LEN(@colName) + 1, 999));
					SET @colSqlType = SUBSTRING(@colDef, 1, CHARINDEX(' ', @colDef + ' ') - 1);
					SET @colDef = ' ' + LTRIM(SUBSTRING(@colDef, LEN(@colSqlType) + 1, 999)) + ' ';
					SELECT @colType = CASE WHEN @colDef LIKE '% null %' THEN NullableType ELSE BaseType END,
						@IsBaseTypeNullable = IsBaseTypeNullable,
						@colReader = ReaderMethod,
						@colMaxLength = Length
					FROM build.ConvertToNetType(@colSqlType);
					--PRINT '        -> found ' + Convert(varchar, @colIndex) + '. column "' + @colName + '" with type "' + @colType + '" and options [' + RTRIM(LTRIM(@colDef)) + ']';

					-- remember column
					INSERT INTO build.TEMP_Columns (appPackageID, generator, objectID, resultsetIndex, resultset, colIndex, colType, colName, sqlType, colDefinition)
					VALUES (@appPackageID, @generator, @objectID, @tableIndex, @tableName, @colIndex - 1, 'C', @colName, @colSqlType, @colDef);
				END
				IF @pos2 > 0 AND @posFirst = @posFormat
				BEGIN
					-- format found
					SET @formatDef = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @posFormat + 10, @pos2 - @posFormat - 10), CHAR(9), ' ')));
					SET @formatName = SUBSTRING(@formatDef, 1, CHARINDEX(' ', @formatDef + ' ') - 1);
					SET @formatArgs = LTRIM(SUBSTRING(@formatDef, LEN(@formatName) + 1, 999));
					--PRINT '          -> found format-getter "' + @formatName + '" and args [' + RTRIM(LTRIM(@formatArgs)) + ']';

					-- remember column
					INSERT INTO build.TEMP_Columns (appPackageID, generator, objectID, resultsetIndex, resultset, colIndex, colType, colName, sqlType, colDefinition)
					VALUES (@appPackageID, @generator, @objectID, @tableIndex, @tableName, @colIndex - 1, 'F', @formatName, @formatArgs, @formatDef);
				END

				-- search for next column
				SET @posCol =
					CASE 
						WHEN @pos2 = 0 THEN 0 
						ELSE CHARINDEX('--§COL', @procDefinition, @pos2)
					END;
				SET @posFormat =
					CASE 
						WHEN @pos2 = 0 THEN 0 
						ELSE CHARINDEX('--§FORMAT ', @procDefinition, @pos2)
					END;
			END
			
			-- search for relations
			SET @pos2 = @firstPos;
			SET @posCol = CHARINDEX('--§RELATION', @procDefinition, @pos2);
			WHILE @posCol < @lastPos AND @posCol > 0
			BEGIN
				-- end of col definition
				SET @pos2 = CHARINDEX('§', @procDefinition, @posCol + 5);
				IF @pos2 > 0
				BEGIN
					-- relation found
					SET @colIndex += 1;
					SET @colDef = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @posCol + 12, @pos2 - @posCol - 12), CHAR(9), ' ')));
					--PRINT '		-> found relation "' + @colDef + '"';

					DECLARE @relName nvarchar(max) = SUBSTRING(@colDef, 1, CHARINDEX(' ', @colDef + ' ') - 1);
					SET @colDef = ' ' + LTRIM(SUBSTRING(@colDef, LEN(@relName) + 1, 999)) + ' ';
					DECLARE @posArrow int = CHARINDEX('->', @relName);
					DECLARE @posPoint int = CHARINDEX('.', @relName, @posArrow);
					IF @posArrow = 0 OR @posPoint = 0
					BEGIN
						RAISERROR('Relation definition "%s" is not valid! expected: --§RELATION {childTableFieldName}->{parentTable}.{parentTableFieldName} {options} §', 16, 1, @relName);
						RETURN;
					END
					SET @colName = SUBSTRING(@relName, 1, @posArrow - 1);
					DECLARE @parentTable sysname = SUBSTRING(@relName, @posArrow + 2, @posPoint - @posArrow - 2);
					DECLARE @parentCol sysname = SUBSTRING(@relName, @posPoint + 1, 999);

					INSERT INTO build.TEMP_Relations (appPackageID, generator, objectID, relationName, parentTable, parentColumns, childTable, childColumns)
					VALUES (@appPackageID, @generator, @objectID, @tableName + '.' + @relName, @parentTable, @parentCol, @tableName, @colName);
				END

				-- search for next relation
				SET @posCol =
					CASE 
						WHEN @pos2 = 0 THEN 0 
						ELSE CHARINDEX('--§RELATION ', @procDefinition, @pos2)
					END;
			END
		END
			
		SET @pos = 
			CASE 
				WHEN @pos2 = 0 THEN 0 
				ELSE CHARINDEX('--§TABLE', @procDefinition, @pos2)
			END;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- search for --§ROLECHECK
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @RoleName nvarchar(max), @ModuleName nvarchar(max);
	SET @pos = CHARINDEX('--§ROLECHECK', @procDefinition);
	WHILE @pos > 0 
	BEGIN
		SET @pos2 = CHARINDEX('§', @procDefinition, @pos + 5);
		IF @pos2 > 0
		BEGIN
			SET @RoleName = RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @pos + 12, @pos2 - @pos - 12), CHAR(9), ' ')));
			SET @ModuleName = '';
			IF CHARINDEX('/', @RoleName) > 0
			BEGIN
				SET @ModuleName = SUBSTRING(@RoleName, 1, CHARINDEX('/', @RoleName));
				SET @RoleName = LTRIM(SUBSTRING(@RoleName, LEN(@ModuleName) + 1, 999));
				SET @ModuleName = RTRIM(@ModuleName);
			END
			INSERT INTO build.TEMP_RoleChecks (appPackageID, generator, objectID, moduleName, roleName)
			VALUES (@appPackageID, @generator, @objectID, @ModuleName, @RoleName);
			PRINT 'SECURITY CHECK with module "' + @ModuleName + '" / role "' + @RoleName + '"';
		END

		SET @pos =
			CASE 
				WHEN @pos2 = 0 THEN 0 
				ELSE CHARINDEX('--§ROLECHECK', @procDefinition, @pos2)
			END;
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------
	-- search for flags
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @Flags nvarchar(max)
	SET @pos = CHARINDEX('--§FLAGS', @procDefinition);
	WHILE @pos > 0
	BEGIN
		SET @pos2 = CHARINDEX('§', @procDefinition, @pos + 5);
		IF @pos2 > 0
		BEGIN
			SET @Flags = UPPER(RTRIM(LTRIM(REPLACE(SUBSTRING(@procDefinition, @pos + 8, @pos2 - @pos - 8), ' ', ''))));

			INSERT INTO build.TEMP_Flags (appPackageID, generator, objectID, flag)
			VALUES (@appPackageID, @generator, @objectID, @Flags);
			PRINT '    -> found flags "' + @Flags;
		END

		SET @pos =
			CASE 
				WHEN @pos2 = 0 THEN 0 
				ELSE CHARINDEX('--§FLAGS', @procDefinition, @pos2)
			END;
	END

	-- Tests
	-- EXEC build.ParseProcedure 'te-ehs-ra', 'typescript', 1935345959, 1;		-- param with table type sample
	-- EXEC build.ParseProcedure 'ils', 'csharp', 1042154808, 1;				-- param with table type sample
	-- EXEC build.ParseProcedure 'te-capex-fc', 'typescript', 1908917872, 1;	-- exec sample
	-- EXEC build.ParseProcedure 'te-capex-fc', 'typescript', 1999346187, 1;	-- multiple resultsets sample
	-- EXEC build.ParseProcedure 'te-ehs-ra', 'reports', 176719682, 1;			-- relations sample
	-- EXEC build.ParseProcedure 'po-shop', 'typescript', 1996586201, 1;		-- format sample
	-- EXEC build.ParseProcedure 'fire-connect', 'csharp', 866818150, 1;		-- model name sample
	-- EXEC build.ParseProcedure 'barcode', 'csharp', 478624748, 1;				-- referenced procedures sample
	-- SELECT * FROM build.TEMP_Columns WHERE objectID = 1999346187 ORDER BY resultsetindex, resultset, colIndex;
	-- SELECT * FROM build.TEMP_Relations WHERE objectID = 176719682 ORDER BY relationName;
END
GO
