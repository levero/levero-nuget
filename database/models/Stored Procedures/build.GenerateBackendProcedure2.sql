SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2020-11-18
-- Description:	Generates a backend model description for one procedure
-- =============================================
CREATE PROCEDURE [build].[GenerateBackendProcedure2]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@objectID					int,
	@backendModelDefinition		nvarchar(max) OUT
AS
BEGIN
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for backend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @backend_proc nvarchar(max) = '  "{SP}": {
    "Action": "{SP}",
    "Sql": "EXECUTE [{SCHEMA}].[{PROCNAME}] {PARAMS_CALL}",
    "Parameters": [{PARAMS_DEF}
    ],
    "Tables": [{TABLES_DEF}
    ],
    "Roles": [{ROLES}
    ],
	"Flags": "{FLAGS}"
  }
';
	DECLARE @backend_param nvarchar(max) = '
      {"ParamName": "@{PARAMNAME}", "DataName": "{PARAMNAME}", "TypeName": "{PARAMTYPE}", "TableType": {TABLETYPE}}';
	DECLARE @backend_table nvarchar(max) = '
      {"TableName": "{TABLENAME}", "Fields": [{FIELDS}
        ], "Relations": [{RELATIONS}
        ]
	  }';
	DECLARE @backend_field nvarchar(max) = '
        {"FieldName": "{FIELDNAME}", "Index": {FIELDINDEX}, "TypeName": "{FIELDTYPE}", "IsNullable": {NULLABLE}}';
	DECLARE @backend_relation nvarchar(max) = '
        {"PrimaryKeyTableName": "{PARENT}", "PrimaryKeyFieldNames": [{PARENTCOLS}], "ForeignKeyFieldNames": [{CHILDCOLS}]}';
	DECLARE @backend_role nvarchar(max) = '
      {"Module": "{MODULE}", "RoleName": "{ROLENAME}"}';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @schemaName sysname, @name sysname, @modelNamespace sysname, @modelNameProc sysname, @modify_date datetime, @modulIDprefix sysname;
	SELECT
		@schemaName = schemaName,
		@name = [name],
		@modelNamespace = IsNull(modelNamespace, 'LeveroModels.DefaultNamespace'),
		@modelNameProc = modelName,
		@modify_date = modify_date,
		@modulIDprefix = IsNull(NullIf(modulID, '') + '|', '')
	FROM build.TEMP_Procedures2
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	--PRINT 'building ' + @modelNameProc + ' from ' + @schemaName + '.' + @name;

	-- reset variables
	DECLARE @hasUser bit = 0, @hasAppID bit = 0, @parameterID int;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- get parameters of procedure
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @sql nvarchar(max), @CodeParamCalls nvarchar(max) = null, @CodeParamDefs nvarchar(max) = null;
	DECLARE @paramName sysname, @paramSqlType sysname, @isOutput bit, @tableTypeID int, @isNullable bit;
	DECLARE @modelParams nvarchar(max), @modelPrepare nvarchar(max);
	SET @parameterID = 1;
	WHILE @parameterID > 0
	BEGIN
		SET @paramName = NULL;
		SET @modelParams = '';
		SET @modelPrepare = '';

		SELECT 
			@paramName = colName,
			@paramSqlType = sqlType,
			@tableTypeID = tableTypeObjectID,
			@isOutput = CASE WHEN colDefinition LIKE '% out %' THEN 1 ELSE 0 END,
			@isNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultset = '@' 
			AND colIndex = @parameterID - 1;

		IF @paramName IS NULL
			SET @parameterID = -1;
		ELSE
		BEGIN
			DECLARE @backendTTmodel nvarchar(max) = 'null', @paramType nVarchar(MAX);
			--PRINT '    -> found ' + Convert(varchar, @parameterID) + '. parameter ' + @paramName + ' with type ' + @paramSqlType;

			SELECT 
				@paramType = IsNull(TypeNameSpace + '.', '') + BaseType
			FROM build.ConvertToNetType(@paramSqlType);

			-- system parameter?
			IF @paramName LIKE '~_%' ESCAPE '~'
			BEGIN
				IF @paramName = '_UserID'
				BEGIN
					SET @hasUser = 1;
					IF @paramSqlType <> 'varchar(10)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(10))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_LogonUserID'
				BEGIN
					IF @paramSqlType <> 'varchar(10)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(10))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_User'		-- OBSOLETE - soll durch _UserID ersetzt werden
				BEGIN
					SET @hasUser = 1;
					IF @paramSqlType <> 'nvarchar(30)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(30))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppID'
				BEGIN
					SET @hasAppID = 1;
					IF @paramSqlType <> 'varchar(15)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(15))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppPackageID'
				BEGIN
					IF @paramSqlType <> 'varchar(15)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(15))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppPackageVersion'
				BEGIN
					IF @paramSqlType <> 'varchar(47)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(47))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_TenantID'
				BEGIN
					IF @paramSqlType <> 'varchar(15)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(15))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_EnvironmentID'
				BEGIN
					IF @paramSqlType <> 'nvarchar(82)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(82))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_AppEnvironmentID'
				BEGIN
					IF @paramSqlType <> 'nvarchar(82)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be nvarchar(82))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE IF @paramName = '_ClientIP'
				BEGIN
					IF @paramSqlType <> 'varchar(48)'
					BEGIN
						RAISERROR('Procedure %s.%s has a system parameter "%s" with wrong datatype "%s"! (must be varchar(48))', 16, 1, @schemaName, @name, @paramName, @paramSqlType);
						RETURN;
					END
				END
				ELSE
				BEGIN
					RAISERROR('Procedure %s.%s has an unknown system parameter "%s"!', 16, 1, @schemaName, @name, @paramName);
					RETURN;
				END
			END
			ELSE IF @tableTypeID IS NOT NULL
			BEGIN
				DECLARE @resultset sysname = '@' + @paramName;
				EXEC build.GenerateBackendTableValuedParameter2 @appPackageID, @generator, @objectID, @resultset, @backendTTmodel OUT;
			END

			-- backend params 
			SET @CodeParamCalls = ISNULL(@CodeParamCalls + ', ', '') + '@' + @paramName;
			SET @CodeParamDefs = IsNull(@CodeParamDefs + ',', '') + Replace(REPLACE(REPLACE(@backend_param, 
				'{PARAMNAME}', @paramName),
				'{PARAMTYPE}', CASE WHEN @tableTypeID IS NOT NULL THEN @paramSqlType ELSE @paramType END),
				'{TABLETYPE}', @backendTTmodel);

			SET @parameterID += 1;
		END
	END;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- read schema info (resultsets and fields)
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @tableIndex int = 1, @tableCount int = 0, @tableName sysname;
	DECLARE @CodeTables nvarchar(max) = null;
	WHILE @tableIndex > 0
	BEGIN
		-- search for table
		SET @tableName = NULL;
		SELECT 
			@tableName = resultset
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID 
			AND generator = @generator 
			AND objectID = @objectID 
			AND resultsetIndex = @tableIndex
			AND colIndex = -1;
			
		IF @tableName IS NULL
			SET @tableIndex = -1;
		ELSE
		BEGIN
			-- table found
			SET @tableCount += 1;
			--PRINT '    -> found table "' + @tableName + '"';

			-- search for columns
			DECLARE @colIndex int = 1, @colName sysname, @colDef nvarchar(max), @colSqlType sysname, @colType sysname;
			DECLARE @CodeColumns nvarchar(max) = null;

			WHILE @colIndex > 0
			BEGIN
				SET @colName = NULL;
				SELECT 
					@colName = colName,
					@colSqlType = sqlType,
					@colDef = colDefinition
				FROM build.TEMP_Columns2
				WHERE appPackageID = @appPackageID 
					AND generator = @generator 
					AND objectID = @objectID 
					AND resultsetIndex = @tableIndex
					AND colIndex = @colIndex - 1
					AND colType = 'C';

				IF @colName IS NULL
					SET @colIndex = -1;
				ELSE
				BEGIN
					-- column found
					SELECT 
						@colType = IsNull(TypeNameSpace + '.', '') + BaseType
					FROM build.ConvertToNetType(@colSqlType);
					--PRINT '        -> found ' + Convert(varchar, @colIndex) + '. column "' + @colName + '" with type "' + @colType + '" and options [' + RTRIM(LTRIM(@colDef)) + ']';

					-- build field description
					SET @CodeColumns = IsNull(@CodeColumns + ',', '') + REPLACE(REPLACE(REPLACE(REPLACE(@backend_field,
						'{FIELDNAME}', @colName),
						'{FIELDINDEX}', Convert(varchar, @colIndex - 1)),
						'{FIELDTYPE}', @colType),
						'{NULLABLE}',  CASE WHEN @colDef LIKE '% null %' THEN 'true' ELSE 'false' END);

				END
				-- search for next column
				SET @colIndex += 1;
			END

			-- search for relations
			DECLARE @relIndex int = 1, @relParent sysname, @relParentCols nVarchar(max), @relChildCols nVarchar(MAX);
			DECLARE @CodeRelations nVarchar(max) = NULL, @relName sysname;
			UPDATE build.TEMP_Relations2
			SET done = 0
			WHERE appPackageID = @appPackageID 
				AND generator = @generator 
				AND objectID = @objectID;

			WHILE @relIndex > 0
			BEGIN
				SET @relParent = NULL;
				SELECT 
					@relParent = parentTable,
					@relParentCols = parentColumns,
					@relChildCols = childColumns,
					@relName = relationName
				FROM build.TEMP_Relations2
				WHERE appPackageID = @appPackageID 
					AND generator = @generator 
					AND objectID = @objectID 
					AND childTable = @tableName
					AND done = 0;

				IF @relParent IS NULL
					SET @relIndex = -1;
				ELSE
				BEGIN
					-- relation found
					--PRINT '        -> found ' + Convert(varchar, @relIndex) + '. relation ' + @relChildCols + ' -> ' + @relParent + '.' + @relParentCols;

					UPDATE build.TEMP_Relations2
					SET done = 1
					WHERE appPackageID = @appPackageID 
						AND generator = @generator 
						AND objectID = @objectID
						AND relationName = @relName;

					-- build relation description
					SET @CodeRelations = IsNull(@CodeRelations + ',', '') + REPLACE(REPLACE(REPLACE(@backend_relation,
						'{PARENT}', @relParent),
						'{PARENTCOLS}', '"' + Replace(Replace(Replace(@relParentCols, ', ', '", "'), ',', '", "'), ' ', '", "') + '"'),
						'{CHILDCOLS}', '"' + Replace(Replace(Replace(@relChildCols, ', ', '", "'), ',', '", "'), ' ', '", "') + '"');

				END
				-- search for next relation
				SET @relIndex += 1;
			END

			-- add to tables
			SET @CodeTables = IsNull(@CodeTables + ',', '') + Replace(REPLACE(REPLACE(@backend_table,
				'{TABLENAME}', @tableName),
				'{FIELDS}', IsNull(@CodeColumns, '')),
				'{RELATIONS}', IsNull(@CodeRelations, ''));
		END

		-- search for next table		
		SET @tableIndex += 1;
	END


	---------------------------------------------------------------------------------------------------------------------------------------------
	-- search for --§ROLECHECK
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @CodeRoles nvarchar(max) = '';

	SELECT 
		@CodeRoles = IsNull(@CodeRoles + ',', '') + Replace(Replace(@backend_role, 
			'{MODULE}', moduleName), 
			'{ROLENAME}', roleName)
	FROM build.TEMP_RoleChecks2
	WHERE appPackageID = @appPackageID 
		AND generator = @generator 
		AND objectID = @objectID;


	---------------------------------------------------------------------------------------------------------------------------------------------
	-- search for flags
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @CodeFlags nvarchar(max) = null;

	SELECT @CodeFlags = IsNull(@CodeFlags + ',', '') + Flag
	FROM build.TEMP_Flags2
	WHERE appPackageID = @appPackageID 
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- generate code
	---------------------------------------------------------------------------------------------------------------------------------------------
	--PRINT '@modelNameProc: ' + IsNull(@modelNameProc, '<null>');
	--PRINT '@schemaName: ' + IsNull(@schemaName, '<null>');
	--PRINT '@name: ' + IsNull(@name, '<null>');
	--PRINT '@CodeParamCalls: ' + IsNull(@CodeParamCalls, '<null>');
	--PRINT '@CodeParamDefs: ' + IsNull(@CodeParamDefs, '<null>');
	--PRINT '@CodeTables: ' + IsNull(@CodeTables, '<null>');
	--PRINT '@CodeRoles: ' + IsNull(@CodeRoles, '<null>');
	--PRINT '@CodeFlags: ' + IsNull(@CodeFlags, '<null>');

	SET @backendModelDefinition = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@backend_proc,
		'{SP}', @modulIDprefix + @modelNameProc),
		'{SCHEMA}', @schemaName),
		'{PROCNAME}', @name),
		'{PARAMS_CALL}', IsNull(@CodeParamCalls, '')),
		'{PARAMS_DEF}', IsNull(@CodeParamDefs, '')),
		'{TABLES_DEF}', IsNull(@CodeTables, '')),
		'{ROLES}', IsNull(@CodeRoles, '')),
		'{FLAGS}', IsNull(@CodeFlags, ''));


	-- for debugging
	--PRINT '------------------------------ B A C K E N D ------------------------------';
	--PRINT Convert(varchar, LEN(@backendModelDefinition)) + ' characters';
	--PRINT @backendModelDefinition;
	--PRINT '---------------------------------------------------------------------------';

	-- Tests
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendProcedure2 'te-ehs-ra', 'actions', 1919345902, @b OUT; PRINT @b;
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendProcedure2 'te-ehs-ra', 'actions', 1798297466, @b OUT; PRINT @b;
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendProcedure2 'te-ehs-ra', 'actions', 1935345959, @b OUT; PRINT @b;
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendProcedure2 'fire-connect', 'actions', 866818150, @b OUT; PRINT @b;
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendProcedure2 'levero-trans', 'reports', 84911374, @b OUT; PRINT @b;
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendProcedure2 'te-datapool', 'actions', 253296012, @b OUT; PRINT @b;
END
GO
