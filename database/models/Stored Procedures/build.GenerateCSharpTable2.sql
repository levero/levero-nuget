SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Generates a model for a result table
-- =============================================
CREATE PROCEDURE [build].[GenerateCSharpTable2]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@csharp_mode				varchar(max),
	@objectID					int,
	@tableIndex					smallint,
	@modelName					sysname,
	@OutputType					varchar(10) = 'SELECT'
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @modeNULLABLE bit = 0;
	DECLARE @modeJSON bit = 0;

	-- generator mode nullable?
	IF (@csharp_mode + ',') LIKE '%[:,]nullable,%'
		SET @modeNULLABLE = 1;

	-- generator mode json?
	IF (@csharp_mode + ',') LIKE '%[:,]json,%'
		SET @modeJSON = 1;

	DECLARE @procModelNamespace nvarchar(max), @procModelName nvarchar(max), @tableModelName nvarchar(max), @modify_date datetime; 
	SELECT
		@procModelNamespace = IsNull(modelNamespace, 'LeveroModels.DefaultNamespace'),
		@procModelName = IsNull(modelName, '<unknown procedure model name>'),
		@modify_date = modify_date
	FROM build.TEMP_Procedures2
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	-- get table model name
	SELECT
		@tableModelName = colName
	FROM build.TEMP_Columns2
	WHERE appPackageID = @appPackageID
		AND generator = @generator
		AND objectID = @objectID
		AND resultsetIndex = @tableIndex
		AND colIndex = -1;

	-- if table model name is not found in this model generator space (= sub procedure is not part of this model generator) -> take parent model name
	IF @tableModelName IS NOT NULL 
		AND NOT EXISTS(SELECT * FROM build.TEMP_Procedures2 WHERE appPackageID = @appPackageID AND generator = @generator AND modelName = @tableModelName)
		SET @tableModelName = @procModelName;

	IF @procModelName <> IsNull(@tableModelName, '<unknown table model name>')
		RETURN;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for table types
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @ttTable nvarchar(max) = 'using System;
using Levero.Persistence;
using Levero.Persistence.DAL;{JSONUSING}

#pragma warning disable IDE0079 // Remove unnecessary suppression
#pragma warning disable IDE0016 // Use ''throw'' expression
#pragma warning disable IDE0022 // Use expression body for methods
#pragma warning disable IDE0025 // Use expression body for properties
#pragma warning disable IDE0049 // Simplify Names
#pragma warning disable IDE0051 // Remove unused private members

namespace {MODELNAMESPACE}
{
    public partial class {MODELNAME}__{TABLENAME} : BaseItem<{MODELNAME}__{TABLENAME}>, IAfterInit{INTERFACES}
    {
		private const string NOT_INITIALIZED = "<not initialized>";

{CONTENT}        protected internal virtual void AfterInit()
        {
			// is called after setting all properties with values from database query
            // can be overriden by a DTO class to setup additional properties
        }

		void IAfterInit.AfterInit() => this.AfterInit();

        public override string ToString()
        {
            return $"{MODELNAME}__{TABLENAME}:{TOSTRING}";
        }
    }
}';
	DECLARE @ttJsonUsing nVarchar(max) = '
using Newtonsoft.Json;';

	DECLARE @ttTableProperty nvarchar(max) = '        internal {COLTYPE} _{COLNAME}{DEFAULT};	// needed for loading from sql server

        /// <summary>
		/// Field {COLNAME} with sql type {SQLTYPE} and options [{OPTIONS}]
		/// </summary>{JSONPROPERTY}
        public {COLTYPE} {COLNAME}
        {
            get => this._{COLNAME};
            set
            {
                if (this._{COLNAME} == value)
                    return;
{NULLCHECK}{LENCHECK}                this._{COLNAME} = value;
                this.RaisePropertyChanged("{COLNAME}");
            }
        }

';
	DECLARE @ttTablePropertyReadonly nvarchar(max) = '        internal {COLTYPE} _{COLNAME}{DEFAULT};	// needed for loading from sql server

        /// <summary>
		/// Field {COLNAME} with sql type {SQLTYPE} and options [{OPTIONS}]
		/// </summary>{JSONPROPERTY}
        public {COLTYPE} {COLNAME}
        {
            get => this._{COLNAME};
        }

';

	DECLARE @ttTablePropertyReadonlyJson nvarchar(max) = '        internal {COLTYPE} _{COLNAME}{DEFAULT};	// needed for loading from sql server

        /// <summary>
		/// Field {COLNAME} with sql type {SQLTYPE} and options [{OPTIONS}]
		/// </summary>{JSONPROPERTY}
        public {COLTYPE} {COLNAME}
        {
            get => this._{COLNAME};
            private set => this._{COLNAME} = value;	// JsonDeserializer needs private setter and attribute JsonProperty
        }

';

	DECLARE @ttJsonProperty nvarchar(max) = '
		[JsonProperty]';

	DECLARE @ttTableNullCheck nvarchar(max) = '                if (value == null)
                    throw new Exception("Property {MODELNAME}__{TABLENAME}.{COLNAME} must not be null!");
';
	DECLARE @ttTableLenCheck nvarchar(max) = '                if (value.Length > {MAXLEN})
                    throw new Exception("Property {MODELNAME}__{TABLENAME}.{COLNAME} may only be {MAXLEN} character(s) long!");
';
	DECLARE @ttTableLenCheckNullable nvarchar(max) = '                if (value != null && value.Length > {MAXLEN})
                    throw new Exception("Property {MODELNAME}__{TABLENAME}.{COLNAME} may only be {MAXLEN} character(s) long!");
';
	DECLARE @ttTableEqualityIntf nvarchar(max) = ', IEquatable<{MODELNAME}__{TABLENAME}>';
	DECLARE @ttTableEqualityImpl nvarchar(max) = '        public bool Equals({MODELNAME}__{TABLENAME} other)
        {
{COMPARE}            return true;
        }

        public override bool Equals(object{MODENULLABLE} obj)
        {
            if (obj is {MODELNAME}__{TABLENAME} other)
                return base.Equals(other);
            return false;
        }

        public override int GetHashCode()
        {
            return {HASH};
        }

';
	DECLARE @ttTableEqualityCompareNullable nvarchar(max) = '            if (this.{COLNAME} == null ^ other.{COLNAME} == null) return false;
            if (this.{COLNAME} != null && !this.{COLNAME}.Equals(other.{COLNAME})) return false;
';
	DECLARE @ttTableEqualityCompare nvarchar(max) = '            if (!this.{COLNAME}.Equals(other.{COLNAME})) return false;
';
	DECLARE @ttTableEqualityHash nvarchar(max) = 'this.{COLNAME}.GetHashCode()';
	DECLARE @ttTableEqualityHashNullable nvarchar(max) = '(this.{COLNAME}?.GetHashCode() ?? 0)';
	DECLARE @ttTableEqualityNextHash nvarchar(max) = '
                ^ ';
	DECLARE @ttTableToString nvarchar(max) = '\r\n  {COLNAME}={this.{COLNAME}{NULLABLE}}';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load columns
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @columnID int = 1, @CodeContent nvarchar(max) = '', @CodeEqualPK nvarchar(max) = '';
	DECLARE @CodeIntf nvarchar(max) = '', @CodeToString nvarchar(max) = '', @CodeHashPK nvarchar(max) = null;

	WHILE @columnID > 0
	BEGIN
		DECLARE @colName sysname, @colIsNullable bit, @colIsReadonly bit, @sqlType sysname, @colDef nvarchar(max) = ' ';
		-- search for column
		SELECT TOP 1
			@colName = colName, 
			@sqlType = sqlType,
			@colDef = colDefinition,
			@colIsNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END,
			@colIsReadonly = CASE WHEN colDefinition LIKE '% readonly %' THEN 1 ELSE 0 END
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND resultsetIndex = @tableIndex
			AND colIndex = @columnID - 1;

		IF @@ROWCOUNT = 0
			SET @columnID = 0;
		ELSE
		BEGIN
			DECLARE @netType sysname, @netBaseType sysname, @isBaseTypeNullable bit, @colMaxLen int;
			SELECT @netType = CASE WHEN @colIsNullable = 1 THEN NullableType ELSE BaseType END,
				@netBaseType = BaseType,
				@isBaseTypeNullable = IsBaseTypeNullable,
				@colMaxLen = [Length]
			FROM build.ConvertToNetType(@sqlType);
			--PRINT '        -> found ' + Convert(varchar, @columnID) + '. column ' + @colName + ' with type ' + @sqlType + ' and options [' + RTRIM(LTRIM(@colDef)) + ']';

			-- in nullable mode every type get's a '?' if it can be null 
			IF @modeNULLABLE = 1 AND @colIsNullable = 1 AND @netType NOT LIKE '%?'
				SET @netType += '?';

			-- prepare ToString
			SET @CodeToString += REPLACE(REPLACE(@ttTableToString, 
				'{COLNAME}', @colName), 
				'{NULLABLE}', CASE WHEN @ColDef LIKE '% null %' THEN '?.ToString() ?? "<null>"' ELSE '' END);

			-- hash
			IF @colDef LIKE '% key %'
				SET @CodeHashPK = IsNull(@CodeHashPK + @ttTableEqualityNextHash, '') + 
					REPLACE(CASE WHEN @colIsNullable = 1 OR @isBaseTypeNullable = 1 THEN @ttTableEqualityHashNullable ELSE @ttTableEqualityHash END, '{COLNAME}', @colName);

			-- equality
			IF @colDef LIKE '% key %'
				SET @CodeEqualPK += REPLACE(CASE WHEN @colIsNullable = 1 OR @isBaseTypeNullable = 1 THEN @ttTableEqualityCompareNullable ELSE @ttTableEqualityCompare END, '{COLNAME}', @colName);

			-- regular parameter
			SET @CodeContent += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
				CASE 
					WHEN @colIsReadonly = 0 THEN @ttTableProperty 
					WHEN @modeJSON = 1 THEN @ttTablePropertyReadonlyJson
					ELSE @ttTablePropertyReadonly 
				END,
				'{JSONPROPERTY}', CASE WHEN @modeJSON = 0 THEN '' ELSE @ttJsonProperty END),
				'{NULLCHECK}', CASE WHEN @colIsNullable = 0 AND @isBaseTypeNullable = 1 THEN @ttTableNullCheck ELSE '' END),	-- must be before TABLENAME and COLNAME
				'{LENCHECK}', CASE 
								WHEN @colMaxLen IS NULL THEN '' 
								WHEN @colIsNullable = 0 AND @isBaseTypeNullable = 1 THEN @ttTableLenCheck 
								ELSE @ttTableLenCheckNullable 
							  END),
				'{MAXLEN}', IsNull(Convert(varchar, @colMaxLen), '')),
				'{MODELNAME}', @procModelName), 
				'{TABLENAME}', @modelName), 
				'{COLNAME}', @colName), 
				'{COLTYPE}', @netType), 
				'{SQLTYPE}', @sqlType), 
				'{OPTIONS}', RTRIM(LTRIM(@colDef))),
				'{DEFAULT}', CASE WHEN @netType = 'String' THEN ' = NOT_INITIALIZED' ELSE '' END);

			SET @columnID += 1;
		END
	END;

	-- equality
	IF Len(@CodeEqualPK) > 0
	BEGIN
		SET @CodeIntf += REPLACE(REPLACE(@ttTableEqualityIntf, '{MODELNAME}', @procModelName), '{TABLENAME}', @modelName);
		SET @CodeContent += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttTableEqualityImpl, 
			'{MODELNAME}', @procModelName), 
			'{TABLENAME}', @modelName), 
			'{COMPARE}', @CodeEqualPK), 
			'{HASH}', @CodeHashPK), 
			'{MODENULLABLE}', CASE WHEN @modeNULLABLE = 1 THEN '?' ELSE '' END);
	END		

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build together
	---------------------------------------------------------------------------------------------------------------------------------------------

	DECLARE @table nvarchar(max);
	SET @table = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ttTable, 
		'{MODELNAMESPACE}', @procModelNamespace), 
		'{MODELNAME}', @procModelName), 
		'{TABLENAME}', @modelName), 
		'{INTERFACES}', @CodeIntf), 
		'{JSONUSING}', CASE WHEN @modeJSON = 0 THEN '' ELSE @ttJsonUsing END), 
		'{CONTENT}', @CodeContent), 
		'{TOSTRING}', @CodeToString);

	IF @OutputType = 'PRINT'
	BEGIN
		PRINT '------------------------ C O D E   T A B L E ------------------------------';
		PRINT Convert(varchar, LEN(@table)) + ' characters';
		PRINT @table;
		PRINT '---------------------------------------------------------------------------';
	END 
	ELSE 
		SELECT 
			@procModelName + '__' + @modelName + '.schema.cs' AS [FileName],
			@table AS Content,
			@modify_date AS modify_date;

	-- Tests
	-- DECLARE @Cols build.TT_Columns, @now datetime = GetDate();
	-- INSERT INTO @Cols VALUES(1, 'Seriennummer', 'varchar(20)', ' key ');
	-- INSERT INTO @Cols VALUES(2, 'Wert', 'money', ' ');
	-- INSERT INTO @Cols VALUES(3, 'GutscheinArt', 'char(1)', ' null ');
	-- EXEC build.GenerateCSharpTable2 'TestTableType', 'RLH_WT_Gutschein', 'Wareneingang_Load', @Cols, 1, @now, 'PRINT';
END
GO
