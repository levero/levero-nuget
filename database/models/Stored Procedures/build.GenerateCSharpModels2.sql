SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2019-01-30
-- Description:	Generates all models
-- =============================================
CREATE PROCEDURE [build].[GenerateCSharpModels2]
	@AppPackageID				varchar(15),
	@AppPackageVersion			varchar(10),
	@OutputType					varchar(10) = 'SELECT'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @AppVersionComparable int = build.VersionToInt(@AppPackageVersion);
	DECLARE @generator varchar(15) = 'csharp';
	DECLARE @True	bit = 1;
	DECLARE @False	bit = 0;
    DECLARE @flagIGNORE_C# sysname = 'IGNORE_C#';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- collect procedures
	---------------------------------------------------------------------------------------------------------------------------------------------
	
	-- which database, schema and prefix?
	DECLARE @databaseName sysname, @listOfSchema nvarchar(max), @listOfPrefix nvarchar(max), @removePrefix bit, @modelNamespace nvarchar(max), @listOfModules nvarchar(max);
	DECLARE @mode varchar(max);
	SELECT 
		@databaseName = DatabaseName,
		@listOfSchema = CSharp_ListOfSchema,
		@listOfPrefix = CSharp_ListOfPrefix,
		@listOfModules = CSharp_ListOfModules,
		@removePrefix = CSharp_RemovePrefix,
		@modelNamespace = CSharp_ModelNamespace,
		@mode = CSharp_Mode
	FROM build.Settings2
	WHERE AppPackageID = @AppPackageID;
	
	-- get models and fill table build.TEMP_Procedures2
	EXEC build.CollectModels2 @generator, @AppPackageID, '', @databaseName, @AppPackageVersion, @listOfSchema, @listOfPrefix, @removePrefix;
	
	-- get modules 
	DECLARE @Modules TABLE(
		ModulID varchar(15) PRIMARY KEY,
		IsDone bit DEFAULT 0
	);
	INSERT INTO @Modules (ModulID)
	SELECT Item
	FROM build.Split2(@listOfModules, ',', @True, @True);

	DECLARE @ModulID varchar(30), @ModulDb sysname, @ModulVersion varchar(10);
	WHILE EXISTS(SELECT * FROM @Modules WHERE IsDone = 0)
	BEGIN
		SELECT TOP(1) 
			@ModulID = m.ModulID,
			@ModulDb = IsNull(NullIf(s.DatabaseName, '*'), @databaseName),
			@ModulVersion = IsNull(NullIf(s.ModulVersion, '*'), @AppPackageVersion),
			@listOfSchema = s.Typescript_ListOfSchema,
			@listOfPrefix = s.Typescript_ListOfPrefix,
			@listOfModules = s.Typescript_ListOfModules
		FROM @Modules m
			LEFT OUTER JOIN build.Settings2 s ON s.AppID = '*' AND s.AppPackageID = m.ModulID
		WHERE IsDone = 0;
		UPDATE @Modules 
		SET IsDone = 1
		WHERE ModulID = @ModulID;

		-- let modules be recursive
		INSERT INTO @Modules (ModulID)
		SELECT s.Item
		FROM build.Split2(@listOfModules, ',', @True, @True) s
		WHERE s.Item NOT IN (SELECT ModulID FROM @Modules);

		-- get models and fill table build.TEMP_Procedures2 
		EXEC build.CollectModels2 @Generator, @AppPackageID, @ModulID, @ModulDb, @ModulVersion, @listOfSchema, @listOfPrefix, @removePrefix;
	END

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- check duplicates
	---------------------------------------------------------------------------------------------------------------------------------------------
	PRINT 'check for duplicate model names...';
	IF EXISTS(SELECT modelName, COUNT(*) 
			  FROM build.TEMP_Procedures2 
			  WHERE appPackageID = @AppPackageID AND generator = @Generator AND IsExternal = 0
			  GROUP BY modelName, modulID
			  HAVING COUNT(*) > 1)
	BEGIN
		RAISERROR('There are procedures from different schemas but with the same name!', 16, 1);
		RETURN;
	END
	
	---------------------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------------------

	-- set model namespace
	UPDATE build.TEMP_Procedures2
	SET modelNamespace = @modelNamespace,
		done = 0
	WHERE appPackageID = @AppPackageID
		AND generator = @generator;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- run through all procedures and build models
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @objectID int, @isExtern bit;

	WHILE EXISTS(SELECT * FROM build.TEMP_Procedures2 WHERE appPackageID = @AppPackageID AND generator = @generator AND done = 0)
	BEGIN
		-- next procedure
		SELECT TOP 1
			@objectID = objectID,
			@isExtern = isExternal
		FROM build.TEMP_Procedures2 
		WHERE appPackageID = @AppPackageID 
			AND generator = @generator
			AND done = 0
		ORDER BY modelName;

		-- mark as done
		UPDATE build.TEMP_Procedures2 
		SET done = 1
		WHERE appPackageID = @AppPackageID
			AND generator = @generator
			AND objectID = @objectID;

		---------------------------------------------------------------------------------------------------------------------------------------------
		-- build procedure
		---------------------------------------------------------------------------------------------------------------------------------------------

        IF NOT EXISTS(SELECT * FROM build.TEMP_Flags2 tf WHERE tf.appPackageID = @AppPackageID AND tf.generator = @generator AND tf.objectID = @objectID AND tf.flag = @flagIGNORE_C#)
        BEGIN
		    IF @mode = 'sql' OR @mode like 'sql:%'
		    BEGIN
			    EXEC build.GenerateCSharpProcedure2_Sql @AppPackageID, @generator, @mode, @objectID, @isExtern, @OutputType;
		    END
		    ELSE IF @mode = 'backendhub' OR @mode like 'backendhub:%'
		    BEGIN
			    IF @isExtern = 0
				    EXEC build.GenerateCSharpProcedure2_BackendHub @AppPackageID, @generator, @mode, @objectID, @OutputType;
		    END
		    ELSE
		    BEGIN
			    RAISERROR('CSharp mode %s is not allowed!', 16, 1, @mode);
			    RETURN;
		    END
		END
	END

	-- Tests
	-- EXEC build.GenerateCSharpModels2 'rlh-wt-gutschei', '2', 'PRINT';
	-- EXEC build.GenerateCSharpModels2 'ils', '2', 'PRINT';
	-- EXEC build.GenerateCSharpModels2 'fire', '2', 'PRINT';
END
GO
