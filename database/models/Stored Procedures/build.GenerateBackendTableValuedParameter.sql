SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-10-19
-- Description:	Generates a backend model description for a table type parameter
-- =============================================
CREATE PROCEDURE [build].[GenerateBackendTableValuedParameter]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@objectID					int,
	@paramName					sysname,
	@backendModelDefinition		nvarchar(max) OUT
AS
BEGIN
	SET NOCOUNT ON;
	
	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for backend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @backendModels nvarchar(max) = null;
	DECLARE @backend_json nvarchar(max) = '[{PARAMS}
    ]';
-- old:
--	DECLARE @backend_param nvarchar(max) = '
--	            {"ColumnName": "{COLNAME}", "TypeName": "{COLTYPE}", "Nullable": {NULLABLE}}';
-- backwards compatibility - until all backends are > V3.8.0:
	DECLARE @backend_param nvarchar(max) = '
      {"ColumnName": "{COLNAME}", "ColumnIndex": {INDEX}, "TypeName": "{COLTYPE}", "Nullable": {NULLABLE}, "Options": "{COLDEFINITION}", "SqlDbType": "{SQLDBTYPE}", "MaxLength": {MAXLENGTH}, "Precision": {PRECISION}, "Scale": {SCALE}}';
-- new:
--	DECLARE @backend_param nvarchar(max) = '
--      {"ColumnName": "{COLNAME}", "TypeName": "{COLTYPE}", "Options": "{COLDEFINITION}", "SqlDbType": "{SQLDBTYPE}", "MaxLength": {MAXLENGTH}, "Precision": {PRECISION}, "Scale": {SCALE}}';



	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load columns
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @colName sysname, @sqlType sysname, @columnID int = 1, @colDef nVarchar(MAX), @columns nvarchar(max) = '', @colPrepare nvarchar(max) = '';
	DECLARE @maxLength int, @precision tinyInt, @scale tinyint;
	DECLARE @colIsNullable bit, @isComputed bit;

	WHILE @columnID > 0
	BEGIN
		-- search for column
		SET @colName = NULL;

		-- search for column
		SELECT TOP 1
			@colName = colName, 
			@sqlType = sqlType,
			@colDef = colDefinition,
			@colIsNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END
		FROM build.TEMP_Columns
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND resultset = @paramName
			AND colIndex = @columnID - 1;

		IF @colName IS NULL
			SET @columnID = -1;
		ELSE
		BEGIN
			DECLARE @netType sysname, @netBaseType sysname, @isBaseTypeNullable bit, @colMaxLen int, @colPrecision int, @colScale int;
			DECLARE @colSqlDbType sysname, @typePrefix nVarchar(MAX);
			SELECT @netType = CASE WHEN @colIsNullable = 1 THEN NullableType ELSE BaseType END,
				@netBaseType = BaseType,
				@typePrefix = IsNull(TypeNameSpace + '.', ''),
				@isBaseTypeNullable = IsBaseTypeNullable,
				@colMaxLen = [Length],
				@colPrecision = [Precision],
				@colScale = Scale,
				@colSqlDbType = SqlDbType
			FROM build.ConvertToNetType(@sqlType);
			--PRINT '        -> found ' + Convert(varchar, @columnID) + '. column ' + @colName + ' with type ' + @sqlType + ' and options [' + RTRIM(LTRIM(@colDef)) + ']';

			-- backend params 
			SET @backendModels = IsNull(@backendModels + ',', '') + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@backend_param, 
				'{COLNAME}', @colName),
				'{INDEX}', @columnID - 1),
				'{COLTYPE}', @typePrefix + @netBaseType),
				'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN 'true' ELSE 'false' END),		-- NULLABLE is obsolete
				'{COLDEFINITION}', @colDef),
				'{SQLDBTYPE}', @colSqlDbType),
				'{MAXLENGTH}', IsNull(Convert(varchar(10), @colMaxLen), 'null')),
				'{PRECISION}', IsNull(Convert(varchar(10), @colPrecision), 'null')),
				'{SCALE}', IsNull(Convert(varchar(10), @colScale), 'null'));

		END
		SET @columnID += 1;
	END;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build together
	---------------------------------------------------------------------------------------------------------------------------------------------

	SET @backendModelDefinition = REPLACE(@backend_json,
		'{PARAMS}', IsNull(@backendModels, ''));


	--PRINT '------------------------------ B A C K E N D ------------------------------';
	--PRINT Convert(varchar, LEN(@backendModelDefinition)) + ' characters';
	--PRINT @backendModelDefinition;
	--PRINT '---------------------------------------------------------------------------';

	-- Tests
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendTableValuedParameter 'LeveroApps', 2046630334, '[ehsra].[TT_SaveRisk_V1]', @b OUT; PRINT @b;
	-- DECLARE @b nvarchar(max); EXEC build.GenerateBackendTableValuedParameter 'FireConnect', 834818036, '[fire].[TT_FDisk_ImportMember_V1]', @b OUT; PRINT @b;
END
GO
