SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ing. Karl Prokupek
-- Create date: 2018-06-22
-- Description:	Generates a frontend model description for a table type parameter
-- =============================================
CREATE PROCEDURE [build].[GenerateTypescriptTableValuedParameter2]
	@appPackageID				varchar(15),
	@generator					varchar(15),
	@objectID					int,				-- of procedure!
	@paramName					sysname,
	@tableTypeName				sysname,
	@frontendModelName			sysname,
	@frontendModelDefinition	nvarchar(max) OUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @procModelName nvarchar(max); 
	SELECT
		@procModelName = modelName
	FROM build.TEMP_Procedures2
	WHERE appPackageID = @appPackageID
		AND generator = @generator 
		AND objectID = @objectID;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- constants and templates for frontend
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @frontend_tableType nvarchar(max) =
'/* ***************************************************************************************************************/
/** Request Table Valued Parameter {TT} */
export class {NAME} {{CONSTANTS}

  constructor({COLUMNS}) {
    {PREPARE}
  }
}

';

    DECLARE @frontend_init_tools nvarchar(max) = '
    const _tools = ServiceLocator.injector.get(LeveroToolsService);';
    DECLARE @frontend_constMaxLen nvarchar(max) = '
  /** param: {COLNAME} {SQLTYPE} */
  public static readonly {COLNAME}_MaxLength = {MAXLEN};';
    DECLARE @frontend_columns nvarchar(max) = '
    /** sql type: {SQLTYPE}, nullable: {NULLABLE}{DEFAULT}{PRIMARYKEY} */
    public {COLNAME}: {COLTYPE},';
    DECLARE @frontend_prepare nvarchar(max) = '
    this.{COLNAME} = _tools.{PREPARE}(this.{COLNAME}, true);';

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- load columns
	---------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE @columnID int = 1, @columns nvarchar(max) = '', @colPrepare nvarchar(max) = '', @colConstants nvarchar(max) = '';

	WHILE @columnID > 0
	BEGIN
		DECLARE @colName sysname, @colIsNullable bit, @colIsReadOnly bit, @colIsKey bit, @sqlType sysname, @colDef nvarchar(max) = ' ', @colDefault NVARCHAR(max);
		-- search for column
		SELECT TOP 1
			@colName = colName, 
			@sqlType = sqlType,
			@colDef = colDefinition,
			@colIsNullable = CASE WHEN colDefinition LIKE '% null %' THEN 1 ELSE 0 END,
            @colIsKey = CASE WHEN colDefinition LIKE '% key %' THEN 1 ELSE 0 END,
			@colIsReadOnly = CASE 
				WHEN colDefinition LIKE '% computed %' THEN 1 
				WHEN colDefinition LIKE '% identity %' THEN 1 
				ELSE 0
			END,
            @colDefault = defaultValue
		FROM build.TEMP_Columns2
		WHERE appPackageID = @appPackageID
			AND generator = @generator
			AND objectID = @objectID
			AND resultset = @paramName
			AND colIndex = @columnID - 1;

		IF @@ROWCOUNT = 0
			SET @columnID = -1;
		ELSE IF @colIsReadOnly = 1
		BEGIN
			--PRINT '        -> found ' + Convert(varchar, @columnID) + '. column ' + @colName + ' with type ' + @sqlType + ' and READONLY !!!';
			DECLARE @dummy int;
		END
		ELSE
		BEGIN
			--PRINT '        -> found ' + Convert(varchar, @columnID) + '. column ' + @colName + ' with type ' + @sqlType;

			-- regular parameter
			SET @columns += REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_columns,
				'{COLNAME}', @colName), 
				'{COLTYPE}', build.GetParamTypeForFrontend(@sqlType)), 
				'{SQLTYPE}', @sqlType), 
				'{NULLABLE}', CASE WHEN @colIsNullable = 1 THEN 'true' ELSE 'false' END),
                '{DEFAULT}', ISNULL(', default: ' + @colDefault, '')),
                '{PRIMARYKEY}', CASE WHEN @colIsKey = 1 THEN ', primary key column' ELSE ''END);
			SET @colPrepare += ISNULL(REPLACE(REPLACE(@frontend_prepare, 
				'{COLNAME}', @colName), 
				'{PREPARE}', build.GetParamPrepareForFrontend(@sqlType)), '');

			-- is this parameter a string?
			IF @sqlType LIKE '%char([0-9]%'
				SET @colConstants += REPLACE(REPLACE(REPLACE(@frontend_constMaxLen,
				'{COLNAME}', @colName), 
				'{SQLTYPE}', @sqlType),
				'{MAXLEN}', REPLACE(SUBSTRING(@sqlType, CHARINDEX('(', @sqlType) + 1, 999), ')', ''));

		END
		SET @columnID += 1;
	END;

    IF @colPrepare LIKE '%_tools.%'
        SET @colPrepare = @frontend_init_tools + @colPrepare;

	---------------------------------------------------------------------------------------------------------------------------------------------
	-- build together
	---------------------------------------------------------------------------------------------------------------------------------------------
	--PRINT '@frontend_tableType: ' + IsNull(@frontend_tableType, '<null>');
	--PRINT '@tableTypeName: ' + IsNull(@tableTypeName, '<null>');
	--PRINT '@frontendModelName: ' + IsNull(@frontendModelName, '<null>');
	--PRINT '@columns: ' + IsNull(@columns, '<null>');
	--PRINT '@colPrepare: ' + IsNull(@colPrepare, '<null>');
	SET @frontendModelDefinition = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@frontend_tableType, 
		'{TT}', @tableTypeName), 
		'{NAME}', @frontendModelName), 
		'{COLUMNS}', SUBSTRING(@columns, 1, LEN(@columns) - 1)), 
		'{PREPARE}', @colPrepare),
		'{CONSTANTS}', @colConstants);

	--PRINT '----------------------------- F R O N T E N D -----------------------------';
	--PRINT Convert(varchar, LEN(@frontendModelDefinition)) + ' characters';
	--PRINT @frontendModelDefinition;
	--PRINT '---------------------------------------------------------------------------';

	-- Tests
	-- DECLARE @f nvarchar(max); EXEC build.GenerateTypescriptTableType2 'te-capex-fc', 'typescript', 2046630334, '@Risks', '[ehsra].[TT_SaveRisk_V1]', 'SaveRiskAnalysis_Risks', @f OUT; PRINT @f;
END
GO
