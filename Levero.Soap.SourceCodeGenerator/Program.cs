﻿using System.Text;
using System.Xml.Linq;

namespace MyApp // Note: actual namespace depends on the project name.
{
    internal static class Program
    {
        public static async Task Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("need arguments \"path/url of WSDL\" \"path of output file\" \"namespace\" ");
                return;
            }

            var wsdlPath = args[0];
            var outputFile = args[1];
            var @namespace = args[2];
            var access = "internal";

            Stream wsdlStream;
            if (wsdlPath.ToLower().StartsWith("http"))
            {
                Console.WriteLine($"read WSDL from url {wsdlPath}...");
                using var client = new HttpClient();
                wsdlStream = await client.GetStreamAsync(wsdlPath);
            }
            else
            {
                Console.WriteLine($"read WSDL from path {wsdlPath}...");
                wsdlStream = File.Open(wsdlPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            }

            var wsdl = await XDocument.LoadAsync(wsdlStream, LoadOptions.PreserveWhitespace, CancellationToken.None);
            if (wsdl == null || wsdl.Root == null)
            {
                Console.WriteLine("WSDL is not a valid XML.");
                return;
            }

            Console.WriteLine($"create output file {outputFile}...");
            using var output = new StreamWriter(File.Open(outputFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read), Encoding.UTF8);

            Console.WriteLine($"write headers");
            output.WriteLine($"using System;");
            output.WriteLine($"using System.Collections;");
            output.WriteLine($"using System.Collections.Generic;");
            output.WriteLine($"using System.Diagnostics.CodeAnalysis;");
            output.WriteLine($"using System.Linq;");
            output.WriteLine($"using System.Net.Http;");
            output.WriteLine($"using System.Runtime.CompilerServices;");
            output.WriteLine($"using System.Security.Policy;");
            output.WriteLine($"using System.Text;");
            output.WriteLine($"using System.Threading;");
            output.WriteLine($"using System.Threading.Tasks;");
            output.WriteLine($"using System.Xml.Linq;");
            output.WriteLine();
            output.WriteLine($"namespace {@namespace}");
            output.WriteLine($"{{");


            Console.WriteLine($"read namespaces...");
            var nsXml = wsdl.Root.GetDefaultNamespace();
            var nsSoap = wsdl.Root.GetNamespaceOfPrefix("soap") ?? throw new Exception($"Namespace 'soap' not found!");
            var nsSchema = XNamespace.Get("http://www.w3.org/2001/XMLSchema");

            Console.WriteLine($"search for service...");
            foreach (var service in wsdl.Root.Elements(nsXml + "service"))
            {
                // get service and create class header
                var serviceName = service.GetValueFromAttribute("name", "Missing attribute 'name' in element 'service'!");
                Console.WriteLine($"  => found service {serviceName}");
                output.WriteLine($"\tinternal static partial class {serviceName}");
                output.WriteLine($"\t{{");

                var sourcecodebuffer = new List<string>();

                Console.WriteLine($"search for ports...");
                foreach (var port in service.Elements(nsXml + "port"))
                {
                    var portName = port.GetValueFromAttribute("name", $"Missing attribute 'name' in element 'port' of service '{serviceName}'!");
                    var portBinding = GetXNameFromAttribute(port, "binding", $"Missing attribute 'binding' in port '{portName}'!");
                    Console.WriteLine($"  => found port {portName} with binding {portBinding.LocalName}");

                    Console.WriteLine($"search for binding...");
                    var binding = wsdl.Root.Elements(nsXml + "binding")
                        .Where(it => it.GetValueFromAttribute("name") == portBinding.LocalName)
                        .FirstOrDefault()
                        ?? throw new Exception($"Binding '{portBinding.LocalName}' not found!");
                    var bindingType = GetXNameFromAttribute(binding, "type") ?? throw new Exception($"Missing attribute 'type' in binding '{portBinding.LocalName}'!");

                    // output all namespaces
                    foreach (var ns in wsdl.Root.Attributes().Where(it => it.Name.Namespace == XNamespace.Xmlns))
                        output.WriteLine($"\t\tprivate static readonly XNamespace ns_{ns.Name.LocalName} = XNamespace.Get(\"{ns.Value}\");");
                    output.WriteLine();

                    // Default instance for given address url
                    var address = port.Element(nsSoap + "address");
                    var addressUrl = address?.GetValueFromAttribute("location");
                    if (addressUrl != null)
                    {
                        output.WriteLine($"\t\tpublic static {bindingType.LocalName} Default {{ get; }} = new {bindingType.LocalName}(\"{addressUrl}\");");
                        output.WriteLine();
                    }

                    // search for message type
                    Console.WriteLine($"search for portType of binding...");
                    var portType = wsdl.Root.Elements(nsXml + "portType")
                        .FirstOrDefault(it => it.GetValueFromAttribute("name") == bindingType.LocalName)
                        ?? throw new Exception($"PortType '{bindingType.LocalName}' not found!");

                    // output class for portType
                    output.WriteLine($"\t\t{access} partial class {bindingType.LocalName}");
                    output.WriteLine($"\t\t{{");
                    output.WriteLine($"\t\t\tpublic {bindingType.LocalName}(string address)");
                    output.WriteLine($"\t\t\t{{");
                    output.WriteLine($"\t\t\t\tthis.Address = new Uri(address);");
                    output.WriteLine($"\t\t\t\tthis.HttpClient = new HttpClient();");
                    output.WriteLine($"\t\t\t}}");
                    output.WriteLine();
                    output.WriteLine($"\t\t\tpublic Uri Address {{ get; }}");
                    output.WriteLine($"\t\t\tpublic HttpClient HttpClient {{ get; }}");
                    output.WriteLine();

                    // search for operations
                    Console.WriteLine($"search for operations...");
                    foreach (var operation in portType.Elements(nsXml + "operation"))
                    {
                        var operationName = operation.GetValueFromAttribute("name", $"Missing attribute 'name' in element 'operation' of portType '{bindingType.LocalName}'!");
                        Console.WriteLine($"  => found operation {operationName}");

                        // get input message details
                        var inputMessageElement = operation.Element(nsXml + "input") ?? throw new Exception($"Missing element 'input' in operation '{operationName}' of portType '{bindingType.LocalName}'!");
                        var inputMessageType = GetXNameFromAttribute(inputMessageElement, "message") ?? throw new Exception($"Missing attribute 'message' in operation '{operationName}' of portType '{bindingType.LocalName}'!");
                        var inputType = wsdl.Root.Elements(nsXml + "message")
                            .FirstOrDefault(it => it.GetValueFromAttribute("name") == inputMessageType.LocalName) ?? throw new Exception($"Missing message type '{inputMessageType.LocalName}'!");
                        var inputMessages = inputType.Elements(nsXml + "part")
                            .Select(it => new
                            {
                                element = it,
                                name = GetValueFromAttribute(it, "name", $"Missing attribute 'name' in message '{inputMessageType.LocalName}'!"),
                                type = GetXNameFromAttribute(it, "element", $"Missing attribute 'element' in operation '{inputMessageType.LocalName}'!")
                            })
                            .ToArray();

                        // get output message details
                        var outputMessageElement = operation.Element(nsXml + "output") ?? throw new Exception($"Missing element 'output' in operation '{operationName}' of portType '{bindingType.LocalName}'!");
                        var outputMessageType = GetXNameFromAttribute(outputMessageElement, "message") ?? throw new Exception($"Missing attribute 'message' in operation '{operationName}' of portType '{bindingType.LocalName}'!");
                        var outputType = wsdl.Root.Elements(nsXml + "message")
                            .FirstOrDefault(it => it.GetValueFromAttribute("name") == outputMessageType.LocalName) ?? throw new Exception($"Missing message type '{outputMessageType.LocalName}'!");
                        var outputMessages = outputType.Elements(nsXml + "part")
                            .Select(it => new
                            {
                                element = it,
                                name = GetValueFromAttribute(it, "name", $"Missing attribute 'name' in message '{outputMessageType.LocalName}'!"),
                                type = GetXNameFromAttribute(it, "element", $"Missing attribute 'element' in operation '{outputMessageType.LocalName}'!")
                            })
                            .ToArray();

                        // output operation
                        var inputParameterDeclarations = "";
                        for (var i = 0; i < inputMessages.Length; i++)
                        {
                            if (i > 0)
                                inputParameterDeclarations += ", ";
                            inputParameterDeclarations += $"{inputMessages[i].type.LocalName} p{i}";
                        }
                        var inputParameterDeclarationsCT = inputParameterDeclarations.Length > 0 ? inputParameterDeclarations + ", " : inputParameterDeclarations;
                        var inputParameterList = "";
                        for (var i = 0; i < inputMessages.Length; i++)
                        {
                            if (i > 0)
                                inputParameterList += ", ";
                            inputParameterList += $"p{i}";
                        }
                        var inputParameterListCT = inputParameterList.Length > 0 ? inputParameterList + ", " : inputParameterList;
                        var returnTypeDeclations = "";
                        for (var i = 0; i < outputMessages.Length; i++)
                        {
                            if (i > 0)
                                returnTypeDeclations += ", ";
                            returnTypeDeclations += $"{outputMessages[i].type.LocalName}";
                        }
                        var returnStatement = "";
                        for (var i = 0; i < outputMessages.Length; i++)
                        {
                            if (i > 0)
                                returnStatement += ", ";
                            returnStatement += $"output.{outputMessages[i].name}";
                        }
                        if (outputMessages.Length > 1)
                            returnStatement = $"({returnStatement})";

                        output.WriteLine($"\t\t\t#region Operation {operationName}");
                        output.WriteLine();
                        output.WriteLine($"\t\t\t//public Task<{returnTypeDeclations}> {operationName}Async({inputParameterDeclarations})");
                        output.WriteLine($"\t\t\t//\t=> this.{operationName}Async({inputParameterListCT}CancellationToken.None);");
                        output.WriteLine();
                        output.WriteLine($"\t\t\tpublic async Task<{returnTypeDeclations}> {operationName}Async({inputParameterDeclarationsCT}CancellationToken cancellationToken)");
                        output.WriteLine($"\t\t\t{{");
                        output.WriteLine($"\t\t\t\t// build input message");
                        output.WriteLine($"\t\t\t\tvar input = new {inputMessageType.LocalName}");
                        output.WriteLine($"\t\t\t\t{{");
                        for (var i = 0; i < inputMessages.Length; i++)
                        {
                            output.Write($"\t\t\t\t\t{inputMessages[i].name} = p{i}");
                            if (i > 0)
                                output.Write(",");
                            output.WriteLine();
                        }
                        output.WriteLine($"\t\t\t\t}};");
                        output.WriteLine();
                        output.WriteLine($"\t\t\t\t// do work");
                        output.WriteLine($"\t\t\t\tvar output = await this.{operationName}Async(input, CancellationToken.None);");
                        output.WriteLine();
                        output.WriteLine($"\t\t\t\t// return output message");
                        output.WriteLine($"\t\t\t\treturn {returnStatement};");
                        output.WriteLine($"\t\t\t}}");
                        output.WriteLine();
                        output.WriteLine($"\t\t\tpublic async Task<{outputMessageType.LocalName}> {operationName}Async({inputMessageType.LocalName} request, CancellationToken cancellationToken)");
                        output.WriteLine($"\t\t\t{{");
                        output.WriteLine($"\t\t\t\t// create envelope");
                        output.WriteLine($"\t\t\t\tvar requestEnvelope = SoapEnvelope.CreateRequest(request);");
                        output.WriteLine();
                        output.WriteLine($"\t\t\t\t// bul http reques");
                        output.WriteLine($"\t\t\t\tvar requestHttp = new StringContent(requestEnvelope.Serialize(), Encoding.UTF8, \"text/xml\");");
                        output.WriteLine();
                        output.WriteLine($"\t\t\t\t// send request");
                        output.WriteLine($"\t\t\t\tawait this.HttpClient.PostAsync(this.Address, requestHttp, cancellationToken);");
                        output.WriteLine($"\t\t\t}}");
                        output.WriteLine();
                        output.WriteLine($"\t\t\t#endregion");
                        output.WriteLine();

                        // remember message types
                        sourcecodebuffer.Add($"\t\t{access} partial class {inputMessageType.LocalName}");
                        sourcecodebuffer.Add($"\t\t{{");
                        sourcecodebuffer.Add($"\t\t\tpublic {inputMessageType.LocalName}()");
                        sourcecodebuffer.Add($"\t\t\t{{");
                        sourcecodebuffer.Add($"\t\t\t\t//this.Xml = new XElement(nsCheditCheck + \"individualOrderCheckRequest\", individualOrderCheckRequest.Xml);");
                        sourcecodebuffer.Add($"\t\t\t}}");
                        sourcecodebuffer.Add($"\t\t");
                        sourcecodebuffer.Add($"\t\t\t//internal XElement Xml {{ get; }}");
                        sourcecodebuffer.Add("");
                        foreach (var i in inputMessages)
                            sourcecodebuffer.Add($"\t\t\tpublic {i.type.LocalName} {i.name} {{ get; }} = new();");
                        sourcecodebuffer.Add($"\t\t}}");
                        sourcecodebuffer.Add("");

                        sourcecodebuffer.Add($"\t\t{access} partial class {outputMessageType.LocalName}");
                        sourcecodebuffer.Add($"\t\t{{");
                        sourcecodebuffer.Add($"\t\t\tpublic {outputMessageType.LocalName}()");
                        sourcecodebuffer.Add($"\t\t\t{{");
                        sourcecodebuffer.Add($"\t\t\t\t//this.Xml = new XElement(nsCheditCheck + \"individualOrderCheckRequest\", individualOrderCheckRequest.Xml);");
                        sourcecodebuffer.Add($"\t\t\t}}");
                        sourcecodebuffer.Add($"\t\t");
                        sourcecodebuffer.Add($"\t\t\t//internal XElement Xml {{ get; }}");
                        sourcecodebuffer.Add("");
                        foreach (var o in outputMessages)
                            sourcecodebuffer.Add($"\t\t\tpublic {o.type.LocalName} {o.name} {{ get; }} = new();");
                        sourcecodebuffer.Add($"\t\t}}");
                        sourcecodebuffer.Add("");
                    }

                    output.WriteLine($"\t\t}}");
                    output.WriteLine();
                }

                // output all message types
                output.WriteLine($"\t\t#region Message Types");
                output.WriteLine();
                foreach (var line in sourcecodebuffer)
                    output.WriteLine(line);
                sourcecodebuffer.Clear();
                output.WriteLine($"\t\t#endregion");
                output.WriteLine();

                // output all complex types (records)
                Console.WriteLine("search for all complex types...");
                output.WriteLine($"\t\t#region Complex Types");
                output.WriteLine();
                var typesElement = wsdl.Root.Element(nsXml + "types") ?? throw new Exception("Missing element 'types'!");
                var schemaElement = typesElement.Element(nsSchema + "schema") ?? throw new Exception("Missing element 'schema'!");
                var complexTypes = schemaElement.Elements(nsSchema + "complexType").ToArray();
                foreach (var typeElement in complexTypes)
                {
                    var complexTypeName = typeElement.GetValueFromAttribute("name", $"Missing attribute 'name' from complexType!");
                    var isAbstract = typeElement.GetValueFromAttribute("abstract") == "true";
                    var baseClass = "";
                    var sequenceElement = typeElement.Element(nsSchema + "sequence");
                    if (sequenceElement == null)
                    {
                        // vielleicht complexContent/extension[base=]/sequence
                        var extensionElement = typeElement.Element(nsSchema + "complexContent")?.Element(nsSchema + "extension");
                        if (extensionElement != null)
                        {
                            var extensionBase = extensionElement.GetXNameFromAttribute("base");
                            if (extensionBase != null)
                                baseClass = $": {extensionBase.LocalName}";
                            sequenceElement = extensionElement.Element(nsSchema + "sequence");
                        }
                    }
                    Console.WriteLine($"  => found {(isAbstract ? "abstract " : "")}complex type {complexTypeName}{baseClass}");

                    // output type
                    output.WriteLine($"\t\t#region Complex Type {complexTypeName}");
                    output.WriteLine();
                    output.WriteLine($"\t\t{access} {(isAbstract ? "abstract " : "")}partial class {complexTypeName}{baseClass}");
                    output.WriteLine($"\t\t{{");
                    output.WriteLine($"\t\t\tpublic {complexTypeName}()");
                    output.WriteLine($"\t\t\t{{");
                    output.WriteLine($"\t\t\t\t//this.Xml = new XElement(nsCheditCheck + \"individualOrderCheckRequest\", individualOrderCheckRequest.Xml);");
                    output.WriteLine($"\t\t\t}}");
                    output.WriteLine($"\t\t");
                    output.WriteLine($"\t\t\t//internal XElement Xml {{ get; }}");
                    output.WriteLine("");

                    // properties...
                    if (sequenceElement != null)
                    {
                        var properties = sequenceElement.Elements(nsSchema + "element").ToArray();
                        foreach (var propertyElement in properties)
                        {
                            var propertyName = propertyElement.GetValueFromAttribute("name", $"Missing attribute 'name' in complex Type {complexTypeName}!");
                            if (propertyName.isCSharpKeyword())
                                propertyName = "@" + propertyName;
                            var propertyType = propertyElement.GetXNameFromAttribute("type", $"Missing attribute 'type' in property {propertyName} of complex Type {complexTypeName}!");
                            var propertyMinCount = propertyElement.GetValueFromAttribute("minOccurs");
                            var propertyMaxCount = propertyElement.GetValueFromAttribute("maxOccurs");
                            var propertyNameCSharp = propertyType.LocalName switch
                            {
                                "boolean" => "bool",
                                "string" => "string",
                                _ => propertyType.LocalName
                            };
                            if (propertyMaxCount != null)
                                propertyNameCSharp += "[]?";
                            else if (propertyMinCount == "0")
                                propertyNameCSharp += "?";

                            // has annotations?
                            var documentation = propertyElement.Element(nsSchema + "annotation")?.Element(nsSchema + "documentation")?.Value;

                            Console.WriteLine($"    => property {propertyNameCSharp} {complexTypeName}.{propertyName}");

                            if (documentation != null)
                            {
                                output.WriteLine($"\t\t\t/// <summary>");
                                foreach (var line in documentation.WrapWithWords())
                                    output.WriteLine($"\t\t\t/// {line}");
                                output.WriteLine($"\t\t\t/// </summary>");
                            }
                            output.WriteLine($"\t\t\tpublic {propertyNameCSharp} {propertyName} {{ get; set; }}");
                            output.WriteLine();
                        }
                    }
                    output.WriteLine($"\t\t}}");
                    output.WriteLine();
                    output.WriteLine($"\t\t#endregion");
                    output.WriteLine();
                }
                output.WriteLine($"\t\t#endregion");
                output.WriteLine();

                output.WriteLine($"\t}}");
            }
            output.WriteLine($"}}");
            output.Close();
        }

        // get XName with namespace and localName
        static XName? GetXName(this XElement element, string? name)
        {
            if (name == null) return null;
            var parts = name.Split(':');
            var ns = parts.Length == 2 ?
                element.GetNamespaceOfPrefix(parts[0]) ?? throw new Exception($"Namespace '{parts[0]}' not found!") :
                element.GetDefaultNamespace();
            return ns.GetName(parts.Last());
        }
        static XName? GetXNameFromAttribute(this XElement element, XName attributeName)
            => GetXName(element, GetValueFromAttribute(element, attributeName));
        static XName GetXNameFromAttribute(this XElement element, XName attributeName, string errorMessage)
            => GetXName(element, GetValueFromAttribute(element, attributeName)) ?? throw new Exception(errorMessage);

        static string? GetValueFromAttribute(this XElement element, XName attributeName)
            => element?.Attribute(attributeName)?.Value;
        static string GetValueFromAttribute(this XElement element, XName attributeName, string errorMessage)
            => element?.Attribute(attributeName)?.Value ?? throw new Exception(errorMessage);

        static IEnumerable<string> WrapWithWords(this string s, int maxLength = 80)
        {
            var line = "";
            foreach (var word in s.Split(new[] { ' ', '\r', '\n', '\t' }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (line.Length + word.Length < maxLength)
                {
                    line += word;
                    line += ' ';
                }
                else
                {
                    yield return line;
                    line = "";
                }
            }
            if (line.Length > 0)
                yield return line;
        }

        static string[] keywords = { "private" };

        static bool isCSharpKeyword(this string s)
            => Array.IndexOf(keywords, s) >= 0;
    }
}
