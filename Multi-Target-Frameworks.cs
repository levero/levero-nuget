﻿



// multi-targeting frameworks: see https://docs.microsoft.com/en-us/dotnet/standard/frameworks


// AutoGenerateBindingRedirects ist bei .NET Core sinnlos und führt auch zu seltsame Warnungen:
// warning MSB3277: Found conflicts between different versions of "Microsoft.Win32.Registry" that could not be resolved.These reference conflicts are listed in the build log when log verbosity is set to detailed.
// deshalb in csproj nur für .Net 4.x erlauben:
//  <PropertyGroup Condition = "$(TargetFramework.StartsWith('net4'))" >
//    < AutoGenerateBindingRedirects > true </ AutoGenerateBindingRedirects >
//  </ PropertyGroup >


// was auch nur über die csproj Datei einstellbar ist: die C# Sprachversion
//     <LangVersion>8.0</LangVersion>


// DO NOT USE Environment.UserInteractive !!! because it is implemented as "return true" in .NET Core!!


// DLLs from nuget packages are only included in OutputType WinEXE and not Library.
// to force including use property
//    <CopyLocalLockFileAssemblies>true</CopyLocalLockFileAssemblies>


// .NET Core WinForms or WPF Support must be enabled with SDK-Type:
// <Project Sdk="Microsoft.NET.Sdk.WindowsDesktop">
// requires  <UseWpf>true</UseWpf>  or  <UseWindowsForms>true</UseWindowsForms>



// Deactivate a complete folder for a specific target:
//
//   <ItemGroup Condition = "$(TargetFramework.StartsWith('netcore'))" >
//     <Compile Remove="net4x\**" />
//     <EmbeddedResource Remove = "net4x\**" />
//     <None Remove="net4x\**" />
//   </ItemGroup>

