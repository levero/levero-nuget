﻿namespace Test.WinForms.Controls
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.threeStateioLabel1 = new Levero.WinForms.Controls.ThreeStateIOLabel();
            this.radRectangle = new System.Windows.Forms.RadioButton();
            this.grpShape = new System.Windows.Forms.GroupBox();
            this.radArrowRightToLeft = new System.Windows.Forms.RadioButton();
            this.radArrowLeftToRight = new System.Windows.Forms.RadioButton();
            this.grpState = new System.Windows.Forms.GroupBox();
            this.radStateFalse = new System.Windows.Forms.RadioButton();
            this.radStateTrue = new System.Windows.Forms.RadioButton();
            this.radStateNull = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.transparentImage1 = new Levero.WinForms.Controls.TransparentImage();
            this.grpShape.SuspendLayout();
            this.grpState.SuspendLayout();
            this.SuspendLayout();
            // 
            // threeStateioLabel1
            // 
            this.threeStateioLabel1.ColorOfStateFalse = System.Drawing.Color.Red;
            this.threeStateioLabel1.ColorOfStateNull = System.Drawing.Color.Silver;
            this.threeStateioLabel1.ColorOfStateTrue = System.Drawing.Color.LimeGreen;
            this.threeStateioLabel1.Location = new System.Drawing.Point(99, 57);
            this.threeStateioLabel1.Name = "threeStateioLabel1";
            this.threeStateioLabel1.Shape = Levero.WinForms.Controls.ThreeStateIOShapeEnum.ArrowLeftToRight;
            this.threeStateioLabel1.Size = new System.Drawing.Size(154, 40);
            this.threeStateioLabel1.State = false;
            this.threeStateioLabel1.TabIndex = 0;
            this.threeStateioLabel1.Text = "Eingang";
            // 
            // radRectangle
            // 
            this.radRectangle.AutoSize = true;
            this.radRectangle.Location = new System.Drawing.Point(6, 22);
            this.radRectangle.Name = "radRectangle";
            this.radRectangle.Size = new System.Drawing.Size(77, 19);
            this.radRectangle.TabIndex = 1;
            this.radRectangle.Text = "Rectangle";
            this.radRectangle.UseVisualStyleBackColor = true;
            this.radRectangle.CheckedChanged += new System.EventHandler(this.radRectangle_CheckedChanged);
            // 
            // grpShape
            // 
            this.grpShape.Controls.Add(this.radArrowRightToLeft);
            this.grpShape.Controls.Add(this.radArrowLeftToRight);
            this.grpShape.Controls.Add(this.radRectangle);
            this.grpShape.Location = new System.Drawing.Point(99, 164);
            this.grpShape.Name = "grpShape";
            this.grpShape.Size = new System.Drawing.Size(115, 100);
            this.grpShape.TabIndex = 2;
            this.grpShape.TabStop = false;
            this.grpShape.Text = "Shape";
            // 
            // radArrowRightToLeft
            // 
            this.radArrowRightToLeft.AutoSize = true;
            this.radArrowRightToLeft.Location = new System.Drawing.Point(6, 72);
            this.radArrowRightToLeft.Name = "radArrowRightToLeft";
            this.radArrowRightToLeft.Size = new System.Drawing.Size(76, 19);
            this.radArrowRightToLeft.TabIndex = 3;
            this.radArrowRightToLeft.Text = "Arrow <<";
            this.radArrowRightToLeft.UseVisualStyleBackColor = true;
            this.radArrowRightToLeft.CheckedChanged += new System.EventHandler(this.radRectangle_CheckedChanged);
            // 
            // radArrowLeftToRight
            // 
            this.radArrowLeftToRight.AutoSize = true;
            this.radArrowLeftToRight.Checked = true;
            this.radArrowLeftToRight.Location = new System.Drawing.Point(6, 47);
            this.radArrowLeftToRight.Name = "radArrowLeftToRight";
            this.radArrowLeftToRight.Size = new System.Drawing.Size(76, 19);
            this.radArrowLeftToRight.TabIndex = 2;
            this.radArrowLeftToRight.TabStop = true;
            this.radArrowLeftToRight.Text = "Arrow >>";
            this.radArrowLeftToRight.UseVisualStyleBackColor = true;
            this.radArrowLeftToRight.CheckedChanged += new System.EventHandler(this.radRectangle_CheckedChanged);
            // 
            // grpState
            // 
            this.grpState.Controls.Add(this.radStateFalse);
            this.grpState.Controls.Add(this.radStateTrue);
            this.grpState.Controls.Add(this.radStateNull);
            this.grpState.Location = new System.Drawing.Point(233, 166);
            this.grpState.Name = "grpState";
            this.grpState.Size = new System.Drawing.Size(115, 100);
            this.grpState.TabIndex = 3;
            this.grpState.TabStop = false;
            this.grpState.Text = "State";
            // 
            // radStateFalse
            // 
            this.radStateFalse.AutoSize = true;
            this.radStateFalse.Location = new System.Drawing.Point(6, 72);
            this.radStateFalse.Name = "radStateFalse";
            this.radStateFalse.Size = new System.Drawing.Size(51, 19);
            this.radStateFalse.TabIndex = 3;
            this.radStateFalse.Text = "False";
            this.radStateFalse.UseVisualStyleBackColor = true;
            this.radStateFalse.CheckedChanged += new System.EventHandler(this.radRectangle_CheckedChanged);
            // 
            // radStateTrue
            // 
            this.radStateTrue.AutoSize = true;
            this.radStateTrue.Checked = true;
            this.radStateTrue.Location = new System.Drawing.Point(6, 47);
            this.radStateTrue.Name = "radStateTrue";
            this.radStateTrue.Size = new System.Drawing.Size(47, 19);
            this.radStateTrue.TabIndex = 2;
            this.radStateTrue.TabStop = true;
            this.radStateTrue.Text = "True";
            this.radStateTrue.UseVisualStyleBackColor = true;
            this.radStateTrue.CheckedChanged += new System.EventHandler(this.radRectangle_CheckedChanged);
            // 
            // radStateNull
            // 
            this.radStateNull.AutoSize = true;
            this.radStateNull.Location = new System.Drawing.Point(6, 22);
            this.radStateNull.Name = "radStateNull";
            this.radStateNull.Size = new System.Drawing.Size(47, 19);
            this.radStateNull.TabIndex = 1;
            this.radStateNull.Text = "Null";
            this.radStateNull.UseVisualStyleBackColor = true;
            this.radStateNull.CheckedChanged += new System.EventHandler(this.radRectangle_CheckedChanged);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Gold;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(73, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 140);
            this.label8.TabIndex = 15;
            this.label8.Text = "H\r\no\r\np\r\np\r\ne\r\nr";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // transparentImage1
            // 
            this.transparentImage1.CenterImage = true;
            this.transparentImage1.Image = ((System.Drawing.Image)(resources.GetObject("transparentImage1.Image")));
            this.transparentImage1.Location = new System.Drawing.Point(502, 95);
            this.transparentImage1.Name = "transparentImage1";
            this.transparentImage1.Size = new System.Drawing.Size(134, 93);
            this.transparentImage1.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.transparentImage1);
            this.Controls.Add(this.threeStateioLabel1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.grpState);
            this.Controls.Add(this.grpShape);
            this.Name = "Form1";
            this.Text = "Form1";
            this.grpShape.ResumeLayout(false);
            this.grpShape.PerformLayout();
            this.grpState.ResumeLayout(false);
            this.grpState.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Levero.WinForms.Controls.ThreeStateIOLabel threeStateioLabel1;
        private System.Windows.Forms.RadioButton radRectangle;
        private System.Windows.Forms.GroupBox grpShape;
        private System.Windows.Forms.RadioButton radArrowRightToLeft;
        private System.Windows.Forms.RadioButton radArrowLeftToRight;
        private System.Windows.Forms.GroupBox grpState;
        private System.Windows.Forms.RadioButton radStateFalse;
        private System.Windows.Forms.RadioButton radStateTrue;
        private System.Windows.Forms.RadioButton radStateNull;
        private System.Windows.Forms.Label label8;
        private Levero.WinForms.Controls.TransparentImage transparentImage1;
    }
}