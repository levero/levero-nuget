using Levero.WinForms.Controls;
using System;
using System.Windows.Forms;

namespace Test.WinForms.Controls
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void radRectangle_CheckedChanged(object sender, EventArgs e)
        {
            this.threeStateioLabel1.Shape =
                this.radArrowLeftToRight.Checked ? ThreeStateIOShapeEnum.ArrowLeftToRight :
                this.radArrowRightToLeft.Checked ? ThreeStateIOShapeEnum.ArrowRightToLeft :
                ThreeStateIOShapeEnum.Rectangle;

            this.threeStateioLabel1.State =
                this.radStateTrue.Checked ? true :
                this.radStateFalse.Checked ? false :
                null;

        }
    }
}