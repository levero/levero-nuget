﻿using Levero.ShipmentConnector;
using System;
using System.Threading.Tasks;

namespace Test.ShipmentConnector
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Test GLS Shipment Connector");
            Console.WriteLine("--------------------------------------");

            Console.WriteLine("Prepare GLS connector config");
            // test config
            //var config = new GLSConnectorConfig
            //{
            //    ContactID = "040aa2eSMm",
            //    EndpointUrl = "https://shipit-wbm-test01.gls-group.eu",
            //    Port = 8443,
            //    User = "API-Test-AT",
            //    Password = "OAGRpD7s7wKJ89RILN8a"
            //};

            // prod config
            var config = new GLSConnectorConfig
            {
                ContactID = "040a35aaci",
                EndpointUrl = "https://shipit-wbm-at01.gls-group.eu",
                Port = 8443,
                User = "0404000042",
                Password = "PrcybL3AnM7cKh7568D7"
            };

            // create GLS connector instance
            Console.WriteLine("Create GLS connector instance");
            var gls = new GLSConnector(config, message => LogInfo(message));

            Console.WriteLine("Register shipment");
            var shipment = new GLSShipmentItem
            {
                ShipmentReferenceID = "LS2110017",
                ShippingDate = DateTime.Now,
                Consignee = new Consignee
                {
                    ConsigneeReferenceID = "2801717",
                    Name = "Testi Test",
                    CountryCode = "AT",
                    ZIPCode = "3830",
                    City = "Waidhofen an der Thaya",
                    Street = "Lindenhofstraße 10"
                },
                ShipmentUnits = new ShipmentUnit[1]
                {
                    new ShipmentUnit{ ShipmentUnitReferenceID = "P1", Weight = 18.7m }
                },
                PrintingOptions = new PrintingOptions
                {
                    TemplateSet = "ZPL_200",
                    LabelFormat = "ZEBRA"
                }
            };

            try
            {
                //var result = await gls.RegisterShipment(shipment);
                // Z5QZLQXG|Z5QZLQXH
                var result = await gls.CancelShipment("Z5QZLQXH");

                //Console.WriteLine("Created parcels:");
                //foreach (var r in result.CreatedShipment.ParcelData)
                //    Console.WriteLine(r.TrackID);
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
            }
            finally
            {
                // Console.ReadKey();
            }
        }

        static void LogInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        static void LogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
