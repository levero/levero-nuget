﻿using Levero.ALM.Build;
using Levero.ALM.Deploy;
using Levero.ALM.Offline;
using Levero.ALM.Update;
using Levero.Persistence.DAL;
using System;

namespace Test.Deployment
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                //// Test zip
                //var temp = Path.GetTempFileName();
                //var s = "absölkwöero öasfjöaljeröwij rösaljf öalskjfölasjfrw3orjaölsjflaskjföowiruQWÖIJFASL:KDFJASLFJASLK:FJ OAWEIUR PQOU RJÖWLSJKAF:LAWEJR QLRJQ ÖJFDSLFJWÖOFSAÖOFDJASLFJSAOIÖRE UÖAOWJR Öqu öpsojf öwoiufjlskjdfl saijueföUWCÖouecöQM UFoöwief hlowfeh asoölufcöwpvuxqp,WEOUIc qörifmwALOIRXUqöoxeu QOWCUAWLCUF AOWLSIREUFAWLOIEF JSYLFJ WÖOACRMAÖOUXJabsölkwöero öasfjöaljeröwij rösaljf öalskjfölasjfrw3orjaölsjflaskjföowiruQWÖIJFASL:KDFJASLFJASLK:FJ OAWEIUR PQOU RJÖWLSJKAF:LAWEJR QLRJQ ÖJFDSLFJWÖOFSAÖOFDJASLFJSAOIÖRE UÖAOWJR Öqu öpsojf öwoiufjlskjdfl saijueföUWCÖouecöQM UFoöwief hlowfeh asoölufcöwpvuxqp,WEOUIc qörifmwALOIRXUqöoxeu QOWCUAWLCUF AOWLSIREUFAWLOIEF JSYLFJ WÖOACRMAÖOUXJabsölkwöero öasfjöaljeröwij rösaljf öalskjfölasjfrw3orjaölsjflaskjföowiruQWÖIJFASL:KDFJASLFJASLK:FJ OAWEIUR PQOU RJÖWLSJKAF:LAWEJR QLRJQ ÖJFDSLFJWÖOFSAÖOFDJASLFJSAOIÖRE UÖAOWJR Öqu öpsojf öwoiufjlskjdfl saijueföUWCÖouecöQM UFoöwief hlowfeh asoölufcöwpvuxqp,WEOUIc qörifmwALOIRXUqöoxeu QOWCUAWLCUF AOWLSIREUFAWLOIEF JSYLFJ WÖOACRMAÖOUXJabsölkwöero öasfjöaljeröwij rösaljf öalskjfölasjfrw3orjaölsjflaskjföowiruQWÖIJFASL:KDFJASLFJASLK:FJ OAWEIUR PQOU RJÖWLSJKAF:LAWEJR QLRJQ ÖJFDSLFJWÖOFSAÖOFDJASLFJSAOIÖRE UÖAOWJR Öqu öpsojf öwoiufjlskjdfl saijueföUWCÖouecöQM UFoöwief hlowfeh asoölufcöwpvuxqp,WEOUIc qörifmwALOIRXUqöoxeu QOWCUAWLCUF AOWLSIREUFAWLOIEF JSYLFJ WÖOACRMAÖOUXJabsölkwöero öasfjöaljeröwij rösaljf öalskjfölasjfrw3orjaölsjflaskjföowiruQWÖIJFASL:KDFJASLFJASLK:FJ OAWEIUR PQOU RJÖWLSJKAF:LAWEJR QLRJQ ÖJFDSLFJWÖOFSAÖOFDJASLFJSAOIÖRE UÖAOWJR Öqu öpsojf öwoiufjlskjdfl saijueföUWCÖouecöQM UFoöwief hlowfeh asoölufcöwpvuxqp,WEOUIc qörifmwALOIRXUqöoxeu QOWCUAWLCUF AOWLSIREUFAWLOIEF JSYLFJ WÖOACRMAÖOUXJabsölkwöero öasfjöaljeröwij rösaljf öalskjfölasjfrw3orjaölsjflaskjföowiruQWÖIJFASL:KDFJASLFJASLK:FJ OAWEIUR PQOU RJÖWLSJKAF:LAWEJR QLRJQ ÖJFDSLFJWÖOFSAÖOFDJASLFJSAOIÖRE UÖAOWJR Öqu öpsojf öwoiufjlskjdfl saijueföUWCÖouecöQM UFoöwief hlowfeh asoölufcöwpvuxqp,WEOUIc qörifmwALOIRXUqöoxeu QOWCUAWLCUF AOWLSIREUFAWLOIEF JSYLFJ WÖOACRMAÖOUXJabsölkwöero öasfjöaljeröwij rösaljf öalskjfölasjfrw3orjaölsjflaskjföowiruQWÖIJFASL:KDFJASLFJASLK:FJ OAWEIUR PQOU RJÖWLSJKAF:LAWEJR QLRJQ ÖJFDSLFJWÖOFSAÖOFDJASLFJSAOIÖRE UÖAOWJR Öqu öpsojf öwoiufjlskjdfl saijueföUWCÖouecöQM UFoöwief hlowfeh asoölufcöwpvuxqp,WEOUIc qörifmwALOIRXUqöoxeu QOWCUAWLCUF AOWLSIREUFAWLOIEF JSYLFJ WÖOACRMAÖOUXJ";
                //var content = Encoding.UTF8.GetBytes(s);
                //File.WriteAllBytes(temp, content);

                //var zipped = Compression.ZipToBytes(temp);
                //Console.WriteLine($"zipped from {content.Length} to {zipped.Length} bytes");

                //var unzipped = Compression.UnzipToBytes(zipped);
                //if (unzipped.Length != content.Length)
                //    Console.WriteLine("unzipped length is not correct!");
                //else
                //{
                //    var a = Encoding.UTF8.GetString(unzipped);
                //    if (a!=s)
                //        Console.WriteLine("unzipped content is not correct");
                //    else
                //        Console.WriteLine("zipping WORKS!!");
                //}
                //return;

                BaseDataLayer.Init(new SqlDataLayer(Properties.Settings.Default.Dev));

                // Create offline package
                //var buildDescription = new BuildDescription
                //{
                //    AppPackageID = "te-capex-fc",
                //    OfflinePackageStorages = new string[] {
                //        "\\\\MOND\\LeveroAppPackages\\te"}
                //};
                //var oap = new OfflineAppPackage(buildDescription)
                //{
                //    Logger = msg => Console.WriteLine(msg),
                //    UserID = "SYSTEM"
                //};

                //oap.Export(new Version(1, 13, 0));
                // import offline package
                // oap.Import(new System.IO.FileInfo(@"\\MOND\LeveroAppPackages\te\te-capex-fc_4295819264_1.13.0.zip"));

                // -------------------------
                // Auto import offline packages
                // -------------------------
                //var importService = new OfflineAppPackageImportService(new System.IO.DirectoryInfo(@"\\MOND\LeveroAppPackages\te"), true)
                //{
                //    Logger = msg => Console.WriteLine(msg),
                //    UserID = "SYSTEM"
                //};

                //importService.Run();

                //while (Console.Read() != 'q') ;

                //BaseDataLayer.Init(new SqlDataLayer(Properties.Settings.Default.AT708DB01));

                //// build package
                //Console.WriteLine("BUILD---------------------------------------------");
                //Console.WriteLine("init...");
                //var bap = new BuildAppPackage
                //{
                //    Logger = msg => Console.WriteLine(msg),
                //    UserID = "SYSTEM"
                //};

                //Console.WriteLine("validate...");
                //if (!bap.ValidateBuild())
                //    return;

                //Console.WriteLine("build...");
                //bap.BuildAndUpload();



                //Console.WriteLine();
                //Console.WriteLine("RELEASE---------------------------------------------");
                //Console.WriteLine("init...");
                //var rap = new ReleaseAppPackage(bap.Package.AppPackageID, bap.AppPackageVersionID.Value)
                //{
                //    Logger = msg => Console.WriteLine(msg),
                //    UserID = "SYSTEM"
                //};

                //Console.WriteLine("release channel Beta...");
                //rap.Release("Beta");
                //Console.WriteLine($" -> released.");




                Console.WriteLine();
                Console.WriteLine("DOWNLOAD---------------------------------------------");
                Console.WriteLine("init...");
                //var dap = new DeployAppPackage("SRV-APP-1-backend-Dev")
                var dap = new DeployAppPackage("SRV-APP-1-te-capex-fc-Dev")
                {
                    Logger = msg => Console.WriteLine(msg),
                    UserID = "SYSTEM"
                };

                Console.WriteLine("download...");
                var info = dap.Download();
                if (info == null)
                    Console.WriteLine(" -> no update available");
                else
                    Console.WriteLine($" -> update V{info.AppPackageVersion} available!");


                if (info == null) return;




                Console.WriteLine();
                Console.WriteLine("UPDATE---------------------------------------------");
                Console.WriteLine("init...");
                var uap = new UpdateAppPackage(info, info.UpdateFolder)
                {
                    Logger = msg => Console.WriteLine(msg),
                    UserID = "SYSTEM"
                };

                Console.WriteLine("update...");
                uap.Update();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.ResetColor();
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
