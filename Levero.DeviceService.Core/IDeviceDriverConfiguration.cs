﻿using Microsoft.Extensions.DependencyInjection;

namespace Levero.DeviceService
{
    public interface IDeviceDriverConfiguration
    {
        void Configure(IServiceCollection services);
    }
}
