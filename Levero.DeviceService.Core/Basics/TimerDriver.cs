﻿using Levero.DeviceService;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

[assembly: DeviceDriver("timer", typeof(LeveroApps.DeviceService.Basics.TimerDriver), true)]

namespace LeveroApps.DeviceService.Basics
{
    public class TimerDriver : BaseDeviceDriver
    {
        public TimerDriver(ILogger<TimerDriver> _log)
        {
            this.log = _log;
        }

        public const string TimerNextDay = "nextDay";

        private readonly ILogger log;
        private readonly List<int> timers = new List<int>();
        private bool timerNextDay = false;
        private DateTime? lastTimer;
        private Timer? everySecondTimer;

        public override Task InitAndRun(DeviceConfiguration config, CancellationToken cancellationToken = default)
        {
            // get options
            foreach (var timer in config.Port.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (String.Compare(timer, TimerNextDay, true) == 0)
                    this.timerNextDay = true;
                else if (Int32.TryParse(timer, out var seconds) && seconds >= 0)
                    this.timers.Add(seconds);
                else
                    throw new Exception($"Timer '{timer}' is not valid! must be seconds or '{TimerNextDay}'.");
            }

            // start timer
            this.everySecondTimer = new Timer(this.EverySecond, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));

            return Task.CompletedTask;
        }

        public void EverySecond(object? state)
        {
            var now = DateTime.Now;
            var seconds = (int)now.Subtract(now.Date).TotalSeconds;


            // check second-based-timers
            foreach (var timer in this.timers)
                if (seconds % timer == 0)
                {
                    this.log.LogDebug($"Timer based on seconds: {timer}s");
                    this.IpcSpread(
                        new Dictionary<string, object?>
                        {
                            { "timer", timer.ToString() },
                            { "seconds", timer }
                        },
                        TimeSpan.FromSeconds(timer - 1));
                }

            // check nextDay-timer
            if (this.timerNextDay && this.lastTimer.HasValue && this.lastTimer.Value.Date != now.Date)
            {
                this.log.LogDebug($"Timer next day");
                this.IpcSpread(new Dictionary<string, object?> { { "timer", TimerNextDay } });
            }

            // remember last timer event
            this.lastTimer = now;
        }

        public override async Task Shutdown(CancellationToken cancellationToken = default)
        {
            if (this.everySecondTimer != null)
                await this.everySecondTimer.DisposeAsync();
        }
    }
}
