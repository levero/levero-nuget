﻿using Levero.DeviceService.Ipc;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.DeviceService
{
    public interface IDeviceDriver
    {
        /// <summary>
        /// will be set directly after constructor call
        /// </summary>
        string DeviceName { get; set; }

        /// <summary>
        /// will be set directly after constructor call
        /// </summary>
        IIpcChannel Channel { get; set; }

        Task InitAndRun(DeviceConfiguration config, CancellationToken cancellationToken = default);
        Task Shutdown(CancellationToken cancellationToken = default);
    }
}
