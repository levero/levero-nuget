﻿using Levero.DeviceService.Ipc;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.DeviceService
{
    public abstract class BaseDeviceDriver : IDeviceDriver
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        // will be set directly after constructor call
        public string DeviceName { get; set; }
        public IIpcChannel Channel { get; set; }
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.

        public virtual Task InitAndRun(DeviceConfiguration config, CancellationToken cancellationToken = default)
            => Task.CompletedTask;

        public virtual Task Shutdown(CancellationToken cancellationToken = default)
            => Task.CompletedTask;
    }
}
