﻿namespace Levero.DeviceService
{
    public sealed class DeviceServiceConfiguration
    {
        public DeviceConfiguration? Ipc { get; set; }

        public DeviceConfiguration[]? Devices { get; set; }
    }
}
