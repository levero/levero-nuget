﻿using System;
using System.Collections.Generic;

namespace Levero.DeviceService
{
    public sealed class DeviceConfiguration
    {
        private const string constInvalid = "<undefined>";

        public string Name { get; set; } = constInvalid;

        public string Driver { get; set; } = constInvalid;

        public string Port { get; set; } = constInvalid;

        public Dictionary<string, string> Options { get; } = new Dictionary<string, string>();

        internal void Validate()
        {
            if (this.Driver.NullIf(constInvalid).NullIfWhitespace() == null)
                throw new ArgumentNullException($"Configuration of device {this.Name} is invalid: {nameof(this.Driver)} is empty!");
            if (this.Port.NullIf(constInvalid).NullIfWhitespace() == null)
                throw new ArgumentNullException($"Configuration of device {this.Name} is invalid: {nameof(this.Port)} is empty!");
        }
    }
}
