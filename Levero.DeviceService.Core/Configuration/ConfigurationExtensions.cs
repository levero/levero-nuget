﻿using Levero.Common.Services;
using System.Collections.Generic;

namespace Microsoft.Extensions.Configuration
{
    public static class ConfigurationExtensions
    {
        public const string DeviceService_ServiceName = "LeveroDeviceService";
        public const string DeviceService_ServiceDisplayName = "Levero Device Service";
        public const string DeviceService_ServiceDescription = "Handles devices for LeveroApps (for example RFID, Barcodescanner)";

        public static IConfigurationBuilder AddDeviceService_WindowsServiceConfiguration(this IConfigurationBuilder builder)
            => builder.AddInMemoryCollection(new Dictionary<string, string?>
            {
                { $"{nameof(WindowsServiceConfiguration)}:{DeviceService_ServiceName}:DisplayName", DeviceService_ServiceDisplayName },
                { $"{nameof(WindowsServiceConfiguration)}:{DeviceService_ServiceName}:Description", DeviceService_ServiceDescription }
            });
    }
}
