﻿using System;

namespace Levero.DeviceService
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public sealed class DeviceDriverAttribute : Attribute
    {
        public DeviceDriverAttribute(string name, Type driverClass, bool isSingleton = false)
        {
            this.Name = name;
            this.DriverClass = driverClass;
            this.IsSingleton = isSingleton;
        }

        public DeviceDriverAttribute(string name, Type driverClass, Type configurationClass, bool isSingleton = false)
        {
            this.Name = name;
            this.DriverClass = driverClass;
            this.ConfigurationClass = configurationClass;
            this.IsSingleton = isSingleton;
        }

        public string Name { get; private set; }
        public Type DriverClass { get; private set; }
        public Type? ConfigurationClass { get; private set; }
        public bool IsSingleton { get; private set; }
    }
}
