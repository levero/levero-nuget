﻿namespace Levero.DeviceService.Ipc
{
    public enum IpcState
    {
        Disconnected,
        Connected
    }
}