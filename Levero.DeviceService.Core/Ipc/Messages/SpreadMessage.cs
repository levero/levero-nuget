﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static Levero.Common.NullableHelper;

namespace Levero.DeviceService.Ipc
{
    [JsonObject]
    public sealed class SpreadMessage
    {
        public SpreadMessage()
        {
            // needed for JsonConvert
        }

        public SpreadMessage(string sender, Dictionary<string, object?> data, string? receiver = null, TimeSpan? ttl = null, string? token = null)
        {
            this.Sender = sender;
            this.Timestamp = DateTime.Now;
            this.TimeToLive = ttl;
            this.Receiver = receiver;
            this.Data = data ?? this.Data;
            this.Token = token;
        }

        public SpreadMessage(string sender, string? receiver = null, TimeSpan? ttl = null, string? token = null)
        {
            this.Sender = sender;
            this.Timestamp = DateTime.Now;
            this.TimeToLive = ttl;
            this.Receiver = receiver;
            this.Token = token;
        }

        [JsonProperty(PropertyName = "sender", Required = Required.Always)]
        public string Sender { get; set; } = NOT_INITIALIZED;

        [JsonProperty(PropertyName = "timestamp", Required = Required.Always)]
        public DateTime Timestamp { get; set; } = DateTime.Now;

        [JsonIgnore]
        public TimeSpan? TimeToLive { get; set; }

        [JsonProperty(PropertyName = "receiver", Required = Required.Default)]
        public string? Receiver { get; set; }

        [JsonProperty(PropertyName = "data", Required = Required.Always)]
        public Dictionary<string, object?> Data { get; private set; } = new Dictionary<string, object?>();

        [JsonProperty(PropertyName = "token", Required = Required.Default)]
        public string? Token { get; set; }
    }
}
