﻿using Newtonsoft.Json;

namespace Levero.DeviceService.Ipc
{
    [JsonObject]
    public class ConnectionStateMessage
    {
        [JsonProperty(PropertyName = "clients", Required = Required.Always)]
        public string[] Clients { get; set; } = { };
    }
}
