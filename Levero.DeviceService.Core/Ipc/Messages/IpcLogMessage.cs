﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Levero.DeviceService.Ipc
{
    public class IpcLogMessage : IpcBaseMessage
    {
        public static IpcLogMessage Create(LogLevel level, string deviceName, string message)
        {
            return new IpcLogMessage
            {
                LogText = $"{Environment.MachineName}:{Process.GetCurrentProcess().Id} - {deviceName} - {level}:  {message}",
                LogLevel = (int)level
            };
        }

        public IpcLogMessage(Dictionary<string, object?>? messageData = null)
            : base(messageData)
        {
        }

        public const string propertyNameLogText = "log";
        public const string propertyNameLogLevel = "level";

        public string LogText
        {
            get => (string)this.MessageData.TryGetValue(propertyNameLogText)!;
            set => this.MessageData[propertyNameLogText] = value;
        }

        public int LogLevel
        {
            get => (int)this.MessageData.TryGetValue(propertyNameLogLevel)!;
            set => this.MessageData[propertyNameLogLevel] = value;
        }
    }
}
