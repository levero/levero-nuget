﻿using System;
using System.Collections.Generic;

namespace Levero.DeviceService.Ipc
{
    public class IpcCodeMessage : IpcBaseMessage
    {
        public static IpcCodeMessage Create(string? code)
            => new IpcCodeMessage { Code = code };

        public IpcCodeMessage(Dictionary<string, object?>? messageData = null)
            : base(messageData)
        {
        }

        public const string propertyNameCode = "code";

        public string? Code
        {
            get => (string)this.MessageData.TryGetValue(propertyNameCode)!;
            set => this.MessageData[propertyNameCode] = value;
        }
    }
}
