﻿using System.Collections.Generic;

namespace Levero.DeviceService.Ipc
{
    public abstract class IpcBaseMessage
    {
        public IpcBaseMessage(Dictionary<string, object?>? messageData = null)
        {
            this.MessageData = messageData ?? new Dictionary<string, object?>();
        }

        public readonly Dictionary<string, object?> MessageData;
    }
}
