﻿using Levero.DeviceService;
using Levero.DeviceService.Ipc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

[assembly: DeviceDriver(LocalIpcChannel.LocalIpcDriverName, typeof(LocalIpcChannel))]

namespace Levero.DeviceService.Ipc
{
    public sealed class LocalIpcChannel : BaseDeviceDriver, IIpcChannel
    {
        // constants
        public const string LocalIpcDriverName = "<local-ipc>";

        // fields
        private readonly List<string> deviceNames = new List<string>();

        // properties
        public string? GroupName { get; private set; }
        public IpcState State => IpcState.Connected;


        // events
        public event Func<ConnectionStateMessage, Task>? ConnectionStateReceived;
        public event Func<SpreadMessage, Task>? SpreadReceived;

        #region Init and shutdown

        public override Task InitAndRun(DeviceConfiguration config, CancellationToken cancellationToken)
        {
            // LocalIpcChannel is only called if config is null!!!
            this.GroupName = "::1";

            // register dummy device "log"
            return this.RegisterDevice("log");
        }

        public override Task Shutdown(CancellationToken cancellationToken)
            // nothing to do...
            => Task.CompletedTask;

        #endregion

        #region Communication methods for devices

        public Task RegisterDevice(string deviceName)
        {
            // Register call is only needed once
            if (!this.deviceNames.Contains(deviceName))
            {
                this.deviceNames.Add(deviceName);

                // raise ConnectionState event
                this.OnConnectionState(new ConnectionStateMessage { Clients = this.deviceNames.ToArray() });
            }
            return Task.CompletedTask;
        }

        public Task UnregisterDevice(string deviceName)
        {
            // Register call is only needed once
            if (this.deviceNames.Contains(deviceName))
            {
                this.deviceNames.Remove(deviceName);

                // raise ConnectionStateReceived event
                this.OnConnectionState(new ConnectionStateMessage { Clients = this.deviceNames.ToArray() });
            }
            return Task.CompletedTask;
        }

        public Task Spread(SpreadMessage msg)
        {
            _ = msg ?? throw new ArgumentNullException(nameof(msg));

            msg.Timestamp = DateTime.Now;
            // do not wait...
            this.SpreadReceived?.Invoke(msg);

            return Task.CompletedTask;
        }

        public Task<string> RegisterWebhook(string appID, string tenantID, string sender, string? receiver, string? previousToken, bool useBodyAsMessage)
            => throw new NotImplementedException("local ipc cannot create web hook callbacks");

        public Task<string> GetMyIPv4()
            => Task.FromResult((Common.Network.NetworkInfo.GetOwnIPv4() ?? System.Net.IPAddress.Loopback).ToString());

        #endregion

        #region Receiving Endpoints

        private void OnConnectionState(ConnectionStateMessage state)
        {
            // shall we wait??? waiting blocks signalR...
            this.ConnectionStateReceived?.Invoke(state);
        }

        #endregion
    }
}