﻿using System;
using System.Threading.Tasks;

namespace Levero.DeviceService.Ipc
{
    public interface IIpcChannel : IDeviceDriver
    {
        /// <summary>
        /// at client side GroupName may be null, at server side the IPv4 is taken as default value of GroupName
        /// </summary>
        string? GroupName { get; }
        IpcState State { get; }

        // no CancellationToken because it will be called asynchronly without control
        event Func<ConnectionStateMessage, Task> ConnectionStateReceived;
        event Func<SpreadMessage, Task> SpreadReceived;

        Task RegisterDevice(string deviceName);
        Task UnregisterDevice(string deviceName);
        Task Spread(SpreadMessage msg);
        Task<string> RegisterWebhook(string appID, string tenantID, string sender, string? receiver, string? previousToken, bool useBodyAsMessage);
        Task<string> GetMyIPv4();
    }
}