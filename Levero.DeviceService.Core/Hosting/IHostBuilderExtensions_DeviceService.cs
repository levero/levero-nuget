﻿using Levero.Common.CommandLine;
using Levero.DeviceService;
using Levero.DeviceService.Ipc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Reflection;

namespace Levero.Common.Services
{
    public static class IHostBuilderExtensions_DeviceService
    {
        public static IHostBuilder UseDeviceService(this IHostBuilder builder)
            => builder.ConfigureServices((context, services) =>
            {
                var appInfo = builder.GetAppInfo();
                var log = builder.GetInitialLogger("Levero.Common.Services.DeviceDriverLoader");

                // load all from current assembly
                LoadDeviceDriver(services, log, typeof(DeviceHost).Assembly);

                // load from entry assembly
                if (Assembly.GetEntryAssembly() != null)
                    LoadDeviceDriver(services, log, Assembly.GetEntryAssembly()!);

                // load all drivers
                foreach (var file in appInfo.AppPath.GetFiles("*-driver.dll"))
                    LoadDeviceDriver(services, log, Assembly.LoadFrom(file.FullName));

                // configure DeviceServiceConfiguration
                services.Configure<DeviceServiceConfiguration>(context.Configuration.GetSection("device-service"));

                // add default IPC
                services.Configure<DeviceServiceConfiguration>(config =>
                {
                    config.Ipc ??= new DeviceConfiguration { Driver = LocalIpcChannel.LocalIpcDriverName, Port = "<not used>" };
                    config.Ipc.Name = LeveroDeviceService.DeviceNameIpc;

                    // validate
                    config.Ipc.Validate();
                    foreach (var device in config.Devices ?? throw new ArgumentNullException($"Configuration of devices is invalid: no devices found!"))
                        device.Validate();
                });

                // add Levero device service twice...
                services.AddSingleton<LeveroDeviceService>();
                // add default command to start and stop devices
                services.AddCommandLineCommand(
                    provider => provider.GetRequiredService<LeveroDeviceService>());

                // add factory for getting ipc channel
                services.AddTransient(
                    provider => provider.GetRequiredService<LeveroDeviceService>().Channel);

                // add factory for getting devices
                services.AddTransient(
                    provider => provider.GetRequiredService<LeveroDeviceService>().Devices);
            });

        private static void LoadDeviceDriver(IServiceCollection services, ILogger log, Assembly asm)
        {
            var drivers = asm.GetCustomAttributes<DeviceDriverAttribute>();
            foreach (var attr in drivers)
            {
                log.LogInformation($"load driver {attr.Name} [V{asm.GetName().Version?.ToString(3) ?? "?.?.?"}]...");

                var interfaces = attr.DriverClass.GetInterfaces();
                if (!interfaces.Any(it => it == typeof(IDeviceDriver)))
                    throw new Exception($"Device driver class {attr.DriverClass} from assembly {asm.GetName().Name} does not implement {nameof(IDeviceDriver)}!");

                // register device driver description
                services.AddSingleton(attr);

                // register device driver
                services.AddTransient(attr.DriverClass);

                if (attr.ConfigurationClass != null)
                {
                    var configClass = Activator.CreateInstance(attr.ConfigurationClass) as IDeviceDriverConfiguration;
                    _ = configClass ?? throw new Exception($"Device driver configuration class {attr.ConfigurationClass} from assembly {asm.GetName().Name} does not implement {nameof(IDeviceDriverConfiguration)}!");
                    configClass.Configure(services);
                }
            }
        }
    }
}
