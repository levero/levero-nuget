﻿using Levero.Common;
using Levero.Common.CommandLine;
using Levero.DeviceService.Ipc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.DeviceService
{
    internal partial class LeveroDeviceService : CommandLineCommand
    {
        public LeveroDeviceService(IHost _host, ILogger<LeveroDeviceService> _logger, AppInfo _appInfo,
            IEnumerable<DeviceDriverAttribute> _drivers,
            IOptions<DeviceServiceConfiguration> _config)
            : base(CommonCommands.DEFAULT, Array.Empty<string>(),
                  "loads and runs devices which are configured in configuration file 'device-configuration.json'.")
        {
            this.host = _host;
            this.log = _logger;
            this.appInfo = _appInfo;
            this.drivers = _drivers.ToArray();
            this.config = _config.Value;
            this.devices = new Dictionary<string, IDeviceDriver>();

            // do not exit after Startup!
            base.DoNotExit = true;

            this.log.LogDebug("LeveroDeviceService is created.");
        }

        public const string DeviceNameIpc = "ipc";

        private readonly IHost host;
        private readonly ILogger log;
        private readonly AppInfo appInfo;
        private readonly DeviceServiceConfiguration config;
        private readonly DeviceDriverAttribute[] drivers;
        private IIpcChannel? _channel;
        private readonly Dictionary<string, IDeviceDriver> devices;

        public IIpcChannel Channel
            => this._channel ?? throw new Exception($"{nameof(LeveroDeviceService)}.{nameof(this.Channel)} cannot be used now because IPC channel is not yet created!");

        public IReadOnlyDictionary<string, IDeviceDriver> Devices
            => this.devices;

        public override Task<int> Validator(CommandLineContext context, CancellationToken cancellationToken = default)
        {
            this.log.LogDebug($"{nameof(LeveroDeviceService)} is validating...");

            // Load IPC channel type
            // configuration has been validated in IHostBuilderExtensions_DeviceDriverLoader.RunDeviceLoader
            if (!this.InitDevice<IIpcChannel>(
                 context,
                 this.config.Ipc!,
                 out var ipcdevice))
                throw new Exception($"{nameof(LeveroDeviceService)}: initializing IPC channel failed!");
            this._channel = ipcdevice;

            // load all other devices 
            this.config.Devices
                .ForEach(deviceConfig =>
                {
                    if (this.InitDevice<IDeviceDriver>(
                        context,
                        deviceConfig,
                        out var device))
                        this.devices.Add(deviceConfig.Name, device);
                });

            this.log.LogInformation($"{nameof(LeveroDeviceService)} validated successfully.");
            return Task.FromResult(CommonCommands.SUCCESS);
        }

        private bool InitDevice<TInterface>(CommandLineContext context, DeviceConfiguration deviceConfig, [MaybeNullWhen(false)] out TInterface device)
            where TInterface : class, IDeviceDriver
        {
            device = default;

            // search for driver
            var driverName = deviceConfig.Driver!;
            this.log.LogInformation($"load driver {driverName} for device {deviceConfig.Name}");
            var driver = this.drivers.FirstOrDefault(it => String.Compare(it.Name, driverName, true) == 0);
            if (driver == null)
            {
                this.log.LogWarning($"Driver {driverName} not found!");
                context.WriteLine($"Driver {driverName} not found!");
                return false;
            }

            // validate singletons
            if (driver.IsSingleton && String.Compare(deviceConfig.Name, driverName, true) != 0)
            {
                this.log.LogWarning($"Device {deviceConfig.Name} must have same name as singleton device driver {driverName}!");
                context.WriteLine($"Device {deviceConfig.Name} must have same name as singleton device driver {driverName}!");
                return false;
            }

            // create instance of driver
            device = this.host.Services.GetService(driver.DriverClass) as TInterface;
            if (device == null)
            {
                this.log.LogWarning($"Driver {driverName} does not implement interface {nameof(TInterface)}!");
                context.WriteLine($"Driver {driverName} does not implement interface {nameof(TInterface)}!");
                return false;
            }
            device.DeviceName = deviceConfig.Name;
            device.Channel = this._channel ?? (IIpcChannel)device;

            // successful initiated
            return true;
        }

        public override async Task<int> Handler(CommandLineContext context, CancellationToken cancellationToken = default)
        {
            this.log.LogDebug($"{nameof(LeveroDeviceService)} is starting...");
            try
            {
                // register stop
                context.StopHandler = this.StopDevices;

                // Load IPC channel type
                // configuration has been validated in IHostBuilderExtensions_DeviceDriverLoader.RunDeviceLoader
                await this.RunDevice(
                    context,
                    this.config.Ipc!,
                    this._channel!,
                    cancellationToken);
                // wait until InitAndRun is ready

                // load all other devices (do not wait for end of InitAndRun of each own)
                foreach (var deviceConfig in this.config.Devices!)
                    if (this.devices.TryGetValue(deviceConfig.Name, out var device))
                        await this.RunDevice(
                            context,
                            deviceConfig,
                            device,
                            cancellationToken);

                this.log.LogInformation($"{nameof(LeveroDeviceService)} started successfully.");
                return 0;
            }
            catch (Exception ex)
            {
                this.log.LogError(ex, $"{nameof(LeveroDeviceService)} failed starting!");
                throw;
            }
        }

        private async Task RunDevice<TInterface>(CommandLineContext context, DeviceConfiguration deviceConfig,
            TInterface device, CancellationToken cancellationToken)
            where TInterface : class, IDeviceDriver
        {
            this.log.LogInformation($"start device {deviceConfig.Name} / class {device.GetType().FullName}");

            // init and start device
            context.Write1Block(0, $"{deviceConfig.Name}: starting driver {deviceConfig.Driver}...");
            await device.InitAndRun(deviceConfig, cancellationToken);
            context.Write1Block(0, $"{deviceConfig.Name}: started.");

            // register device in channel
            await device.Channel.RegisterDevice(device.DeviceName);
        }

        public async Task StopDevices(CommandLineContext context, CancellationToken cancellationToken)
        {
            this.log.LogDebug($"{nameof(LeveroDeviceService)}.{nameof(StopDevices)} is called.");
            try
            {
                // shutdown all devices
                await Task.WhenAll(this.devices.ToArray().Select(device =>
                {
                    this.log.LogInformation($"stop device {device.Key}...");
                    this.devices.Remove(device.Key);

                    // unregister device in channel
                    device.Value.Channel.UnregisterDevice(device.Value.DeviceName);

                    // shutdown device
                    return device.Value.Shutdown(cancellationToken);
                }));

                // shutdown ipc 
                if (this._channel != null)
                {
                    this.log.LogInformation($"stop device {this._channel.DeviceName}...");
                    await this._channel.Shutdown(cancellationToken);
                    this._channel = null;
                }

                this.log.LogInformation($"{nameof(LeveroDeviceService)}.{nameof(StopDevices)} was successful.");
            }
            catch (Exception ex)
            {
                this.log.LogError(ex, $"{nameof(LeveroDeviceService)}.{nameof(StopDevices)} failed.");
                throw;
            }
        }
    }
}
