﻿using Levero.Common;
using Levero.Common.CommandLine;
using Levero.Common.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;
using System;
using System.Runtime.InteropServices;

namespace Levero.DeviceService
{
    public static class DeviceHost
    {
        public static IHostBuilder CreateBuilder(
            Action<IConfigurationBuilder>? hostConfig = null,
            Action<HostBuilderContext, IConfigurationBuilder>? appConfig = null,
            Action<HostBuilderContext, ILoggingBuilder>? configLogging = null,
            Action<HostBuilderContext, IServiceCollection>? serviceConfig = null,
            Action<HostBuilder>? addonBuilder = null)
        {
            // prepare appInfo
            var loggerFactoryForInit = LoggerFactory.Create(cfg => cfg.AddEventSourceLogger());
            var appInfo = new AppInfo(loggerFactoryForInit.CreateLogger<AppInfo>());

            // there are 3 options:
            //  o start as windows service
            //  o start as console app
            //  o start as console app with arguments -> do setup...

            // setup host with default settings
            // Host.CreateDefaultBuilder is defined in https://github.com/dotnet/runtime/blob/master/src/libraries/Microsoft.Extensions.Hosting/src/Host.cs
            var builder = new HostBuilder();

            builder.UseEnvironment(appInfo.IsRunningAsWindowsService ? "service" : "")

                .UseContentRoot(appInfo.AppPath.FullName)

                .ConfigureHostConfiguration(config =>
                {
                    // set properties
                    builder.Properties[typeof(ILoggerFactory).FullName!] = loggerFactoryForInit;
                    builder.Properties[typeof(AppInfo).FullName!] = appInfo;

                    config.AddEnvironmentVariables(prefix: "DOTNET_");
                    //if (args != null)
                    //    config.AddCommandLine(args);

                    config.SetBasePath(appInfo.AppPath.FullName);

                    hostConfig?.Invoke(config);
                })

                .ConfigureAppConfiguration((context, config) =>
                {
                    var env = context.HostingEnvironment;

                    var reloadOnChange = context.Configuration.GetValue("hostBuilder:reloadConfigOnChange", defaultValue: true);

                    // add default device service config
                    config.AddDeviceService_WindowsServiceConfiguration();

                    // add app settings
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: reloadOnChange)
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: reloadOnChange);

                    //if (env.IsDevelopment() && !string.IsNullOrEmpty(env.ApplicationName))
                    //{
                    //    var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                    //    if (appAssembly != null)
                    //        config.AddUserSecrets(appAssembly, optional: true);
                    //}
                    //
                    //config.AddEnvironmentVariables();
                    //
                    //if (args != null)
                    //    config.AddCommandLine(args);

                    appConfig?.Invoke(context, config);
                })

                .ConfigureLogging((hostingContext, logging) =>
                {
                    var isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

                    // IMPORTANT: This needs to be added *before* configuration is loaded, this lets
                    // the defaults be overridden by the configuration.
                    if (isWindows && appInfo.IsRunningAsWindowsService)
                        // Default the EventLogLoggerProvider to warning or above
                        logging.AddFilter<EventLogLoggerProvider>(level => level >= LogLevel.Warning);

                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));

                    // always add event source logger
                    logging.AddEventSourceLogger();

                    if (isWindows && appInfo.IsRunningAsWindowsService)
                        // Add the EventLogLoggerProvider on windows machines (and if running as windows service)
                        logging.AddEventLog();

                    configLogging?.Invoke(hostingContext, logging);
                })

                .ConfigureServices((context, services) =>
                {
                    appInfo.AttachValuesFromConfiguration(context.Configuration.GetSection("AppInfo").Get<ConfigurableAppInfo>());
                    services.AddSingleton(appInfo);

                    services.AddCommandLineCommand_Defaults();
                    services.AddHostedService<CommandLineService>();

                    serviceConfig?.Invoke(context, services);
                });

            // addon builder?
            addonBuilder?.Invoke(builder);

            // setup as windows service?
            if (appInfo.IsRunningAsWindowsService)
                return builder
                    .UseWindowsService();

            // other option is running in console
            return builder
                .UseConsoleLifetime();
        }

#pragma warning disable IDE0053 // Use expression body for lambda expression

        public static IHostBuilder CreateDefaultBuilder()
            => CreateBuilder(
                appConfig: (context, config) =>
                {
                    // at least add device configuration
                    config.AddJsonFileUpToRoot("device-configuration.json", optional: false, reloadOnChange: false);
                },
                addonBuilder: builder=>
                {
                    builder.UseWindowsServiceInstaller();
                    builder.UseDeviceService();
                }
            );

#pragma warning restore IDE0053 // Use expression body for lambda expression

        public static ILogger<TLogger> GetInitialLogger<TLogger>(this IHostBuilder builder)
            => ((ILoggerFactory)builder.Properties[typeof(ILoggerFactory).FullName!]
                ?? throw new ArgumentNullException(nameof(ILoggerFactory)))
                .CreateLogger<TLogger>();

        public static ILogger GetInitialLogger(this IHostBuilder builder, string categoryName)
            => ((ILoggerFactory)builder.Properties[typeof(ILoggerFactory).FullName!]
                ?? throw new ArgumentNullException(nameof(ILoggerFactory)))
                .CreateLogger(categoryName);

        public static AppInfo GetAppInfo(this IHostBuilder builder)
            => (AppInfo)builder.Properties[typeof(AppInfo).FullName!]
                ?? throw new ArgumentNullException(nameof(AppInfo));
    }
}
