﻿using Levero.DeviceService.Ipc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Levero.DeviceService
{
    public static class IDeviceDriverExtensions
    {
        public static Task IpcSpread(this IDeviceDriver device, Dictionary<string, object?> data)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                data ?? throw new ArgumentNullException(nameof(data)),
                null,
                null)) ?? Task.CompletedTask;

        public static Task IpcSpread(this IDeviceDriver device, Dictionary<string, object?> data, string? receiver)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                data ?? throw new ArgumentNullException(nameof(data)),
                receiver,
                null)) ?? Task.CompletedTask;

        public static Task IpcSpread(this IDeviceDriver device, Dictionary<string, object?> data, TimeSpan? timeToLive)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                data ?? throw new ArgumentNullException(nameof(data)),
                null,
                timeToLive)) ?? Task.CompletedTask;

        public static Task IpcSpread(this IDeviceDriver device, Dictionary<string, object?> data, string? receiver, TimeSpan? timeToLive)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                data ?? throw new ArgumentNullException(nameof(data)),
                receiver,
                timeToLive)) ?? Task.CompletedTask;

        public static Task IpcSpread(this IDeviceDriver device, IpcBaseMessage msg)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                msg?.MessageData ?? throw new ArgumentNullException(nameof(msg)),
                null,
                null)) ?? Task.CompletedTask;

        public static Task IpcSpread(this IDeviceDriver device, IpcBaseMessage msg, string? receiver)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                msg?.MessageData ?? throw new ArgumentNullException(nameof(msg)),
                receiver,
                null)) ?? Task.CompletedTask;

        public static Task IpcSpread(this IDeviceDriver device, IpcBaseMessage msg, string? receiver, TimeSpan? timeToLive)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                msg?.MessageData ?? throw new ArgumentNullException(nameof(msg)),
                receiver,
                timeToLive)) ?? Task.CompletedTask;

        public static Task IpcSpread(this IDeviceDriver device, IpcBaseMessage msg, TimeSpan? timeToLive)
            => device?.Channel.Spread(new SpreadMessage(
                device.DeviceName,
                msg?.MessageData ?? throw new ArgumentNullException(nameof(msg)),
                null,
                timeToLive)) ?? Task.CompletedTask;
    }
}
