# Device Service

## Introduction
**Device Service** is an application, which runs as windows service and is able to load and run different devices configured by ```device-configuration.json```.

## Dependencies
**Device Service** needs .NET Framework 4.6.1 or higher.

## Shim executable
As **Device Service** is only a library you must create an empty ```.NET Windows Executable``` for the language you want (for example ```C#```).
Then you add nuget package ```Levero-DeviceService``` (Version 1.0.0 or higher) and in your ```Program.Main``` you call ```Levero.DeviceService.DefaultProgram.Main```.
That's it.

## Commandline arguments
```DefaultProgram.Main``` supports the following commandline options:
 - without arguments there will be a messagebox with help
 - ```-install [domain\user]``` installs a windows service (username is optional and may be NETWORK (=default value), SYSTEM or username with preceeding machine or domain nameDomäne)
 - ```-uninstall``` uninstalls the windows service
 - ```-start``` starts the windows service
 - ```-stop``` stopps the windows service
 - ```-console``` runs the **Device Service** in console
shortcuts:
 - ```-i``` shortcut for ```-install```
 - ```-u``` shortcut for ```-uninstall```
 - ```-c``` shortcut for ```-console```

If you run **Device Service** in console there is not automatic start of your devices (must be done manually with key ```d```) so you can use it for listening the ipc communication.

## Configuration
The configuration file must be in application folder or above and must be named ```device-configuration.json```.
It may contains a special device ```ipc``` which is loaded before all other devices. If it is obmitted or set to null there is a fallback to a local in-process communication.

Each device in collection ```devices``` must have a name (=name of property in devices object), a ```driver``` property of type ```string```, a ```port``` property of type ```string``` and an options collection with properties which the driver needs (see ```paperName``` for device ```print``` with driver ```print-pdf```).

```
{
  "ipc":{
    "driver": "your-ipc-driver or leave ipc null for in-process-communication",
    "port": "your-ipc-driver-communication-port",
    "options": {
    }
  },
  "verbose":  true,
  "devices": {
    "print": {
      "driver": "print-pdf",
      "port": "http://your-reporting-system/pdf/{filename}",
      "options": {
        "paperName": "57x19mm"
      }
    },
    "barcode": {
      "driver": "barcode",
      "port": "serial/COM3",
      "options": {
        "endOfLine": "\u0003",
        "suppressChars": "\u0002\r\n",
        "product": "honeywell-1900-gsr-2"
      }
    },
    "rfid": {
      "driver": "rfid",
      "port": "serial/COM5",
      "options": {
      }
    }
  }
}
```

## Device drivers
First you create a library project which include the nuget package ```Levero.DeviceService.Core```. Then you create one class per device driver and include the interface ```IDeviceDriver```. 
To let **Device Service** find you driver assembly you must name it ```{your-assembly-name}-driver.dll``` and place it in the same folder as the **Device Service** executable. 
To let **Device Service** find you driver class you must decorate it with an assembly wide attribute ```DeviceDriverAttribute``` which needs a name and the implementing type.

Beispiel:
```
[assembly: DeviceDriver("print-pdf", typeof(YourCompany.DeviceService.PrintPDF.PrintPDF))]
namespace YourCompany.DeviceService.PrintPDF {
  public class PrintPDF : IDeviceDriver {
    ...
  }
}
```

if you build your own ipc channel implementation you must implement interface ```IIpcChannel``` which is a successor of ```IDeviceDriver```.