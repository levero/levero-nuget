﻿using Levero.DeviceService.Ipc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Levero.DeviceService.Ports
{
    public class SerialPortReader : BaseDeviceDriver
    {
        public SerialPortReader(ILogger<SerialPortReader> _log)
            : this((ILogger)_log)
        { }

        protected SerialPortReader(ILogger _log)
        {
            this.log = _log;
            this.log.LogDebug($"{this.GetType().Name} has been created.");
        }

        protected readonly ILogger log;
        private const string constDummyPort = "dummy";
        private const string constSerialPortPrefix = "serial/";

        public const string constOptionResetBuffer = "resetBuffer";
        public const string constOptionEndOfLine = "endOfLine";
        public const string constOptionSuppressChars = "suppressChars";

        public const char STX = (char)2;
        public const char ETX = (char)3;
        public const char LF = (char)10;
        public const char CR = (char)13;
        public const char ESC = (char)27;

        private readonly StringBuilder Buffer = new();
        private readonly object sync = new();
        private static readonly TimeSpan defaultTTL = TimeSpan.FromSeconds(10);

        public override Task InitAndRun(DeviceConfiguration config, CancellationToken cancellationToken)
        {
            this.log.LogDebug($"{nameof(SerialPortReader)} is starting...");

            if (!config.Port.StartsWith(constSerialPortPrefix) && config.Port != constDummyPort)
                throw new NotImplementedException($"Device driver can only handle serial ports! port={config.Port}");

            // if end of line is configured
            if (config.Options.ContainsKey(constOptionResetBuffer))
                this.ResetBufferChars = config.Options[constOptionResetBuffer].ToCharArray();

            // if end of line is configured
            if (config.Options.ContainsKey(constOptionEndOfLine))
                this.EndOfLineChars = config.Options[constOptionEndOfLine].ToCharArray();

            // if suppressChars is configured
            if (config.Options.ContainsKey(constOptionSuppressChars))
                this.SuppressChars = config.Options[constOptionSuppressChars].ToCharArray();

            // try to open Port
            this.Buffer.Length = 0;
            if (config.Port != constDummyPort)
            {
                this.Port = new SerialPort(config.Port.Substring(constSerialPortPrefix.Length));
                this.Port.DataReceived += this.Port_DataReceived;
                this.log.LogInformation($"{nameof(SerialPortReader)} open serial port {this.Port.PortName}...");
                this.Port.Open();
            }

            // register client after port is opened
            this.Channel.RegisterDevice(this.DeviceName);

            this.log.LogDebug($"{nameof(SerialPortReader)} started.");
            return Task.CompletedTask;
        }

        public override Task Shutdown(CancellationToken cancellationToken)
        {
            this.log.LogDebug($"{nameof(SerialPortReader)} stopping...");
            try
            {
                this.Channel.UnregisterDevice(this.DeviceName);
                if (this.Port != null)
                {
                    this.Port.DataReceived -= this.Port_DataReceived;
                    this.Port.Close();
                }
            }
            finally
            {
                this.Port?.Dispose();
                this.Port = null;
            }
            this.Buffer.Length = 0;
            return Task.CompletedTask;
        }

        public SerialPort? Port { get; private set; }
        public char[] ResetBufferChars { get; set; } = Array.Empty<char>();
        public char[] EndOfLineChars { get; set; } = { '\r', '\n' };
        public char[] SuppressChars { get; set; } = Array.Empty<char>();

        private async void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (this.Port == null) return;
            try
            {
                switch (e.EventType)
                {
                    case SerialData.Chars:
                        lock (this.sync)
                            while (this.Port.BytesToRead > 0)
                            {
                                var c = (char)(byte)this.Port.ReadByte();
                                if (this.ResetBufferChars.Contains(c))
                                    this.Buffer.Length = 0;
                                else if (!this.SuppressChars.Contains(c))
                                    this.Buffer.Append(c);
                            }
                        await this.CheckInput();
                        break;

                    case SerialData.Eof:
                    default:
                        // can this happen?
                        break;
                }
            }
            catch (Exception ex)
            {
                lock (this.sync)
                    this.Buffer.Length = 0;
                this.log.LogError(ex, $"{nameof(SerialPortReader)}.{nameof(Port_DataReceived)} failed!");
            }
        }

        private async Task CheckInput()
        {
            try
            {
                if (this.EndOfLineChars.Length > 0)
                {
                    // line-based
                    while (this.Buffer.Length >= 0)
                    {
                        var buffer = this.Buffer.ToString();
                        var index = buffer.IndexOfAny(this.EndOfLineChars);
                        if (index < 0)
                            return;

                        // cut out data
                        var data = buffer.Substring(0, index);
                        lock (this.sync)
                            // and remove from buffer
                            this.Buffer.Remove(0, index + this.EndOfLineChars.Length);

                        // send
                        await this.SendData(data);
                    }
                }
                else
                {
                    string toSend;
                    // token-based
                    lock (this.sync)
                    {
                        // take all
                        toSend = this.Buffer.ToString();

                        // and remove from buffer
                        this.Buffer.Length = 0;
                    }
                    await this.SendData(toSend);
                }
            }
            catch (Exception ex)
            {
                lock (this.sync)
                    this.Buffer.Length = 0;
                this.log.LogError("CheckInput failed: {ex}", ex);
            }
        }

        private async Task SendData(string data)
        {
            this.log.LogDebug($"SendData: {HttpUtility.UrlPathEncode(data) ?? "<null>"}");

            // prepare data
            var ipcdata = this.PrepareData(data);

            // send if not empty
            if (ipcdata != null)
                await this.IpcSpread(ipcdata, defaultTTL);
        }

        protected virtual IpcCodeMessage? PrepareData(string? data)
            => IpcCodeMessage.Create(data);

        /// <summary>
        /// return true, if a IpcCodeMessage can be created with this data
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        protected virtual bool ProcessBinaryData(byte b)
            => false;
    }
}
