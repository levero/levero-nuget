﻿using Levero.DeviceService.Ipc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Levero.DeviceService.Ports
{
    public abstract class BinarySerialPortReader : BaseDeviceDriver
    {
        public BinarySerialPortReader(ILogger<BinarySerialPortReader> _log)
            : this((ILogger)_log)
        { }

        protected BinarySerialPortReader(ILogger _log)
        {
            this.log = _log;
            this.log.LogDebug($"{this.GetType().Name} has been created.");
        }

        protected readonly ILogger log;
        private const string constDummyPort = "dummy";
        private const string constSerialPortPrefix = "serial/";
        private static readonly TimeSpan defaultTTL = TimeSpan.FromSeconds(10);

        public override Task InitAndRun(DeviceConfiguration config, CancellationToken cancellationToken)
        {
            this.log.LogDebug($"{nameof(BinarySerialPortReader)} is starting...");

            if (!config.Port.StartsWith(constSerialPortPrefix) && config.Port != constDummyPort)
                throw new NotImplementedException($"Device driver can only handle serial ports! port={config.Port}");

            // try to open Port
            if (config.Port != constDummyPort)
            {
                this.Port = new SerialPort(config.Port.Substring(constSerialPortPrefix.Length));
                this.Port.DataReceived += this.Port_DataReceived;
                this.log.LogInformation($"{nameof(BinarySerialPortReader)} open serial port {this.Port.PortName}...");
                this.Port.Open();
            }

            // register client after port is opened
            this.Channel.RegisterDevice(this.DeviceName);

            this.log.LogDebug($"{nameof(BinarySerialPortReader)} started.");
            return Task.CompletedTask;
        }

        public override Task Shutdown(CancellationToken cancellationToken)
        {
            this.log.LogDebug($"{nameof(BinarySerialPortReader)} stopping...");
            try
            {
                this.Channel.UnregisterDevice(this.DeviceName);
                if (this.Port != null)
                {
                    this.Port.DataReceived -= this.Port_DataReceived;
                    this.Port.Close();
                }
            }
            finally
            {
                this.Port?.Dispose();
                this.Port = null;
            }
            return Task.CompletedTask;
        }

        public SerialPort? Port { get; private set; }

        private async void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (this.Port == null) return;
            try
            {
                switch (e.EventType)
                {
                    case SerialData.Chars:
                        while (this.Port.BytesToRead > 0)
                        {
                            // read one byte
                            var b = (byte)this.Port.ReadByte();

                            // prepare data
                            var ipcdata = this.ProcessData(b);

                            // send if not empty
                            if (ipcdata != null)
                                await this.IpcSpread(ipcdata, defaultTTL);
                        }
                        break;

                    case SerialData.Eof:
                    default:
                        // can this happen?
                        break;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(ex, $"{nameof(BinarySerialPortReader)}.{nameof(Port_DataReceived)} failed!");
            }
        }

        /// <summary>
        /// return a ipc message, if a valid code has been received
        /// </summary>
        /// <param name="b">a received byte from serial port</param>
        /// <returns></returns>
        protected abstract IpcCodeMessage? ProcessData(byte b);
    }
}
