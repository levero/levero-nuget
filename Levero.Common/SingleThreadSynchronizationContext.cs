using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// see https://devblogs.microsoft.com/pfxteam/await-synchronizationcontext-and-console-apps/
/// </summary>
public sealed class SingleThreadSynchronizationContext : SynchronizationContext
{
    private readonly BlockingCollection<KeyValuePair<SendOrPostCallback, object?>> m_queue = new();

    public override void Post(SendOrPostCallback d, object? state)
    {
        m_queue.Add(
            new KeyValuePair<SendOrPostCallback, object?>(d, state));
    }

    public void RunOnCurrentThread()
    {
        while (m_queue.TryTake(out var workItem, Timeout.Infinite))
            workItem.Key(workItem.Value);
    }

    public void Complete() { m_queue.CompleteAdding(); }

    public static void Run(Func<Task> func)
    {
        var prevCtx = SynchronizationContext.Current;
        try
        {
            var syncCtx = new SingleThreadSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(syncCtx);

            var t = func();
            t.ContinueWith(
                delegate { syncCtx.Complete(); }, 
                TaskScheduler.Default);

            syncCtx.RunOnCurrentThread();

            t.GetAwaiter().GetResult();
        }
        finally { SynchronizationContext.SetSynchronizationContext(prevCtx); }
    }
}