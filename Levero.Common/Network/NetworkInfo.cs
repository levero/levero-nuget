﻿using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Levero.Common.Network
{
    public static class NetworkInfo
    {
        public static IPAddress? GetOwnIPv4()
        {
            // get own ip address
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // can handle IPv4?
                if (!nic.Supports(NetworkInterfaceComponent.IPv4)) continue;

                // is active?
                if (nic.OperationalStatus != OperationalStatus.Up) continue;

                // get IP properties
                var properties = nic.GetIPProperties();

                // query all unicast addresses
                foreach (var ip in properties.UnicastAddresses)
                    // if this address is IPv4 ...
                    if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        // ... and not the loopback address
                        if (!ip.Address.Equals(IPAddress.Loopback))
                            // result found!
                            return ip.Address;
            }
            return null;
        }
    }
}
