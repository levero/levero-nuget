﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Levero.Common
{
    public class AppInfo
    {
        public AppInfo(ILogger<AppInfo> logger)
        {
            this.log = logger;
            this.log.LogDebug("AppInfo is created.");

            // command line arguments
            var commandline = Environment.GetCommandLineArgs();
            this.CommandLineExecutable = commandline.FirstOrDefault()
                ?? throw new Exception("Environment.GetCommandLineArgs() was empty!");
            this.CommandLineArgs = commandline.Skip(1).ToArray();
            this.log.LogDebug($"{nameof(this.CommandLineExecutable)}: {this.CommandLineExecutable}");
            if (this.CommandLineArgs.Length == 0)
                this.log.LogDebug($"{nameof(this.CommandLineArgs)}: <none>");
            else
                this.log.LogDebug($"{nameof(this.CommandLineArgs)}: \"{String.Join("\" \"", this.CommandLineArgs)}\"");

            // get entry assembly
            this.EntryAssembly = Assembly.GetEntryAssembly();

            // search for AppPath
            this.AppPath = new DirectoryInfo(AppContext.BaseDirectory);
            this.log.LogDebug($"{nameof(this.AppPath)}: {this.AppPath.FullName}");

            // get AppVersion from entry assembly
            this.AppVersion = this.EntryAssembly?.GetName().Version;
            this.log.LogDebug($"{nameof(this.AppVersion)}: {this.AppVersion?.ToString() ?? "<null>"}");

            // get file path of executable (in .NET Core this path != location of entry assembly)
            var path = this.EntryAssembly?.Location.NullIfEmpty();
            if (path != null)
            { 
                var isDll = ".dll".Equals(Path.GetExtension(path), StringComparison.InvariantCultureIgnoreCase);
                var exePath = isDll ? Path.ChangeExtension(path, ".exe") : path;

                if (isDll && !File.Exists(exePath))
                    this.log.LogWarning($"{nameof(this.ExecutablePath)} is a library and executable with same name was not found!\r\nExecutable: {exePath}");
                else
                {
                    this.ExecutablePath = new FileInfo(exePath);
                    this.log.LogDebug($"{nameof(this.ExecutablePath)}: {this.ExecutablePath.FullName}");
                }
            }
            else
                this.log.LogWarning($"{nameof(this.ExecutablePath)} not found because entry assembly was not found!");

            // is curent process 64 bit?
            var current = Process.GetCurrentProcess();
            this.Is64bitProcess = WindowsAPI.Is64Process(current);
            this.log.LogDebug($"{nameof(this.Is64bitProcess)}: {this.Is64bitProcess}");

            // is admin?
            this.HasAdminPermissions = WindowsAPI.IsAdmin;
            this.log.LogDebug($"{nameof(this.HasAdminPermissions)}: {this.HasAdminPermissions}");

            // how is app started?
            switch (WindowsAPI.GetRunningTypeFromSecurityIdentifier())
            {
                case WindowsAPI.AppRunningType.Interactive:
                    this.IsRunningInteractive = true;
                    break;
                case WindowsAPI.AppRunningType.WindowsService:
                    this.IsRunningAsWindowsService = true;
                    break;
                case WindowsAPI.AppRunningType.Batch:
                    this.IsRunningAsBatchJob = true;
                    break;
                default:
                    // console apps running as a service under LocalSystem have only 3 WellknownSids: BuiltinAdministratorsSid, WorldSid, AuthenticatedUserSid
                    // I dont know why. Testing if parent process is called "services" helps.
                    this.IsRunningAsWindowsService = WindowsAPI.IsProcessRunningAsService(current);
                    break;
            }
            this.log.LogDebug($"{nameof(this.IsRunningInteractive)}: {this.IsRunningInteractive}");
            this.log.LogDebug($"{nameof(this.IsRunningAsWindowsService)}: {this.IsRunningAsWindowsService}");
            this.log.LogDebug($"{nameof(this.IsRunningAsBatchJob)}: {this.IsRunningAsBatchJob}");
        }

        public void OverrideAppVersion(Version? newVersion)
        {
            this.log.LogInformation($"Override AppVersion from {this.AppVersion?.ToString(3) ?? "<null>"} to {newVersion?.ToString(3) ?? "<null>"}");
            this.AppVersion = newVersion ?? this.AppVersion;
        }

        public void AttachValuesFromConfiguration(ConfigurableAppInfo? configurable)
        {
            this.log.LogInformation($"Attach configurable appInfo with TenantID = {configurable?.TenantID ?? "<null>"}, AppID = {configurable?.AppID ?? "<null>"}, AppPackageID = {configurable?.AppPackageID ?? "<null>"}, DeploymentChannelID = {configurable?.DeploymentChannelID ?? "<null>"}");
            this.FromConfiguration = configurable;
        }

        private readonly ILogger log;

        /// <summary>
        /// get command line executable
        /// </summary>
        public string CommandLineExecutable { get; }

        /// <summary>
        /// get command line arguments
        /// </summary>
        public string[] CommandLineArgs { get; }

        /// <summary>
        /// only for caching - is equal to Assembly.GetEntryAssembly()
        /// </summary>
        public Assembly? EntryAssembly { get; }

        /// <summary>
        /// this is the folder which contains the entry assembly. If no entry assembly found then the folder of Levero.Common.dl
        /// </summary>
        public DirectoryInfo AppPath { get; }

        /// <summary>
        /// this is the version of the entry assembly.
        /// </summary>
        public Version? AppVersion { get; private set; }

        /// <summary>
        /// this is the file path of the entry assembly. 
        /// If this is a .NET Core assembly (.dll) then the extension is changed to .exe if this file exists.
        /// </summary>
        public FileInfo? ExecutablePath { get; }

        /// <summary>
        /// true if OS and process are 64 bit
        /// </summary>
        public bool Is64bitProcess { get; }

        /// <summary>
        /// true if user identity has admin permissions and process is started with high priviledges.
        /// </summary>
        public bool HasAdminPermissions { get; }

        /// <summary>
        /// is true if current process is running interactive
        /// </summary>
        public bool IsRunningInteractive { get; }

        /// <summary>
        /// is true if current process is running as windows service
        /// </summary>
        public bool IsRunningAsWindowsService { get; }

        /// <summary>
        /// is true if current process is started by task schedular
        /// </summary>
        public bool IsRunningAsBatchJob { get; }

        /// <summary>
        /// is true if any debugger is attached.
        /// </summary>
#pragma warning disable CA1822 // Mark members as static
        public bool IsDebuggerAttached => Debugger.IsAttached;
#pragma warning restore CA1822 // Mark members as static

        /// <summary>
        /// this sub class will be loaded from appSettings.
        /// </summary>
        public ConfigurableAppInfo? FromConfiguration { get; private set; }

        public Dictionary<string, object> Properties { get; } = new Dictionary<string, object>();
    }
}
