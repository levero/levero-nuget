﻿using System;
using System.Collections.Concurrent;

namespace Levero.Common
{
    /// <summary>
    /// Class for caching one value with a desired lifetime
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public sealed class Cache<TValue>
    {
        public Cache(TimeSpan lifetime)
        {
            this._LifeTime = lifetime;
        }

#nullable disable
        private TValue _Cache = default;
#nullable restore
        private readonly TimeSpan _LifeTime;
        private DateTime? _LastInvoked;
        private readonly object _Sync = new object();

        public TValue Lookup(Func<TValue> getter)
        {
            if (!this._LastInvoked.HasValue || DateTime.Now.Subtract(this._LastInvoked.Value) > this._LifeTime)
                lock (this._Sync)
                {
                    this._Cache = getter();
                    this._LastInvoked = DateTime.Now;
                }
            return this._Cache;
        }

        public void Reset()
        {
            lock (this._Sync)
            {
                this._Cache = default;
                this._LastInvoked = null;
            }
        }
    }
}
