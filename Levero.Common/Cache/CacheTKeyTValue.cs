﻿using System;
using System.Collections.Concurrent;

namespace Levero.Common
{
    /// <summary>
    /// Class for caching multiple values with a desired lifetime. 
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public sealed class Cache<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
    {
        public Cache(TimeSpan lifetime)
        {
            this._LifeTime = lifetime;
        }

        private readonly ConcurrentDictionary<TKey, Cache<TValue>> _Cache = new ConcurrentDictionary<TKey, Cache<TValue>>();
        private readonly TimeSpan _LifeTime;

        public TValue Lookup(TKey key, Func<TKey, TValue> getter)
        {
            if (!this._Cache.TryGetValue(key, out var cacheValue))
                this._Cache.TryAdd(key, cacheValue = new Cache<TValue>(this._LifeTime));
            return cacheValue.Lookup(() => getter(key));
        }

        public void Reset() 
            => this._Cache.Clear();
    }
}
