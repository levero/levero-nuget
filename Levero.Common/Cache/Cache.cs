﻿using System;

namespace Levero.Common
{
    public static class Cache
    {
        /// <summary>
        /// shortcut for TimeSpan.FromMinutes(5)
        /// </summary>
        public static readonly TimeSpan LifeTime_5min = TimeSpan.FromMinutes(5);

        /// <summary>
        /// shortcut for TimeSpan.FromMinutes(15)
        /// </summary>
        public static readonly TimeSpan LifeTime_15min = TimeSpan.FromMinutes(15);

        /// <summary>
        /// shortcut for TimeSpan.FromDays(1)
        /// </summary>
        public static readonly TimeSpan LifeTime_1day = TimeSpan.FromDays(1);
    }
}
