﻿using System.IO;
using System.IO.Compression;

namespace Levero.Common
{
    public static class Compression
    {
        public static byte[] UnzipToBytes(byte[] content)
        {
            using (var input = new MemoryStream(content))
            using (var output = new MemoryStream())
            {
                using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                    decompress.CopyTo(output);
                return output.ToArray();
            }
        }

        public static byte[] UnzipToBytes(Stream input)
        {
            using (var output = new MemoryStream())
            {
                using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                    decompress.CopyTo(output);
                return output.ToArray();
            }
        }

        public static byte[] UnzipToBytes(string inputFile)
        {
            using (var input = File.OpenRead(inputFile))
            using (var output = new MemoryStream())
            {
                using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                    decompress.CopyTo(output);
                return output.ToArray();
            }
        }

        public static void UnzipToStream(byte[] content, Stream output)
        {
            using (var input = new MemoryStream(content))
            using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                decompress.CopyTo(output);
        }

        public static void UnzipToStream(Stream input, Stream output)
        {
            using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                decompress.CopyTo(output);
        }

        public static void UnzipToStream(string inputFile, Stream output)
        {
            using (var input = File.OpenRead(inputFile))
            using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                decompress.CopyTo(output);
        }

        public static void UnzipToFile(byte[] content, string outputFile)
        {
            using (var input = new MemoryStream(content))
            using (var output = File.OpenWrite(outputFile))
            using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                decompress.CopyTo(output);
        }

        public static void UnzipToFile(Stream input, string outputFile)
        {
            using (var output = File.OpenWrite(outputFile))
            using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                decompress.CopyTo(output);
        }

        public static void UnzipToFile(string inputFile, string outputFile)
        {
            using (var input = File.OpenRead(inputFile))
            using (var output = File.OpenWrite(outputFile))
            using (var decompress = new GZipStream(input, CompressionMode.Decompress))
                decompress.CopyTo(output);
        }




        public static byte[] ZipToBytes(byte[] content)
        {
            using (var input = new MemoryStream(content))
            using (var output = new MemoryStream())
            {
                using (var compress = new GZipStream(output, CompressionMode.Compress))
                    input.CopyTo(compress);
                return output.ToArray();
            }
        }

        public static byte[] ZipToBytes(Stream input)
        {
            using (var output = new MemoryStream())
            {
                using (var compress = new GZipStream(output, CompressionMode.Compress))
                    input.CopyTo(compress);
                return output.ToArray();
            }
        }

        public static byte[] ZipToBytes(string inputFile)
        {
            using (var input = File.OpenRead(inputFile))
            using (var output = new MemoryStream())
            {
                using (var compress = new GZipStream(output, CompressionMode.Compress))
                    input.CopyTo(compress);
                return output.ToArray();
            }
        }

        public static void ZipToStream(byte[] content, Stream output)
        {
            using (var input = new MemoryStream(content))
            using (var compress = new GZipStream(output, CompressionMode.Compress))
                input.CopyTo(compress);
        }

        public static void ZipToStream(Stream input, Stream output)
        {
            using (var compress = new GZipStream(output, CompressionMode.Compress))
                input.CopyTo(compress);
        }

        public static void ZipToStream(string inputFile, Stream output)
        {
            using (var input = File.OpenRead(inputFile))
            using (var compress = new GZipStream(output, CompressionMode.Compress))
                input.CopyTo(compress);
        }

        public static void ZipToFile(byte[] content, string outputFile)
        {
            using (var input = new MemoryStream(content))
            using (var output = File.OpenWrite(outputFile))
            using (var compress = new GZipStream(output, CompressionMode.Compress))
                input.CopyTo(compress);
        }

        public static void ZipToFile(Stream input, string outputFile)
        {
            using (var output = File.OpenWrite(outputFile))
            using (var compress = new GZipStream(output, CompressionMode.Compress))
                input.CopyTo(compress);
        }

        public static void ZipToFile(string inputFile, string outputFile)
        {
            using (var input = File.OpenRead(inputFile))
            using (var output = File.OpenWrite(outputFile))
            using (var compress = new GZipStream(output, CompressionMode.Compress))
                input.CopyTo(compress);
        }
    }
}
