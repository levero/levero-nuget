﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.CommandLine
{
    public sealed class CommandVersion : CommandLineCommand
    {
        public CommandVersion(AppInfo _appInfo)
            : base(CommonCommands.Version, CommonCommands.Version_Aliases.Split(' '), CommonCommands.Version_Description)
        {
            this.appInfo = _appInfo;
        }

        private readonly AppInfo appInfo;

        public override Task<int> Handler(CommandLineContext context, CancellationToken cancellationToken = default)
        {
            // show all assemblyies not starting with System and Microsoft
            context.WriteLine("Loaded assemblies:");
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies()
                .Select(it => it.GetName())
                .Where(it => it.Name != null && !it.Name.StartsWith("System.") && !it.Name.StartsWith("Microsoft."))
                .OrderBy(it => it.Name))
                context.Write1Block(3, $"{asm.Name} Version {asm.Version}");

            context.WriteLine();
            return Task.FromResult(CommonCommands.SUCCESS);
        }
    }
}
