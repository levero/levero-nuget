﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.CommandLine
{
    public sealed class CommandHelp : CommandLineCommand
    {
        public CommandHelp(AppInfo _appInfo, IServiceProvider _services)
            : base(CommonCommands.Help, CommonCommands.Help_Aliases.Split(' '), CommonCommands.Help_Description)
        {
            this.appInfo = _appInfo;
            this.services = _services;
        }

        private readonly AppInfo appInfo;
        private readonly IServiceProvider services;

        public override Task<int> Handler(CommandLineContext context, CancellationToken cancellationToken = default)
        {
            var cmds = this.services.GetServices<CommandLineCommand>()
                .OrderByDescending(it => it.Name == CommonCommands.DEFAULT)
                .ThenBy(it => it.Name);
            var indent2 = 3 + cmds.Max(it => it.Name.Length) + 2;
            var switches = cmds.SelectMany(it => it.Options).Where(it => it.IsSwitch).ToArray();
            if (switches.Length > 0)
                indent2 = Math.Max(indent2, 6 + switches.Max(it => it.Name.Length) + 4);

            context.WriteLine("Usage:");
            var toolName = Path.GetFileNameWithoutExtension(this.appInfo.ExecutablePath?.Name ?? this.appInfo.CommandLineExecutable);
            context.Write1Block(3, $"{toolName} [command] [options]");
            context.WriteLine();
            context.WriteLine("Commands:");
            context.Write2Blocks(3, "<none>", indent2, $"without command the app is starting command {CommonCommands.DEFAULT}");
            context.WriteLine();
            foreach (var cmd in cmds)
            {
                context.Write2Blocks(3, cmd.Name, indent2, cmd.Description);
                if (cmd.Aliases.Length > 0)
                    context.Write1Block(indent2, String.Concat(
                        cmd.Aliases.Length == 1 ? "alias:   " : "aliases: ",
                        String.Join(" ", cmd.Aliases)));

                // options with values
                foreach (var option in cmd.Options.Where(it => !it.IsSwitch))
                {
                    context.Write1Block(6, option.IsRequired ? $"{option.Name} <value>" : $"[{option.Name} <value>]");
                    context.Write1Block(indent2, option.Description);
                    context.Write1Block(indent2, $"type:    {option.TypeOfValue}");
                    if (option.DefaultValue != null)
                        context.Write1Block(indent2, $"default: {option.DefaultValue}");
                    if (option.Aliases.Length > 0)
                        context.Write1Block(indent2, String.Concat(
                            option.Aliases.Length == 1 ? "alias:   " : "aliases: ",
                            String.Join(" ", option.Aliases)));
                }

                // switches
                foreach (var option in cmd.Options.Where(it => it.IsSwitch))
                {
                    context.Write1Block(6, $"[{option.Name}]");
                    context.Write1Block(indent2, option.Description);
                    if (option.Aliases.Length > 0)
                        context.Write1Block(indent2, String.Concat(
                            option.Aliases.Length == 1 ? "alias:   " : "aliases: ",
                            String.Join(" ", option.Aliases)));
                }
                context.WriteLine();
            }
            return Task.FromResult(CommonCommands.SUCCESS);
        }
    }
}
