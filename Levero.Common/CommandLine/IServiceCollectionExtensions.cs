﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Levero.Common.CommandLine
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddCommandLineCommand<TImplementation>(this IServiceCollection services)
            where TImplementation : CommandLineCommand
            => services.AddSingleton<CommandLineCommand, TImplementation>();

        public static IServiceCollection AddCommandLineCommand<TImplementation>(this IServiceCollection services, 
            Func<IServiceProvider, TImplementation> implementationFactory)
            where TImplementation : CommandLineCommand
            => services.AddSingleton<CommandLineCommand, TImplementation>(implementationFactory);
       

        /// <summary>
        /// adds help and version commands
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddCommandLineCommand_Defaults(this IServiceCollection services)
            => services
                .AddCommandLineCommand<CommandHelp>()
                .AddCommandLineCommand<CommandVersion>();
    }
}
