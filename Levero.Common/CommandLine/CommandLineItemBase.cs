﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Levero.Common.CommandLine
{
    public class CommandLineItemBase
    {
        public CommandLineItemBase(
            string name,
            string[] aliases,
            string description)
        {
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Aliases = aliases ?? throw new ArgumentNullException(nameof(aliases));
            this.Description = description ?? throw new ArgumentNullException(nameof(description));
        }

        /// <summary>
        /// command name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// alias names for this command
        /// </summary>
        public string[] Aliases { get; }

        /// <summary>
        /// description of this option
        /// </summary>
        public string Description { get; }

        protected internal bool IsToken(string token)
        {
#pragma warning disable IDE0046 // Convert to conditional expression
            if (String.Compare(token, this.Name, true) == 0)
                return true;
            return this.Aliases.Any(it => String.Compare(token, it, true) == 0);
#pragma warning restore IDE0046 // Convert to conditional expression
        }
    }
}
