﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.CommandLine
{
    public class CommandLineService : IHostedService
    {
        public CommandLineService(ILogger<CommandLineService> _log, IHost _host, AppInfo _appInfo,
            IEnumerable<CommandLineCommand> _commands)
        {
            this.log = _log;
            this.host = _host;
            this.appInfo = _appInfo;
            this.commands = _commands.ToArray();

            // in windows service mode do not write to console
            this.sb = new StringBuilder();
            this.write = this.appInfo.IsRunningAsWindowsService ?
                new Action(() => this.log.LogInformation(this.sb.ToString())) :
                new Action(() => Console.WriteLine(this.sb.ToString()));
            this.bufferWidth = this.appInfo.IsRunningAsWindowsService ? 80 : Console.BufferWidth;

            this.log.LogDebug($"{nameof(CommandLineService)} is started with {this.commands.Length} command(s).");
        }

        private readonly ILogger log;
        private readonly IHost host;
        private readonly AppInfo appInfo;
        private readonly CommandLineCommand[] commands;
        private readonly StringBuilder sb;
        private readonly Action write;
        private readonly int bufferWidth;
        private CommandLineContext? context;
        private static readonly Dictionary<string, object?> emptyOptions = new Dictionary<string, object?>();

        public static int ExitCode { get; private set; }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            // get help command (if available)
            var showHelp = false;
            var doNotExit = false;
            var help = this.commands?.FirstOrDefault(it => it.Name == CommonCommands.Help);

            try
            {
                var toolName = Path.GetFileNameWithoutExtension(this.appInfo.ExecutablePath?.Name ?? this.appInfo.CommandLineExecutable);
                this.ConsoleWriter(0, $"{toolName} Version {this.appInfo.AppVersion?.ToString() ?? "<unknown>"}", null, null);
                // search for copyright
                if (this.appInfo.EntryAssembly?.GetCustomAttribute(typeof(AssemblyCopyrightAttribute)) is AssemblyCopyrightAttribute cp)
                    this.ConsoleWriter(0, cp.Copyright, null, null);
                this.ConsoleWriter(0, String.Empty, null, null);

                // get first token
                var args = new Queue<string>(this.appInfo.CommandLineArgs);
                if (args.Count == 0)
                    // if no argument is found -> search for default command
                    args.Enqueue(CommonCommands.DEFAULT);

                var token = args.Dequeue();

                // search for command
                var command = this.commands?.FirstOrDefault(it => it.IsToken(token));
                if (command == null)
                    throw new Exception($"CMD:Command {token} not found!");

                // do not show help twice if there is an error
                if (command == help)
                    help = null;

                // parse options
                var options = command.Options.ToDictionary(it => it.Name, it => it.DefaultValue);
                while (args.TryDequeue(out var token2))
                {
                    var option = command.Options.FirstOrDefault(it => it.IsToken(token));
                    if (option == null)
                        throw new Exception($"CMD:Command {command.Name}: option {token} is unknown!");

                    // TODO upgrade options parsing: enable options without name (example: start [service])
                    if (option.IsSwitch)
                        options[option.Name] = true;
                    else
                    {
                        if (!args.TryDequeue(out token2))
                            throw new Exception($"CMD:Command {command.Name}: option {option.Name} needs an argument!");
                        switch (option.TypeOfValue)
                        {
                            case CommandLineOptionType.String:
                                options[option.Name] = token2;
                                break;
                            case CommandLineOptionType.Int32:
                                if (!Int32.TryParse(token2, out var ivalue))
                                    throw new Exception($"CMD:Command {command.Name}: argument {token2} of option {option.Name} must be a 32bit integer!");
                                options[option.Name] = ivalue;
                                break;
                            case CommandLineOptionType.Double:
                                if (!Double.TryParse(token2, out var dvalue))
                                    throw new Exception($"CMD:Command {command.Name}: argument {token2} of option {option.Name} must be a double!");
                                options[option.Name] = dvalue;
                                break;
                        }
                    }
                }

                // all required options available?
                // TODO

                this.context = new CommandLineContext(options, this.ConsoleWriter);

                // validate command
                if (!this.appInfo.IsRunningAsWindowsService)
                    Console.ForegroundColor = ConsoleColor.Yellow;
                ExitCode = await command.Validator(this.context, cancellationToken);
                if (!this.appInfo.IsRunningAsWindowsService)
                    Console.ResetColor();

                // run command (if validation was successful)
                if (ExitCode == CommonCommands.SUCCESS)
                    ExitCode = await command.Handler(this.context, cancellationToken);
                doNotExit = command.DoNotExit;
            }
            catch (Exception ex) when (ex.Message.StartsWith("CMD:"))
            {
                this.log.LogWarning(ex.Message.Substring(4));
                showHelp = true;

                if (!this.appInfo.IsRunningAsWindowsService)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(ex.Message.Substring(4));
                    Console.ResetColor();
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(ex, "Command line service failed.");
                showHelp = true;

                if (!this.appInfo.IsRunningAsWindowsService)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ResetColor();
                    Console.WriteLine();
                }
            }
            finally
            {
                // show help if there was an error
                if (showHelp)
                    _ = help?.Handler(new CommandLineContext(emptyOptions, this.ConsoleWriter));

                // exit if command is no long running service
                if (!doNotExit)
                    try { await this.host.StopAsync(); }
                    catch (TaskCanceledException) { }
                else if (this.appInfo.IsRunningInteractive)
                    Console.WriteLine("\nPress Ctrl+C to shut down.");
            }
        }

        private void ConsoleWriter(int pos1, string text1, int? pos2, string? text2)
        {
            this.sb.Length = 0;

            WriteMultiLine(pos1, text1 ?? String.Empty);

            if (text2 != null)
                WriteMultiLine(pos2 ?? this.sb.Length + 1, text2);

            this.write();

            void WriteMultiLine(int pos, string text)
            {
                var lines = text.Replace("\r\n", "\n").Replace('\r', '\n').Split('\n').ToList();
                while (lines.Count > 0)
                {
                    if (pos > 0)
                        this.sb.Append(' ', Math.Max(1, pos - this.sb.Length));
                    WriteWithWordWrap(pos, lines[0]);
                    lines.RemoveAt(0);
                    if (lines.Count > 0)
                    {
                        this.write();
                        this.sb.Length = 0;
                    }
                }
            }

            void WriteWithWordWrap(int pos, string text)
            {
                var toWrite = text;
                if (this.bufferWidth <= this.sb.Length)
                    throw new Exception($"{nameof(CommandLineService)}: Console Buffer too small!");
                while ((this.sb.Length + toWrite.Length) > this.bufferWidth)
                {
                    // write as much as possible
                    var part1 = toWrite.Substring(0, this.bufferWidth - this.sb.Length).TrimEnd();
                    var splitPos = part1.LastIndexOf(' ');
                    if (splitPos > 0)
                        part1 = part1.Substring(0, splitPos);
                    // what is left for next line?
                    toWrite = toWrite.Substring(part1.Length).TrimStart();
                    this.sb.Append(toWrite);
                    // write this line
                    this.write();
                    // prepare next line
                    this.sb.Length = 0;
                    if (pos > 0)
                        this.sb.Append(' ', pos);
                }
                this.sb.Append(toWrite);
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (this.context?.StopHandler == null) return;

            try { await this.context.StopHandler(this.context, cancellationToken); }
            catch (Exception ex)
            { this.log.LogError(ex, $"{nameof(CommandLineContext)}.{nameof(CommandLineContext.StopHandler)} failed."); }
        }
    }
}
