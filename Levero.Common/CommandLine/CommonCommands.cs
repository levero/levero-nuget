﻿namespace Levero.Common.CommandLine
{
    public static class CommonCommands
    {
        /// <summary>
        /// Default command which is executed if no command found
        /// </summary>
        public const string DEFAULT = "run";

        /// <summary>
        /// Exitcode for success
        /// </summary>
        public const int SUCCESS = 0;

        public const string Version = "version";
        public const string Version_Aliases = "-v /v --version -version /version";
        public const string Version_Description = "display version of main executable and loaded assemblies";

        public const string Help = "help";
        public const string Help_Aliases = "? -? /? -h /h --help -help /help";
        public const string Help_Description = "display available commands and options";
    }
}
