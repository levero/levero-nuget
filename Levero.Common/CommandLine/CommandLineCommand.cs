﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.CommandLine
{
    public abstract class CommandLineCommand : CommandLineItemBase
    {
        protected CommandLineCommand(string name, string[] aliases, string description)
            : base(name, aliases, description)
        {
        }

        public bool DoNotExit { get; protected set; }

        /// <summary>
        /// validator for this option with an writeError function which writes error message to user and 
        /// indicates an error. It can be called more than once. If called no times the option can be executed.
        /// 
        /// Argument values are stored in Arguments array.
        /// </summary>
        /// <param name="writeError"></param>
        public virtual Task<int> Validator(CommandLineContext context, CancellationToken cancellationToken = default)
            => Task.FromResult(CommonCommands.SUCCESS);

        /// <summary>
        /// executes the action.
        /// Argument values are stored in Arguments array.
        /// </summary>
        /// <returns></returns>
        public abstract Task<int> Handler(CommandLineContext context, CancellationToken cancellationToken = default);

        /// <summary>
        /// adds arguments this option
        /// </summary>
        public List<CommandLineOption> Options { get; } = new List<CommandLineOption>();


        public override string ToString()
            => $"Command {this.Name}: {this.Description}";
    }
}
