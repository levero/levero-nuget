﻿using System;

namespace Levero.Common.CommandLine
{
    public enum CommandLineOptionType
    {
        Switch,
        String,
        Int32,
        Double
    }
    public sealed class CommandLineOption : CommandLineItemBase
    {
        public static CommandLineOption CreateSwitch(string name, string description, params string[] aliases)
            => new CommandLineOption(name, aliases, description, false, CommandLineOptionType.Switch, false);

        public static CommandLineOption CreateString(string name, string description, bool isRequired, string? defaultValue, params string[] aliases)
            => new CommandLineOption(name, aliases, description, isRequired, CommandLineOptionType.String, defaultValue);

        public static CommandLineOption CreateInt32(string name, string description, bool isRequired, int? defaultValue, params string[] aliases)
            => new CommandLineOption(name, aliases, description, isRequired, CommandLineOptionType.Int32, defaultValue);

        public static CommandLineOption CreateDouble(string name, string description, bool isRequired, double? defaultValue, params string[] aliases)
            => new CommandLineOption(name, aliases, description, isRequired, CommandLineOptionType.Double, defaultValue);

        private CommandLineOption(string name, string[] aliases, string description,
            bool isRequired, CommandLineOptionType typeOfValue, object? defaultValue)
            : base(name, aliases, description)
        {
            this.IsRequired = isRequired;
            this.TypeOfValue = typeOfValue;
            this.DefaultValue = defaultValue;
        }

        public bool IsSwitch => this.TypeOfValue == CommandLineOptionType.Switch;

        public bool IsRequired { get; }

        public CommandLineOptionType TypeOfValue { get; }

        public object? DefaultValue { get; }

        public override string ToString()
            => $"Option {this.Name} of type {this.TypeOfValue}: {this.Description}";
    }
}
