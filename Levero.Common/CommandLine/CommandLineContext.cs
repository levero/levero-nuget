﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.CommandLine
{
    public class CommandLineContext
    {
        public CommandLineContext(Dictionary<string, object?> _values, Action<int, string, int?, string?> _writer)
        {
            this.OptionValues = _values;
            this.writer = _writer;
        }

        private readonly Action<int, string, int?, string?> writer;

        public Dictionary<string, object?> OptionValues { get; }

        public Func<CommandLineContext, CancellationToken, Task>? StopHandler { get; set; }

        public void WriteLine()
            => this.writer(0, String.Empty, null, null);

        public void WriteLine(string text)
            => this.writer(0, text, null, null);

        public void Write1Block(int pos1, string text1)
            => this.writer(pos1, text1, null, null);

        public void Write2Blocks(int pos1, string text1, int pos2, string text2)
            => this.writer(pos1, text1, pos2, text2);
    }
}
