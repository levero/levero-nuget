﻿using Microsoft.Extensions.FileProviders;
using System;
using System.IO;
using System.Linq;

namespace Microsoft.Extensions.Configuration
{
    public static class ConfigurationExtensions
    {
        public static IConfigurationBuilder AddJsonFileUpToRoot(this IConfigurationBuilder config, string configFileName, bool optional = false, bool reloadOnChange = false)
        {
            // see https://github.com/dotnet/runtime/blob/master/src/libraries/Microsoft.Extensions.Configuration.FileExtensions/src/FileConfigurationExtensions.cs
            // and https://github.com/dotnet/runtime/blob/master/src/libraries/Microsoft.Extensions.FileProviders.Physical/src/PhysicalFileProvider.cs
            var fileProvider = config.GetFileProvider() as PhysicalFileProvider;
            var path = fileProvider?.Root ?? AppDomain.CurrentDomain.BaseDirectory;

            if (path == null)
                throw new ArgumentException("cannot retrieve base folder!\r\nUse builder.SetBasePath() before setting Configuration.");

            var folder = new DirectoryInfo(path);
            while (folder != null && folder.Exists)
            {
                var configPath = folder.GetFiles(configFileName).FirstOrDefault();
                if (configPath != null)
                    return config.AddJsonFile(configPath.FullName);

                folder = folder.Parent;
            }

            return config;
        }
    }
}
