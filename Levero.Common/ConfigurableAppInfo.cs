﻿namespace Levero.Common
{
    public class ConfigurableAppInfo
    {
        /// <summary>
        /// is loaded from appSettings.json: AppInfo.TenantID
        /// </summary>
        public string? TenantID { get; set; }

        /// <summary>
        /// is loaded from appSettings.json: AppInfo.AppID
        /// </summary>
        public string? AppID { get; set; }

        /// <summary>
        /// is loaded from appSettings.json: AppInfo.AppPackageID
        /// </summary>
        public string? AppPackageID { get; set; }

        /// <summary>
        /// is loaded from appSettings.json: AppInfo.DeploymentChannelID
        /// </summary>
        public string? DeploymentChannelID { get; set; }
    }
}
