﻿using Levero.Common.CommandLine;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.Services
{
    public sealed class CommandUninstall : CommandLineCommand
    {
        public CommandUninstall(WindowsServiceInstaller _installer)
            : base(ServiceCommands.Uninstall, ServiceCommands.Uninstall_Aliases.Split(' '), ServiceCommands.Uninstall_Description)
        {
            this.installer = _installer;
        }

        private readonly WindowsServiceInstaller installer;

        public override Task<int> Validator(CommandLineContext context, CancellationToken cancellationToken = default)
            => this.installer.Validate(context.WriteLine, cancellationToken);

        public override Task<int> Handler(CommandLineContext context, CancellationToken cancellationToken = default)
            => this.installer.Uninstall(cancellationToken);
    }
}
