﻿using Levero.Common.CommandLine;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.Services
{
    public sealed partial class WindowsServiceInstaller
    {
        private readonly ILogger<WindowsServiceInstaller> log;
        private readonly WindowsServiceConfiguration[] services;
        private readonly AppInfo appInfo;
        private bool? isValidated;

        public WindowsServiceInstaller(ILogger<WindowsServiceInstaller> log, AppInfo appInfo, IOptions<WindowsServicesConfiguration> config)
            : this(log,
                appInfo,
                config.Value
                    .Where(kvp => kvp.Value != null)
                    .Select(kvp =>
                    {
                        kvp.Value.ServiceName = kvp.Value.ServiceName.NullIfWhitespace() ?? kvp.Key;
                        return kvp.Value;
                    })
                    .ToArray())
        {
        }

        public WindowsServiceInstaller(ILogger<WindowsServiceInstaller> log, AppInfo appInfo, WindowsServiceConfiguration[] services)
        {
            this.log = log;
            this.appInfo = appInfo;
            this.services = services;
        }

        public Task<int> Validate(Action<string> outputFaults, CancellationToken cancellationToken = default)
        {
            this.isValidated = false;
            // check services
            if (this.services.NullIfEmpty() == null)
            {
                outputFaults($"{nameof(WindowsServiceInstaller)} needs at least one service!");
                return Task.FromResult(-6);
            }
            foreach (var sc in this.services)
            {
                sc.IsValidated = true;
                void output(string fault)
                {
                    sc.IsValidated = false;
                    outputFaults(fault);
                }

                // check ServiceName
                if (String.IsNullOrWhiteSpace(sc.ServiceName))
                    output($"{nameof(sc.ServiceName)} is empty!");
                else if (sc.ServiceName != null)
                {
                    var test = sc.ServiceName;
                    foreach (var ch in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
                        test = test.Replace(ch, '_');
                    test = test.Replace("_", "").Replace(" ", "<SPACE>").Replace("\r", "<CR>").Replace("\n", "<LF>").Replace("\t", "<TAB>");
                    if (test.Length > 0)
                        output($"Service {sc.ServiceName}: {nameof(sc.ServiceName)} contains invalid characters {test}!");
                    if (sc.ServiceName.Length > 256)
                        output($"Service {sc.ServiceName}: {nameof(sc.ServiceName)} may not be longer than 256 characters!");
                }

                // start type
                switch (sc.StartType)
                {
                    case ServiceStartMode.Automatic:
                    case ServiceStartMode.Disabled:
                    case ServiceStartMode.Manual:
                        break;
                    default:
                        output($"Service {sc.ServiceName}: {nameof(sc.StartType)} '{sc.StartType}' is not supported!");
                        break;
                }

                // check DisplayName
                if (sc.DisplayName?.Length > 256)
                    output($"Service {sc.ServiceName}: {nameof(sc.DisplayName)} may not be longer than 256 characters!");
                sc.DisplayName = sc.DisplayName.NullIfWhitespace()?.Trim();

                // check Description
                sc.Description = sc.Description.NullIfWhitespace()?.Trim();

                // normalize username / password
                if (sc.ServiceAccount_Username == null ||
                    sc.ServiceAccount_Username.ToUpper() == WindowsServiceConfiguration.ServiceAccount_NetworkService)
                {
                    sc.PreparedUsername = "NT AUTHORITY\\NetworkService";
                    sc.PreparedPassword = null;
                    sc.GrantUserRunAsService = false;
                }
                else if (sc.ServiceAccount_Username.ToUpper() == WindowsServiceConfiguration.ServiceAccount_LocalSystem)
                {
                    sc.PreparedUsername = "LocalSystem";
                    sc.PreparedPassword = null;
                    sc.GrantUserRunAsService = false;
                }
                else
                {
                    var parts = sc.ServiceAccount_Username.Split('\\');
                    if (parts.Length != 2)
                        output($"Service {sc.ServiceName}: username '{sc.ServiceAccount_Username}' must be SYSTEM, NETWORK or with format Domain\\UserName!");
                    if (sc.ServiceAccount_Username.Contains(" "))
                        output($"Service {sc.ServiceName}: username '{sc.ServiceAccount_Username}' must not contain spaces!");
                    sc.PreparedUsername = sc.ServiceAccount_Username;

                    if (String.IsNullOrWhiteSpace(sc.ServiceAccount_Password))
                        output($"Service {sc.ServiceName}: password must not be empty if using a custom account!");
                    sc.PreparedPassword = sc.ServiceAccount_Password;
                }

                // others...
                sc.WaitTimeForStart ??= TimeSpan.FromSeconds(10);
                sc.WaitTimeForStop ??= TimeSpan.FromSeconds(10);
            }

            // all services must be validated!
            this.isValidated = this.services.All(it => it.IsValidated);
            return Task.FromResult(this.isValidated.Value ? CommonCommands.SUCCESS : -6);
        }

        public async Task<int> StartAndWaitForExit(string filename, string? arguments = null, string verb = "open", CancellationToken cancellationToken = default)
        {
            // write into event log
            this.log.LogInformation($"ShellExecute {verb} \"{filename}\" {arguments}");

            // start and wait
            var p = Process.Start(new ProcessStartInfo
            {
                FileName = filename,
                Arguments = arguments ?? String.Empty,
                Verb = verb,
                UseShellExecute = true  // Default-Value is different between .NET Framework (true) and .NET Core (false) !!!!
            });
            if (p == null)
                // write into event log
                this.log.LogError($"ShellExecute {verb} \"{filename}\" failed without exception", verb, filename);
            else
            {
                await p.WaitForExitAsync(cancellationToken);
                // write into event log
                this.log.LogInformation($"ShellExecute {verb} \"{filename}\" exited with code {p.ExitCode}", verb, filename);
            }
            return p?.ExitCode ?? Int32.MinValue;
        }

        public async Task StartAndWaitForExit(string filename, string? arguments, string verb, int expectedExitCode, CancellationToken cancellationToken = default)
        {
            var exitCode = await this.StartAndWaitForExit(filename, arguments, verb, cancellationToken);
            if (exitCode != expectedExitCode)
                throw new Exception($"ShellExecute {verb} \"{filename}\" {arguments} exited with code {exitCode} instead of {expectedExitCode}!");
        }

        public Task<int> RestartProcessAsAdmin(string arguments, CancellationToken cancellationToken = default)
            => this.StartAndWaitForExit(this.appInfo.ExecutablePath!.FullName, arguments, "runas", cancellationToken);

        public ServiceController? GetService(WindowsServiceConfiguration sc)
            => ServiceController.GetServices().SingleOrDefault(it => it.ServiceName == sc.ServiceName);

        public async Task<int> Install(CancellationToken cancellationToken = default)
        {
            if (!this.isValidated.HasValue)
                throw new Exception("Validation must be executed!");
            if (!this.isValidated.Value)
                throw new Exception("Validation was not successful!");

            // check if process runs with admin rights
            if (!WindowsAPI.IsAdmin)
                return await this.RestartProcessAsAdmin(ServiceCommands.Install, cancellationToken);

            // Services installieren
            foreach (var sc in this.services)
                try
                {
                    await this.InstallService(sc, cancellationToken);

                    // set Netsh permissions if logon as user
                    if (sc.HttpServerPort.HasValue && sc.PreparedPassword != null)
                    {
                        _ = await this.StartAndWaitForExit("netsh",
                            $"http add urlacl url=http://+:{sc.HttpServerPort.Value}/ user=\"{sc.PreparedUsername}\"",
                            "open",
                            cancellationToken);

                        using var reg = Registry.LocalMachine.CreateSubKey($"Software\\LeveroApps\\Services\\{sc.ServiceName}");
                        reg.SetValue("netsh", $"http delete urlacl url=http://+:{sc.HttpServerPort.Value}/", RegistryValueKind.String);
                    }

                    // Service starten
                    var result = 0;
                    if (!await this.InternalStart(sc, r => result = r, cancellationToken))
                        return result;
                }
                catch (Exception ex)
                {
                    this.log.LogError(ex, $"Service {sc.ServiceName}: Installing windows service failed!");
                    return -1;
                }
            return CommonCommands.SUCCESS;
        }

        private async Task InstallService(WindowsServiceConfiguration sc, CancellationToken cancellationToken)
        {
            // setup Windows service with sc.exe
            // https://docs.microsoft.com/de-de/windows-server/administration/windows-commands/sc-create

            // is service already installed?
            sc.Svc = this.GetService(sc);
            var mode = sc.Svc == null ? "create" : "config";

            // build command line arguments of sc.exe
            var args = $"{mode} \"{sc.ServiceName}\"";

            // get executable path from context
            args += $" binpath= \"{this.appInfo.ExecutablePath}\"";

            // more than one service in this process?
            if (this.services.Length > 1)
                args += " type= share";

            // start type
            switch (sc.StartType)
            {
                case ServiceStartMode.Automatic:
                    args += " start= auto"; break;
                case ServiceStartMode.Manual:
                    args += " start= demand"; break;
                case ServiceStartMode.Disabled:
                    args += " start= disabled"; break;
            }

            // which user?
            if (sc.PreparedUsername != null)
            {
                args += $" obj= \"{sc.PreparedUsername}\"";
                if (sc.PreparedPassword != null)
                    args += $" password= \"{sc.PreparedPassword}\"";

                if (sc.GrantUserRunAsService)
                {
                    // grant permission "run as service" to user
                    WindowsAPI.GrantLogonAsService(sc.PreparedUsername);
                    this.services.Where(it => String.Compare(it.PreparedUsername, sc.PreparedUsername, true) == 0)
                        .ForEach(it => it.GrantUserRunAsService = false);
                }
            }

            // display name
            if (sc.DisplayName != null)
                args += $" DisplayName= \"{sc.DisplayName}\"";

            // run command
            await this.StartAndWaitForExit("sc.exe", args, "open", 0, cancellationToken);

            // description
            if (sc.Description != null)
            {
                args = $"description {sc.ServiceName} \"{sc.Description.Trim()}\"";

                // run command
                await this.StartAndWaitForExit("sc.exe", args, "open", 0, cancellationToken);
            }

            sc.Svc ??= this.GetService(sc);
        }

        public async Task<int> Uninstall(CancellationToken cancellationToken = default)
        {
            if (!this.isValidated.HasValue)
                throw new Exception("Validation must be executed!");
            if (!this.isValidated.Value)
                throw new Exception("Validation was not successful!");

            // check if process runs with admin rights
            if (!WindowsAPI.IsAdmin)
                return await this.RestartProcessAsAdmin(ServiceCommands.Uninstall, cancellationToken);

            var result = 0;
            foreach (var sc in this.services)
                try
                {
                    // Service suchen
                    sc.Svc = this.GetService(sc);
                    if (sc.Svc == null) continue;

                    if (!await this.InternalStop(sc, r => result = r, cancellationToken))
                        return result;

                    // Netsh-Berechtigungen entfernen
                    using (var reg = Registry.LocalMachine.CreateSubKey($"Software\\LeveroApps\\Services\\{sc.ServiceName}"))
                    {
                        var netshArgs = reg.GetValue("netsh", String.Empty)?.ToString();
                        if (!String.IsNullOrEmpty(netshArgs))
                        {
                            _ = await this.StartAndWaitForExit("netsh", netshArgs, cancellationToken: cancellationToken);

                            reg.DeleteValue("netsh");
                        }
                    }

                    // Services deinstallieren
                    await this.UninstallService(sc, cancellationToken);
                }
                catch (Exception ex)
                {
                    this.log.LogError(ex, $"Service {sc.ServiceName}: Uninstalling windows service failed!");
                    return -5;
                }
            return CommonCommands.SUCCESS;
        }

        private Task UninstallService(WindowsServiceConfiguration sc, CancellationToken cancellationToken)
        {
            // build command line arguments of sc.exe
            var args = $"delete {sc.ServiceName}";

            // run command
            return this.StartAndWaitForExit("sc.exe", args, "open", 0, cancellationToken);
        }

        public async Task<bool> InstallForConsole(CancellationToken cancellationToken = default)
        {
            foreach (var sc in this.services)
            {
                if (!sc.HttpServerPort.HasValue)
                    continue;

                // only run netsh if no service is installed!
                if (this.GetService(sc) == null)
                    _ = await this.StartAndWaitForExit("netsh",
                        $"http add urlacl url=http://+:{sc.HttpServerPort.Value}/ user={Environment.UserDomainName}\\{Environment.UserName}",
                        "runas", cancellationToken);
            }
            return true;
        }

        public async Task<bool> UninstallForConsole(CancellationToken cancellationToken = default)
        {
            foreach (var sc in this.services)
            {
                if (!sc.HttpServerPort.HasValue)
                    continue;

                // only delete netsh if no service is installed!
                if (this.GetService(sc) == null)
                    _ = await this.StartAndWaitForExit("netsh",
                        $"http delete urlacl url=http://+:{sc.HttpServerPort.Value}/",
                        "runas", cancellationToken);
            }
            return true;
        }

        public async Task<int> Start(CancellationToken cancellationToken = default)
        {
            if (!this.isValidated.HasValue)
                throw new Exception("Validation must be executed!");
            if (!this.isValidated.Value)
                throw new Exception("Validation was not successful!");

            // check if process runs with admin rights
            if (!WindowsAPI.IsAdmin)
                return await this.RestartProcessAsAdmin(ServiceCommands.Start, cancellationToken);

            var result = 0;
            foreach (var sc in this.services)
            {
                sc.Svc = this.GetService(sc);
                if (!await this.InternalStart(sc, r => result = r, cancellationToken))
                    break;
            }
            return result;
        }

        private async Task<bool> InternalStart(WindowsServiceConfiguration sc, Action<int> result, CancellationToken cancellationToken)
        {
            result(-2);
            if (sc.Svc == null)
            {
                this.log.LogError($"Service {sc.ServiceName}: windows service cannot be started because it is not installed!");
                return false;
            }

            // Service stoppen
            if (sc.Svc.Status == ServiceControllerStatus.Stopped)
                try
                {
                    await Task.Run(() =>
                    {
                        sc.Svc.Start();
                        sc.Svc.WaitForStatus(ServiceControllerStatus.Running, sc.WaitTimeForStart!.Value);
                    }, cancellationToken);
                }
                catch (System.ServiceProcess.TimeoutException ex)
                {
                    this.log.LogError(ex, $"Service {sc.ServiceName}: Starting windows service timed out after {sc.WaitTimeForStart!.Value.TotalSeconds:0} seconds!");
                    return false;
                }
                catch (Exception ex)
                {
                    this.log.LogError(ex, $"Service {sc.ServiceName}: Starting windows service failed!");
                    return false;
                }

            // Kontrolle ob gestoppt
            if (sc.Svc.Status != ServiceControllerStatus.Running)
            {
                this.log.LogError($"Service {sc.ServiceName}: Windows service is in state {sc.Svc.Status} and cannot be started!");
                result(-3);
                return false;
            }
            result(0);
            return true;
        }

        public async Task<int> Stop(CancellationToken cancellationToken = default)
        {
            if (!this.isValidated.HasValue)
                throw new Exception("Validation must be executed!");
            if (!this.isValidated.Value)
                throw new Exception("Validation was not successful!");

            // check if process runs with admin rights
            if (!WindowsAPI.IsAdmin)
                return await this.RestartProcessAsAdmin(ServiceCommands.Stop, cancellationToken);

            var result = 0;
            foreach (var sc in this.services)
            {
                sc.Svc = this.GetService(sc);
                if (!await this.InternalStop(sc, r => result = r, cancellationToken))
                    break;
            }
            return result;
        }

        private async Task<bool> InternalStop(WindowsServiceConfiguration sc, Action<int> result, CancellationToken cancellationToken)
        {
            result(-4);
            if (sc.Svc == null)
            {
                this.log.LogError($"Service {sc.ServiceName}: windows service cannot be stopped because it is not installed!");
                return false;
            }

            // Service stoppen
            if (sc.Svc.Status == ServiceControllerStatus.Running)
                try
                {
                    await Task.Run(() =>
                    {
                        sc.Svc.Stop();
                        sc.Svc.WaitForStatus(ServiceControllerStatus.Stopped, sc.WaitTimeForStop!.Value);
                    }, cancellationToken);
                }
                catch (System.ServiceProcess.TimeoutException ex)
                {
                    this.log.LogError(ex, $"Service {sc.ServiceName}: Stopping windows service timed out after {sc.WaitTimeForStop!.Value.TotalSeconds:0} seconds!");
                    return false;
                }
                catch (Exception ex)
                {
                    this.log.LogError(ex, $"Service {sc.ServiceName}: Stopping windows service failed!");
                    return false;
                }

            // Kontrolle ob gestoppt
            if (sc.Svc.Status != ServiceControllerStatus.Stopped)
            {
                this.log.LogError($"Service {sc.ServiceName}: Windows service is in state {sc.Svc.Status} and cannot be stopped!");
                result(-3);
                return false;
            }
            result(0);
            return true;
        }
    }
}
