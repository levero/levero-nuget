﻿namespace Levero.Common.Services
{
    public static class ServiceCommands
    {
        public const string Install = "install";
        public const string Install_Aliases = "i -i /i --install -install /install";
        public const string Install_Description = "installs all registered services";

        public const string Start = "start";
        public const string Start_Aliases = "--start -start /start";
        public const string Start_Description = "display available commands and options";

        public const string Stop = "stop";
        public const string Stop_Aliases = "--stop -stop /stop";
        public const string Stop_Description = "display available commands and options";

        public const string Uninstall = "uninstall";
        public const string Uninstall_Aliases = "u -u /u --uninstall -uninstall /uninstall";
        public const string Uninstall_Description = "uninstalls all registered services";
    }
}
