﻿using System;
using System.ServiceProcess;

namespace Levero.Common.Services
{
    [Serializable]
    public sealed class WindowsServiceConfiguration
    {
        public string? ServiceName { get; set; }

        public string? DisplayName { get; set; }

        public string? Description { get; set; }

        public ServiceStartMode StartType { get; set; } = ServiceStartMode.Automatic;

        /// <summary>
        /// if service is running a http service, netsh commands must be generated and called.
        /// </summary>
        public int? HttpServerPort { get; set; }

        /// <summary>
        /// may be SYSTEM, NETWORK, domain\username or machine\username.
        /// if null NETWORK will be used.
        /// </summary>
        public string ServiceAccount_Username { get; set; } = ServiceAccount_NetworkService;

        /// <summary>
        /// if username is not SYSTEM or NETWORK a password is required.
        /// </summary>
        public string? ServiceAccount_Password { get; set; }

        /// <summary>
        /// How long may take the start process of service.
        /// if null it will be 10 seconds.
        /// </summary>
        public TimeSpan? WaitTimeForStart { get; set; }

        /// <summary>
        /// How long may take the stop process of service.
        /// if null it will be 10 seconds.
        /// </summary>
        public TimeSpan? WaitTimeForStop { get; set; }

        public const string ServiceAccount_LocalSystem = "SYSTEM";
        public const string ServiceAccount_NetworkService = "NETWORK";

        ///////////////////////////////////////////////////////////////////////
        /// internals
        ///////////////////////////////////////////////////////////////////////

        internal string? PreparedUsername;
        internal string? PreparedPassword;
        internal bool GrantUserRunAsService;
        internal bool IsValidated;

        internal ServiceController? Svc;
    }
}
