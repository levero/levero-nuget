﻿using Levero.Common.CommandLine;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.Services
{
    public sealed class CommandStart : CommandLineCommand
    {
        public CommandStart(WindowsServiceInstaller _installer)
            : base(ServiceCommands.Start, ServiceCommands.Start_Aliases.Split(' '), ServiceCommands.Start_Description)
        {
            this.installer = _installer;
        }

        private readonly WindowsServiceInstaller installer;

        public override Task<int> Validator(CommandLineContext context, CancellationToken cancellationToken = default)
            => this.installer.Validate(context.WriteLine, cancellationToken);

        public override Task<int> Handler(CommandLineContext context, CancellationToken cancellationToken = default)
            => this.installer.Start(cancellationToken);
    }
}
