﻿using Levero.Common.CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Levero.Common.Services
{
    public static class IHostBuilderExtensions_ServiceInstaller
    {
        public static IHostBuilder UseWindowsServiceInstaller(this IHostBuilder builder)
            => builder.ConfigureServices((context, services) => 
                services
                    .Configure<WindowsServicesConfiguration>(context.Configuration.GetSection(nameof(WindowsServiceConfiguration)))
                    .AddSingleton<WindowsServiceInstaller>()
                    .AddCommandLineCommand<CommandInstall>()
                    .AddCommandLineCommand<CommandStart>()
                    .AddCommandLineCommand<CommandStop>()
                    .AddCommandLineCommand<CommandUninstall>());
    }
}
