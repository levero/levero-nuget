﻿using Levero.Common.CommandLine;
using System.Threading;
using System.Threading.Tasks;

namespace Levero.Common.Services
{
    public sealed class CommandInstall : CommandLineCommand
    {
        public CommandInstall(WindowsServiceInstaller _installer)
            : base(ServiceCommands.Install, ServiceCommands.Install_Aliases.Split(' '), ServiceCommands.Install_Description)
        {
            this.installer = _installer;
        }

        private readonly WindowsServiceInstaller installer;

        public override Task<int> Validator(CommandLineContext context, CancellationToken cancellationToken = default)
            => this.installer.Validate(context.WriteLine, cancellationToken);

        public override Task<int> Handler(CommandLineContext context, CancellationToken cancellationToken = default)
            => this.installer.Install(cancellationToken);
    }
}
