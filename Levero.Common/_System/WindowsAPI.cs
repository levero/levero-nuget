﻿using Microsoft.Win32.SafeHandles;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;

namespace System
{
    public static class WindowsAPI
    {
        #region Konsole öffnen

        //        public static bool InitConsole()
        //        {
        //            try
        //            {
        //                try
        //                {
        //                    _ = Console.Title;
        //                    // if we can read the title, console has a valid handle
        //                    return true;
        //                }
        //                catch { }

        //#if !NET472
        //                // In .NET Core the code page 437 is not included by default.
        //                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        //#endif

        //                if (Is64Process(Process.GetCurrentProcess()))
        //                {
        //                    // try to attach a console
        //                    Kernel64.AttachConsole(Kernel64.ATTACH_PARENT_PROCESS);
        //                    Kernel64.AllocConsole();
        //                }
        //                else
        //                {
        //                    Kernel32.AllocConsole();
        //                } 
        //            }
        //            finally
        //            {
        //                if (IsInDebugger)
        //                {
        //                    // since VS 2017 there is a strict Console Redirection to output window - this is not the wanted behaviour :-(
        //                    // Get the handle to CONOUT$.    
        //                    var stdHandle = Kernel32.CreateFile("CONOUT$", Kernel32.GENERIC_WRITE, Kernel32.FILE_SHARE_WRITE, 0, Kernel32.OPEN_EXISTING, 0, 0);
        //                    var safeFileHandle = new SafeFileHandle(stdHandle, true);
        //                    var fileStream = new FileStream(safeFileHandle, FileAccess.Write);
        //                    var encoding = Encoding.GetEncoding(Kernel32.MY_CODE_PAGE);
        //                    var standardOutput = new StreamWriter(fileStream, encoding)
        //                    {
        //                        AutoFlush = true
        //                    };
        //                    Console.SetOut(standardOutput);
        //                }
        //            }
        //        }

        public static bool FreeConsole()
        {
            return Is64Process(Process.GetCurrentProcess()) ?
                Kernel64.FreeConsole() :
                Kernel32.FreeConsole();
        }
        #endregion

        #region Informationen zu Prozesse: x64?, RunningAsService?

        public static bool IsInDebugger => Debugger.IsAttached;

        public static bool Is64Process(Process process)
        {
            var pm = process?.Modules.OfType<ProcessModule>()
                .Where(m => m.ModuleName != null && m.ModuleName.ToLower().StartsWith("kernel"))
                .FirstOrDefault()
                ?? throw new Exception("Cannot detect x86/x64 environment!");

            return pm.ModuleName?.ToLower() switch
            {
                "kernel32.dll" => false,
                "kernel64.dll" => true,
                _ => throw new Exception("Cannot detect x86/x64 environment!")
            };
        }

        public enum AppRunningType
        {
            Unknown,
            Interactive,
            WindowsService,
            Batch
        }

        /// <summary>
        /// DO NOT USE Environment.UserInteractive because .NET Core has implemented "return true"
        /// </summary>
        /// <param name="process"></param>
        /// <returns>true, wenn als Service gestartet<para/>
        /// false, wenn interaktiv gestartet<para/>
        /// Exception, wenn keines der beiden zutrifft!</returns>
        public static AppRunningType GetRunningTypeFromSecurityIdentifier()
        {
            // try windows identity first
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            var wi = WindowsIdentity.GetCurrent();
            if (wi?.Groups != null)
                foreach (var group in wi.Groups)
                    if (group is SecurityIdentifier sec)
                    {
                        if (sec.IsWellKnown(WellKnownSidType.InteractiveSid))
                            return AppRunningType.Interactive;
                        else if (sec.IsWellKnown(WellKnownSidType.ServiceSid))
                            return AppRunningType.WindowsService;
                        else if (sec.IsWellKnown(WellKnownSidType.ServiceSid))
                            return AppRunningType.Batch;
                    }
            return AppRunningType.Unknown;
        }

        public static bool IsProcessRunningAsService(Process process)
        {
            try
            {
                // try parent process name afterwards
                // copied from Topshelf.Runtime.Windows.WindowsHostEnvironment
                return GetParent(process)?.ProcessName == "services";
            }
            catch (InvalidOperationException)
            {
                // again, mono seems to fail with this, let's just return false okay?
                return false;
            }
        }

        #endregion

#if NET472
        public class HandleHelper32 : System.Windows.Forms.IWin32Window
        {
            public HandleHelper32()
            {
                this.Handle = User32.GetActiveWindow();
            }

            public HandleHelper32(IntPtr hWnd)
            {
                this.Handle = hWnd;
            }

        #region IWin32Window Member

            public IntPtr Handle { get; }

        #endregion
        }
#endif

        //'FindWindowLike
        //' - Finds the window handles of the windows matching the specified
        //'   parameters
        //'
        //'hwndArray()
        //' - An integer array used to return the window handles
        //'
        //'hWndStart
        //' - The handle of the window to search under.
        //' - The routine searches through all of this window's children and their
        //'   children recursively.
        //' - If hWndStart = 0 then the routine searches through all windows.
        //'
        //'WindowText
        //' - The pattern used with the Like operator to compare window's text.
        //'
        //'ClassName
        //' - The pattern used with the Like operator to compare window's class
        //'   name.
        //'
        //'ID
        //' - A child ID number used to identify a window.
        //' - Can be a decimal number or a hex string.
        //' - Prefix hex strings with "&H" or an error will occur.
        //' - To ignore the ID pass the Visual Basic Null function.
        //'
        //'Returns
        //' - The number of windows that matched the parameters.
        //' - Also returns the window handles in hWndArray()
        //'
        //'----------------------------------------------------------------------
        //'Remove this next line to use the strong-typed declarations

        // Hold the level of recursion:
        private static int level = 0;
        // Hold the number of matching windows:
        private static int iFound = 0;

        public static int FindWindowLike(ref List<IntPtr> hWndArray, IntPtr hWndStart, string WindowText, string Classname, int? ID)
        {
            IntPtr hwnd;
            string sWindowText;
            string sClassname;
            int? sID;
            // Initialize if necessary:
            if (level == 0)
            {
                iFound = 0;
                if (hWndArray == null)
                    hWndArray = new List<IntPtr>();

                if (hWndStart.ToInt32() == 0)
                    hWndStart = User32.GetDesktopWindow();
            }
            // Increase recursion counter:
            level += 1;
            // Get first child window:
            hwnd = User32.GetWindow(hWndStart, User32.GW32.GW_CHILD);
            while (hwnd.ToInt32() != 0)
            {
                //DoEvents ' Not necessary
                // Search children by recursion:
                _ = FindWindowLike(ref hWndArray, hwnd, WindowText, Classname, ID);
                // Get the window text and class name:
                //sWindowText = Space(255)
                var sb = new StringBuilder(255);
                sb.Length = User32.GetWindowText(hwnd, sb, sb.Capacity);
                sWindowText = sb.ToString();
                //sClassname = Space(255)
                sb.Length = 0;
                sb.Length = User32.GetClassName(hwnd, sb, sb.Capacity);
                sClassname = sb.ToString();
                // If window is a child get the ID:
                if (User32.GetParent(hwnd).ToInt32() != 0)
                {
                    var r = User32.GetWindowLong(hwnd, User32.GWL32.GWL_ID);
                    sID = r;//CLng("&H" & Hex(r))
                }
                else
                    sID = null;
                // Check that window matches the search parameters:
                if ((sWindowText == WindowText) && (sClassname == Classname))
                {
                    if (!ID.HasValue)
                    {
                        // If find a match, increment counter and
                        //  add handle to array:
                        iFound += 1;
                        hWndArray.Add(hwnd);
                    }
                    else if (sID.HasValue)
                    {
                        if (sID.Value == ID.Value)
                        {
                            // If find a match increment counter and
                            //  add handle to array:
                            iFound += 1;
                            hWndArray.Add(hwnd);
                        }
                    }
                    //Debug.Print "Window Found: "
                    //Debug.Print "  Window Text  : " & sWindowText
                    //Debug.Print "  Window Class : " & sClassname
                    //Debug.Print "  Window Handle: " & CStr(hwnd)
                }
                // Get next child window:
                hwnd = User32.GetWindow(hwnd, User32.GW32.GW_HWNDNEXT);
            }

            // Decrement recursion counter:
            level -= 1;
            // Return the number of windows found:
            return iFound;
        }


        #region Start Default Browser

        //private static string _BrowserCommandLine = null;



        //public static void StartBrowser(string filename)
        //{
        //    BaseService.Log("START PROCESS: {0}", filename);
        //    try
        //    {
        //        using (TimeMeasure measure = new TimeMeasure("Process.Start"))
        //        {
        //            string commandline = "\"" + filename + "\"";
        //            string arguments = null;
        //            ProcessStartInfo pi = new ProcessStartInfo();

        //            // Wie soll der Browser geöffnet werden
        //            string defaultBrowser = crmOptions.DefaultBrowser.Trim();
        //            if (defaultBrowser.ToUpper() == "DEFAULT")
        //            {
        //                BaseService.Log("Use Default-Browser");
        //                // Load Browser Default settings
        //                if (_BrowserCommandLine == null)
        //                    try
        //                    {
        //                        BaseService.Log("Search for Default-Browser...");
        //                        // Es funktioniert manchmal sehr langsam (> 30 Sekunden), wenn man nur http://abc mit ShellExecute startet
        //                        // Viel schneller ist es, die exe-Datei vom Browser aufzurufen und den Link als Parameter zu übergeben!
        //                        using (RegistryKey classes = Registry.ClassesRoot)
        //                        using (RegistryKey key = classes.OpenSubKey(@"\http\shell\open\command"))
        //                            _BrowserCommandLine = (String)key.GetValue(null);

        //                        BaseService.Log("Default Browser found: {0}", _BrowserCommandLine);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        BaseService.Error(new Exception("Cannot read Default-Browser settings!", ex), false);
        //                    }
        //                defaultBrowser = _BrowserCommandLine;
        //            }
        //            BaseService.Log("Browser commandline: {0}", defaultBrowser);

        //            // Kommandozeile auswerten
        //            if (!String.IsNullOrEmpty(defaultBrowser))
        //            {
        //                if (defaultBrowser.IndexOf("\"%1\"") > 0)
        //                {
        //                    BaseService.Log("Replace \"%1\" from commandline with url");
        //                    commandline = defaultBrowser.Replace("\"%1\"", commandline);
        //                }
        //                else if (defaultBrowser.IndexOf("%1") > 0)
        //                {
        //                    BaseService.Log("Replace %1 from commandline with url");
        //                    commandline = defaultBrowser.Replace("%1", commandline);
        //                }
        //                else
        //                {
        //                    BaseService.Log("Concat commandline with url");
        //                    commandline = defaultBrowser + " " + commandline;
        //                }
        //                BaseService.Log("Split commandline into filename and arguments");
        //                SplitCommandLine(commandline, out commandline, out arguments);
        //            }

        //            BaseService.Log("START PROCESS - filename: {0}", commandline);
        //            BaseService.Log("START PROCESS - arguments: {0}", arguments);
        //            if (ShellExecute(IntPtr.Zero, "open", commandline, arguments, null, ShowCommands.SW_SHOWMAXIMIZED).ToInt64() <= 32)
        //            {
        //                BaseService.Error(new Exception("ShellExecute didn't work!"), false);
        //                XtraMessageBox.Show("Der Browser kann nicht gestartet werden!", "Fehler beim Browser starten");
        //            }
        //            //pi.FileName = commandline;
        //            //pi.Arguments = arguments;
        //            //pi.UseShellExecute = false;
        //            //pi.Verb = "open";

        //            //Process.Start(pi);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        BaseService.Error(ex, false);
        //    }
        //}

        //private static void SplitCommandLine(string commandline, out string filename, out string arguments)
        //{
        //    filename = "";
        //    arguments = "";

        //    var quoted = false;
        //    var isCmd = true;
        //    foreach (var ch in commandline)
        //    {
        //        if (ch == '"')
        //            quoted = !quoted;
        //        if (ch == ' ' && !quoted && isCmd)
        //            isCmd = false;
        //        else if (isCmd)
        //            filename += ch;
        //        else
        //            arguments += ch;
        //    }
        //}

        #endregion

#if NET472
        /// <summary>
        /// Liefert das Handle vom aktiven Fenster in dem aktuellen Thread
        /// </summary>
        public static System.Windows.Forms.IWin32Window ActiveWindow => new HandleHelper32();

        /// <summary>
        /// Liefert das übergeordnete Handle vom aktiven Fenster in dem aktuellen Thread
        /// </summary>
        public static System.Windows.Forms.IWin32Window ActiveTopWindow
        {
            get
            {
                var parent = User32.GetActiveWindow();
                IntPtr hwnd;
                while ((hwnd = User32.GetWindow(parent, User32.GW32.GW_OWNER)).ToInt32() != 0)
                    parent = hwnd;
                return new HandleHelper32(parent);
            }
        }
        /// <summary>
        /// Liefert das Handle vom Fenster mit dem Fokus (Threadunabhängig)
        /// </summary>
        public static System.Windows.Forms.IWin32Window ForegroundWindow => new HandleHelper32(User32.GetForegroundWindow());

        /// <summary>
        /// Liefert das übergeordnete Handle vom Fenster mit dem Fokus (Threadunabhängig)
        /// </summary>
        public static System.Windows.Forms.IWin32Window ForegroundTopWindow
        {
            get
            {
                var parent = User32.GetForegroundWindow();
                IntPtr hwnd;
                while ((hwnd = User32.GetWindow(parent, User32.GW32.GW_OWNER)).ToInt32() != 0)
                    parent = hwnd;
                return new HandleHelper32(parent);
            }
        }
#endif

        /// <summary>
        /// check if user principal has admin permissions
        /// </summary>
        public static bool IsAdmin
        {
            get
            {
                AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                var wi = WindowsIdentity.GetCurrent();
                if (wi != null)
                {
                    var wp = new WindowsPrincipal(wi);
                    return wp.IsInRole(WindowsBuiltInRole.Administrator);
                }
                return false;
            }
        }

        private static Process? GetParent(Process child)
        {
            if (child == null)
                throw new ArgumentNullException("child");

            try
            {
                var parentPid = 0;

                var hnd = Kernel32.CreateToolhelp32Snapshot(Kernel32.TH32CS_SNAPPROCESS, 0);

                if (IntPtr.Zero == hnd)
                    return null;

                var processInfo = new Kernel32.PROCESSENTRY32
                {
                    dwSize = (uint)Marshal.SizeOf(typeof(Kernel32.PROCESSENTRY32))
                };

                if (Kernel32.Process32First(hnd, ref processInfo) == false)
                    return null;

                do
                {
                    if (child.Id == processInfo.th32ProcessID)
                        parentPid = (int)processInfo.th32ParentProcessID;
                }
                while (parentPid == 0 && Kernel32.Process32Next(hnd, ref processInfo));

                if (parentPid > 0)
                    return Process.GetProcessById(parentPid);
            }
            catch //(Exception ex)
            {
                //_log.Error("Unable to get parent process (ignored)", ex);
            }
            return null;
        }

#if NET472
        // Define BCM_SETSHIELD locally, declared originally in Commctrl.h
        public const uint BCM_SETSHIELD = 0x0000160C;
        public const uint WM_CLOSE = 0x0010;

        ///////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Enables the elevated shield icon on the given button control
        /// </summary>
        /// <param name="ThisButton">
        ///     Button control to enable the elevated shield icon on.
        /// </param>
        ///////////////////////////////////////////////////////////////////////
        public static void EnableElevateIcon32(System.Windows.Forms.Button button)
        {
            // Input validation, validate that ThisControl is not null
            if (button == null)
                return;


            // Set button style to the system style
            button.FlatStyle = System.Windows.Forms.FlatStyle.System;

            // Send the BCM_SETSHIELD message to the button control
            User32.SendMessage(button.Handle, BCM_SETSHIELD, new IntPtr(0), new IntPtr(1));
        }
#endif

        public static Process? RunElevated(string Executable, string Arguments)
        {
            var startInfo = new ProcessStartInfo
            {
                UseShellExecute = true,
                WorkingDirectory = Environment.CurrentDirectory,
                FileName = Executable,
                Arguments = Arguments,
                Verb = "runas" // Start als Admin
            };

            return Process.Start(startInfo);
        }

        #region Kontoberechtigungen

        public static void GrantLogonAsService(string accountName)
            => SetRight(accountName, AdvApi32.SE_SERVICE_LOGON_NAME);


        /// <summary>Adds a privilege to an account</summary>
        /// <param name="accountName">Name of an account - "domain\account" or only "account"</param>
        /// <param name="privilegeName">Name ofthe privilege</param>
        /// <returns>The windows error code returned by LsaAddAccountRights</returns>
        public static void SetRight(string accountName, string privilegeName)
        {
            //pointer an size for the SID
            var sid = IntPtr.Zero;
            var sidSize = 0;
            //StringBuilder and size for the domain name
            var nameSize = 0;
            //account-type variable for lookup
            var accountType = 0;
            //initialize a pointer for the policy handle
            var policyHandle = IntPtr.Zero;
            //these attributes are not used, but LsaOpenPolicy wants them to exists
            var ObjectAttributes = new AdvApi32.LSA_OBJECT_ATTRIBUTES();
            //initialize an empty unicode-string
            var systemName = new AdvApi32.LSA_UNICODE_STRING();
            //combine all policies
            var access =
                AdvApi32.LSA_AccessPolicy.POLICY_AUDIT_LOG_ADMIN |
                AdvApi32.LSA_AccessPolicy.POLICY_CREATE_ACCOUNT |
                AdvApi32.LSA_AccessPolicy.POLICY_CREATE_PRIVILEGE |
                AdvApi32.LSA_AccessPolicy.POLICY_CREATE_SECRET |
                AdvApi32.LSA_AccessPolicy.POLICY_GET_PRIVATE_INFORMATION |
                AdvApi32.LSA_AccessPolicy.POLICY_LOOKUP_NAMES |
                AdvApi32.LSA_AccessPolicy.POLICY_NOTIFICATION |
                AdvApi32.LSA_AccessPolicy.POLICY_SERVER_ADMIN |
                AdvApi32.LSA_AccessPolicy.POLICY_SET_AUDIT_REQUIREMENTS |
                AdvApi32.LSA_AccessPolicy.POLICY_SET_DEFAULT_QUOTA_LIMITS |
                AdvApi32.LSA_AccessPolicy.POLICY_TRUST_ADMIN |
                AdvApi32.LSA_AccessPolicy.POLICY_VIEW_AUDIT_INFORMATION |
                AdvApi32.LSA_AccessPolicy.POLICY_VIEW_LOCAL_INFORMATION;

            try
            {
                //get required buffer size
                AdvApi32.LookupAccountName(String.Empty, accountName, sid, ref sidSize, null, ref nameSize, ref accountType);

                //allocate buffers
                var domainName = new StringBuilder(nameSize);
                sid = Marshal.AllocHGlobal(sidSize);

                //lookup the SID for the account
                var result = AdvApi32.LookupAccountName(String.Empty, accountName, sid, ref sidSize, domainName, ref nameSize, ref accountType);

                //say what you're doing
                //Console.WriteLine("LookupAccountName result = " + result);
                //Console.WriteLine("IsValidSid: " + IsValidSid(sid));
                //Console.WriteLine("LookupAccountName domainName: " + domainName.ToString());

                if (!result)
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                //get a policy handle
                var res = AdvApi32.LsaOpenPolicy(ref systemName, ref ObjectAttributes, access, out policyHandle);
                if (res != 0)
                    throw new Win32Exception(AdvApi32.LsaNtStatusToWinError(res));

                //Now that we have the SID an the policy,
                //we can add rights to the account.

                //initialize an unicode-string for the privilege name
                var userRights = new AdvApi32.LSA_UNICODE_STRING[1];
                userRights[0] = new AdvApi32.LSA_UNICODE_STRING
                {
                    Buffer = Marshal.StringToHGlobalUni(privilegeName),
                    Length = (ushort)(privilegeName.Length * UnicodeEncoding.CharSize),
                    MaximumLength = (ushort)((privilegeName.Length + 1) * UnicodeEncoding.CharSize)
                };

                //add the right to the account
                res = AdvApi32.LsaAddAccountRights(policyHandle, sid, userRights, 1);
                if (res != 0)
                    throw new Win32Exception(AdvApi32.LsaNtStatusToWinError(res));
            }
            finally
            {
                AdvApi32.LsaClose(policyHandle);
                AdvApi32.FreeSid(sid);
            }
        }

        #endregion


        #region Console

        public static void EnableVirtualTerminalProcessing()
        {
            IntPtr hOut = Kernel32.GetStdHandle(Kernel32.STD_OUTPUT_HANDLE);
            if ((int)hOut == Kernel32.INVALID_HANDLE_VALUE)
                throw new Exception("STD_OUTPUT_HANDLE cannot be retrieved!");

            if (!Kernel32.GetConsoleMode(hOut, out var mode))
                throw new Exception($"GetConsoleMode failed with {Marshal.GetLastWin32Error():X8}");

            var current = mode;
            mode |= (uint)Kernel32.ConsoleOutputModes.ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            if (current != mode)
                if (!Kernel32.SetConsoleMode(hOut, mode))
                    throw new Exception($"SetConsoleMode failed with {Marshal.GetLastWin32Error():X8}");
        }

        public static void DisableVirtualTerminalProcessing()
        {
            IntPtr hOut = Kernel32.GetStdHandle(Kernel32.STD_OUTPUT_HANDLE);
            if ((int)hOut == Kernel32.INVALID_HANDLE_VALUE)
                throw new Exception("STD_OUTPUT_HANDLE cannot be retrieved!");

            if (!Kernel32.GetConsoleMode(hOut, out var mode))
                throw new Exception($"GetConsoleMode failed with {Marshal.GetLastWin32Error():X8}");

            var current = mode;
            mode &= ~(uint)Kernel32.ConsoleOutputModes.ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            if (current != mode)
                if (!Kernel32.SetConsoleMode(hOut, mode))
                    throw new Exception($"SetConsoleMode failed with {Marshal.GetLastWin32Error():X8}");
        }

        #endregion
    }
}