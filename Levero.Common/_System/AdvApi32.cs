﻿using System.Runtime.InteropServices;
using System.Text;

namespace System
{
    public static class AdvApi32
    {
        // Import the LSA functions

        [DllImport("advapi32", PreserveSig = true)]
        public static extern int LsaOpenPolicy(
            ref LSA_UNICODE_STRING SystemName,
            ref LSA_OBJECT_ATTRIBUTES ObjectAttributes,
            LSA_AccessPolicy DesiredAccess,
            out IntPtr PolicyHandle
        );

        [DllImport("advapi32", SetLastError = true, PreserveSig = true)]
        public static extern int LsaAddAccountRights(
            IntPtr PolicyHandle,
            IntPtr AccountSid,
            LSA_UNICODE_STRING[] UserRights,
            long CountOfRights);

        [DllImport("advapi32")]
        public static extern void FreeSid(IntPtr pSid);

        [DllImport("advapi32", CharSet = CharSet.Auto, SetLastError = true, PreserveSig = true)]
        public static extern bool LookupAccountName(
            string lpSystemName, 
            string lpAccountName,
            IntPtr psid,
            ref int cbsid,
            StringBuilder? domainName, 
            ref int cbdomainLength, 
            ref int use);

        [DllImport("advapi32")]
        public static extern bool IsValidSid(IntPtr pSid);

        [DllImport("advapi32")]
        public static extern long LsaClose(IntPtr ObjectHandle);

        [DllImport("advapi32")]
        public static extern int LsaNtStatusToWinError(long status);

        // define the structures

        [StructLayout(LayoutKind.Sequential)]
        public struct LSA_UNICODE_STRING
        {
            public ushort Length;
            public ushort MaximumLength;
            public IntPtr Buffer;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct LSA_OBJECT_ATTRIBUTES
        {
            public int Length;
            public IntPtr RootDirectory;
            public LSA_UNICODE_STRING ObjectName;
            public uint Attributes;
            public IntPtr SecurityDescriptor;
            public IntPtr SecurityQualityOfService;
        }

        // enum all policies

        [Flags]
        public enum LSA_AccessPolicy : uint
        {
            POLICY_VIEW_LOCAL_INFORMATION = 0x00000001,
            POLICY_VIEW_AUDIT_INFORMATION = 0x00000002,
            POLICY_GET_PRIVATE_INFORMATION = 0x00000004,
            POLICY_TRUST_ADMIN = 0x00000008,
            POLICY_CREATE_ACCOUNT = 0x00000010,
            POLICY_CREATE_SECRET = 0x00000020,
            POLICY_CREATE_PRIVILEGE = 0x00000040,
            POLICY_SET_DEFAULT_QUOTA_LIMITS = 0x00000080,
            POLICY_SET_AUDIT_REQUIREMENTS = 0x00000100,
            POLICY_AUDIT_LOG_ADMIN = 0x00000200,
            POLICY_SERVER_ADMIN = 0x00000400,
            POLICY_LOOKUP_NAMES = 0x00000800,
            POLICY_NOTIFICATION = 0x00001000
        }

        public const string SE_SERVICE_LOGON_NAME = "SeServiceLogonRight";

        public enum LogonType : int
        {
            LOGON32_LOGON_Interactive = 2,
            LOGON32_LOGON_Network = 3,
            LOGON32_LOGON_Batch = 4,
            LOGON32_LOGON_Service = 5,
            LOGON32_LOGON_Unlock = 7,
            LOGON32_LOGON_NetworkClearText = 8,
            LOGON32_LOGON_NewCredentials = 9,
        }

        public enum LogonProvider : int
        {
            LOGON32_PROVIDER_Default = 0,
            LOGON32_PROVIDER_WinNT40 = 2,
            LOGON32_PROVIDER_WinNT50 = 3,
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(
            string lpszUsername,
            string lpszDomain,
            string lpszPassword,
            LogonType dwLogonType,
            LogonProvider dwLogonProvider,
            ref IntPtr phToken);


    }
}
