﻿using System.Runtime.InteropServices;

namespace System
{
    public static class Kernel64
    {
        [DllImport("kernel64")]
        public static extern bool AllocConsole();

        [DllImport("kernel64")]
        public static extern bool FreeConsole();

        [DllImport("kernel32")]
        public static extern bool AttachConsole(int dwProcessId);

        public const int ATTACH_PARENT_PROCESS = -1;
    }
}
