﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static class ExceptionExtensions
    {
        public static string GetDeepMessage(this Exception? ex)
        {
            if (ex == null)
                return "<null>";
            return GetMessages(ex).ToArray().StringJoin(Environment.NewLine)!;
        }

        private static IEnumerable<string> GetMessages(Exception ex, bool details = false)
        {
            if (ex is AggregateException aex)
            {
                foreach (var inner in aex.Flatten().InnerExceptions)
                    foreach (var msg in GetMessages(inner, details))
                        yield return msg;
            }
            else
            {
                yield return ex.Message;
                if (ex.InnerException != null)
                    foreach (var msg in GetMessages(ex.InnerException, true))
                        yield return msg;
            }
        }
    }
}
