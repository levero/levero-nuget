﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace System
{
    public static class DictionaryExtensions
    {
        [return: MaybeNull]
        public static TValue TryGetValue<TKey, TValue>(this Dictionary<TKey, TValue> source, TKey key, TValue? defaultValue = default)
            where TKey : notnull
        {
            return source != null
                && source.TryGetValue(key, out var result)
                ? result
                : defaultValue;
        }
    }
}
