﻿namespace System
{
    public static class ArrayOfValueTypeExtensions
    {
        public static T TryGet<T>(this T[]? source, int index, T defaultValue = default)
            where T : struct
        {
            if (source == null ||
                index < source.GetLowerBound(0) ||
                index > source.GetUpperBound(0))
                return defaultValue;

            return source[index];
        }

        public static T TryGet<T>(this T[]? source, int index, Func<T> getDefaultValue)
            where T : struct
        {
            if (source == null ||
                index < source.GetLowerBound(0) ||
                index > source.GetUpperBound(0))
            {
                return getDefaultValue == null ? (default) : getDefaultValue();
            }
            return source[index];
        }

        public static T TryGet<T>(this T[]? source, int index, Func<T[]?, T> getDefaultValue)
            where T : struct
        {
            if (source == null ||
                index < source.GetLowerBound(0) ||
                index > source.GetUpperBound(0))
            {
                return getDefaultValue == null ? (default) : getDefaultValue(source);
            }
            return source[index];
        }
    }
}
