﻿using System.Diagnostics.CodeAnalysis;

namespace System.Collections.Generic
{
    public static class QueueExtensions
    {
#if NET472
        public static bool TryDequeue<T>(this Queue<T> queue, out T result)
            where T : notnull
        {
            if (queue.Count == 0)
            {
#pragma warning disable CS8601 // Possible null reference assignment.
                result = default;
#pragma warning restore CS8601 // Possible null reference assignment.
                return false;
            }
            result = queue.Dequeue();
            return true;
        }
#endif
    }
}
