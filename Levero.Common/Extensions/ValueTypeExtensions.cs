﻿namespace System
{
    public static class ValueTypeExtensions
    {
        /// <summary>
        /// if the value is equal to argument "equalsTo" then null is returned.
        /// Method Object.Equals is used for comparison
        /// </summary>
        /// <typeparam name="T">any value type (struct)</typeparam>
        /// <param name="value">a not nullable value</param>
        /// <param name="equalsTo">a not nullable reference</param>
        /// <returns>null if equal to reference or the unchanged value</returns>
        public static T? NullIf<T>(this T value, T equalsTo)
            where T : struct
            => equalsTo.Equals(value) ? (T?)null : value;

        /// <summary>
        /// if the nullable value is equal to argument "equalsTo" then null is returned.
        /// Method Object.Equals is used for comparison
        /// </summary>
        /// <typeparam name="T">any value type (struct)</typeparam>
        /// <param name="value">a nullable value</param>
        /// <param name="equalsTo">a not nullable reference</param>
        /// <returns>null if equal to reference or the unchanged value</returns>
        public static T? NullIf<T>(this T? value, T equalsTo)
            where T : struct
            => equalsTo.Equals(value) ? (T?)null : value;
    }
}
