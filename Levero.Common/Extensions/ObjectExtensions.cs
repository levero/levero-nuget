﻿using System.Collections;

namespace System
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// if the object is equal to argument "equalsTo" then null is returned.
        /// Method Object.Equals is used for comparison
        /// </summary>
        /// <typeparam name="T">any reference type</typeparam>
        /// <param name="obj">a nullable object</param>
        /// <param name="equalsTo">a not nullable reference object</param>
        /// <returns>null if equal to reference or the unchanged object</returns>
        public static T? NullIf<T>(this T? obj, T equalsTo)
            where T : class
            => equalsTo != null && equalsTo.Equals(obj) ? null : obj;
    }
}
