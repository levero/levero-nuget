﻿using System.Threading;
using System.Threading.Tasks;

namespace System.Diagnostics
{
    public static class ProcessExtensions
    {
        /// <summary>
        ///  Instructs the System.Diagnostics.Process component to wait the specified number
        //   of milliseconds for the associated process to exit.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="timeOutInMilliseconds">The amount of time, in milliseconds, to wait for the associated process to exit.
        //     The maximum is the largest possible value of a 32-bit integer, which represents infinity to the operating system.</param>
        /// <returns></returns>
        public static Task<bool> WaitForExitAsync(this Process process, int timeOutInMilliseconds = Int32.MaxValue) 
            => Task.Run(() => process.WaitForExit(timeOutInMilliseconds));

        /// <summary>
        ///  Instructs the System.Diagnostics.Process component to wait the specified number
        //   of milliseconds for the associated process to exit.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static Task<bool> WaitForExitAsync(this Process process, CancellationToken cancellationToken) 
            => Task.Run(() => process.WaitForExit(Int32.MaxValue), cancellationToken);
    }
}
