﻿namespace System
{
    public static class ArrayOfClassExtensions
    {
        public static T? TryGet<T>(this T[]? source, int index, T? defaultValue = default)
            where T : class
        {
            if (source == null ||
                index < source.GetLowerBound(0) ||
                index > source.GetUpperBound(0))
                return defaultValue;

            return source[index];
        }

        public static T? TryGet<T>(this T[]? source, int index, Func<T> getDefaultValue)
            where T : class
        {
            if (source == null ||
                index < source.GetLowerBound(0) ||
                index > source.GetUpperBound(0))
            {
                return getDefaultValue == null ? (default) : getDefaultValue();
            }
            return source[index];
        }

        public static T? TryGet<T>(this T[]? source, int index, Func<T[]?, T> getDefaultValue)
            where T : class
        {
            if (source == null ||
                index < source.GetLowerBound(0) ||
                index > source.GetUpperBound(0))
            {
                return getDefaultValue == null ? (default) : getDefaultValue(source);
            }
            return source[index];
        }

        public static int IndexOf<T>(this T[] source, T item)
            => Array.IndexOf(source, item);

        /// <summary>
        /// if the Array is empty (=> Length == 0) then null is returned.
        /// </summary>
        /// <param name="a">a nullable Array</param>
        /// <returns>null if empty or the unchanged array</returns>
        public static T[]? NullIfEmpty<T>(this T[]? a)
            => a?.Length == 0 ? null : a;
    }
}
