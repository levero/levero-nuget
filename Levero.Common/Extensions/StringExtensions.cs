﻿namespace System
{
    public static class StringExtensions
    {
        /// <summary>
        /// calls String.Format(format, arg0);
        /// <para>see <see cref="String.Format(String, Object)"/></para>
        /// </summary>
        /// <param name="format"></param>
        /// <param name="arg0"></param>
        /// <returns></returns>
        [Obsolete("use interpolated strings instead")]
        public static string f(this string format, object arg0)
            => String.Format(format, arg0);

        /// <summary>
        /// calls String.Format(format, arg0, arg1);
        /// <para>see <see cref="String.Format(String, Object, Object)"/></para>
        /// </summary>
        /// <param name="format"></param>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        [Obsolete("use interpolated strings instead")]
        public static string f(this string format, object arg0, object arg1)
            => String.Format(format, arg0, arg1);

        /// <summary>
        /// calls String.Format(format, arg0, arg1, arg2);
        /// <para>see <see cref="String.Format(String, Object, Object, Object)"/></para>
        /// </summary>
        /// <param name="format"></param>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        [Obsolete("use interpolated strings instead")]
        public static string f(this string format, object arg0, object arg1, object arg2)
            => String.Format(format, arg0, arg1, arg2);

        /// <summary>
        /// calls String.Format(format, args);
        /// <para>see <see cref="String.Format(String, Object[])"/></para>
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [Obsolete("use interpolated strings instead")]
        public static string f(this string format, params object[] args)
            => String.Format(format, args);

        /// <summary>
        /// if the String is empty (=> String.IsNullOrEmpty) then null is returned.
        /// </summary>
        /// <param name="s">a nullable String</param>
        /// <returns>null if empty or the unchanged String</returns>
        public static string? NullIfEmpty(this string? s)
            => String.IsNullOrEmpty(s) ? null : s;

        /// <summary>
        /// if the String is empty or whitespace (=> String.IsNullOrWhitespace) then null is returned.
        /// </summary>
        /// <param name="s">a nullable String</param>
        /// <returns>null if whitespace or the unchanged String</returns>
        public static string? NullIfWhitespace(this string? s)
            => String.IsNullOrWhiteSpace(s) ? null : s;

        /// <summary>
        /// if the String is equal to argument "equalsTo" then null is returned.
        /// </summary>
        /// <param name="s">a nullable String</param>
        /// <param name="equalsTo">a not nullable reference value</param>
        /// <returns>null if equal to reference or the unchanged String</returns>
        public static string? NullIf(this string? s, string equalsTo)
            => equalsTo != null && equalsTo.Equals(s) ? null : s;

        /// <summary>
        /// if the String is equal to argument "equalsTo" then null is returned.
        /// The StringComparison type can be provided.
        /// </summary>
        /// <param name="s">a nullable String</param>
        /// <param name="equalsTo">a not nullable reference value</param>
        /// <param name="comparisonType">the comparison type</param>
        /// <returns>null if equal to reference or the unchanged String</returns>
        public static string? NullIf(this string? s, string equalsTo, StringComparison comparisonType)
            => equalsTo != null && equalsTo.Equals(s, comparisonType) ? null : s;

        /// <summary>
        /// if String is longer than argument maxLength then it is trimmed and an ellipsis (unicode U+2026) is concated.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string? EllipsisUnicode(this string? s, int maxLength)
            => s?.Length > maxLength && maxLength > 1 ? s.Substring(0, maxLength - 1) + '\u2026' : s;

        /// <summary>
        /// if String is longer than argument maxLength then it is trimmed and an ellipsis (3 dots) is concated.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string? Ellipsis(this string? s, int maxLength)
            => s?.Length > maxLength && maxLength > 3 ? s.Substring(0, maxLength - 3) + "..." : s;
    }
}
