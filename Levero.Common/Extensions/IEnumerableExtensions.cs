﻿using System.Collections.Generic;

namespace System
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// calls String.Join&lt;<typeparamref name="T"/>&gt;(separator, source); after a null-check.<br/>
        /// <para>see <see cref="String.Join{T}(String, IEnumerable{T})"/></para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">may be null</param>
        /// <param name="separator">default is comma and space</param>
        /// <returns>null if source is null</returns>
        public static string? StringJoin<T>(this IEnumerable<T>? source, string separator = ", ")
        { 
            if (source == null)
                return null;

            return String.Join<T>(separator, source);
        }

        public static void ForEach<T>(this IEnumerable<T>? source, Action<T> todo)
        {
            _ = todo ?? throw new ArgumentNullException(nameof(todo));

            if (source != null)
                foreach (var item in source)
                    todo(item);
        }
    }
}
