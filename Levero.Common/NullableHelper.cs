﻿namespace Levero.Common
{
    public static class NullableHelper
    {
        /// <summary>
        /// (1) use this constant for string(!) properties which are deserialized by Newtonsoft.Json and 
        /// have attribute [JsonProperty(Required = Required.Always)]<br/>
        /// (2) use this constant for string(!) properties which are initialized by a "Init" method. You can 
        /// query initialization state with extensions method <see cref="NullableHelper.IsNotInitialized"/>.<para />
        /// write "using static Levero.Common.NullableHelper;" to 
        /// </summary>
        public const string NOT_INITIALIZED = "<not initialized>";

        /// <summary>
        /// checks if value of property (or field) is NOT_INITIALIZED which should the default value of not nullable strings.<br/>
        /// see constant <see cref="NullableHelper.NOT_INITIALIZED"/>
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsNotInitialized(this string s) 
            => NOT_INITIALIZED.Equals(s);
    }
}
