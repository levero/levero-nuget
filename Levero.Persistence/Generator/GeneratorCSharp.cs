﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Levero.Persistence.Generator
{
    public class GeneratorCSharp
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        // nullable checker ignores check of RootNamespace... wtf...
        public GeneratorCSharp(FileInfo projectFile, string subFolder, Action<string>? logger = null)
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
            this.Logger = logger;

            // Projectfile
            this.ProjectFile = projectFile
                ?? throw new ArgumentNullException(nameof(projectFile));
            if (!this.ProjectFile.Exists)
                throw new FileNotFoundException($"C# project file {projectFile.FullName} not found!");

            // Destination folder
            this.DestinationFolder = new DirectoryInfo(Path.Combine(this.ProjectFile.DirectoryName!, subFolder));
            if (!this.DestinationFolder.Exists)
                this.DestinationFolder.Create();

            // RootNamespace
            using (var reader = this.ProjectFile.OpenText())
            {
                var root = XElement.Load(reader);
                foreach (var propertyGroup in root.Elements().Where(it => it.Name.LocalName == "PropertyGroup"))
                {
                    var rootNS = propertyGroup.Elements()
                        .Where(it => it.Name.LocalName == "RootNamespace")
                        .FirstOrDefault();
                    if (!String.IsNullOrEmpty(rootNS?.Value))
                    {
                        this.RootNamespace = rootNS!.Value;
                        break;
                    }
                }
            }
            if (this.RootNamespace == null)
                throw new Exception($"Setting Project.PropertyGroup.RootNamespace not found in project file {this.ProjectFile.FullName}!");

            // ModelNamespace
            this.ModelNamespace = this.RootNamespace + "." + subFolder.Replace('\\', '.');
        }

        public FileInfo ProjectFile { get; }
        public DirectoryInfo DestinationFolder { get; }
        public string RootNamespace { get; }
        public string ModelNamespace { get; }

        private readonly Action<string>? Logger;

        private const string ModelGeneratorDatabase = "LeveroModels";
        private const string ModelGeneratorSchema = "build";
        private const string ModelGeneratorProcedure = "GenerateModels2";
        private const string ModelGeneratorTarget = "csharp";

        public void RunGenerator(string appPackageId, double appPackageVersion)
        {
            using var unit = new NoTransactionWork();
            using var cmd = unit.PrepareProcedure(ModelGeneratorDatabase, ModelGeneratorSchema, ModelGeneratorProcedure);
            this.GeneratedFiles.Clear();
            cmd.AddInputParameter("AppPackageID", appPackageId);
            cmd.AddInputParameter("AppPackageVersion", appPackageVersion);
            cmd.AddInputParameter("FrontendGenerator", ModelGeneratorTarget);
            cmd.AddOutputParameter<string>("WatcherCommand");

            using (var reader = cmd.ExecuteReader())
                while (true)
                {
                    while (reader.Read())
                        this.GeneratedFiles.Add(new GeneratedFile(reader));
                    if (!reader.NextResult())
                        break;
                }
            this.UpdateFiles();
        }


        public Task RunGeneratorAndWatch(string appPackageId, string appPackageVersion, CancellationToken cancelToken)
        {
            string? RunGenerator(UnitOfWork unit)
            {
                this.Logger?.Invoke(">> running >>");
                DbParameter watcher;
                this.GeneratedFiles.Clear();
                using (var cmd = unit.PrepareProcedure(ModelGeneratorDatabase, ModelGeneratorSchema, ModelGeneratorProcedure))
                {
                    cmd.AddInputParameter("AppPackageID", appPackageId);
                    cmd.AddInputParameter("AppPackageVersion", appPackageVersion);
                    cmd.AddInputParameter("FrontendGenerator", ModelGeneratorTarget);
                    watcher = cmd.AddOutputParameter<string>("WatcherCommand");
                    watcher.Size = Int32.MaxValue;

                    using var reader = cmd.ExecuteReader();
                    while (true)
                    {
                        while (reader.Read())
                            this.GeneratedFiles.Add(new GeneratedFile(reader));
                        if (!reader.NextResult())
                            break;
                    }
                }
                this.UpdateFiles();
                this.Logger?.Invoke("...waiting...");
                return (string?)watcher.Value;
            }

            return Task.Run(() =>
            {
                try
                {
                    string watcherCmd;
                    using (var unit = new NoTransactionWork())
                        // first run
                        watcherCmd = RunGenerator(unit)
                            ?? throw new Exception("Watcher-Command is <null>!");

                    while (!cancelToken.WaitHandle.WaitOne(2000))
                        // run watcher until user wants to cancel
                        using (var unit = new NoTransactionWork())
                        using (var cmd = unit.PrepareCommand(watcherCmd))
                        {
                            try
                            {
                                using var reader = cmd.ExecuteReader();
                                if (reader.Read())
                                    // next run
                                    watcherCmd = RunGenerator(unit)
                                        ?? throw new Exception("Watcher-Command is <null>!");
                            }
                            catch (Exception ex)
                            {
                                this.Logger?.Invoke($"ERROR: {ex.Message}");
                            }
                        }
                    this.Logger?.Invoke("exit.");
                }
                catch (Exception ex)
                {
                    this.Logger?.Invoke("FAILED: " + ex.Message);
                }
            }, cancelToken);
        }


        private readonly List<GeneratedFile> GeneratedFiles = new List<GeneratedFile>();

        private void UpdateFiles()
        {
            // get list of current files
            var existingFiles = this.DestinationFolder
                .GetFiles("*.schema.cs")
                .ToList();

            // delete files which are not in new files
            foreach (var existingFile in existingFiles.ToArray())
            {
                if (this.GeneratedFiles.Any(it => String.Compare(it.Filename, existingFile.Name, true) == 0))
                    continue;
                // file not needed anymore -> delete
                this.Logger?.Invoke("   delete: " + existingFile.Name);
                existingFile.Delete();
                existingFiles.Remove(existingFile);
            }

            // create or update new files
            foreach (var file in this.GeneratedFiles.ToArray())
            {
                // full path
                var fullpath = Path.Combine(this.DestinationFolder.FullName, file.Filename);

                // does it exist?
                if (existingFiles.Any(it => String.Compare(it.Name, file.Filename, true) == 0))
                {
                    // has content changed?
                    if (File.ReadAllText(fullpath) == file.Content)
                    {
                        //this.Logger?.Invoke("not changed: " + file.Filename);
                        continue;
                    }
                    this.Logger?.Invoke("   update: " + file.Filename);
                }
                else
                    this.Logger?.Invoke("   create: " + file.Filename);

                // file has changed or is new
                File.WriteAllText(fullpath, file.Content);
            }
        }
    }
}
