﻿using System;
using System.Data.Common;

namespace Levero.Persistence.Generator
{
    internal class GeneratedFile
    {
        public GeneratedFile(string filename, string content, DateTime modify_date)
        {
            this.Filename = filename;
            this.Content = content;
            this.ModifyDate = modify_date;
        }

        public GeneratedFile(DbDataReader reader)
            : this(reader.GetString(0), reader.GetString(1), reader.GetDateTime(2))
        { }

        public string Filename { get; }
        public string Content { get; }
        public DateTime ModifyDate { get; }
    }
}
