namespace Levero.Persistence
{
    public class NoTransactionWork : UnitOfWork
    {
        public NoTransactionWork(UnitOfWorkScope scope)
            : base(scope, true, false)
        {

        }

        public NoTransactionWork()
            : this(UnitOfWorkScope.Explizit)
        {
        }
    }
}
