﻿using System;
using System.Data;
using System.Data.Common;

namespace Levero.Persistence
{
    public class AutoCreateAndCommitIfNull_UnitOfWork : IUnitOfWork
    {
        public AutoCreateAndCommitIfNull_UnitOfWork(IUnitOfWork? unit)
        {
            unit ??= UnitOfWork.GetActive(false, false);
            if (unit == null)
            {
                this.InternalCreated = true;
                unit = new UnitOfWork();
            }
            this._unit = unit;
        }

        public AutoCreateAndCommitIfNull_UnitOfWork()
            : this(null)
        {
        }

        private readonly IUnitOfWork _unit;
        public bool InternalCreated { get; private set; }

        #region IDisposable Members

        public void Dispose()
        {
            if (this.InternalCreated)
            {
                if (!this._unit.Closed)
                    this._unit.Commit();
                this._unit.Dispose();
                this.InternalCreated = false;
            }
        }

        #endregion

        #region IUnitOfWork Members

        public void Commit() => this._unit.Commit();

        public void Rollback() => this._unit.Rollback();

        public bool Closed => this._unit.Closed;

        DbConnection? IUnitOfWork.Connection => this._unit.Connection;

        DbTransaction? IUnitOfWork.Transaction => this._unit.Transaction;

        DbCommand IUnitOfWork.PrepareCommand(string sqlCommand) => this._unit.PrepareCommand(sqlCommand);

        DbCommand IUnitOfWork.PrepareProcedure(string schema, string name) => this._unit.PrepareProcedure(schema, name);

        int IUnitOfWork.ExecuteNonQuery(DbCommand cmd) => this._unit.ExecuteNonQuery(cmd);

        object? IUnitOfWork.ExecuteScalar(DbCommand cmd) => this._unit.ExecuteScalar(cmd);

        DataTable[] IUnitOfWork.ExecuteGetTable(DbCommand cmd) => this._unit.ExecuteGetTable(cmd);

        public void OnCommit(Action<IUnitOfWork> action) => this._unit.OnCommit(action);

        public void OnRollback(Action<IUnitOfWork> action) => this._unit.OnRollback(action);

        public string? OverrideTenantSchema
        {
            get => this._unit.OverrideTenantSchema;
            set => this._unit.OverrideTenantSchema = value;
        }

        #endregion
    }

}
