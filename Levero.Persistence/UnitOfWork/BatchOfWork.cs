using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Levero.Persistence
{
    public class BatchOfWork : UnitOfWork
    {
        public BatchOfWork(UnitOfWorkScope scope)
            : base(scope, true, false)
        {
        }

        public BatchOfWork()
            : base(UnitOfWorkScope.Explizit, true, false)
        {
        }

        private readonly List<DbCommand> m_Jobs = new List<DbCommand>();

        public override void Commit()
        {
            if (this.Closed)
                return;
            // Jetzt Transaction aufbauen und dann alles abarbeiten...
            this._Transaction = this._Connection!.BeginTransaction();

            foreach (var cmd in this.m_Jobs)
            {
                cmd.Connection = this._Connection;
                cmd.Transaction = this._Transaction;
                cmd.ExecuteNonQuery();
            }

            base.Commit();
        }

        public override int ExecuteNonQuery(DbCommand cmd)
        {
            this.m_Jobs.Add(cmd);
            return 1;
        }

        public override object ExecuteScalar(DbCommand cmd)
        {
            throw new Exception("Cannot use BatchOfWork with database commands which returns data!");
        }

        public override DataTable[] ExecuteGetTable(DbCommand cmd)
        {
            throw new Exception("Cannot use BatchOfWork with database commands which returns data!");
        }
    }
}
