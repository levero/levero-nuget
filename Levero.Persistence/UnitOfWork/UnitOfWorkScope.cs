namespace Levero.Persistence
{
    public enum UnitOfWorkScope
    {
        Explizit,
        Thread,
        Application
    }
}
