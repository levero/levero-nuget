using Levero.Persistence.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;

namespace Levero.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork()
            : this(DefaultScope, true, true)
        {
        }

        public UnitOfWork(UnitOfWorkScope Scope)
            : this(Scope, true, true)
        {
        }

        public UnitOfWork(UnitOfWorkScope Scope, bool UseTransaction)
            : this(Scope, true, UseTransaction)
        {
        }

        protected UnitOfWork(UnitOfWorkScope Scope, bool SetConnection, bool SetTransaction)
        {
            this._Scope = Scope;
            switch (Scope)
            {
                case UnitOfWorkScope.Thread:
                    if (_ThreadUnits.ContainsKey(Thread.CurrentThread))
                        throw new Exception("This Thread has already an UnitOfWork!");
                    _ThreadUnits.Add(Thread.CurrentThread, this);
                    break;
                case UnitOfWorkScope.Application:
                    if (_GlobalUnit != null)
                        throw new Exception("This Application has already an UnitOfWork!");
                    _GlobalUnit = this;
                    break;
            }
            if (SetConnection)
                try
                {
                    this._Connection = BaseDataLayer.ActiveLayer.GetConnection();
                    this._Connection.Open();

                    if (SetTransaction)
                        this._Transaction = this._Connection.BeginTransaction();
                }
                catch
                {
                    this.Rollback();
                    throw;
                }
        }

        public static UnitOfWorkScope DefaultScope = UnitOfWorkScope.Thread;
        private static IUnitOfWork? _GlobalUnit;
        private static readonly Dictionary<Thread, IUnitOfWork> _ThreadUnits = new Dictionary<Thread, IUnitOfWork>();
        protected DbConnection? _Connection;
        protected DbTransaction? _Transaction;
        private readonly UnitOfWorkScope _Scope;
        private bool _Closed = false;
        protected List<DbCommand> _Commands = new List<DbCommand>();

        public string? OverrideTenantSchema { get; set; }

        public bool Closed
        {
            get => this._Closed;
            protected set => this._Closed |= value;
        }

        protected internal void CheckClosed()
        {
            if (this._Closed)
                throw new Exception("This UnitOfWork is already closed!");
        }

        public DbConnection? Connection
        {
            get
            {
                this.CheckClosed();
                return this._Connection;
            }
        }

        public DbTransaction? Transaction
        {
            get
            {
                this.CheckClosed();
                return this._Transaction;
            }
        }

        public virtual void Commit()
        {
            if (this.Closed)
                return;
            if (this._Transaction?.Connection != null)
            {
                if (this._Connection != this._Transaction.Connection)
                    throw new Exception("KEINE GLEICHEN VERBINDUNGEN BEI COMMIT!");
                this._Transaction.Commit();
                this._Transaction = null;

                // Alle Benutzerdefinierten Aktionen jetzt ausführen
                if (this.OnCommitList != null)
                    foreach (var action in this.OnCommitList)
                        try { action(this); }
                        catch { }
                this.OnCommitList = null;
                this.OnRollbackList = null;
            }
            this.Rollback();
        }

        public virtual void Rollback()
        {
            if (this.Closed)
                return;
            try
            {
                // Transaction zurücknehmen
                if (this._Transaction?.Connection != null)
                {
                    // Diese Zeilen haben die tatsächliche Exception versteckt und waren somit kontraproduktiv.
                    // In einen Rollback kommt man ja sowieso nur wegen einer Exception.
                    //if (m_Connection != m_Transaction.Connection)
                    //    throw new Exception("KEINE GLEICHEN VERBINDUNGEN BEI COMMIT!");
                    this._Transaction.Rollback();
                }

                // Verbindung schließen
                if (this._Connection != null)
                    this._Connection.Close();

                // Commands entfernen
                while (this._Commands.Count > 0)
                {
                    this._Commands[0].Dispose();
                    this._Commands.RemoveAt(0);
                }
            }
            finally
            {
                // Je nach Scope wieder entfernen
                this.RemoveUnit();

                // Abschließen
                this.Closed = true;
                this._Connection = null;
                this._Transaction = null;

                // Alle Benutzerdefinierten Aktionen jetzt ausführen
                if (this.OnRollbackList != null)
                    foreach (var action in this.OnRollbackList)
                        try { action(this); }
                        catch { }
                this.OnCommitList = null;
                this.OnRollbackList = null;
            }
        }

        private List<Action<IUnitOfWork>>? OnCommitList;
        private List<Action<IUnitOfWork>>? OnRollbackList;

        public virtual void OnCommit(Action<IUnitOfWork> action)
        {
            if (action == null) return;
            if (this.OnCommitList == null)
                this.OnCommitList = new List<Action<IUnitOfWork>>();
            this.OnCommitList.Add(action);
        }

        public virtual void OnRollback(Action<IUnitOfWork> action)
        {
            if (action == null) return;
            if (this.OnRollbackList == null)
                this.OnRollbackList = new List<Action<IUnitOfWork>>();
            this.OnRollbackList.Add(action);
        }

        protected void RemoveUnit()
        {
            switch (this._Scope)
            {
                case UnitOfWorkScope.Thread:
                    if (_ThreadUnits.ContainsValue(this))
                        _ThreadUnits.Remove(_ThreadUnits.First(it => it.Value == this).Key);
                    break;
                case UnitOfWorkScope.Application:
                    if (_GlobalUnit == this)
                        _GlobalUnit = null;
                    break;
            }
        }

        public virtual void Dispose()
            => this.Rollback();

        public static IUnitOfWork? GetActive(bool CreateIfNoExists)
        {
            return GetActive(CreateIfNoExists, false);
        }

        public static IUnitOfWork? GetActive(bool CreateIfNoExists, bool ForceUnitOfWork)
        {
            if (_ThreadUnits.ContainsKey(Thread.CurrentThread))
                return _ThreadUnits[Thread.CurrentThread];
            if (_GlobalUnit == null && CreateIfNoExists)
                return new UnitOfWork();
            if (_GlobalUnit == null && ForceUnitOfWork)
                throw new Exception("UnitOfWork required but not available!");
            return _GlobalUnit;
        }

        protected internal virtual DbCommand PrepareCommand()
            => this.PrepareCommand("");

        public virtual DbCommand PrepareCommand(string sqlCommand)
        {
            this.CheckClosed();
            var cmd = BaseDataLayer.ActiveLayer.GetCommand();
            cmd.Connection = this.Connection;
            cmd.Transaction = this.Transaction;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sqlCommand;
            this._Commands.Add(cmd);
            return cmd;
        }

        public virtual DbCommand PrepareProcedure(string schema, string name)
        {
            this.CheckClosed();
            var cmd = BaseDataLayer.ActiveLayer.GetCommand();
            cmd.Connection = this.Connection;
            cmd.Transaction = this.Transaction;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = $"[{schema}].[{name}]";
            this._Commands.Add(cmd);
            return cmd;
        }

        public virtual DbCommand PrepareProcedure(string database, string schema, string name)
        {
            this.CheckClosed();
            var cmd = BaseDataLayer.ActiveLayer.GetCommand();
            cmd.Connection = this.Connection;
            cmd.Transaction = this.Transaction;
            cmd.CommandType = CommandType.StoredProcedure;
            if (schema == null)
                cmd.CommandText = $"[{database}]..[{name}]";
            else
                cmd.CommandText = $"[{database}].[{schema}].[{name}]";
            this._Commands.Add(cmd);
            return cmd;
        }

        public virtual int ExecuteNonQuery(DbCommand cmd)
        {
            this.CheckClosed();
            cmd.Connection = this._Connection;
            if (this._Transaction != null)
                cmd.Transaction = this._Transaction;

            return cmd.ExecuteNonQuery();
        }

        public virtual object? ExecuteScalar(DbCommand cmd)
        {
            this.CheckClosed();
            cmd.Connection = this._Connection;
            if (this._Transaction != null)
                cmd.Transaction = this._Transaction;

            return cmd.ExecuteScalar();
        }

        public virtual DataTable[] ExecuteGetTable(DbCommand cmd)
        {
            this.CheckClosed();
            cmd.Connection = this._Connection;
            if (this._Transaction != null)
                cmd.Transaction = this._Transaction;

            var da = BaseDataLayer.ActiveLayer.GetDataAdapter(cmd);
            var dset = new DataSet();
            da.Fill(dset);
            return dset.Tables
                .OfType<DataTable>()
                .ToArray();
        }
    }
}
