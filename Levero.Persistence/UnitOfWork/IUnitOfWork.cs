﻿using System;
using System.Data;
using System.Data.Common;

namespace Levero.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();

        void Rollback();

        bool Closed { get; }

        string? OverrideTenantSchema { get; set; }

        DbConnection? Connection { get; }

        DbTransaction? Transaction { get; }

        DbCommand PrepareCommand(string sqlCommand);

        DbCommand PrepareProcedure(string schema, string name);

        int ExecuteNonQuery(DbCommand cmd);

        object? ExecuteScalar(DbCommand cmd);

        DataTable[] ExecuteGetTable(DbCommand cmd);

        void OnCommit(Action<IUnitOfWork> action);

        void OnRollback(Action<IUnitOfWork> action);
    }
}
