using Levero.Persistence.DAL;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Levero.Persistence
{
    public class AsyncUnitOfWork : UnitOfWork, IAsyncDisposable
    {
        public static Task<AsyncUnitOfWork> CreateAsync()
            => CreateAsync(DefaultScope, true, true);

        public static Task<AsyncUnitOfWork> CreateAsync(UnitOfWorkScope Scope)
            => CreateAsync(Scope, true, true);

        public static Task<AsyncUnitOfWork> CreateAsync(UnitOfWorkScope Scope, bool UseTransaction)
            => CreateAsync(Scope, true, UseTransaction);

        private AsyncUnitOfWork(UnitOfWorkScope Scope)
            : base(Scope, false, false)
        {
        }

        private static async Task<AsyncUnitOfWork> CreateAsync(UnitOfWorkScope Scope, bool SetConnection, bool SetTransaction)
        {
            var result = new AsyncUnitOfWork(Scope);

            if (SetConnection)
                try
                {
                    result._Connection = BaseDataLayer.ActiveLayer.GetConnection();
                    await result._Connection.OpenAsync();

                    if (SetTransaction)
#if NET472
                        result._Transaction = result._Connection.BeginTransaction();
#else
                        result._Transaction = await result._Connection.BeginTransactionAsync();
#endif
                }
                catch
                {
                    await result.RollbackAsync();
                    throw;
                }
            return result;
        }

        public override void Commit()
            => throw new NotSupportedException("Commit is not supported in Async mode!");

        public async virtual Task CommitAsync()
        {
            if (this.Closed)
                return;
            if (this._Transaction?.Connection != null)
            {
                if (this._Connection != this._Transaction.Connection)
                    throw new Exception("KEINE GLEICHEN VERBINDUNGEN BEI COMMIT!");
#if NET472
                this._Transaction.Commit();
#else
                await this._Transaction.CommitAsync();
#endif
                this._Transaction = null;

                // Alle Benutzerdefinierten Aktionen jetzt ausführen
                if (this.OnCommitList != null)
                    foreach (var action in this.OnCommitList)
                        try { await action(this); }
                        catch { }
                this.OnCommitList = null;
                this.OnRollbackList = null;
            }
            await this.RollbackAsync();
        }

        public override void Rollback()
            => throw new NotSupportedException("Rollback is not supported in Async mode!");

        public async virtual Task RollbackAsync()
        {
            if (this.Closed)
                return;
            try
            {
                // Transaction zurücknehmen
                if (this._Transaction?.Connection != null)
                {
                    // Diese Zeilen haben die tatsächliche Exception versteckt und waren somit kontraproduktiv.
                    // In einen Rollback kommt man ja sowieso nur wegen einer Exception.
                    //if (m_Connection != m_Transaction.Connection)
                    //    throw new Exception("KEINE GLEICHEN VERBINDUNGEN BEI COMMIT!");
#if NET472
                    this._Transaction.Rollback();
#else
                    await this._Transaction.RollbackAsync();
#endif
                }

                // Verbindung schließen
                if (this._Connection != null)
#if NET472
                    this._Connection.Close();
#else
                    await this._Connection.CloseAsync();
#endif

                // Commands entfernen
                while (this._Commands.Count > 0)
                {
#if NET472
                    this._Commands[0].Dispose();
#else
                    await this._Commands[0].DisposeAsync();
#endif
                    this._Commands.RemoveAt(0);
                }
            }
            finally
            {
                // Je nach Scope wieder entfernen
                this.RemoveUnit();

                // Abschließen
                this.Closed = true;
                this._Connection = null;
                this._Transaction = null;

                // Alle Benutzerdefinierten Aktionen jetzt ausführen
                if (this.OnRollbackList != null)
                    foreach (var action in this.OnRollbackList)
                        try { await action(this); }
                        catch { }
                this.OnCommitList = null;
                this.OnRollbackList = null;
            }
        }

        private List<Func<AsyncUnitOfWork, Task>>? OnCommitList;
        private List<Func<AsyncUnitOfWork, Task>>? OnRollbackList;

        public override void OnCommit(Action<IUnitOfWork> action)
            => throw new NotSupportedException("OnCommit with Action<IUnitOfWork> is not supported in Async mode!");

        public void OnCommit(Func<AsyncUnitOfWork, Task> action)
        {
            if (action == null) return;
            if (this.OnCommitList == null)
                this.OnCommitList = new List<Func<AsyncUnitOfWork, Task>>();
            this.OnCommitList.Add(action);
        }

        public override void OnRollback(Action<IUnitOfWork> action)
            => throw new NotSupportedException("OnRollback with Action<IUnitOfWork> is not supported in Async mode!");

        public void OnRollback(Func<AsyncUnitOfWork, Task> action)
        {
            if (action == null) return;
            if (this.OnRollbackList == null)
                this.OnRollbackList = new List<Func<AsyncUnitOfWork, Task>>();
            this.OnRollbackList.Add(action);
        }

        public override void Dispose()
            => this.RollbackAsync().Wait();

        public virtual async ValueTask DisposeAsync()
            => await this.RollbackAsync();

        public virtual async Task<int> ExecuteNonQueryAsync(DbCommand cmd)
        {
            this.CheckClosed();
            cmd.Connection = this._Connection;
            if (this._Transaction != null)
                cmd.Transaction = this._Transaction;

            return await cmd.ExecuteNonQueryAsync();
        }

        public virtual async Task<object?> ExecuteScalarAsync(DbCommand cmd)
        {
            this.CheckClosed();
            cmd.Connection = this._Connection;
            if (this._Transaction != null)
                cmd.Transaction = this._Transaction;

            return await cmd.ExecuteScalarAsync();
        }
    }
}
