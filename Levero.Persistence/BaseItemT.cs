﻿namespace Levero.Persistence
{
    public class BaseItem<T> : BaseItem
    {
        public T Clone()
        {
            return (T)this.MemberwiseClone();
        }
    }
}
