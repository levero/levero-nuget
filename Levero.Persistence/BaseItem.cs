﻿using System.ComponentModel;

namespace Levero.Persistence
{
    public class BaseItem : INotifyPropertyChanged
    {
        public bool HasChanged { get; private set; }

        public event PropertyChangedEventHandler? PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            this.HasChanged = true;
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
