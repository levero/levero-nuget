using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

namespace Levero.Persistence.DAL
{
    public class SqlDataLayer : BaseDataLayer
    {
        #region Initialisierung und ConnectionString

        public SqlDataLayer(string connectionString)
            : base()
        {
            this._ConnectionString = connectionString;
        }

        private readonly string _ConnectionString;


        #endregion

        #region Erzeuge Parameter

        private const string ParameterPrefix = "@";

        protected internal override DbParameter CreateParameter(DbParameterCollection Parameters,
            string ParameterName, object? Value)
        {
            ParameterName = ParameterPrefix + ParameterName;
            // this.SearchUniqueParamName(Parameters, ref ParameterName);
            var param = new SqlParameter(ParameterName, Value ?? DBNull.Value);
            Parameters.Add(param);
            return param;
        }

        protected internal override DbParameter CreateParameter<T>(DbParameterCollection Parameters,
            string ParameterName, [AllowNull] T Value)
        {
            return this.CreateParameter(Parameters, ParameterName, (object?)Value);
        }

        protected internal override DbParameter CreateTableParameter(DbParameterCollection Parameters,
            string ParameterName, DataTable Table, string SqlTypeName)
        {
            ParameterName = ParameterPrefix + ParameterName;
            // this.SearchUniqueParamName(Parameters, ref ParameterName);
            var param = new SqlParameter(ParameterName, Table)
            {
                Direction = ParameterDirection.Input,
                SqlDbType = SqlDbType.Structured,
                TypeName = SqlTypeName
            };
            Parameters.Add(param);
            return param;
        }

        protected internal override DbParameter CreateTableParameter(DbParameterCollection Parameters,
            string ParameterName, IEnumerable<SqlDataRecord>? Rows, string SqlTypeName)
        {
            ParameterName = ParameterPrefix + ParameterName;
            // this.SearchUniqueParamName(Parameters, ref ParameterName);
            var param = new SqlParameter(ParameterName, Rows)
            {
                Direction = ParameterDirection.Input,
                SqlDbType = SqlDbType.Structured,
                TypeName = SqlTypeName
            };
            Parameters.Add(param);
            return param;
        }

        #endregion

        public override DbConnection GetConnection()
            => new SqlConnection(this._ConnectionString);

        public override DbCommand GetCommand()
            => new SqlCommand() { CommandTimeout = DefaultCommandTimeoutInSec };

        protected internal override DbDataAdapter GetDataAdapter(DbCommand cmd)
            => new SqlDataAdapter((SqlCommand)cmd);

        internal override string GetCurrentDatabaseName()
        {
            var csb = new SqlConnectionStringBuilder(this._ConnectionString);
            if (!String.IsNullOrEmpty(csb.InitialCatalog))
                return csb.InitialCatalog;

            using var con = new SqlConnection(this._ConnectionString);
            con.Open();
            using var cmd = new SqlCommand("SELECT DB_NAME();", con);
            return Convert.ToString(cmd.ExecuteScalar())!;
        }
    }
}
