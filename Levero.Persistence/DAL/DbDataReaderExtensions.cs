﻿using System.Data.Common;
using System.Diagnostics.CodeAnalysis;

namespace System.Data
{
    public static class DbDataReaderExtensions
    {
        /// <summary>
        /// Reads a single character from a string field of length 1.
        /// Null-check must be done before!!! (it calls GetString under the hood)
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="ordinal"></param>
        /// <returns>first character of string field or 0x00 if string is empty</returns>
        public static char GetCharFromString(this DbDataReader reader, int ordinal)
        {
            var s = reader.GetString(ordinal);
            if (s.Length > 0)
                return s[0];
            return '\0';
        }

        /// <summary>
        /// Reads a single character from a string field of length 1.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="ordinal"></param>
        /// <returns>first character of string field or 0x00 if string is empty</returns>
        public static char? GetNullableCharFromString(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            var s = reader.GetString(ordinal);
            if (s.Length > 0)
                return s[0];
            return '\0';
        }

        /// <summary>
        /// Reads a single character from a string field of length 1.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="ordinal"></param>
        /// <returns>first character of string field or 0x00 if string is empty</returns>
        public static char GetNullableCharFromString(this DbDataReader reader, int ordinal, char defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            var s = reader.GetString(ordinal);
            if (s.Length > 0)
                return s[0];
            return '\0';
        }

        /// <summary>
        /// Reads a byte array from a binary field.
        /// Result is null if binary field is DBNull.
        /// Null-check must be done before!!! (it calls GetBytes under the hood)
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public static byte[] GetBytes(this DbDataReader reader, int ordinal)
        {
            var buffer = new byte[reader.GetBytes(ordinal, 0, null, 0, 0)];
            reader.GetBytes(ordinal, 0, buffer, 0, buffer.Length);
            return buffer;
        }

        /// <summary>
        /// Reads a byte array from a binary field.
        /// Result is null if binary field is DBNull.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public static byte[]? GetNullableBytes(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            var buffer = new byte[reader.GetBytes(ordinal, 0, null, 0, 0)];
            reader.GetBytes(ordinal, 0, buffer, 0, buffer.Length);
            return buffer;
        }

        public static bool? GetNullableBoolean(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetBoolean(ordinal);
        }

        public static byte? GetNullableByte(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetByte(ordinal);
        }

        public static char? GetNullableChar(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetChar(ordinal);
        }

        public static DateTime? GetNullableDateTime(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetDateTime(ordinal);
        }

        public static decimal? GetNullableDecimal(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetDecimal(ordinal);
        }

        public static double? GetNullableDouble(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetDouble(ordinal);
        }

        public static float? GetNullableFloat(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetFloat(ordinal);
        }

        public static Guid? GetNullableGuid(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetGuid(ordinal);
        }

        public static short? GetNullableInt16(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetInt16(ordinal);
        }

        public static int? GetNullableInt32(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetInt32(ordinal);
        }

        public static long? GetNullableInt64(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetInt64(ordinal);
        }

        public static string? GetNullableString(this DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
                return null;
            return reader.GetString(ordinal);
        }

        public static bool GetNullableBoolean(this DbDataReader reader, int ordinal, bool defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetBoolean(ordinal);
        }

        public static byte GetNullableByte(this DbDataReader reader, int ordinal, byte defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetByte(ordinal);
        }

        public static char GetNullableChar(this DbDataReader reader, int ordinal, char defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetChar(ordinal);
        }

        public static DateTime GetNullableDateTime(this DbDataReader reader, int ordinal, DateTime defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetDateTime(ordinal);
        }

        public static decimal GetNullableDecimal(this DbDataReader reader, int ordinal, decimal defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetDecimal(ordinal);
        }

        public static double GetNullableDouble(this DbDataReader reader, int ordinal, double defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetDouble(ordinal);
        }

        public static float GetNullableFloat(this DbDataReader reader, int ordinal, float defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetFloat(ordinal);
        }

        public static Guid GetNullableGuid(this DbDataReader reader, int ordinal, Guid defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetGuid(ordinal);
        }

        public static short GetNullableInt16(this DbDataReader reader, int ordinal, short defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetInt16(ordinal);
        }

        public static int GetNullableInt32(this DbDataReader reader, int ordinal, int defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetInt32(ordinal);
        }

        public static long GetNullableInt64(this DbDataReader reader, int ordinal, long defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetInt64(ordinal);
        }

        [return: NotNullIfNotNull("defaultValue")]
        public static string? GetNullableString(this DbDataReader reader, int ordinal, string? defaultValue)
        {
            if (reader.IsDBNull(ordinal))
                return defaultValue;
            return reader.GetString(ordinal);
        }
    }
}
