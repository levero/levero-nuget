﻿namespace Levero.Persistence.DAL
{
    public interface IAfterInit
    {
        void AfterInit();
    }
}
