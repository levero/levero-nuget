﻿using Levero.Persistence.DAL;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;

namespace Levero.Persistence
{
    public static class DbCommandExtensions
    {
        public static DbParameter AddInputParameter<T>(this DbCommand cmd, string name, [AllowNull] T value = default)
        {
            var param = BaseDataLayer.ActiveLayer.CreateParameter(cmd.Parameters, name, value);
            param.Direction = ParameterDirection.Input;
            AutoDetectType(value, param);
            return param;
        }

        public static DbParameter AddTableParameter(this DbCommand cmd, string name, DataTable table, string sqlTypeName) 
            => BaseDataLayer.ActiveLayer.CreateTableParameter(cmd.Parameters, name, table, sqlTypeName);

        public static DbParameter AddTableParameter(this DbCommand cmd, string name, IEnumerable<SqlDataRecord>? rows, string sqlTypeName) 
            => BaseDataLayer.ActiveLayer.CreateTableParameter(cmd.Parameters, name, rows, sqlTypeName);

        public static DbParameter AddOutputParameter<T>(this DbCommand cmd, string name)
        {
            var param = BaseDataLayer.ActiveLayer.CreateParameter(cmd.Parameters, name, default(T));
            param.Direction = ParameterDirection.Output;
            AutoDetectType(default(T), param);
            return param;
        }

        public static DbParameter AddOutputParameter<T>(this DbCommand cmd, string name, [MaybeNull] out T parameter, int size = 0)
        {
            parameter = default;
            var param = BaseDataLayer.ActiveLayer.CreateParameter(cmd.Parameters, name, parameter);
            param.Direction = ParameterDirection.Output;
            param.Size = size;
            AutoDetectType(parameter, param);
            return param;
        }

        private static void AutoDetectType<T>([AllowNull] T value, DbParameter param)
        {
            if ((object?)value is null)
            {
                // automatic type detection does not work with null values
                if (typeof(T) == typeof(string))
                    param.DbType = DbType.String;

                else if (typeof(T) == typeof(byte?))
                    param.DbType = DbType.Byte;
                else if (typeof(T) == typeof(sbyte?))
                    param.DbType = DbType.SByte;
                else if (typeof(T) == typeof(ushort?))
                    param.DbType = DbType.UInt16;
                else if (typeof(T) == typeof(int?))
                    param.DbType = DbType.Int32;
                else if (typeof(T) == typeof(uint?))
                    param.DbType = DbType.UInt32;
                else if (typeof(T) == typeof(long?))
                    param.DbType = DbType.Int64;
                else if (typeof(T) == typeof(ulong?))
                    param.DbType = DbType.UInt64;

                else if (typeof(T) == typeof(decimal?))
                    param.DbType = DbType.Decimal;
                else if (typeof(T) == typeof(double?))
                    param.DbType = DbType.Double;
                else if (typeof(T) == typeof(float?))
                    param.DbType = DbType.Single;

                else if (typeof(T) == typeof(DateTime?))
                    param.DbType = DbType.DateTime;

                else if (typeof(T) == typeof(bool?))
                    param.DbType = DbType.Boolean;

                else if (typeof(T) == typeof(byte[]))
                    param.DbType = DbType.Binary;

                else if (typeof(T) == typeof(Guid?))
                    param.DbType = DbType.Guid;
            }
        }
    }
}
