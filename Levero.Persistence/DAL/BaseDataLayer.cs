using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;

namespace Levero.Persistence.DAL
{
    public abstract class BaseDataLayer
    {
        #region Initialisierung

        protected BaseDataLayer()
        {
        }

        #endregion

        #region Aktiver DataLayer und Initialisierung

        public static int DefaultCommandTimeoutInSec = 60;

        private static BaseDataLayer? _ActiveLayer = null;

        public static bool HasActiveLayer 
            => _ActiveLayer != null;

        public static BaseDataLayer ActiveLayer 
            => _ActiveLayer ?? throw new Exception("BaseDataLayer must be initialized!");

        public static void Init(BaseDataLayer layer) 
            => _ActiveLayer = layer ?? throw new ArgumentNullException(nameof(layer));

        #endregion

        #region Abstraktes

        protected internal abstract DbParameter CreateParameter(DbParameterCollection Parameters,
            string ParameterName, object Value);

        protected internal abstract DbParameter CreateParameter<T>(DbParameterCollection Parameters,
            string ParameterName, [AllowNull] T Value);

        protected internal abstract DbParameter CreateTableParameter(DbParameterCollection Parameters,
            string ParameterName, DataTable Value, string sqlTypeName);

        protected internal abstract DbParameter CreateTableParameter(DbParameterCollection Parameters,
            string ParameterName, IEnumerable<SqlDataRecord>? Rows, string sqlTypeName);

        public abstract DbConnection GetConnection();

        public abstract DbCommand GetCommand();

        protected internal abstract DbDataAdapter GetDataAdapter(DbCommand cmd);

        internal abstract string GetCurrentDatabaseName();

        #endregion
    }
}
