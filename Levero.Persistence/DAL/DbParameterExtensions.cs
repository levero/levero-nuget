﻿using System;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;

namespace Levero.Persistence
{
    public static class DbParameterExtensions
    {
        public static void ApplyValue<T>(this DbParameter param, [AllowNull] out T? parameter)
            => parameter = DBNull.Value.Equals(param.Value) ? default : (T?)param.Value;
    }
}
