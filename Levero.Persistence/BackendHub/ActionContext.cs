﻿using System;

namespace Levero.Persistence.BackendHub
{
    [Serializable]
    public class ActionContext
    {
        public string? impersonateUserID { get; set; }
        public string? impersonatePassword { get; set; }
    }
}
