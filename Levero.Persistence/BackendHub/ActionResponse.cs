﻿using System;

namespace Levero.Persistence.BackendHub
{
    [Serializable]
    public class ActionResponse
    {
        public int _invocationId { get; set; }
        public ActionResponseAction[]? response { get; set; }
        public ActionResponseError? error { get; set; }
    }
}
