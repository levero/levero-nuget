﻿using System;
using System.Collections.Generic;

namespace Levero.Persistence.BackendHub
{
    [Serializable]
    public class ActionResponseAction
    {
        public string? action { get; set; }
        public Dictionary<string, Dictionary<string, object?>[]>? data { get; set; }
    }
}
