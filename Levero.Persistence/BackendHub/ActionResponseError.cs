﻿using System;

namespace Levero.Persistence.BackendHub
{
    [Serializable]
    public class ActionResponseError
    {
        public string? message { get; set; }
        public string? details { get; set; }
    }
}
