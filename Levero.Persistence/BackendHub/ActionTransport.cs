﻿using System;
using System.Collections.Generic;

namespace Levero.Persistence.BackendHub
{
    [Serializable]
    public class ActionTransport
    {
        public string? action { get; set; }
        public ActionContext? actionContext { get; set; }
        public Dictionary<string, object?> data { get; } = new Dictionary<string, object?>();
    }
}
