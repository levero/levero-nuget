﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Levero.ShipmentConnector
{
    public class BaseConnector
    {
        public BaseConnector(GLSConnectorConfig config, Action<string>? logger = null)
        {
            this.Configuration = config;
            this.Logger = logger;
        }

        public HttpClient HttpClient { get; set; }

        public IBaseConfig Configuration { get; private set; }
        public Action<string>? Logger { get; private set; }

        internal void Log(string message)
        {
            if (this.Logger == null)
                return;

            this.Logger.Invoke(message);
        }
    }
}
