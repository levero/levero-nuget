﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Levero.ShipmentConnector
{
    public class GLSConnector : BaseConnector
    {
        public GLSConnector(GLSConnectorConfig config, Action<string>? logger = null) : base(config, logger)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));
            if (String.IsNullOrEmpty(config.ContactID))
                throw new Exception("Invalid configuration: \"ContactID\" must be specified");
            if (String.IsNullOrEmpty(config.User))
                throw new Exception("Invalid configuration: \"User\" must be specified");
            if (String.IsNullOrEmpty(config.Password))
                throw new Exception("Invalid configuration: \"Password\" must be specified");

            // create and prepare http client by applying required headers
            this.HttpClient = new HttpClient();
            // set Authorization
            var authString = $"{Convert.ToBase64String(Encoding.UTF8.GetBytes($"{config.User}:{config.Password}"))}";
            this.HttpClient.DefaultRequestHeaders.Add("Authorization", $"Basic {authString}");
            // set Accept
            this.HttpClient.DefaultRequestHeaders.Add("Accept", "application/glsVersion1+json, application/json");

            // preapre urls
            this.shipmentUrl = $"{config.BackendUrl}/shipments";
            this.cancelShipmentUrl = $"{config.BackendUrl}/shipments/cancel/{{trackId}}";
        }

        private readonly string shipmentUrl;
        private readonly string cancelShipmentUrl;

        /// <summary>
        /// Registers a new GLS shipment with one or multiple parcels
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public async Task<GLSShipmentResponse> RegisterShipment(GLSShipmentItem shipment, string productCode = "PARCEL")
        {
            if (shipment == null)
                throw new ArgumentNullException(nameof(shipment));
            if (String.IsNullOrEmpty(productCode))
                throw new ArgumentNullException(nameof(productCode), "Parameter must not be null or empty");

            if (shipment.ShipmentUnits.Length == 0)
                throw new Exception("Nothing to register - no shipment units defined");

            this.Log("build request for shipment registration...");

            // build request body
            var consignee = shipment.Consignee;

            var request = new GLSShipmentRequest
            {
                Shipment = new GLSShipment
                {
                    ShipmentReference = shipment.ShipmentReferenceID != null ? new string[1] { shipment.ShipmentReferenceID } : null,
                    ShippingDate = shipment.ShippingDate.HasValue ? shipment.ShippingDate.Value.ToString("yyyy-MM-dd") : null,
                    Product = productCode,
                    Consignee = new GLSConsignee
                    {
                        ConsigneeID = consignee.ConsigneeReferenceID,
                        Address = new GLSAddress
                        {
                            Name1 = consignee.Name,
                            Name2 = consignee.AdditionalName1,
                            Name3 = consignee.AdditionalName2,
                            CountryCode = consignee.CountryCode,
                            ZIPCode = consignee.ZIPCode,
                            City = consignee.City,
                            Street = consignee.Street,
                            StreetNumber = consignee.StreetNumber,
                            EMail = consignee.EMail,
                            ContactPerson = consignee.ContactPerson,
                            FixedLinePhoneNumber = consignee.FixedLinePhonenumber,
                            MobilePhoneNumber = consignee.MobilePhoneNumber
                        }
                    },
                    Shipper = new GLSShipper
                    {
                        ContactID = ((GLSConnectorConfig)this.Configuration).ContactID
                    },
                    ShipmentUnit = shipment.ShipmentUnits.ToList().Select(it => new GLSShipmentUnit
                    {
                        ShipmentUnitReference = new string[1] { it.ShipmentUnitReferenceID },
                        Weight = it.Weight
                    }).ToArray()
                },
                PrintingOptions = new GLSPrintingOptions
                {
                    ReturnLabels = new GLSReturnLabels
                    {
                        TemplateSet = shipment.PrintingOptions.TemplateSet,
                        LabelFormat = shipment.PrintingOptions.LabelFormat
                    }
                }
            };

            // build http content
            var content = this.ToHttpContent<GLSShipmentRequest>(request);

            // make request
            this.Log("Call Shipment Request...");
            var result = await this.HttpClient.PostAsync(this.shipmentUrl, content);

            if (!result.IsSuccessStatusCode)
            {
                this.Log("Failed to register shipment");

                // get error message
                var message = String.Join(Environment.NewLine, result.Headers.GetValues("Message"));
                throw new Exception(message);
            }

            // parse response
            this.Log("Handle Shipment Response...");
            
            var response = await result.Content.ReadAsStringAsync();
            this.Log(response);
            
            // deserialize response
            var returnValue = JsonSerializer.Deserialize<GLSShipmentResponse>(response);

            return returnValue!;
        }

        /// <summary>
        /// Cancels a GLS shipment parcel
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public async Task<GLSCancelShipmentResponse> CancelShipment(string trackId)
        {
            if (String.IsNullOrEmpty(trackId))
                throw new ArgumentNullException(nameof(trackId));
            
            // make request
            this.Log("Call Cancel Shipment Request...");
            var result = await this.HttpClient.PostAsync(this.cancelShipmentUrl.Replace("{trackId}", trackId), null);

            if (!result.IsSuccessStatusCode)
                throw new Exception("Failed to cancel shipment due to unknown issue");

            // parse response
            this.Log("Handle Cancel Shipment Response...");

            var response = await result.Content.ReadAsStringAsync();
            this.Log(response);

            // deserialize response
            var returnValue = JsonSerializer.Deserialize<GLSCancelShipmentResponse>(response);

            return returnValue!;
        }

        private HttpContent ToHttpContent<T>(T data)
        {
            JsonSerializerOptions options = new()
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
            };

            var json = JsonSerializer.Serialize<T>(data, options);

            // log it
            this.Log(json);

            var content = new StringContent(json, Encoding.UTF8, "application/glsVersion1+json");

            return content;
        }
    }
}
