﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levero.ShipmentConnector
{
    public class GLSShipmentResponse
    {
        public GLSCreatedShipment CreatedShipment { get; set; }
    }

    public class GLSCreatedShipment
    {
        public string[] ShipmentReference { get; set; }
        public GLSParcelData[] ParcelData { get; set; }
        public GLSPrintData[] PrintData { get; set; }
        public string CustomerID { get; set; }
        public string PickupLocation { get; set; }
    }

    public class GLSParcelData
    {
        public string TrackID { get; set; }
        public string[]? ShipmentUnitReference { get; set; }
        public string ParcelNumber { get; set; }
    }

    public class GLSPrintData
    {
        public string Data { get; set; }
        public string LabelFormat { get; set; }
    }
}
