﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levero.ShipmentConnector
{
    public class GLSConnectorConfig : IBaseConfig
    {
        public string EndpointUrl { get; set; }
        public int Port { get; set; }
        public string BackendUrl => $"{this.EndpointUrl}:{this.Port}/backend/rs";
        public string User { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// Shipper identifier as given by GLS
        /// </summary>
        public string ContactID { get; set; }
    }
}
