﻿using System;

namespace Levero.ShipmentConnector
{
    public class GLSShipmentItem
    {
        /// <summary>
        /// Reference for this consignment of parcels
        /// </summary>
        public string? ShipmentReferenceID { get; set; }
        /// <summary>
        /// Date for the shipment
        /// </summary>
        public DateTime? ShippingDate { get; set; }
        /// <summary>
        /// Consignee/recipient of parcels
        /// </summary>
        public Consignee Consignee { get; set; }
        /// <summary>
        /// Details for each shipment unit (one unit for each parcel)
        /// </summary>
        public ShipmentUnit[] ShipmentUnits { get; set; }
        /// <summary>
        /// Printing options for the returned label
        /// https://shipit.gls-group.eu/webservices/3_0_6/doxygen/WS-REST-API/rest_shipment_processing.html#restCreateParcelsPrintingOptions
        /// </summary>
        public PrintingOptions PrintingOptions { get; set; }
    }

    public class Consignee
    {
        /// <summary>
        /// Identifier for the recipient of the parcels
        /// </summary>
        public string? ConsigneeReferenceID { get; set; }
        /// <summary>
        /// Name of the consignee
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Additional consignee name information
        /// </summary>
        public string? AdditionalName1 { get; set; }
        /// <summary>
        /// Additional consignee name information
        /// </summary>
        public string? AdditionalName2 { get; set; }
        /// <summary>
        /// Destination country of the shipment
        /// </summary>
        public string CountryCode { get; set; }
        /// <summary>
        /// ZIP code of the consignee's city
        /// </summary>
        public string ZIPCode { get; set; }
        /// <summary>
        /// City of the consignee
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Street of the consignee (may include street number)
        /// </summary>
        public string Street { get; set; }
        /// <summary>
        /// Street number (if not part of the street)
        /// </summary>
        public string? StreetNumber { get; set; }
        /// <summary>
        /// Email address of the consignee
        /// </summary>
        public string? EMail { get; set; }
        /// <summary>
        /// Contact person to print on the label
        /// </summary>
        public string? ContactPerson { get; set; }
        /// <summary>
        /// Fixed line phone number of the consignee
        /// </summary>
        public string? FixedLinePhonenumber { get; set; }
        /// <summary>
        /// Mobile phone number of the consignee
        /// </summary>
        public string? MobilePhoneNumber { get; set; }
    }

    public class ShipmentUnit
    {
        /// <summary>
        /// Reference for the shipment unit
        /// </summary>
        public string ShipmentUnitReferenceID { get; set; }
        /// <summary>
        /// Weight of the shipment unit
        /// </summary>
        public decimal Weight { get; set; }
    }

    public class PrintingOptions
    {
        /// <summary>
        /// Possible values: NONE, D_200, PF_4_I, PF_4_I_200, PF_4_I_300, PF_8_D_200, T_200_BF, T_300_BF, ZPL_200, ZPL_300
        /// </summary>
        public string TemplateSet { get; set; }
        /// <summary>
        /// Possible values: PDF, ZEBRA, INTERMEC, DATAMAX, TOSHIBA
        /// </summary>
        public string LabelFormat { get; set; }
    }
}
