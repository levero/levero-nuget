﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Levero.ShipmentConnector
{
    public class GLSCancelShipmentResponse
    {
        public string TrackID { get; set; }
        [JsonPropertyName("result")]
        public string Result { get; set; }
    }
}
