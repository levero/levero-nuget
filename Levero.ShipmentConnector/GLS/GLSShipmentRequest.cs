﻿using System;
using System.Text.Json.Serialization;

namespace Levero.ShipmentConnector
{
    /// <summary>
    /// Class that represents the schema for a GLS Shipment request
    /// 
    /// NOTE: Not all properties has been taken!
    /// See https://shipit.gls-group.eu/webservices/3_0_6/doxygen/WS-REST-API/rest_shipment_processing.html#REST_API_REST_F_114 for more details
    /// </summary>
    public class GLSShipmentRequest
    {
        public GLSShipment Shipment { get; set; } = new GLSShipment();
        public GLSPrintingOptions PrintingOptions { get; set; } = new GLSPrintingOptions();
    }

    public class GLSShipment
    {
        public string[]? ShipmentReference { get; set; }
        /// <summary>
        /// Use format YYYY-MM-DD
        /// </summary>
#warning TODO: is there a way to specify the required date format as serialization option?
        public string? ShippingDate { get; set; }
        public string Product { get; set; } = "PARCEL";
        public GLSConsignee Consignee { get; set; } = new GLSConsignee();
        public GLSShipper Shipper { get; set; } = new GLSShipper();
        public GLSShipmentUnit[] ShipmentUnit { get; set; } = Array.Empty<GLSShipmentUnit>();
    }

    public class GLSPrintingOptions
    {
        public GLSReturnLabels ReturnLabels { get; set; } = new GLSReturnLabels();
    }

    public class GLSReturnLabels
    {
        public string? TemplateSet { get; set; }
        public string? LabelFormat { get; set; }
    }

    public class GLSConsignee
    {
        public string? ConsigneeID { get; set; }
        public GLSAddress Address { get; set; } = new GLSAddress();

    }

    public class GLSAddress
    {
        public string Name1 { get; set; } = "<undefined>";
        public string? Name2 { get; set; }
        public string? Name3 { get; set; }
        public string CountryCode { get; set; } = "<undefined>";
        public string? Province { get; set; }
        public string ZIPCode { get; set; } = "<undefined>";
        public string City { get; set; } = "<undefined>";
        public string Street { get; set; } = "<undefined>";
        public string? StreetNumber { get; set; }
        [JsonPropertyName("eMail")]
        public string? EMail { get; set; }
        public string? ContactPerson { get; set; }
        [JsonPropertyName("FixedLinePhonenumber")]
        public string? FixedLinePhoneNumber { get; set; }
        public string? MobilePhoneNumber { get; set; }
    }

    public class GLSShipper
    {
        public string ContactID { get; set; } = "<undefined>";
    }

    public class GLSShipmentUnit
    {
        public string[]? ShipmentUnitReference { get; set; }
        public decimal? Weight { get; set; }
        public string? Note1 { get; set; }
        public string? Note2 { get; set; }
    }
}