﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Levero.WinForms.Controls
{
    [DefaultProperty(nameof(Image))]
    [DefaultBindingProperty(nameof(Image))]
    [Docking(DockingBehavior.Ask)]
    public class TransparentImage : ScrollableControl
    {
        public TransparentImage()
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            SetStyle(ControlStyles.ResizeRedraw, false);
            SetStyle(ControlStyles.Selectable, false);

            base.BackColor = Color.Transparent;
        }

        #region Constants

        private const int WS_EX_TRANSPARENT = 0x00000020;
        private const int WS_EX_CLIENTEDGE = 0x00000200;
        private const int WS_BORDER = 0x00800000;

        #endregion

        #region Fields

        private Image? _image;
        private bool _centerImage;

        #endregion

        #region Properties

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Color BackColor { get => base.BackColor; set { } }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_BORDER;
                cp.ExStyle &= ~WS_EX_CLIENTEDGE;
                cp.ExStyle |= WS_EX_TRANSPARENT;
                return cp;
            }
        }

        /// <summary>
        ///  Retrieves the Image that the <see cref='PictureBox'/> is currently displaying.
        /// </summary>
        [Localizable(true)]
        [Bindable(true)]
        public Image? Image
        {
            get => this._image;
            set
            {
                this._image = value;
                this.Invalidate();
            }
        }

        [Localizable(true)]
        [DefaultValue(false)]
        public bool CenterImage
        {
            get => this._centerImage;
            set
            {
                this._centerImage = value;
                this.Invalidate();
            }
        }

        #endregion

        #region Methods

        protected Rectangle GetImageRectangle()
        {
            var rect = this.ClientRectangle;
            rect.X += this.Padding.Left;
            rect.Y += this.Padding.Top;
            rect.Width -= this.Padding.Horizontal;
            rect.Height -= this.Padding.Vertical;
            if (this._centerImage && rect.Width > this._image?.Width)
                rect.X += (rect.Width - this._image.Width) / 2;
            if (this._centerImage && rect.Height > this._image?.Height)
                rect.Y += (rect.Height - this._image.Height) / 2;
            if (this._image != null)
                rect.Size = this._image.Size;
            return rect;
        }

        #endregion

        #region Drawing Image

        protected override void OnPaint(PaintEventArgs e)
        {
            if (_image is not null && e is not null)
                e.Graphics.DrawImage(this._image, this.GetImageRectangle());
        }

        #endregion
    }
}
