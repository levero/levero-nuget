﻿
namespace Levero.WinForms.Controls
{
    public enum ThreeStateIOShapeEnum
    {
        Rectangle = 0,
        ArrowLeftToRight,
        ArrowRightToLeft
    }
}
