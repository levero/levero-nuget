﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using static System.Windows.Forms.AxHost;

namespace Levero.WinForms.Controls
{
    [DefaultProperty(nameof(Text))]
    [DefaultBindingProperty(nameof(State))]
    public sealed class ThreeStateIOLabel : Control
    {
        public ThreeStateIOLabel()
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            //SetStyle(ControlStyles.Opaque, true);
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            //SetStyle(ControlStyles.FixedHeight, false);
            SetStyle(ControlStyles.Selectable, false);

            //base.DoubleBuffered = true;
            //base.BackColor = Color.Black;

            this.UpdateColors();
            this.UpdateShape();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._Polygon?.Dispose();
                this._BlackPen?.Dispose();
                this._FontBrush?.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Fields

        private bool? _State = null;
        private Color _ColorOfStateTrue = Color.LimeGreen;
        private Color _ColorOfStateFalse = Color.Red;
        private Color _ColorOfStateNull = Color.Silver;
        private ThreeStateIOShapeEnum _Shape = ThreeStateIOShapeEnum.Rectangle;

        private GraphicsPath? _Polygon;
        private PointF? _TextStart;
        private Color _FillColor = Color.Silver;
        private Pen? _BlackPen = new Pen(Color.Black, 2f);
        private SolidBrush? _FontBrush;

        #endregion

        #region Properties

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor { get => base.BackColor; set { } }

        [DefaultValue(null)]
        public bool? State
        {
            get => this._State;
            set
            {
                if (value == this._State) return;
                this._State = value;
                this.UpdateColors();
            }
        }

        public Color ColorOfStateTrue
        {
            get => this._ColorOfStateTrue;
            set
            {
                if (value == this._ColorOfStateTrue) return;
                this._ColorOfStateTrue = value;
                this.UpdateColors();
            }
        }

        public Color ColorOfStateFalse
        {
            get => this._ColorOfStateFalse;
            set
            {
                if (value == this._ColorOfStateFalse) return;
                this._ColorOfStateFalse = value;
                this.UpdateColors();
            }
        }

        public Color ColorOfStateNull
        {
            get => this._ColorOfStateNull;
            set
            {
                if (value == this._ColorOfStateNull) return;
                this._ColorOfStateNull = value;
                this.UpdateColors();
            }
        }

        [DefaultValue(ThreeStateIOShapeEnum.Rectangle)]
        public ThreeStateIOShapeEnum Shape
        {
            get => this._Shape;
            set
            {
                if (value == this._Shape) return;
                this._Shape = value;
                this.UpdateShape();
            }
        }

        #endregion

        #region Methods for Painting

        private void UpdateColors()
        {
            var color = this._State switch
            {
                true => this._ColorOfStateTrue,
                false => this._ColorOfStateFalse,
                _ => this._ColorOfStateNull,
            };
            // not changed?
            if (color.Equals(this._FillColor)) return;

            this._FillColor = color;
            this.Invalidate();
        }

        private void UpdateShape()
        {
            // shape is cached, so only clear cache and wait for invalidate
            this._Polygon?.Dispose();
            this._Polygon = null;

            this.Parent?.Invalidate(new Rectangle(this.Location, this.Size), true);
        }

        protected override void OnResize(EventArgs e)
        {
            this.UpdateShape();
            base.OnResize(e);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            this._TextStart = null;
            base.OnTextChanged(e);
            base.Invalidate();
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            this._FontBrush?.Dispose();
            this._FontBrush = null;
            base.OnForeColorChanged(e);
        }

        private void PreparePaint()
        {
            float H = this.Height;
            float W = this.Width;
            float HH = H / 2f;      // half height

            float h = this.Height - 1;
            float w = this.Width - 1;
            float hh = h / 2f;      // half height

            this._Polygon?.Dispose();

            var regionPolygon = new GraphicsPath();
            this._Polygon = new GraphicsPath(); // must be 1 pixel smaller than region!
            switch (this._Shape)
            {
                case ThreeStateIOShapeEnum.ArrowLeftToRight:
                    regionPolygon.AddPolygon(new[]
                    {
                        new PointF(0, 0),
                        new PointF(W - HH, 0),
                        new PointF(W, HH),
                        new PointF(W - HH, H),
                        new PointF(0, H),
                        new PointF(HH, HH),
                        new PointF(0, 0),
                    });
                    this._Polygon.AddPolygon(new[]
                    {
                        new PointF(0, 0),
                        new PointF(w - hh, 0),
                        new PointF(w, hh - 0.5f),
                        new PointF(w, hh + 1f),
                        new PointF(w - hh + 0.5f, H),
                        new PointF(0, H),
                        new PointF(0, H - 1f),
                        new PointF(hh, hh + 0.5f),
                        new PointF(0, 0.5f),
                    });
                    break;
                case ThreeStateIOShapeEnum.ArrowRightToLeft:
                    regionPolygon.AddPolygon(new[]
                    {
                        new PointF(0, HH),
                        new PointF(HH, 0),
                        new PointF(W, 0),
                        new PointF(W - HH, HH),
                        new PointF(W, H),
                        new PointF(HH, H),
                        new PointF(0, HH),
                    });
                    this._Polygon.AddPolygon(new[]
                    {
                        new PointF(0, hh),
                        new PointF(hh, 0),
                        new PointF(w, 0),
                        new PointF(w, 0.5f),
                        new PointF(w - hh, hh + 0.5f),
                        new PointF(w, H - 0.5f),
                        new PointF(w, H),
                        new PointF(hh - 0.5f, H),
                        new PointF(0, hh + 1f),
                    });
                    break;
                default:
                    regionPolygon.AddRectangle(this.ClientRectangle);
                    this._Polygon.AddRectangle(new RectangleF(0, 0, W, H));
                    break;
            }
            this.Region = new Region(regionPolygon);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            // cache polygon
            if (this._Polygon == null)
                this.PreparePaint();

            // cache text measurement
            if (this._TextStart == null)
            {
                var textSize = e.Graphics.MeasureString(this.Text, this.Font);
                this._TextStart = new PointF((this.Width - textSize.Width) / 2f, (this.Height - textSize.Height) / 2f);
            }

            // cache font color
            this._FontBrush ??= new SolidBrush(this.ForeColor);

            // paint
            e.Graphics.Clear(this._FillColor);
            e.Graphics.DrawPath(this._BlackPen!, this._Polygon!);
            e.Graphics.DrawString(this.Text, this.Font, this._FontBrush, this._TextStart.Value);
        }

        #endregion
    }
}