﻿using Levero.ALM.Update;
using Newtonsoft.Json;
using System;

namespace Levero.ALM.Build
{
    [JsonObject]
    public class BuildDescription
    {
        [JsonProperty(PropertyName = "appPackageID", Required = Required.Always)]
        public string AppPackageID { get; set; } = "<undefined>";

        [JsonProperty(PropertyName = "rootFolder")]
        public string? RootFolder { get; set; }

        [JsonProperty(PropertyName = "rules", Required = Required.Always)]
        public BuildFileRule[] Rules { get; set; } = Array.Empty<BuildFileRule>();

        [JsonProperty(PropertyName = "steps", Required = Required.Always)]
        public UpdateStep[] UpdateSteps { get; set; } = Array.Empty<UpdateStep>();

        [JsonProperty(PropertyName = "offlinePackageStorages")]
        public string[]? OfflinePackageStorages { get; set; }
    }
}
