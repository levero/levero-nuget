﻿using Newtonsoft.Json;

namespace Levero.ALM.Build
{
    [JsonObject]
    public class BuildFileRule
    {
        /// <summary>
        /// sub-folder for source files, default and root path is solution folder
        /// </summary>
        [JsonProperty(PropertyName = "source")]
        public string? SourceFolder { get; set; }

        /// <summary>
        /// sub-folder for destination files, default and root path is application folder
        /// </summary>
        [JsonProperty(PropertyName = "target")]
        public string? TargetFolder { get; set; }

        /// <summary>
        /// comma separated list of file search masks for including files (for example: *.exe,*.dll)
        /// </summary>
        [JsonProperty(PropertyName = "include")]
        public string? IncludeMask { get; set; }

        /// <summary>
        /// comma separated list of file search masks for excluding files (for example: *.exe,*.dll)
        /// </summary>
        [JsonProperty(PropertyName = "exclude")]
        public string? ExcludeMask { get; set; }

        /// <summary>
        /// set to true to search in sub folders
        /// </summary>
        [JsonProperty(PropertyName = "recursive")]
        public bool Recursive { get; set; }

        /// <summary>
        /// indicates that the first found file is the main executable which is to start after windows startup
        /// </summary>
        [JsonProperty(PropertyName = "isMainExecutable")]
        public bool IsMainExecutable { get; set; }

        /// <summary>
        /// indicates that this is a .NET Core assembly.
        /// </summary>
        [JsonProperty(PropertyName = "isNETCore")]
        public bool IsNETCore { get; set; }

        /// <summary>
        /// indicates that the first found file is the main executable AND it is a .NET Core assembly which means
        /// that corresponding .DLL and the dependency and runtime configs is also imported.
        /// </summary>
        [JsonProperty(PropertyName = "isNETCoreMainExecutable")]
        public bool IsNETCoreMainExecutable
        {
            get => this.IsMainExecutable && this.IsNETCore;
            set
            {
                if (value)
                {
                    this.IsMainExecutable = true;
                    this.IsNETCore = true;
                }
            }
        }

        /// <summary>
        /// indicates that the first found file should be used for reading app package version
        /// </summary>
        [JsonProperty(PropertyName = "readVersion")]
        public bool ReadVersion { get; set; }

        /// <summary>
        /// indicates that the first found file is the main configuration file
        /// </summary>
        [JsonProperty(PropertyName = "isMainConfig")]
        public bool IsMainConfig { get; set; }

        /// <summary>
        /// indicates that the first found file is the update executable which is to start to deploy a new package
        /// </summary>
        [JsonProperty(PropertyName = "isUpdateExecutable")]
        public bool IsUpdateExecutable { get; set; }

        /// <summary>
        /// indicates that this files are the debug info files (PDB) which are used for detailed error locations
        /// </summary>
        [JsonProperty(PropertyName = "isDebugInfo")]
        public bool IsDebugInfo { get; set; }

        /// <summary>
        /// filter for file copy step to decide if file is needed (simple expressions like {Variable}=abc
        /// </summary>
        [JsonProperty(PropertyName = "condition")]
        public string? Condition { get; set; }
    }
}