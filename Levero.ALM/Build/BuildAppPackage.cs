﻿using Levero.ALM.Update;
using Levero.Common;
using Levero.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Levero.ALM.Build
{
    public class BuildAppPackage : BaseAppPackage
    {
        public BuildAppPackage()
            : this(SearchBuildPackage())
        {
        }

        /// <summary>
        /// use for build and upload package
        /// </summary>
        /// <param name="packageJson"></param>
        public BuildAppPackage(FileInfo packageJson)
            : this(JsonConvert.DeserializeObject<BuildDescription>(File.ReadAllText(packageJson.FullName))
                  ?? throw new Exception($"Cannot deserialize Build Description in file {packageJson.FullName}."))
        {
        }

        /// <summary>
        /// use for build and upload package
        /// </summary>
        /// <param name="package"></param>
        public BuildAppPackage(BuildDescription package)
        {
            this.Package = package ?? throw new ArgumentNullException(nameof(package));
        }

        public BuildDescription Package { get; }
        public Version? AppPackageVersion { get; private set; }

        public long? AppPackageVersionID => this.AppPackageVersion?.ToVersionID();


        public static FileInfo SearchBuildPackage()
        {
            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            var currentFolder = new DirectoryInfo(Path.GetDirectoryName(assemblyLocation)
                ?? throw new Exception($"Cannot get directory name of {assemblyLocation}."));
            while (currentFolder != null)
            {
                var config = currentFolder.GetFiles(LeveroPackageBuildFileName).FirstOrDefault();
                if (config != null)
                    return config;
                currentFolder = currentFolder.Parent;
            }
            throw new FileNotFoundException($"levero package configuration '{LeveroPackageBuildFileName}' was not found in current or parent folders!");
        }

        public bool ValidateBuild()
        {
            try
            {
                if (this.Package == null)
                    throw new ArgumentException($"{nameof(this.Package)} is null!");

                this.Package.RootFolder = this.Package.RootFolder ?? Directory.GetCurrentDirectory();
                if (!Directory.Exists(this.Package.RootFolder))
                    throw new ArgumentException($"{nameof(this.Package)}.{nameof(this.Package.RootFolder)} must be a valid folder!");

                this.Package.AppPackageID = this.Package.AppPackageID?.Trim() ?? String.Empty;
                if (this.Package.AppPackageID.Length < 1 || this.Package.AppPackageID.Length > 15)
                    throw new ArgumentException($"{nameof(this.Package)}.{nameof(this.Package.AppPackageID)} must be a string with 1 to 15 characters!");

                if (this.Package.Rules.NullIfEmpty() == null)
                    throw new ArgumentException($"{nameof(this.Package)}.{nameof(this.Package.Rules)} must be an array of {nameof(BuildFileRule)} with at least one row!");

                if (this.Package.UpdateSteps.NullIfEmpty() == null)
                    throw new ArgumentException($"{nameof(this.Package)}.{nameof(this.Package.UpdateSteps)} must be an array of {nameof(UpdateStep)} with at least one row!");

                foreach (var file in this.Package.Rules)
                {
                    file.SourceFolder ??= String.Empty;
                    file.TargetFolder ??= file.SourceFolder;
                    file.IncludeMask ??= "*.*";
                }

                return true;
            }
            catch (Exception ex)
            {
                this.Log($"Package validation failed: {ex.Message}");
                return false;
            }
        }

        /// <summary>
        /// Shall be called before BuildAndUpload to clean up all app package relevant things related to this machine
        /// which breaks the build process.
        /// This is needed by the frontend and backend apps on development machines. The details about the to build app package already exists at the database.
        /// So we need to clean up this data first before continuing the build process. 
        /// </summary>
        /// <param name="appPackageVersion"></param>
        public void PreBuildAndUpload(Version appPackageVersion)
        {
            this.Log("Start Pre-BuildAndUpload-Process...");

            using var unit = new UnitOfWork();
            // clean up database for app package
            using var cmd = unit.PrepareProcedure("deploy", "SYS_AppPackage_PreUpload");

            cmd.AddInputParameter("UserID", this.UserID);
            cmd.AddInputParameter("AppPackageID", this.Package.AppPackageID);
            cmd.AddInputParameter("AppPackageVersionID", appPackageVersion.ToVersionID());
            cmd.AddInputParameter("MachineName", Environment.MachineName);

            // prepare
            this.Log($"Clean up database for app package to build...");
            cmd.ExecuteNonQuery();

            // done!
            unit.Commit();
            this.Log($"Pre-BuildAndUpload-Process DONE!");
        }

        public void BuildAndUpload(Version? appPackageVersion = null)
        {
            if (!this.ValidateBuild())
                throw new Exception("Package validation failed!");

            // search for files 
            this.Log($"start search at folder {this.Package.RootFolder}...");
            this.AppPackageVersion = appPackageVersion;

            // save list of files
            var files = new Dictionary<string, (string destination, BuildFileRule rule)>();
            var filesOfRule = new List<string>();
            string? fileMainExecutable = null, fileUpdateExecutable = null, fileMainConfig = null;

            // prepare root path separation
            var prefixLen = this.Package.RootFolder!.Length;
            if (!this.Package.RootFolder.EndsWith("\\"))
                prefixLen++;

            foreach (var rule in this.Package.Rules)
            {
                filesOfRule.Clear();

                // prepare source folder
                var sourceFolder = Path.Combine(this.Package.RootFolder, rule.SourceFolder!);
                if (!Directory.Exists(sourceFolder))
                    throw new Exception($"Folder '{sourceFolder}' of {this.Package.Rules.IndexOf(rule)}. rule not found!");

                // run through all include masks
                foreach (var mask in rule.IncludeMask!.Split(',').Select(it => it.Trim()))
                    if (mask != String.Empty)
                        foreach (var file in Directory.GetFiles(sourceFolder, mask, rule.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                            if (!filesOfRule.Contains(file))
                            {
                                //this.Log($"  include '{file}'");
                                filesOfRule.Add(file);
                            }

                // run through all exclude masks
                if (rule.ExcludeMask != null)
                    foreach (var mask in rule.ExcludeMask.Split(',').Select(it => it.Trim()))
                        if (mask != String.Empty)
                            foreach (var file in Directory.GetFiles(sourceFolder, mask, rule.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                            {
                                //this.Log($"  exclude '{file}'");
                                filesOfRule.Remove(file);
                            }

                // add to total list
                var isFirst = true;
                foreach (var file in filesOfRule)
                {
                    this.Log($"add '{file.Substring(prefixLen)}'");

                    // add to list
                    if (!files.ContainsKey(file))
                        files.Add(file, (file.Substring(prefixLen), rule));

                    // prepare reading version
                    var readVersionFile = file;

                    // check special flags
                    if (isFirst && rule.IsMainExecutable && filesOfRule.Count > 0)
                    {
                        this.Log($"  -> {(fileMainExecutable != null ? "OVERWRITE" : "set")} main executable '{file.Substring(prefixLen)}'");
                        fileMainExecutable = file;

                        if (rule.IsNETCoreMainExecutable)
                        {
                            // .NET Core executables consist of .exe, .dll, .runtimeconfig.json, .deps.json
                            // try to read .dll
                            var dependentFile = Path.ChangeExtension(file, ".dll");
                            if (File.Exists(dependentFile) && !files.ContainsKey(dependentFile))
                            {
                                this.Log($"  -> add dependency '{dependentFile.Substring(prefixLen)}'");
                                files.Add(dependentFile, (dependentFile.Substring(prefixLen), rule));
                                // read version from DLL !!!!
                                readVersionFile = dependentFile;
                            }

                            // try to read .runtimeconfig.json
                            dependentFile = Path.ChangeExtension(file, ".runtimeconfig.json");
                            if (File.Exists(dependentFile) && !files.ContainsKey(dependentFile))
                            {
                                this.Log($"  -> add dependency '{dependentFile.Substring(prefixLen)}'");
                                files.Add(dependentFile, (dependentFile.Substring(prefixLen), rule));
                            }

                            // try to read .deps.json
                            dependentFile = Path.ChangeExtension(file, ".deps.json");
                            if (File.Exists(dependentFile) && !files.ContainsKey(dependentFile))
                            {
                                this.Log($"  -> add dependency '{dependentFile.Substring(prefixLen)}'");
                                files.Add(dependentFile, (dependentFile.Substring(prefixLen), rule));
                            }
                        }
                    }
                    if (isFirst && rule.IsMainConfig && filesOfRule.Count > 0)
                    {
                        this.Log($"  -> {(fileMainConfig != null ? "OVERWRITE" : "set")} main configuration '{file.Substring(prefixLen)}'");
                        fileMainConfig = file;
                    }
                    if (isFirst && rule.IsUpdateExecutable && filesOfRule.Count > 0)
                    {
                        this.Log($"  -> {(fileUpdateExecutable != null ? "OVERWRITE" : "set")} update executable '{file.Substring(prefixLen)}'");
                        fileUpdateExecutable = file;
                    }
                    if (isFirst && rule.ReadVersion && filesOfRule.Count > 0)
                    {
                        this.Log($"  -> {(this.AppPackageVersion != null ? "OVERWRITE" : "get")} version from '{readVersionFile.Substring(prefixLen)}'");
                        this.AppPackageVersion = this.ForceReadVersionFromFile(readVersionFile);
                        this.Log($"  -> set app package version to {this.AppPackageVersion?.ToString(3)}");
                    }

                    isFirst = false;
                }
            }

            // remove base path
            fileMainExecutable = fileMainExecutable?.Substring(prefixLen);
            fileUpdateExecutable = fileUpdateExecutable?.Substring(prefixLen);
            fileMainConfig = fileMainConfig?.Substring(prefixLen);

            // validate results
#pragma warning disable IDE0016 // Use 'throw' expression
            if (fileMainExecutable == null)
                throw new Exception("no main executable was found!");
#pragma warning restore IDE0016 // Use 'throw' expression
            if (this.AppPackageVersion == null)
                throw new Exception("no app package version was found!");
            fileUpdateExecutable ??= fileMainExecutable;

            // Upload
            using var unit = new UnitOfWork();
            // validate app package id and version
            using var cmd = unit.PrepareProcedure("deploy", "SYS_AppPackage_Upload");

            cmd.AddInputParameter("UserID", this.UserID);
            cmd.AddInputParameter("AppPackageID", this.Package.AppPackageID);
            cmd.AddInputParameter("AppPackageVersionID", this.AppPackageVersion.ToVersionID());
            var paramMode = cmd.AddInputParameter("Mode", "PREPARE");
            var paramBinName = cmd.AddInputParameter<string>("BinaryName", null);
            var paramBinContent = cmd.AddInputParameter<byte[]>("BinaryContent", null);
            var paramDetails = cmd.AddInputParameter<string>("Details", null);

            // prepare
            this.Log($"Prepare packaging process...");
            cmd.ExecuteNonQuery();

            paramMode.Value = "UPLOAD";
            try
            {
                // fill datatable with binaries
                foreach (var kvp in files)
                {
                    var file = kvp.Key;
                    var (targetName, rule) = kvp.Value;

                    // read assembly version if possible
                    var version = this.ReadVersionFromFile(file)?.ToString(3);
                    if (version != null)
                        version = DetailVersionPrefix + version;

                    // remember condition if exists
                    var condition = rule.Condition;
                    if (condition != null)
                        condition = $"{DetailConditionPrefix}[{condition}]";

                    // make entry in binary table
                    paramBinName.Value = targetName;
                    paramBinContent.Value = Compression.ZipToBytes(file);
                    paramDetails.Value = new[] {
                                    DetailZip,
                                    rule.IsDebugInfo ? DetailDebugInfo : null,
                                    fileMainExecutable == targetName ? DetailMainExe : null,
                                    fileMainConfig == targetName ? DetailMainConfig : null,
                                    fileUpdateExecutable == targetName ? DetailUpdateExe : null,
                                    version,
                                    condition
                                }
                        .Where(it => it != null)
                        .StringJoin(",");

                    this.Log($"Upload {targetName}...");
                    cmd.ExecuteNonQuery();
                }

                // prepare deployment process description
                var steps = JsonConvert.SerializeObject(this.Package.UpdateSteps);
                {
                    paramBinName.Value = UpdateStepsBinaryName;
                    paramBinContent.Value = Encoding.UTF8.GetBytes(steps);
                    paramDetails.Value = "";

                    this.Log($"Upload update steps...");
                    cmd.ExecuteNonQuery();
                }

                // because of the TE linked server which runs without distributed transactions, we have to delete all entries manually (see the catch block)
                unit.Commit();
            }
            catch
            {
                paramMode.Value = "DELETE";
                paramBinName.Value = DBNull.Value;
                paramBinContent.Value = DBNull.Value;
                cmd.ExecuteNonQuery();

                unit.Commit();
                throw;
            }
        }
    }
}
