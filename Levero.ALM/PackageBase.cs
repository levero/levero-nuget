﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levero.ALM
{
    [JsonObject]
    public class PackageBase
    {
        [JsonProperty(Required = Required.Always)]
        public string AppPackageID { get; set; } = "<undefined>";
    }
}
