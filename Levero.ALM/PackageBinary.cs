﻿using Newtonsoft.Json;
using System;

namespace Levero.ALM
{
    [JsonObject]
    public class PackageBinary
    {
        [JsonProperty(Required = Required.Always)]
        public string BinaryName { get; set; } = "<undefined>";

        [JsonIgnore]
        public byte[]? BinaryContent { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string[] Details { get; set; } = Array.Empty<string>();
    }
}