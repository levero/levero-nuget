﻿using Levero.ALM.Update;
using Newtonsoft.Json;
using System;

namespace Levero.ALM
{
    [JsonObject]
    public class PackageDescription : PackageVersion
    {
        [JsonIgnore]
        public string? UpdateFolder { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string EnvironmentID { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public string MachineName { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.AllowNull)]
        public string? AppID { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string DeploymentProcessID { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public string DeploymentProcessName { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public string DeploymentChannelID { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public string DeploymentChannelName { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public DateTime ReleaseDate { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string ReleasedBy { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public PackageArgument[] Arguments { get; set; } = Array.Empty<PackageArgument>();

        [JsonProperty(Required = Required.Always)]
        public PackageBinary[] Binaries { get; set; } = Array.Empty<PackageBinary>();

        [JsonProperty(Required = Required.Always)]
        public UpdateStep[] UpdateSteps { get; set; } = Array.Empty<UpdateStep>();

        [JsonProperty]
        public string? MainExecutable { get; internal set; }

        [JsonProperty(Required = Required.Always)]
        public string UpdateExecutable { get; internal set; } = "<undefined>";

        [JsonProperty]
        public string? MainConfig { get; internal set; }
    }
}
