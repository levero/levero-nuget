﻿using System;
using System.Reflection;

namespace Levero.ALM
{
    public abstract class BaseAppPackage
    {
        public const string LeveroPackageBuildFileName = "levero-package-build.json";
        public const string LeveroPackageFileName = "levero-package.json";
        public const string UpdateBatchFile = "update.bat";
        public const string UpdateStepsBinaryName = "::update.steps";

        public const string DetailZip = "zip";
        public const string DetailDebugInfo = "debug-info";
        public const string DetailMainExe = "main-exe";
        public const string DetailMainConfig = "main-config";
        public const string DetailUpdateExe = "update-exe";
        public const string DetailVersionPrefix = "version=";
        public const string DetailConditionPrefix = "condition:";

        public const string ArgumentUpdateBaseFolder = "UpdateBaseFolder";
        public const string ArgumentAppFolder = "AppFolder";
        public const string ArgumentMainExecutable = "MainExecutable";
        public const string ArgumentMainConfig = "MainConfig";
        public const string ArgumentEnvironmentConfig = "EnvironmentConfig";
        public const string ArgumentIISFrontendWebSiteName = "WebSiteName";
        public const string ArgumentIISBackendWebSiteNameA = "WebSiteNameA";
        public const string ArgumentIISBackendWebSiteNameB = "WebSiteNameB";
        public const string ArgumentIISBackendWebSiteNameRunning = "WebSiteNameRunning";
        public const string ArgumentIISBackendWebSizeNameStopped = "WebSiteNameStopped";
        public const string ArgumentIISBackendAppPoolNameRunning = "AppPoolNameRunning";
        public const string ArgumentIISBackendAppPoolNameStopped = "AppPoolNameStopped";

        public const string FrontendEnvironmentPlaceholder = "<!--{{ENV_CONFIG}}-->";

        public const string CommandLineArgument_Update = "update";
        public const string CommandLineArgument_WaitForProcessID = "waitforprocess=";

        public static readonly string[] DeploymentProcessesRequireIIS = new string[] { "IIS-backend", "IIS-frontend" };

        public const string UpdateFolder_Prepare = ".prepare";
        public const string UpdateFolder_Ready = ".ready";
        public const string UpdateFolder_Current = ".current";
        public const string UpdateFolder_Previous = ".previous";
        public const string UpdateFolder_Delete = ".delete";

        public const string OfflinePackageFileExtension = ".zip";
        public const string OfflinePackageDescriptionFileName = "levero-offline-package.json";


        public Action<string>? Logger { get; set; }
        public string UserID { get; set; }

        protected BaseAppPackage()
        {
            this.UserID = Environment.UserName;
        }

        protected BaseAppPackage(BaseAppPackage other)
        {
            this.Logger = other.Logger;
            this.UserID = other.UserID;
        }


        protected internal void Log(string message)
            => this.Logger?.Invoke(message);

        protected Version ForceReadVersionFromFile(string file)
            => this.ReadVersionFromFile(file) ?? throw new NotImplementedException("Currently the only method to get app package version is reading from an executable!");

        protected Version? ReadVersionFromFile(string file)
        {
            // maybe it is an executable
            try { return AssemblyName.GetAssemblyName(file).Version; }
            catch { return null; }
        }
    }
}
