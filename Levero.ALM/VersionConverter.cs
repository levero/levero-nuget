﻿using System;

namespace Levero.ALM
{
    public static class VersionConverter
    {
        public static long ToVersionID(this Version version)
        {
            return (((long)version.Major) << 32)
                + (((long)version.Minor) << 16)
                + version.Build;
        }

        public static Version ToVersion(this long versionID)
        {
            return new Version(
                (short)((versionID >> 32) & 0xFFFF),
                (short)((versionID >> 16) & 0xFFFF),
                (short)(versionID & 0xFFFF),
                0);
        }
    }
}
