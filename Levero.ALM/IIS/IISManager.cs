﻿using Microsoft.Web.Administration;
using System;
using System.Linq;

namespace Levero.ALM.IIS
{
    public class IISManager
    {
        public IISManager()
        {
            this.Server = new ServerManager();
        }

        public ServerManager Server { get; private set; }

        public string GetPhysicalPath(string siteName)
        {
            var site = this.GetSite(siteName);
            return site.Applications["/"].VirtualDirectories["/"].PhysicalPath;
        }

        public string GetAppPoolNameFromSite(string siteName)
        {
            var site = this.GetSite(siteName);
            return site.Applications[0].ApplicationPoolName;
        }

        public bool IsAppPoolStarted(string appPoolName)
        {
            var appPool = this.GetAppPool(appPoolName);
            return appPool.State == ObjectState.Started;
        }

        public bool IsAppPoolStarting(string appPoolName)
        {
            var appPool = this.GetAppPool(appPoolName);
            return appPool.State == ObjectState.Starting;
        }

        public bool IsSiteStarted(string siteName)
        {
            var site = this.GetSite(siteName);
            return site.State == ObjectState.Started;
        }

        public bool IsSiteStarting(string siteName)
        {
            var site = this.GetSite(siteName);
            return site.State == ObjectState.Starting;
        }

        public void StartAppPool(string appPoolName)
        {
            var appPool = this.GetAppPool(appPoolName);
            if (appPool.State == ObjectState.Stopped)
                appPool.Start();
        }

        public void StopAppPool(string appPoolName)
        {
            var appPool = this.GetAppPool(appPoolName);
            if (appPool.State == ObjectState.Started)
                appPool.Stop();
        }

        public void StartSite(string siteName)
        {
            var site = this.GetSite(siteName);
            if (site.State == ObjectState.Stopped)
                site.Start();
        }

        public void StopSite(string siteName)
        {
            var site = this.GetSite(siteName);
            if (site.State == ObjectState.Started)
                site.Stop();
        }

        public void SaveChanges()
        {
            this.Server.CommitChanges();
        }

        private ApplicationPool GetAppPool(string appPoolName)
        {
            var appPool = this.Server.ApplicationPools.Where(ap => ap.Name.Equals(appPoolName)).FirstOrDefault();
            if (appPool == null)
                throw new Exception($"Could not find IIS app pool \"{appPoolName}\".");
            return appPool;
        }

        private Site GetSite(string siteName)
        {
            var site = this.Server.Sites.Where(s => s.Name.Equals(siteName)).FirstOrDefault();
            if (site == null)
                throw new Exception($"Could not find IIS site \"{siteName}\".");
            return site;
        }
    }
}
