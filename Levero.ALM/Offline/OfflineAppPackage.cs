﻿using Levero.ALM.Build;
using Levero.ALM.Update;
using Levero.Common;
using Levero.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Levero.ALM.Offline
{
    public class OfflineAppPackage : BaseAppPackage
    {
        /// <summary>
        /// use for build and download/export package (offline package)
        /// </summary>
        /// <param name="package"></param>
        public OfflineAppPackage(BuildDescription? package = null)
        {
            this.Package = package;
        }

        public BuildDescription? Package { get; }
        public Version? AppPackageVersion { get; private set; }

        public void Export(Version appPackageVersion)
        {
            if (this.Package == null)
                throw new ArgumentNullException(nameof(this.Package));

            if (this.Package.OfflinePackageStorages == null || this.Package.OfflinePackageStorages.Length == 0)
                throw new Exception($"Failed to create offline package. No offline package storages have been specified in build description.");

            this.AppPackageVersion = appPackageVersion;

            var offlinePackage = new OfflinePackage();
            var package = new Package();
            var packageVersion = new PackageVersion();
            var binaries = new List<PackageBinary>();
            var packageDependencies = new List<PackageVersionDependency>();
            var deploymentArgs = new List<DeploymentProcessArgument>();


            // ask the database for the specified app package
            using (var unit = new UnitOfWork())
            {
                // load existing app package
                using var cmd = unit.PrepareProcedure("deploy", "SYS_AppPackage_Export");
                cmd.AddInputParameter("UserID", this.UserID);
                cmd.AddInputParameter("AppPackageID", this.Package.AppPackageID);
                cmd.AddInputParameter("AppPackageVersionID", this.AppPackageVersion.ToVersionID());

                using var reader = cmd.ExecuteReader();
                // App Package
                while (reader.Read())
                {
                    package.AppPackageID = reader.GetString(reader.GetOrdinal(nameof(package.AppPackageID)));
                    package.AppPackageName = reader.GetString(reader.GetOrdinal(nameof(package.AppPackageName)));
                    package.Description = reader.GetNullableString(reader.GetOrdinal(nameof(package.Description)));
                    package.OnlyForAppID = reader.GetNullableString(reader.GetOrdinal(nameof(package.OnlyForAppID)));
                    package.DeploymentProcessID = reader.GetString(reader.GetOrdinal(nameof(package.DeploymentProcessID)));
                }

                // App Package Version
                reader.NextResult();
                while (reader.Read())
                {
                    packageVersion.AppPackageID = reader.GetString(reader.GetOrdinal(nameof(packageVersion.AppPackageID)));
                    packageVersion.AppPackageVersionID = reader.GetInt64(reader.GetOrdinal(nameof(packageVersion.AppPackageVersionID)));
                    packageVersion.AppPackageVersion = reader.GetString(reader.GetOrdinal(nameof(packageVersion.AppPackageVersion)));
                    packageVersion.AppPackageVersionMajor = reader.GetInt16(reader.GetOrdinal(nameof(packageVersion.AppPackageVersionMajor)));
                    packageVersion.AppPackageVersionMinor = reader.GetInt16(reader.GetOrdinal(nameof(packageVersion.AppPackageVersionMinor)));
                    packageVersion.AppPackageVersionBuild = reader.GetInt16(reader.GetOrdinal(nameof(packageVersion.AppPackageVersionBuild)));
                    packageVersion.DeployDate = reader.GetDateTime(reader.GetOrdinal(nameof(packageVersion.DeployDate)));
                    packageVersion.DeployedBy = reader.GetString(reader.GetOrdinal(nameof(packageVersion.DeployedBy)));
                    packageVersion.RevokeDate = reader.GetNullableDateTime(reader.GetOrdinal(nameof(packageVersion.RevokeDate)));
                    packageVersion.RevokedBy = reader.GetNullableString(reader.GetOrdinal(nameof(packageVersion.RevokedBy)));
                }

                // App Package Version Binaries
                reader.NextResult();

                var colBinaryName = reader.GetOrdinal("AppPackageBinaryName");
                var colBinaryContent = reader.GetOrdinal("AppPackageBinaryContent");
                var colDetails = reader.GetOrdinal("Details");

                while (reader.Read())
                {
                    // add all binaries to disk and then zip them
                    // info
                    var binary = new PackageBinary
                    {
                        BinaryName = reader.GetString(colBinaryName),
                        Details = reader.GetNullableString(colDetails, String.Empty).ToLower().Split(',')
                    };

                    // content file
                    var binContent = reader.GetBytes(colBinaryContent);

                    // special handling?
                    if (binary.BinaryName == UpdateStepsBinaryName)
                    {
                        offlinePackage.UpdateSteps = JsonConvert.DeserializeObject<UpdateStep[]>(Encoding.UTF8.GetString(binContent))
                            ?? throw new Exception($"Cannot deserialize array of Update Steps from special package {UpdateStepsBinaryName}.");
                        offlinePackage.UpdateStepDetails = binary.Details;
                    }
                    else
                    {
                        // normal file

                        // zipped?
                        if (binary.Details.Contains(DetailZip))
                            binContent = Compression.UnzipToBytes(binContent);

                        binary.BinaryContent = binContent;

                        // remember it
                        binaries.Add(binary);
                    }
                }

                // App Package Version Dependencies
                reader.NextResult();
                while (reader.Read())
                {
                    var dep = new PackageVersionDependency();
                    dep.AppPackageID = reader.GetString(reader.GetOrdinal(nameof(dep.AppPackageID)));
                    dep.AppPackageVersionID = reader.GetInt64(reader.GetOrdinal(nameof(dep.AppPackageVersionID)));
                    dep.AppPackageVersionDependencyID = reader.GetInt32(reader.GetOrdinal(nameof(dep.AppPackageVersionDependencyID)));
                    dep.DependencyType = reader.GetString(reader.GetOrdinal(nameof(dep.DependencyType)));
                    dep.DependOnAppPackageID = reader.GetNullableString(reader.GetOrdinal(nameof(dep.DependOnAppPackageID)));
                    dep.DependOnAppPackageVersionID = reader.GetNullableInt64(reader.GetOrdinal(nameof(dep.DependOnAppPackageVersionID)));

                    packageDependencies.Add(dep);
                }

                // Deployment Process Arguments
                reader.NextResult();
                while (reader.Read())
                {
                    var arg = new DeploymentProcessArgument();
                    arg.DeploymentProcessID = reader.GetString(reader.GetOrdinal(nameof(arg.DeploymentProcessID)));
                    arg.ArgumentKey = reader.GetString(reader.GetOrdinal(nameof(arg.ArgumentKey)));
                    arg.LevelID = reader.GetByte(reader.GetOrdinal(nameof(arg.LevelID)));
                    arg.AppID = reader.GetNullableString(reader.GetOrdinal(nameof(arg.AppID)));
                    arg.AppPackageID = reader.GetNullableString(reader.GetOrdinal(nameof(arg.AppPackageID)));
                    arg.EnvironmentID = reader.GetNullableString(reader.GetOrdinal(nameof(arg.EnvironmentID)));
                    arg.ArgumentValue = reader.GetNullableString(reader.GetOrdinal(nameof(arg.ArgumentValue)));
                    arg.Description = reader.GetNullableString(reader.GetOrdinal(nameof(arg.Description)));

                    deploymentArgs.Add(arg);
                }

                // collect offline package infos
                offlinePackage.AppPackage = package;
                offlinePackage.AppPackageVersion = packageVersion;
                offlinePackage.AppPackageVersionBinaries = binaries.ToArray();
                offlinePackage.AppPackageVersionDependencies = packageDependencies.ToArray();
                offlinePackage.DeploymentProcessArguments = deploymentArgs.ToArray();
            }

            // create the offline package .zip file
            using var ms = new MemoryStream();
            using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
            {
                foreach (var attachment in binaries)
                {
                    var entry = zipArchive.CreateEntry(attachment.BinaryName, CompressionLevel.Fastest);
                    using var entryStream = entry.Open();
                    var stream = new MemoryStream(attachment.BinaryContent!);
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(entryStream);
                }

                // last but not least - add the offline package description to the archive
                var desc = zipArchive.CreateEntry(OfflinePackageDescriptionFileName, CompressionLevel.Fastest);
                using (var entryStream = desc.Open())
                {
                    var content = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(offlinePackage));
                    var stream = new MemoryStream(content);
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(entryStream);
                }
            }

            var zipSaved = 0;
            // save the zip file to all given locations
            for (var i = 0; i < this.Package.OfflinePackageStorages.Length; i++)
            {
                var location = this.Package.OfflinePackageStorages[i];
                // TODO: maybe there are different location types in the future
                if (!Directory.Exists(location))
                {
                    this.Log($"Failed to save Offline Package to {location}. Path does not exist!");
                    continue;
                }

                // save it..
                // build path
                var zipFilePath = Path.Combine(location, $"{this.Package.AppPackageID}_{this.AppPackageVersion.ToVersionID()}_{this.AppPackageVersion}{OfflinePackageFileExtension}");
                using (var zipFile = new FileStream(zipFilePath, FileMode.Create))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    ms.WriteTo(zipFile);
                }

                zipSaved++;
            }

            if (zipSaved == 0)
                throw new Exception("Failed to save Offline Package to any given storages.");

            if (zipSaved != this.Package.OfflinePackageStorages.Length)
                this.Log("Failed to save Offline Package to all given storages. See logs for more information.");
        }

        public void Import(FileInfo offlinePackageZip)
        {
            this.Log("Check offline package archive...");
            if (offlinePackageZip.Extension.ToLower() != OfflinePackageFileExtension)
                throw new Exception($"Invalid {offlinePackageZip.Extension} for offline package import - only .zip.");

            // open the zip archive
            using var archive = ZipFile.Open(offlinePackageZip.FullName, ZipArchiveMode.Read);

            // first check if the archive contains a description file
            this.Log($"Check if file {OfflinePackageDescriptionFileName} is available");
            var entry = archive.GetEntry(OfflinePackageDescriptionFileName);
            if (entry == null)
                throw new Exception($"Invalid Offline Package. Archive does not contain {OfflinePackageDescriptionFileName}");

            this.Log($"Read {OfflinePackageDescriptionFileName} content");
            OfflinePackage opDescContent;
            using (var stream = new StreamReader(entry.Open()))
                opDescContent = JsonConvert.DeserializeObject<OfflinePackage>(stream.ReadToEnd())
                    ?? throw new Exception($"Invalid Offline Package. Archive contains a misformed {OfflinePackageDescriptionFileName}");

            // Import
            using var unit = new UnitOfWork();
            // validate app package id and version
            using var cmd = unit.PrepareProcedure("deploy", "SYS_AppPackage_Import");
            
            this.Log("Start offline package import...");

            var package = opDescContent.AppPackage!;
            var packageVersion = opDescContent.AppPackageVersion!;

            cmd.AddInputParameter("UserID", this.UserID);
            cmd.AddInputParameter("AppPackageID", package.AppPackageID);
            cmd.AddInputParameter("AppPackageVersionID", packageVersion.AppPackageVersionID);
            var paramPackageName = cmd.AddInputParameter("AppPackageName", package.AppPackageName);
            var paramPackageDescripion = cmd.AddInputParameter("AppPackageDescription", package.Description);
            var paramOnlyForAppID = cmd.AddInputParameter("OnlyForAppID", package.OnlyForAppID);
            var paramDeploymentProcessID = cmd.AddInputParameter("DeploymentProcessID", package.DeploymentProcessID);
            var paramDeployDate = cmd.AddInputParameter("DeployDate", packageVersion.DeployDate);
            var paramDeploedBy = cmd.AddInputParameter("DeployedBy", packageVersion.DeployedBy);
            var paramMode = cmd.AddInputParameter("Mode", "PREPARE");
            var paramDependencies = cmd.AddInputParameter<DataTable>("VersionDependencies", null);
            var paramProcessArguments = cmd.AddInputParameter<DataTable>("ProcessArguments", null);
            var paramBinName = cmd.AddInputParameter<string>("BinaryName", null);
            var paramBinContent = cmd.AddInputParameter<byte[]>("BinaryContent", null);
            var paramDetails = cmd.AddInputParameter<string>("Details", null);

            // prepare DataTable for version dependencies
            var dtDep = new DataTable();
            dtDep.Columns.Add("DependencyType", typeof(string)).AllowDBNull = false;
            dtDep.Columns.Add("DependOnAppPackageID", typeof(string)).AllowDBNull = true;
            dtDep.Columns.Add("DependOnAppPackageVersionID", typeof(string)).AllowDBNull = true;

            // fill DataTable
            foreach (var row in opDescContent.AppPackageVersionDependencies!)
            {
                var dataRow = dtDep.NewRow();
                dataRow["DependencyType"] = row.DependencyType;
                dataRow["DependOnAppPackageID"] = row.DependOnAppPackageID ?? (object)DBNull.Value;
                dataRow["DependOnAppPackageVersionID"] = row.DependOnAppPackageVersionID ?? (object)DBNull.Value;
                dtDep.Rows.Add(dataRow);
            }
            paramDependencies.Value = dtDep;

            // preapare DataTable for deployment process arguments
            var dtArg = new DataTable();
            dtArg.Columns.Add("DeploymentProcessID", typeof(string)).AllowDBNull = false;
            dtArg.Columns.Add("ArgumentKey", typeof(string)).AllowDBNull = false;
            dtArg.Columns.Add("LevelID", typeof(byte)).AllowDBNull = false;
            dtArg.Columns.Add("AppID", typeof(string)).AllowDBNull = true;
            dtArg.Columns.Add("AppPackageID", typeof(string)).AllowDBNull = true;
            dtArg.Columns.Add("EnvironmentID", typeof(string)).AllowDBNull = true;
            dtArg.Columns.Add("ArgumentValue", typeof(string)).AllowDBNull = true;
            dtArg.Columns.Add("Description", typeof(string)).AllowDBNull = true;

            // fill DataTable
            foreach (var row in opDescContent.DeploymentProcessArguments!)
            {
                var dataRow = dtArg.NewRow();
                dataRow["DeploymentProcessID"] = row.DeploymentProcessID;
                dataRow["ArgumentKey"] = row.ArgumentKey;
                dataRow["LevelID"] = row.LevelID;
                dataRow["AppID"] = row.AppID ?? (object)DBNull.Value;
                dataRow["AppPackageID"] = row.AppPackageID ?? (object)DBNull.Value;
                dataRow["EnvironmentID"] = DBNull.Value; // we always pass null!
                dataRow["ArgumentValue"] = row.ArgumentValue ?? (object)DBNull.Value;
                dataRow["Description"] = row.Description ?? (object)DBNull.Value;
                dtArg.Rows.Add(dataRow);
            }
            paramProcessArguments.Value = dtArg;

            // prepare
            this.Log("Prepare app package (main data, package version, dependencies and process arguments)...");
            cmd.ExecuteNonQuery();

            // preparation done - reset not anymore used params
            dtDep.Clear();
            paramDependencies.Value = dtDep;
            dtArg.Clear();
            paramProcessArguments.Value = dtArg;
            paramPackageName.Value = DBNull.Value;
            paramPackageDescripion.Value = DBNull.Value;
            paramOnlyForAppID.Value = DBNull.Value;
            paramDeploymentProcessID.Value = DBNull.Value;
            paramDeployDate.Value = DBNull.Value;
            paramDeploedBy.Value = DBNull.Value;

            this.Log("Prepare binary upload...");
            paramMode.Value = "UPLOAD";

            // upload update steps
            this.Log("Upload update steps...");
            var steps = JsonConvert.SerializeObject(opDescContent.UpdateSteps);

            paramBinName.Value = UpdateStepsBinaryName;
            paramBinContent.Value = Encoding.UTF8.GetBytes(steps);
            paramDetails.Value = String.Join(",", opDescContent.UpdateStepDetails!);

            cmd.ExecuteNonQuery();

            foreach (var bin in opDescContent.AppPackageVersionBinaries!)
            {
                paramBinName.Value = bin.BinaryName;
                // get the file from zip archive
                var file = archive.GetEntry(bin.BinaryName)
                    ?? throw new Exception($"Invalid Offline Package. Archive does not contain file {bin.BinaryName} as described in {OfflinePackageDescriptionFileName}");
                paramBinContent.Value = Compression.ZipToBytes(file.Open());
                paramDetails.Value = String.Join(",", bin.Details);

                this.Log($"Upload file {bin.BinaryName}");
                cmd.ExecuteNonQuery();
            }


            unit.Commit();
        }
    }
}
