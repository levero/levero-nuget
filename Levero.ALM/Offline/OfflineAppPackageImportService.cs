﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Levero.ALM.Offline
{
    public class OfflineAppPackageImportService : BaseAppPackage
    {
        public OfflineAppPackageImportService(DirectoryInfo directory, bool useWatcher = false)
        {
            if (!directory.Exists)
                throw new DirectoryNotFoundException($"Directory {directory.FullName} does not exist!");

            this.PackageDirectory = directory;
            this.UseWatcher = useWatcher;
        }

        public Action<string>? SuccessfulImport { get; set; }

        public OfflineAppPackage? Offline;
        public DirectoryInfo PackageDirectory { get; }
        public bool UseWatcher { get; private set; }

        private FileSystemWatcher? FileWatcher;

        public void Run()
        {
            this.Offline = new OfflineAppPackage
            {
                Logger = this.Logger,
                UserID = this.UserID
            };

            this.Log($"start looking for offline app packages in directory {this.PackageDirectory.FullName}");

            var packages = this.PackageDirectory.GetFiles($"*{OfflinePackageFileExtension}");

            if (packages.Count() == 0)
                this.Log("Currently no offline app packages available.");
            else
            {
                this.Log($"Found {packages.Count()} archives, let's validate and import them...");
                // process all found app packages
                foreach (var archive in packages)
                    this.ImportArchive(archive);
            }

            // do we have to start the watcher?
            if (this.UseWatcher == true)
                this.StartFileWatcher();
        }

        public void Stop()
        {
            if (this.FileWatcher == null)
                return;

            this.Log($"Stop watching for offline app packages in directory {this.PackageDirectory.FullName}.");
            this.FileWatcher.Dispose();
        }

        private void ImportArchive(FileInfo archive)
        {
            try
            {
                // start the import
                this.Offline!.Import(archive);

                this.Log($"Successfully imported {archive.FullName}.");

                // delete the archive
                archive.Delete();
                this.Log($"Deleted archive {archive.FullName}.");

                this.SuccessfulImport?.Invoke($"Successfully imported \"{archive.FullName}\"");
            }
            catch (Exception ex)
            {
                this.Log($"Failed to import {archive.FullName}. {ex}");
            }
        }

        private void StartFileWatcher()
        {
            this.Log($"Start watching for offline app packages in directory {this.PackageDirectory.FullName}...");
            var watcher = new FileSystemWatcher(this.PackageDirectory.FullName, $"*{OfflinePackageFileExtension}")
            {
                NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName
            };

            watcher.Changed += this.OnChanged;
            watcher.Created += this.OnChanged;
            watcher.Renamed += this.OnChanged;

            // Begin watching.
            watcher.EnableRaisingEvents = true;

            // remember watcher instance
            this.FileWatcher = watcher;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            // this.Log($"File: {e.FullPath} {e.ChangeType}");

            try
            {
                // found new offline app package - try to import it!

                // is file accessable?
                using (var f = File.Open(e.FullPath, FileMode.Open, FileAccess.Write, FileShare.None))
                { }

                this.Log($"Found new offline app package {e.FullPath} - start import...");
                this.ImportArchive(new FileInfo(e.FullPath));
            }
            catch
            {
                // just swallow it
            }
        }
    }
}
