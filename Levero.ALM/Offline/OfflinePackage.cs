﻿using Levero.ALM.Update;
using Newtonsoft.Json;
using System;

namespace Levero.ALM
{
    [JsonObject]
    public class OfflinePackage
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        [JsonProperty(Required = Required.Always)]
        public Package AppPackage { get; set; }

        [JsonProperty(Required = Required.Always)]
        public PackageVersion AppPackageVersion { get; set; }

        [JsonProperty(Required = Required.Always)]
        public PackageBinary[] AppPackageVersionBinaries { get; set; }

        [JsonProperty(Required = Required.Always)]
        public PackageVersionDependency[] AppPackageVersionDependencies { get; set; }

        [JsonProperty(Required = Required.Always)]
        public DeploymentProcessArgument[] DeploymentProcessArguments { get; set; }

        [JsonProperty(Required = Required.Always)]
        public UpdateStep[] UpdateSteps { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string[] UpdateStepDetails { get; set; } = Array.Empty<string>();
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
    }
}
