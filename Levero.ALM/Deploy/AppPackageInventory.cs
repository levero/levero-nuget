﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Levero.ALM.Deploy
{
    public class AppPackageInventory : BaseAppPackage
    {
        public AppPackageInventory(string appPackageID, long currentAppPackageVersionID, string updateBaseFolder)
        {
            this.AppPackageID = appPackageID ?? throw new ArgumentNullException(nameof(appPackageID));
            this.CurrentAppPackageVersionID = currentAppPackageVersionID;
            this.UpdateBaseFolder = updateBaseFolder ?? throw new ArgumentNullException(nameof(updateBaseFolder));
        }

        public string AppPackageID { get; }
        public long CurrentAppPackageVersionID { get; }
        /// <summary>
        /// Update Base Folder is configured in DeploymentProcessArguments {UpdateBaseFolder} (for example C:\apps\bitt\update)
        /// </summary>
        public string UpdateBaseFolder { get; }
        /// <summary>
        /// Update Package Folder is {UpdateBaseFolder}\{AppPackageID} (for example C:\apps\bitt\update\te-bitt-client)
        /// </summary>
        public string? UpdatePackageFolder { get; private set; }
        /// <summary>
        /// Update Folder is {UpdatePackageFolder}\{AppPackageVersionID}.ready (for example C:\apps\bitt\update\te-bitt-client\16992.ready)
        /// after inventory this is the folder with the newest and ready update
        /// </summary>
        public string? UpdateFolder { get; private set; }
        /// <summary>
        /// Levero-Update-Package in newest Update Folder
        /// </summary>
        public string? UpdateFolderPackage => this.UpdateFolder != null ? Path.Combine(this.UpdateFolder, LeveroPackageFileName) : null;

        public Dictionary<long, string> ReadyForUpdate { get; } = new Dictionary<long, string>();
        public List<string> ToDelete { get; } = new List<string>();
        public Dictionary<string, string> ToMove { get; } = new Dictionary<string, string>();
        public List<string> Unknown { get; } = new List<string>();


        public bool Collect()
        {
            // reset
            this.UpdatePackageFolder = null;
            this.UpdateFolder = null;
            this.ReadyForUpdate.Clear();
            this.ToDelete.Clear();
            this.ToMove.Clear();
            this.Unknown.Clear();
            string? oldPrevPackage = null, newPrevPackage = null;

            // search for update package folder
            this.UpdatePackageFolder = Path.Combine(this.UpdateBaseFolder, this.AppPackageID);
            if (!Directory.Exists(this.UpdatePackageFolder))
                return false;

            // search for all folders in update base path
            foreach (var folder in Directory.GetDirectories(this.UpdatePackageFolder))
            {
                var s = Path.GetFileNameWithoutExtension(folder);
                if (!Int64.TryParse(s, out var versionID))
                {
                    this.Log($"  found unknown folder: {folder}");
                    this.Unknown.Add(folder);
                    continue;
                }

                switch (Path.GetExtension(folder))
                {
                    case UpdateFolder_Prepare:
                        // delete next time
                        this.Log($"  found old prepare-folder to rename to delete: {folder}");
                        this.ToMove.Add(folder, Path.ChangeExtension(folder, UpdateFolder_Delete));
                        break;
                    case UpdateFolder_Ready:
                        // is it newer than current?
                        if (versionID > this.CurrentAppPackageVersionID && File.Exists(Path.Combine(folder, LeveroPackageFileName)))
                        {
                            this.Log($"  found ready folder: {folder}");
                            this.ReadyForUpdate.Add(versionID, folder);
                        }
                        // is it current?
                        else if (versionID == this.CurrentAppPackageVersionID)
                        {
                            this.Log($"  found folder to rename to current: {folder}");
                            this.ToMove.Add(folder, Path.ChangeExtension(folder, UpdateFolder_Current));
                        }
                        // delete because it is too old or package file is missing
                        else
                        {
                            this.Log($"  found folder for delete: {folder}");
                            this.ToDelete.Add(folder);
                        }
                        break;
                    case UpdateFolder_Delete:
                        // simply delete
                        this.Log($"  found folder for delete: {folder}");
                        this.ToDelete.Add(folder);
                        break;
                    case UpdateFolder_Current:
                        if (versionID < this.CurrentAppPackageVersionID)
                        {
                            // it should be previous version
                            this.Log($"  found folder to rename to previous: {folder}");
                            this.ToMove.Add(folder, newPrevPackage = Path.ChangeExtension(folder, UpdateFolder_Previous));
                        }
                        else if (versionID > this.CurrentAppPackageVersionID)
                        {
                            // this is nearly impossible and should be deleted
                            this.Log($"  found folder for delete: {folder}");
                            this.ToDelete.Add(folder);
                        }
                        break;
                    case UpdateFolder_Previous:
                        // should only be deleted if there is another current
                        this.Log($"  found old previous folder: {folder}");
                        oldPrevPackage = folder;
                        break;
                    default:
                        this.Log($"  found unknown folder: {folder}");
                        this.Unknown.Add(folder);
                        break;
                }
            }
            // should previous package be switched?
            if (oldPrevPackage != null && newPrevPackage != null)
            {
                this.Log($"  found folder for delete: {oldPrevPackage}");
                this.ToDelete.Add(oldPrevPackage);
            }

            // find newest ready update
            this.UpdateFolder = this.ReadyForUpdate.OrderByDescending(it => it.Key).Select(it => it.Value).FirstOrDefault();
            this.Log($"  found {this.ReadyForUpdate.Count} ready folders and selected newest: {this.UpdateFolder ?? "<null>"}");

            return true;
        }

        public void CleanUp()
        {
            // first delte all
            foreach (var folder in this.ToDelete.ToArray())
                try
                {
                    this.Log($"remove folder {folder}...");
                    Directory.Delete(folder, true);
                    // if successful then remove
                    this.ToDelete.Remove(folder);
                }
                catch (Exception ex)
                { this.Log($"remove folder {folder} failed! Details: {ex.Message}"); }


            // then move all
            var tryCount = 10;
            while (this.ToMove.Count > 0 && tryCount-- > 0)
                // target name may not be a source name of another folder!
                foreach (var folder in this.ToMove.Where(it => !this.ToMove.ContainsKey(it.Value)).ToArray())
                    try
                    {
                        this.Log($"rename folder {folder.Key} to {folder.Value}...");
                        Directory.Move(folder.Key, folder.Value);
                        // if successful then remove
                        this.ToMove.Remove(folder.Key);
                    }
                    catch (Exception ex)
                    { this.Log($"rename folder {folder.Key} failed! Details: {ex.Message}"); }

        }

        public Process? StartUpdateExecutable()
        {
            if (this.UpdateFolder == null) return null;
            if (this.UpdateFolderPackage == null) return null;

            var package = JsonConvert.DeserializeObject<PackageDescription>(File.ReadAllText(this.UpdateFolderPackage))
                ?? throw new Exception($"Cannot deserialize Package Description in file {this.UpdateFolderPackage}.");
            return DeployAppPackage.StartUpdateExecutable(package, this.UpdateFolder);
        }
    }
}
