﻿using Levero.ALM.Update;
using Levero.Common;
using Levero.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Levero.ALM.Deploy
{
    public class DeployAppPackage : BaseAppPackage
    {
        /// <summary>
        /// use for download package
        /// </summary>
        /// <param name="environmentID"></param>
        public DeployAppPackage(string environmentID)
        {
            this.EnvironmentID = environmentID ?? throw new ArgumentNullException(nameof(environmentID));
        }

        public string EnvironmentID { get; private set; }
        public PackageDescription? Package { get; private set; }

        public PackageDescription? Download(Version? specificVersion) 
            => this.Download(specificVersion?.ToVersionID());

        public PackageDescription? Download(long? specificVersionID = null)
        {
            this.Package = null;
            var result = new PackageDescription();

            // Download
            using var unit = new UnitOfWork();
            // validate app package id and version
            using var cmd = unit.PrepareProcedure("deploy", "SYS_AppPackage_Download");

            cmd.AddInputParameter("UserID", this.UserID);
            cmd.AddInputParameter("EnvironmentID", this.EnvironmentID);
            cmd.AddInputParameter("SpecificVersionID", specificVersionID);

            // prepare
            this.Log($"Download whole package...");
            using var reader = cmd.ExecuteReader();
            if (!reader.Read())
                return null;

            // read header
            result.EnvironmentID = reader.GetString(reader.GetOrdinal(nameof(result.EnvironmentID)));
            result.MachineName = reader.GetString(reader.GetOrdinal(nameof(result.MachineName)));
            result.AppPackageID = reader.GetString(reader.GetOrdinal(nameof(result.AppPackageID)));
            result.AppID = reader.GetNullableString(reader.GetOrdinal(nameof(result.AppID)));
            result.DeploymentProcessID = reader.GetString(reader.GetOrdinal(nameof(result.DeploymentProcessID)));
            result.DeploymentProcessName = reader.GetString(reader.GetOrdinal(nameof(result.DeploymentProcessName)));
            result.AppPackageVersionID = reader.GetInt64(reader.GetOrdinal(nameof(result.AppPackageVersionID)));
            result.AppPackageVersion = reader.GetString(reader.GetOrdinal(nameof(result.AppPackageVersion)));
            result.DeploymentChannelID = reader.GetString(reader.GetOrdinal(nameof(result.DeploymentChannelID)));
            result.DeploymentChannelName = reader.GetString(reader.GetOrdinal(nameof(result.DeploymentChannelName)));
            result.DeployDate = reader.GetDateTime(reader.GetOrdinal(nameof(result.DeployDate)));
            result.DeployedBy = reader.GetString(reader.GetOrdinal(nameof(result.DeployedBy)));
            result.ReleaseDate = reader.GetDateTime(reader.GetOrdinal(nameof(result.ReleaseDate)));
            result.ReleasedBy = reader.GetString(reader.GetOrdinal(nameof(result.ReleasedBy)));

            // read arguments
            if (!reader.NextResult())
                throw new Exception($"Missing second recordset (arguments) of {cmd.CommandText}!");

            var colLevelID = reader.GetOrdinal("LevelID");
            var colArgumentKey = reader.GetOrdinal("ArgumentKey");
            var colArgumentValue = reader.GetOrdinal("ArgumentValue");

            var args = new List<PackageArgument>();
            while (reader.Read())
                args.Add(new PackageArgument
                {
                    Level = (PackageArgumentLevel)reader.GetByte(colLevelID),
                    Key = reader.GetString(colArgumentKey),
                    Value = reader.GetNullableString(colArgumentValue)
                });
            result.Arguments = args.ToArray();

            // generate update path
            var updateBaseFolder = result.Arguments.FirstOrDefault(it => String.Compare(it.Key, ArgumentUpdateBaseFolder, true) == 0)?.Value;
            if (updateBaseFolder == null)
                throw new Exception($"Missing deployment process argument {ArgumentUpdateBaseFolder}!");
            var updateFolder = Path.Combine(updateBaseFolder, result.AppPackageID, result.AppPackageVersionID.ToString() + UpdateFolder_Prepare);
            var finishedFolder = Path.ChangeExtension(updateFolder, UpdateFolder_Ready);
            if (Directory.Exists(finishedFolder))
            {
                var packageFile = Path.Combine(finishedFolder, LeveroPackageFileName);
                if (File.Exists(packageFile))
                {
                    // package has already been downloaded
                    this.Package = result = JsonConvert.DeserializeObject<PackageDescription>(File.ReadAllText(packageFile))
                        ?? throw new Exception($"Invalid Package Description in {packageFile}.");
                    result.UpdateFolder = finishedFolder;
                    return result;
                }
                Directory.Delete(finishedFolder, true);
            }
            if (Directory.Exists(updateFolder))
                Directory.Delete(updateFolder, true);

            // read binaries
            if (!reader.NextResult())
                throw new Exception($"Missing third recordset (binaries) of {cmd.CommandText}!");

            var colBinaryName = reader.GetOrdinal("AppPackageBinaryName");
            var colBinaryContent = reader.GetOrdinal("AppPackageBinaryContent");
            var colDetails = reader.GetOrdinal("Details");

            var bins = new List<PackageBinary>();
            while (reader.Read())
            {
                // info
                var bin = new PackageBinary
                {
                    BinaryName = reader.GetString(colBinaryName),
                    Details = reader.GetNullableString(colDetails, String.Empty).ToLower().Split(',')
                };
                // content
                var binContent = reader.GetBytes(colBinaryContent);
                // zipped?
                if (bin.Details.Contains(DetailZip))
                    binContent = Compression.UnzipToBytes(binContent);

                // special handling?
                if (bin.BinaryName == UpdateStepsBinaryName)
                    result.UpdateSteps = JsonConvert.DeserializeObject<UpdateStep[]>(Encoding.UTF8.GetString(binContent))
                        ?? throw new Exception($"Cannot deserialize array of Update Steps from special package {UpdateStepsBinaryName}.");
                else
                {
                    // normal file
                    bins.Add(bin);

                    var file = Path.Combine(updateFolder, bin.BinaryName);
                    Directory.CreateDirectory(Path.GetDirectoryName(file)
                        ?? throw new Exception($"Cannot get directory name of {file}."));
                    File.WriteAllBytes(file, binContent);

                    // handle flags
                    if (bin.Details.Contains(DetailMainExe))
                        result.MainExecutable = bin.BinaryName;
                    if (bin.Details.Contains(DetailUpdateExe))
                        result.UpdateExecutable = bin.BinaryName;
                    if (bin.Details.Contains(DetailMainConfig))
                        result.MainConfig = bin.BinaryName;
                }
            }
            result.Binaries = bins.ToArray();

            // save package description in update folder
            var filePackage = Path.Combine(updateFolder, LeveroPackageFileName);
            File.WriteAllText(filePackage, JsonConvert.SerializeObject(result, Formatting.Indented));

            // save update.bat in update folder
            var updateBat = Path.Combine(updateFolder, UpdateBatchFile);
            File.WriteAllText(updateBat, $"@\"{result.UpdateExecutable}\" {CommandLineArgument_Update}\r\n");

            // save update.bat in update base folder
            var baseUpdateBat = Path.Combine(Path.GetDirectoryName(updateFolder)!, UpdateBatchFile);
            File.WriteAllText(baseUpdateBat, $"@\"{Path.Combine(Path.GetFileName(finishedFolder), result.UpdateExecutable!)}\" {CommandLineArgument_Update}\r\n");

            // ready -> rename updateFolder
            result.UpdateFolder = finishedFolder;
            Directory.Move(updateFolder, finishedFolder);
            return this.Package = result;
        }

        public Process? StartUpdateExecutable() 
            => StartUpdateExecutable(this.Package!);

        public static Process? StartUpdateExecutable(PackageDescription package, string? updateFolder = null)
        {
            var updateExe = Path.Combine(updateFolder ?? package.UpdateFolder!, package.UpdateExecutable!);
            var args = $"{CommandLineArgument_Update} {CommandLineArgument_WaitForProcessID}{Process.GetCurrentProcess().Id}";
            return Process.Start(updateExe, args);
        }
    }
}
