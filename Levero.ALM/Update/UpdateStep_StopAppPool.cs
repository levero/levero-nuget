﻿using System;
using System.IO;
using System.Linq;

namespace Levero.ALM.Update
{
    internal class UpdateStep_StopAppPool : IUpdateStep
    {
        public string Name => "StopAppPool";

        /// <summary>
        /// Step StopAppPool stops an app pool
        /// </summary>
        /// <param name="arg[0]">String - app pool name to stop (for example: {AppPoolNameRunning})</param>
        public void Init(string[] args)
        {
            this.AppPoolName = args.FirstOrDefault() ?? throw new ArgumentException($"Missing first argument 'AppPoolName' in step #{this.StepID} {this.Name}!");
        }

        public int StepID { get; set; }
        public string AppPoolName { get; private set; } = "<undefined>";


        public void DoWork(UpdateAppPackage context)
        {
            this.AppPoolName = context.ProcessArguments(this.AppPoolName);

            // check if iis manager is available
            if (context.IISManager == null)
                throw new Exception($"Step #{this.StepID} {this.Name} requires an instance of IISManager!");

            // stop the app pool
            context.Log($"Start IIS app ppol '{this.AppPoolName}' in step #{this.StepID} {this.Name}...");
            context.IISManager.StopAppPool(this.AppPoolName);
        }

        public void Commit(UpdateAppPackage context)
        {
            context.IISManager?.SaveChanges();
        }

        public void Rollback(UpdateAppPackage context)
        {
            // nothing to do here
        }
    }
}
