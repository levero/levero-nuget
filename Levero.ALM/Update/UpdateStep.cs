﻿using Newtonsoft.Json;
using System;

namespace Levero.ALM.Update
{
    [JsonObject]
    public class UpdateStep
    {
        /// <summary>
        /// zero-based index of deployment steps to guarantee execution order
        /// </summary>
        [JsonProperty(PropertyName = "id", Required = Required.Always)]
        public int StepID { get; set; }

        /// <summary>
        /// must be a type in assembly Levero.ALM and full qualified name Levero.ALM.Deploy.DeployStep_{className}
        /// which implements IDeployStep
        /// </summary>
        [JsonProperty(PropertyName = "className", Required = Required.Always)]
        public string ClassName { get; set; } = "<undefined>";

        /// <summary>
        /// arguments for IDeployStep.Init method
        /// </summary>
        [JsonProperty(PropertyName = "args", Required = Required.Always)]
        public string[] Arguments { get; set; } = Array.Empty<string>();
    }
}
