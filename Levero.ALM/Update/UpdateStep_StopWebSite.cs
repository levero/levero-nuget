﻿using System;
using System.IO;
using System.Linq;

namespace Levero.ALM.Update
{
    internal class UpdateStep_StopWebSite : IUpdateStep
    {
        public string Name => "StopWebSite";

        /// <summary>
        /// Step StopWebSite stops a web site
        /// </summary>
        /// <param name="arg[0]">String - web site name to stop (for example: {WebSiteNameRunning})</param>
        public void Init(string[] args)
        {
            this.WebSiteName = args.FirstOrDefault() ?? throw new ArgumentException($"Missing first argument 'WebSiteName' in step #{this.StepID} {this.Name}!");
        }

        public int StepID { get; set; }
        public string WebSiteName { get; private set; } = "<undefined>";


        public void DoWork(UpdateAppPackage context)
        {
            this.WebSiteName = context.ProcessArguments(this.WebSiteName);

            // check if iis manager is available
            if (context.IISManager == null)
                throw new Exception($"Step #{this.StepID} {this.Name} requires an instance of IISManager!");

            // start the web site
            context.Log($"Stop IIS site '{this.WebSiteName}' in step #{this.StepID} {this.Name}...");
            context.IISManager.StopSite(this.WebSiteName);
        }

        public void Commit(UpdateAppPackage context)
        {
            context.IISManager?.SaveChanges();
        }

        public void Rollback(UpdateAppPackage context)
        {
            // nothing to do here
        }
    }
}
