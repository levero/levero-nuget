﻿using Microsoft.Win32;
using System;
using System.Linq;

namespace Levero.ALM.Update
{
    public class UpdateStep_RegistryValue : IUpdateStep
    {
        public string Name => "RegistryValue";

        public void Init(string[] args)
        {
            if (args.Length == 0)
                throw new ArgumentException($"1st argument must be path incl registry hive! for example: HKCU\\Software\\YourCompany");
            this.Hive = (args[0].Split('\\').First().ToUpper()) switch
            {
                "HKLM" => Registry.LocalMachine,
                "HKCU" => Registry.CurrentUser,
                "HKCC" => Registry.CurrentConfig,
                "HKCR" => Registry.ClassesRoot,
                "HKUS" => Registry.Users,
                _ => throw new ArgumentException("1st argument must be path incl registry hive! allowed hives: HKLM, HKCU, HKCC, HKCR, HKUS."),
            };
            this.Path = args[0].Substring(5);

            if (args.Length > 1 && !String.IsNullOrEmpty(args[1]))
                this.ItemName = args[1];

            this.ItemKind = RegistryValueKind.String;
            if (args.Length > 2 && !String.IsNullOrEmpty(args[2]))
                if (Enum.TryParse<RegistryValueKind>(args[2], true, out var kind))
                    this.ItemKind = kind;
                else
                    throw new ArgumentException("3rd argument must be empty or a member of Microsoft.Win32.RegistryValueKind!");

            if (args.Length > 3)
                switch (this.ItemKind)
                {
                    case RegistryValueKind.String:
                    case RegistryValueKind.ExpandString:
                    case RegistryValueKind.DWord:
                    case RegistryValueKind.QWord:
                        this.ItemValue = args[3];   // convert later after ProcessArguments in DoWork!
                        break;
                    case RegistryValueKind.MultiString:
                    case RegistryValueKind.Unknown:
                    case RegistryValueKind.Binary:
                    case RegistryValueKind.None:
                    default:
                        throw new ArgumentException("3th argument does not support MultiString, Binary, None or Unknown!");
                }
        }

        public int StepID { get; set; }
        public RegistryKey Hive { get; private set; } = Registry.CurrentUser;
        public string Path { get; private set; } = "<undefined>";
        public string? ItemName { get; private set; }
        public RegistryValueKind ItemKind { get; private set; }
        public object? ItemValue { get; private set; }

        private object? OldValue;
        private RegistryValueKind OldValueKind;

        public void DoWork(UpdateAppPackage context)
        {
            if (this.ItemValue != null)
                switch (this.ItemKind)
                {
                    case RegistryValueKind.String:
                    case RegistryValueKind.ExpandString:
                        this.ItemValue = context.ProcessArguments((string)this.ItemValue);
                        break;
                    case RegistryValueKind.DWord:
                        this.ItemValue = context.ProcessArguments((string)this.ItemValue);
                        if (UInt32.TryParse((string)this.ItemValue, out var dword))
                            this.ItemValue = dword;
                        else
                            throw new ArgumentException("4th argument must be convertable to System.UInt32!");
                        break;
                    case RegistryValueKind.QWord:
                        this.ItemValue = context.ProcessArguments((string)this.ItemValue);
                        if (UInt64.TryParse((string)this.ItemValue, out var qword))
                            this.ItemValue = qword;
                        else
                            throw new ArgumentException("4th argument must be convertable to System.UInt64!");
                        break;
                }

            using var key = this.Hive!.CreateSubKey(this.Path);
            this.OldValue = key.GetValue(this.ItemName);
            if (this.OldValue != null)
            {
                this.OldValueKind = key.GetValueKind(this.ItemName);
                if (this.ItemName != null)
                    key.DeleteValue(this.ItemName);
            }

            if (this.ItemValue != null)
                key.SetValue(this.ItemName, this.ItemValue, this.ItemKind);
        }

        public void Commit(UpdateAppPackage context)
        {
            // do nothing
        }

        public void Rollback(UpdateAppPackage context)
        {
            using (var key = this.Hive!.OpenSubKey(this.Path, true))
                if (key != null)
                {
                    if (this.OldValue != null)
                        key.SetValue(this.ItemName, this.OldValue, this.OldValueKind);
                    else if (this.ItemName != null)
                        key.DeleteValue(this.ItemName);
                }
        }
    }
}
