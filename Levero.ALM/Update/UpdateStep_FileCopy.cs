﻿using System;
using System.IO;
using System.Linq;

namespace Levero.ALM.Update
{
    internal class UpdateStep_FileCopy : IUpdateStep
    {
        public string Name => "FileCopy";

        /// <summary>
        /// Step FILECOPY (copies all binaries)
        /// </summary>
        /// <param name="arg[0]">String - target folder for files (for example: {AppFolder})</param>
        public void Init(string[] args)
        {
            this.TargetFolder = args.FirstOrDefault() ?? throw new ArgumentException($"Missing first argument 'TargetFolder' in step #{this.StepID} {this.Name}!");
        }

        public int StepID { get; set; }
        public string TargetFolder { get; private set; } = "<undefined>";


        public void DoWork(UpdateAppPackage context)
        {
            this.TargetFolder = context.ProcessArguments(this.TargetFolder);

            // handle all binaries
            foreach (var bin in context.Package.Binaries)
            {
                var skipIt = false;
                var sourceFile = Path.Combine(context.UpdateFolder, bin.BinaryName);
                var targetFile = Path.Combine(this.TargetFolder, bin.BinaryName);

                // remember main executable
                if (bin.Details.Contains(BaseAppPackage.DetailMainExe))
                    context.AddArguments(BaseAppPackage.ArgumentMainExecutable, targetFile);

                // remember main config
                if (bin.Details.Contains(BaseAppPackage.DetailMainConfig))
                    context.AddArguments(BaseAppPackage.ArgumentMainConfig, targetFile);

                // check if binary has a condition set
                var condition = bin.Details.Where(it => it.StartsWith(BaseAppPackage.DetailConditionPrefix, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (condition != null)
                {
                    // evaluate the condition ("condition:[{ARG_NAME}=abc]")
                    // just get the expression
                    condition = condition.Substring(BaseAppPackage.DetailConditionPrefix.Length + 1, condition.Length - BaseAppPackage.DetailConditionPrefix.Length - 2);
                    var parts = condition.Split('=');
                    if (parts.Length == 2)
                    {
                        var argumentValue = context.ProcessArguments(parts[0]);
                        var value = context.ProcessArguments(parts[1]);
                        condition = $"{argumentValue}={value}";
                        // now check if argument value equals to the given value - if not, skip this binary
                        skipIt = String.Compare(argumentValue, value, true) != 0; // skip it!
                    }
                }

                // copy file to destination path
                if (!skipIt)
                {
                    // ensure that source file exists
                    if (!File.Exists(sourceFile))
                        throw new Exception($"Missing source file '{sourceFile}' in step #{this.StepID} {this.Name}!");

                    // ensure that (sub)folder exists
                    Directory.CreateDirectory(Path.GetDirectoryName(targetFile)
                        ?? throw new Exception($"Cannot get directory name of {targetFile}."));

                    context.Log($"Create file '{targetFile}' in step #{this.StepID} {this.Name}...");
                    File.Copy(sourceFile, targetFile);
                }
                else
                    context.Log($"Skip file '{targetFile}' in step #{this.StepID} {this.Name} {condition}");
            }
        }

        public void Commit(UpdateAppPackage context)
        {
            // nothing todo
        }

        public void Rollback(UpdateAppPackage context)
        {
            // delete all new binaries
            foreach (var bin in context.Package.Binaries)
            {
                var targetFile = Path.Combine(this.TargetFolder!, bin.BinaryName);

                // copy file to destination path
                context.Log($"ROLLBACK - Create file '{targetFile}' in step #{this.StepID} {this.Name}...");
                File.Delete(targetFile);
            }
        }
    }
}
