﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Levero.ALM.Update
{
    internal class UpdateStep_SetFrontendEnvironmentConfig : IUpdateStep
    {
        public string Name => "SetFrontendEnvironmentConfig";

        /// <summary>
        /// Step SetEnvironmentConfig sets some specific environment settings for a frontend app (i.e. BackendHost, appPackage, appDeployment, ...)
        /// </summary>
        /// <param name="arg[0]">String - name of the environmenmt config (for example: {EnvironmentConfig})</param>
        public void Init(string[] args)
        {
            this.TargetFolder = args[0] ?? throw new ArgumentException($"Missing first argument 'TargetFolder' in step #{this.StepID} {this.Name}!");
            this.EnvironmentConfig = args[1] ?? throw new ArgumentException($"Missing second argument '{BaseAppPackage.ArgumentEnvironmentConfig}' in step #{this.StepID} {this.Name}!");
        }

        public int StepID { get; set; }
        public string TargetFolder { get; private set; } = "<undefined>";
        public string EnvironmentConfig { get; private set; } = "<undefined>";

        private string? MainExecutablePath;


        public void DoWork(UpdateAppPackage context)
        {
            this.TargetFolder = context.ProcessArguments(this.TargetFolder);
            this.EnvironmentConfig = context.ProcessArguments(this.EnvironmentConfig);

            var mainExecutable = context.ProcessArguments($"{{{BaseAppPackage.ArgumentMainExecutable}}}");
            this.MainExecutablePath = Path.Combine(this.TargetFolder, mainExecutable);

            // get the main executable file
            context.Log($"Look for main executable file {mainExecutable}");
            if (!File.Exists(this.MainExecutablePath))
                throw new Exception($"Could not find main executable file {this.MainExecutablePath}");

            // load the content
            var lines = File.ReadAllLines(this.MainExecutablePath).ToList();

            context.Log($"Look for placeholder '{BaseAppPackage.FrontendEnvironmentPlaceholder}'...");
            var lineToReplace = -1;
            for (var i = 0; i < lines.Count; i++)
            {
                var line = lines[i];
                if (line.IndexOf(BaseAppPackage.FrontendEnvironmentPlaceholder, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    lineToReplace = i;

                // write it back
                lines[i] = line;
            }

            // set environment config and overwrite main executable
            if (lineToReplace >= 0)
            {
                context.Log($"Set environment config '{this.EnvironmentConfig}' in step #{this.StepID} {this.Name}...");
                lines[lineToReplace++] = "  <script type=\"text/javascript\">";
                lines.Insert(lineToReplace++, $"    const ENV_CONFIG = JSON.parse('{this.EnvironmentConfig.Replace("'", "\\'")}');");
                lines.Insert(lineToReplace, "  </script>");

                // replace main executable content
                File.WriteAllLines(this.MainExecutablePath, lines);
            }
            else
                context.Log($"Skip setting environment config - could not find placeholder '{BaseAppPackage.FrontendEnvironmentPlaceholder}' in step #{this.StepID} {this.Name}");
        }

        public void Commit(UpdateAppPackage context)
        {
            // nothing to do here
        }

        public void Rollback(UpdateAppPackage context)
        {
            // nothing to do here
        }
    }
}
