﻿using Levero.ALM.IIS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Levero.ALM.Update
{
    public class UpdateAppPackage : BaseAppPackage
    {
        public UpdateAppPackage()
            : this(ForceSearchForPackage())
        {
        }

        public UpdateAppPackage(FileInfo packageFile)
            : this(JsonConvert.DeserializeObject<PackageDescription>(File.ReadAllText(packageFile.FullName))
                   ?? throw new Exception($"Cannot deserialize Package Description in file {packageFile.FullName}."), 
                  packageFile.Directory)
        {
        }

        public UpdateAppPackage(PackageDescription package, DirectoryInfo? updateFolder)
            : this(package, updateFolder?.FullName)
        {
        }

        public UpdateAppPackage(PackageDescription package, string? updateFolder)
        {
            this.Package = package ?? throw new ArgumentNullException(nameof(package));
            this.UpdateFolder = updateFolder ?? throw new ArgumentNullException(nameof(updateFolder));

            // do we need the IISManager for the update process?
            if (DeploymentProcessesRequireIIS.IndexOf(this.Package.DeploymentProcessID) > -1)
                this.IISManager = new IISManager();
        }

        private static FileInfo ForceSearchForPackage()
            => SearchForPackage() ?? throw new FileNotFoundException($"levero package description '{LeveroPackageFileName}' was not found!");

        private static FileInfo? SearchForPackage()
        {
            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            var currentFolder = new DirectoryInfo(Path.GetDirectoryName(assemblyLocation)
                ?? throw new Exception($"Cannot get directory name of {assemblyLocation}."));
            while (currentFolder != null)
            {
                var file = currentFolder.GetFiles(LeveroPackageFileName).FirstOrDefault();
                if (file != null)
                    return file;

                currentFolder = currentFolder.Parent;
            }
            return null;
        }

        public static UpdateAppPackage? AutoDetectUpdateMode()
        {
            var updatePackage = SearchForPackage();
            if (updatePackage == null) return null;

            return new UpdateAppPackage(updatePackage);
        }


        public PackageDescription Package { get; private set; }

        internal void AddArguments(string key, string value)
        {
            this.Log($"add new deploment argument {key} with value '{value}'");
            this.Arguments.Add(new PackageArgument
            {
                Key = key,
                Level = PackageArgumentLevel.UpdateProcess,
                Value = value
            });
        }

        /// <summary>
        /// Folder from which update executable has been started and which contains all binaries
        /// </summary>
        public string UpdateFolder { get; private set; }

        public string? MainExecutable { get; set; }

        /// <summary>
        /// Holds an instance of the Levero.ALM.ISSManager if deployment process id is IIS-backend or IIS-frontend
        /// </summary>
        public IISManager? IISManager { get; private set; }

        private readonly List<PackageArgument> Arguments = new List<PackageArgument>();

        /// <summary>
        /// Temporary folder which will be destroyed after update
        /// </summary>
        public string TempFolder => Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
            "Levero-Deployment",
            this.Package.AppPackageID,
            this.Package.AppPackageVersionID.ToVersion().ToString(3));


        public void Update()
        {
            // create update step instances
            var asm = Assembly.GetExecutingAssembly();
            var ns = typeof(UpdateStep_Delete).Namespace;
            var steps = this.Package.UpdateSteps
                .Select(it =>
                {
                    var type = asm.GetType($"{ns}.UpdateStep_{it.ClassName}")
                        ?? throw new Exception($"Type '{ns}.UpdateStep_{it.ClassName}' not found!");
                    var step = (Activator.CreateInstance(type) as IUpdateStep)
                        ?? throw new Exception($"Type '{ns}.UpdateStep_{it.ClassName}' does not implement instance {nameof(IUpdateStep)}!");
                    step.StepID = it.StepID;
                    step.Init(it.Arguments);
                    return step;
                })
                .ToArray();

            // copy arguments in local list
            this.Arguments.Clear();
            this.Arguments.AddRange(this.Package.Arguments);

            // start update
            var executedSteps = new List<IUpdateStep>();
            try
            {
                // first do work
                foreach (var step in steps.OrderBy(it => it.StepID))
                {
                    executedSteps.Insert(0, step);
                    step.DoWork(this);
                }

                // then commit
                foreach (var step in steps.OrderBy(it => it.StepID))
                    try { step.Commit(this); }
                    catch (Exception iex) { this.Log($"Failed to commit step #{step.StepID} {step.Name}! Details: {iex.Message}"); }

                // renaming current folder is not possible, so this must be done after starting main executable in DeployAppPackage
            }
            catch (Exception ex)
            {
                this.Log($"Failed update with error: {ex.Message}");

                // else rollback
                foreach (var step in executedSteps)
                    try { step.Rollback(this); }
                    catch (Exception iex) { this.Log($"Failed to rollback step #{step.StepID} {step.Name}! Details: {iex.Message}"); }

                throw;
            }
        }

        public Process StartMainExecutable()
        {
            var mainExe = this.Arguments.FirstOrDefault(it => String.Compare(it.Key, ArgumentMainExecutable, true) == 0)?.Value
                ?? throw new Exception($"Failed to get updated main executable to start app!");
            var args = $"{CommandLineArgument_WaitForProcessID}{Process.GetCurrentProcess().Id}";
            return Process.Start(mainExe, args);
        }

        private static readonly Regex searchPlaceHolder = new Regex(@"{(?<VarName>\w+)}");

        internal string ProcessArguments(string textWithPlaceholder)
        {
            var output = new StringBuilder();
            var pos = 0;

            foreach (var match in searchPlaceHolder.Matches(textWithPlaceholder).OfType<Match>())
            {
                var varNameGroup = match.Groups["VarName"];
                //if (!varNameGroup.Success) continue;

                // if there is static text write to output
                if (match.Index > pos)
                    output.Append(textWithPlaceholder.Substring(pos, match.Index - pos));

                // search for placeholder in arguments
                var arg = this.Arguments.FirstOrDefault(it => String.Compare(it.Key, varNameGroup.Value, true) == 0)
                    ?? throw new Exception($"placeholder {match.Value} not found in deployment process arguments!");

                // append value to output
                output.Append(arg.Value);
                pos = match.Index + match.Length;
            }
            // if there is static text write to output
            if (textWithPlaceholder.Length > pos)
                output.Append(textWithPlaceholder.Substring(pos, textWithPlaceholder.Length - pos));

            return output.ToString();
        }
    }
}
