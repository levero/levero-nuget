﻿using System;
using System.Linq;

namespace Levero.ALM.Update
{
    internal class UpdateStep_PrepareASPNetCore : IUpdateStep
    {
        public string Name => "PrepareASPNetCore";

        public void Init(string[] args)
        {
            this.WebSiteNameA = args.ElementAt(0) ?? throw new ArgumentException($"Missing first argument '{BaseAppPackage.ArgumentIISBackendWebSiteNameA}' in step #{this.StepID} {this.Name}!");
            this.WebSiteNameB = args.ElementAt(1) ?? throw new ArgumentException($"Missing second argument '{BaseAppPackage.ArgumentIISBackendWebSiteNameB}' in step #{this.StepID} {this.Name}!");
        }

        public int StepID { get; set; }
        public string WebSiteNameA { get; private set; } = "<undefined>";
        public string WebSiteNameB { get; private set; } = "<undefined>";
        public string? AppPoolNameA { get; private set; }
        public string? AppPoolNameB { get; private set; }
        public string? IISSiteNameA { get; private set; }
        public string? IISSiteNameB { get; private set; }
        public string? IISAppPoolNameA { get; private set; }
        public string? IISAppPoolNameB { get; private set; }
        public string? WebSiteNameRunning { get; private set; }
        public string? WebSiteNameStopped { get; private set; }
        public string? AppPoolNameRunning { get; private set; }
        public string? AppPoolNameStopped { get; private set; }

        public void DoWork(UpdateAppPackage context)
        {
            // get IIS site names from process arguments
            this.IISSiteNameA = context.ProcessArguments(this.WebSiteNameA);
            if (String.IsNullOrEmpty(this.IISSiteNameA))
                throw new Exception($"Missing deployment process argument \"{BaseAppPackage.ArgumentIISBackendWebSiteNameA}\" for app package id \"{context.Package.AppPackageID}\"");
            this.IISSiteNameB = context.ProcessArguments(this.WebSiteNameB);
            if (String.IsNullOrEmpty(this.IISSiteNameB))
                throw new Exception($"Missing deployment process argument \"{BaseAppPackage.ArgumentIISBackendWebSiteNameB}\" for app package id \"{context.Package.AppPackageID}\"");

            // check if iis manager is available
            if (context.IISManager == null)
                throw new Exception($"Step #{this.StepID} {this.Name} requires an instance of IISManager!");

            // get the set app pool names of the web sites
            this.IISAppPoolNameA = context.IISManager.GetAppPoolNameFromSite(this.IISSiteNameA);
            this.IISAppPoolNameB = context.IISManager.GetAppPoolNameFromSite(this.IISSiteNameB);

            // TODO: following code does not consider if an app or site is in state "Starting"
            //      for now, I do not think we get in trouble with this - but we will see :D

            // determine which website is running and which not
            if (context.IISManager.IsSiteStarted(this.IISSiteNameA))
                this.WebSiteNameRunning = this.IISSiteNameA;
            if (context.IISManager.IsSiteStarted(this.IISSiteNameB))
                this.WebSiteNameRunning = this.IISSiteNameB;
            // no one is running? ...just define one!
            if (this.WebSiteNameRunning == null)
                this.WebSiteNameRunning = this.IISSiteNameA;

            this.WebSiteNameStopped = this.WebSiteNameRunning == this.IISSiteNameA ? this.IISSiteNameB : this.IISSiteNameA;

            if (context.IISManager.IsAppPoolStarted(this.IISAppPoolNameA))
                this.AppPoolNameRunning = this.IISAppPoolNameA;
            if (context.IISManager.IsAppPoolStarted(this.IISAppPoolNameB))
                this.AppPoolNameRunning = this.IISAppPoolNameB;
            // no one is running? ...just define one!
            if (this.AppPoolNameRunning == null)
                this.AppPoolNameRunning = this.IISAppPoolNameA;

            this.AppPoolNameStopped = this.AppPoolNameRunning == this.IISAppPoolNameA ? this.IISAppPoolNameB : this.IISAppPoolNameA;

            // set WebSiteNameRunning as argument
            context.AddArguments(BaseAppPackage.ArgumentIISBackendWebSiteNameRunning, this.WebSiteNameRunning);
            // set WebSiteNameStopped as argument
            context.AddArguments(BaseAppPackage.ArgumentIISBackendWebSizeNameStopped, this.WebSiteNameStopped);
            // set AppPoolNameRunning as argument
            context.AddArguments(BaseAppPackage.ArgumentIISBackendAppPoolNameRunning, this.AppPoolNameRunning);
            // set AppPoolNameStopped as argument
            context.AddArguments(BaseAppPackage.ArgumentIISBackendAppPoolNameStopped, this.AppPoolNameStopped);
        }
        public void Commit(UpdateAppPackage context)
        {
            // nothing
        }

        public void Rollback(UpdateAppPackage context)
        {
            // nothing
        }
    }
}
