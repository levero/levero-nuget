﻿using Levero.ALM.IIS;
using System;
using System.Linq;

namespace Levero.ALM.Update
{
    internal class UpdateStep_GetAppFolderFromWebSite : IUpdateStep
    {
        public string Name => "GetAppFolderFromWebSite";

        public int StepID { get; set; }
        public string? AppFolder { get; private set; }
        public string? IISSiteName { get; private set; }
        public string WebSiteName { get; private set; } = "<undefined>";

        public void Init(string[] args)
        {
            this.WebSiteName = args.FirstOrDefault() ?? throw new ArgumentException($"Missing first argument '{BaseAppPackage.ArgumentIISFrontendWebSiteName}' in step #{this.StepID} {this.Name}!");
        }

        public void DoWork(UpdateAppPackage context)
        {
            // get IIS site name of process arguments
            //this.IISSiteName = context.Package.Arguments.FirstOrDefault(it => String.Compare(it.Key, BaseAppPackage.ArgumentIISSiteName, true) == 0)?.Value;
            this.IISSiteName  = context.ProcessArguments(this.WebSiteName!);
            if (this.IISSiteName == null)
                throw new Exception($"Missing deployment process argument \"{BaseAppPackage.ArgumentIISFrontendWebSiteName}\" for app package id \"{context.Package.AppPackageID}\"");

            // check if iis manager is available
            if (context.IISManager == null)
                throw new Exception($"Step #{this.StepID} {this.Name} requires an instance of IISManager!");

            // try to get the physical path of this IIS site
            this.AppFolder = context.IISManager.GetPhysicalPath(this.IISSiteName);

            // set the app folder as argument
            context.AddArguments(BaseAppPackage.ArgumentAppFolder, this.AppFolder);
        }
        public void Commit(UpdateAppPackage context)
        {
            // nothing
        }

        public void Rollback(UpdateAppPackage context)
        {
            // nothing
        }
    }
}
