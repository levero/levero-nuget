﻿namespace Levero.ALM.Update
{
    internal interface IUpdateStep
    {
        int StepID { get; set; }

        string Name { get; }

        void Init(string[] args);

        void DoWork(UpdateAppPackage context);

        void Commit(UpdateAppPackage context);

        void Rollback(UpdateAppPackage context);
    }
}