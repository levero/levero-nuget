﻿using System;
using System.IO;
using System.Linq;

namespace Levero.ALM.Update
{
    internal class UpdateStep_Delete : IUpdateStep
    {
        public string Name => "Delete";

        /// <summary>
        /// Step DELETE
        /// </summary>
        /// <param name="stepID"></param>
        /// <param name="arg[0]">String - search folder for files (for example: {AppFolder}\bin)</param>
        /// <param name="arg[1]">String - search mask for files (for example: *.dll), default mask is *.*</param>
        /// <param name="arg[2]">Boolean - recursive (true) or flat search (false or any other value)</param>
        public void Init(string[] args)
        {
            this.SearchFolder = args.FirstOrDefault() ?? throw new ArgumentException($"Missing first argument 'SearchFolder' in step #{this.StepID} {this.Name}!");
            this.SearchMask = args.Skip(1).FirstOrDefault() ?? "*.*";
            this.Recursive = "true".Equals(args.Skip(2).FirstOrDefault()?.ToLower());
        }

        public int StepID { get; set; }
        public string SearchFolder { get; private set; } = "<undefined>";
        public string SearchMask { get; private set; } = "<undefined>";
        public bool Recursive { get; private set; }

        private string? tempFolder;

        public void DoWork(UpdateAppPackage context)
        {
            // create temp folder for rollback
            this.tempFolder = Path.Combine(context.TempFolder, Guid.NewGuid().ToString("N"));
            Directory.CreateDirectory(this.tempFolder);

            // prepare search folder
            this.SearchFolder = context.ProcessArguments(this.SearchFolder);
            var prefixLen = this.SearchFolder.Length;
            if (!this.SearchFolder.EndsWith("\\"))
                prefixLen++;

            try
            {
                // search for files
                if (Directory.Exists(this.SearchFolder))
                    foreach (var file in Directory.GetFiles(this.SearchFolder, this.SearchMask,
                        this.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                    {
                        // there should be same sub folders in temp folder
                        var targetFile = Path.Combine(this.tempFolder, file.Substring(prefixLen));

                        // ensure that (sub)folder exists
                        Directory.CreateDirectory(Path.GetDirectoryName(targetFile)
                             ?? throw new Exception($"Cannot get directory name of {targetFile}."));

                        // move file to temp path
                        context.Log($"Delete file '{file}' in step #{this.StepID} {this.Name}...");
                        File.Move(file, targetFile);
                    }
            }
            catch
            {
                context.Log("!!!!! FAILED !!!!!");
            }
        }

        public void Commit(UpdateAppPackage context)
        {
            if (this.tempFolder != null) 
                Directory.Delete(this.tempFolder, true);
        }

        public void Rollback(UpdateAppPackage context)
        {
            var prefixLen = this.tempFolder!.Length;
            if (!this.tempFolder.EndsWith("\\"))
                prefixLen++;

            // move back to application folder
            foreach (var file in Directory.GetFiles(this.tempFolder))
            {
                var targetFile = Path.Combine(this.SearchFolder!, file.Substring(prefixLen));
                Directory.CreateDirectory(Path.GetDirectoryName(targetFile)
                    ?? throw new Exception($"Cannot get directory name of {targetFile}."));
                context.Log($"ROLLBACK - Delete file '{file}' in step #{this.StepID} {this.Name}...");
                File.Move(file, targetFile);
            }

            // remove trash
            Directory.Delete(this.tempFolder, true);
        }
    }
}
