﻿using Newtonsoft.Json;

namespace Levero.ALM
{
    [JsonObject]
    public class Package : PackageBase
    {
        [JsonProperty(Required = Required.Always)]
        public string AppPackageName { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.AllowNull)]
        public string? Description { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string? OnlyForAppID { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string DeploymentProcessID { get; set; } = "<undefined>";
    }
}
