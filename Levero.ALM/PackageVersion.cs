﻿using Newtonsoft.Json;
using System;

namespace Levero.ALM
{
    [JsonObject]
    public class PackageVersion : PackageBase
    {
        [JsonProperty(Required = Required.Always)]
        public long AppPackageVersionID { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string AppPackageVersion { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public short AppPackageVersionMajor { get; set; }  

        [JsonProperty(Required = Required.Always)]
        public short AppPackageVersionMinor { get; set; }

        [JsonProperty(Required = Required.Always)]
        public short AppPackageVersionBuild { get; set; }

        [JsonProperty(Required = Required.Always)]
        public DateTime DeployDate { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string DeployedBy { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.AllowNull)]
        public DateTime? RevokeDate { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string? RevokedBy { get; set; }

    }
}
