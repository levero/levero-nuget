﻿namespace Levero.ALM
{
    public class PackageArgument
    {
        public string? Key { get; set; }
        public string? Value { get; set; }

        public PackageArgumentLevel Level { get; set; }
    }
}