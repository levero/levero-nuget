﻿using System;

namespace Levero.ALM.Release
{
    public class DeploymentChannelInfo
    {
        public string DeploymentChannelID { get; set; } = "<undefined>";

        public string DeploymentChannelName { get; set; } = "<undefined>";

        public bool IsDefault { get; set; }

        public bool IsAppAvailable { get; set; }

        /// <summary>
        /// is null if IsAppAvailable = false
        /// </summary>
        public long? LatestAppPackageVersionID { get; set; }

        /// <summary>
        /// is null if IsAppAvailable = false
        /// </summary>
        public string? LatestAppPackageVersion { get; set; }

        /// <summary>
        /// is null if IsAppAvailable = false
        /// </summary>
        public DateTime? DeployDate { get; set; }

        /// <summary>
        /// is null if IsAppAvailable = false
        /// </summary>
        public string? DeployedBy { get; set; }
    }
}
