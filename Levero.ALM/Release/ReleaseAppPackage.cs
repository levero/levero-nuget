﻿using Levero.ALM.Build;
using Levero.Persistence;
using System;
using System.Collections.Generic;
using System.Data;

namespace Levero.ALM.Release
{
    public class ReleaseAppPackage : BaseAppPackage
    {
        public ReleaseAppPackage(string appPackageID, long appPackageVersionID)
        {
            this.AppPackageID = appPackageID ?? throw new ArgumentNullException(nameof(appPackageID));
            if (appPackageVersionID == 0) throw new ArgumentException($"Parameter {nameof(appPackageVersionID)} must be greater than zero! (zero = 'under development' and cannot be released)");
            this.AppPackageVersionID = appPackageVersionID;
        }

        public ReleaseAppPackage(BuildAppPackage build)
            : base(build)
        {
            this.AppPackageID = build.Package.AppPackageID ?? throw new Exception($"{nameof(BuildAppPackage)}.{nameof(BuildAppPackage.Package)} is not valid!");
            this.AppPackageVersionID = build.AppPackageVersionID ?? throw new Exception($"Package {this.AppPackageID} must be build first!");
        }

        public string AppPackageID { get; private set; }
        public long AppPackageVersionID { get; private set; }

        public DeploymentChannelInfo[] GetDeploymentChannels()
        {
            using var unit = new UnitOfWork();
            using var cmd = unit.PrepareProcedure("deploy", "SYS_DeploymentChannels_Load");
            cmd.AddInputParameter("UserID", this.UserID);
            cmd.AddInputParameter("AppPackageID", this.AppPackageID);

            var result = new List<DeploymentChannelInfo>();
            using (var reader = cmd.ExecuteReader())
            {
                var colDeploymentChannelID = reader.GetOrdinal("DeploymentChannelID");
                var colDeploymentChannelName = reader.GetOrdinal("DeploymentChannelName");
                var colIsDefault = reader.GetOrdinal("IsDefault");
                var colIsAppAvailable = reader.GetOrdinal("IsAppAvailable");
                var colLatestAppPackageVersionID = reader.GetOrdinal("LatestAppPackageVersionID");
                var colLatestAppPackageVersion = reader.GetOrdinal("LatestAppPackageVersion");
                var colDeployDate = reader.GetOrdinal("DeployDate");
                var colDeployedBy = reader.GetOrdinal("DeployedBy");

                while (reader.Read())
                    result.Add(new DeploymentChannelInfo
                    {
                        DeploymentChannelID = reader.GetString(colDeploymentChannelID),
                        DeploymentChannelName = reader.GetString(colDeploymentChannelName),
                        IsDefault = reader.GetBoolean(colIsDefault),
                        IsAppAvailable = reader.GetBoolean(colIsAppAvailable),
                        LatestAppPackageVersionID = reader.GetNullableInt64(colLatestAppPackageVersionID),
                        LatestAppPackageVersion = reader.GetNullableString(colLatestAppPackageVersion),
                        DeployDate = reader.GetNullableDateTime(colDeployDate),
                        DeployedBy = reader.GetNullableString(colDeployedBy)
                    });
            }

            return result.ToArray();
        }

        public void Release(string deploymentChannelID)
        {
            using var unit = new UnitOfWork();
            using var cmd = unit.PrepareProcedure("deploy", "SYS_AppPackage_Release");
            
            cmd.AddInputParameter("UserID", this.UserID);
            cmd.AddInputParameter("AppPackageID", this.AppPackageID);
            cmd.AddInputParameter("AppPackageVersionID", this.AppPackageVersionID);
            cmd.AddInputParameter("DeploymentChannelID", deploymentChannelID);

            cmd.ExecuteNonQuery();

            unit.Commit();
        }

        public void Revoke(string? deploymentChannelID)
        {
            using var unit = new UnitOfWork();
            using var cmd = unit.PrepareProcedure("deploy", "SYS_AppPackage_Revoke");

            cmd.AddInputParameter("UserID", this.UserID);
            cmd.AddInputParameter("AppPackageID", this.AppPackageID);
            cmd.AddInputParameter("AppPackageVersionID", this.AppPackageVersionID);
            // if deploymentChannelID IS NULL -> complete package version will be revoked
            cmd.AddInputParameter("DeploymentChannelID", deploymentChannelID);

            cmd.ExecuteNonQuery();

            unit.Commit();
        }

        public void RevokeVersion() 
            => this.Revoke(null);
    }
}
