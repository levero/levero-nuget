﻿using Newtonsoft.Json;
using System;

namespace Levero.ALM
{
    [JsonObject]
    public class PackageVersionDependency : PackageBase
    {
        [JsonProperty(Required = Required.Always)]
        public long AppPackageVersionID { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int AppPackageVersionDependencyID { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string DependencyType { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.AllowNull)]
        public string? DependOnAppPackageID { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public long? DependOnAppPackageVersionID { get; set; }
    }
}
