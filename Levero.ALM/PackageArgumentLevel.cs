﻿namespace Levero.ALM
{
    public enum PackageArgumentLevel
    {
        Default = 0,

        App = 1,

        AppPackage = 2,

        Environment = 3,

        UpdateProcess = 4
    }
}