﻿using Newtonsoft.Json;

namespace Levero.ALM
{
    [JsonObject]
    public class DeploymentProcessArgument
    {
        [JsonProperty(Required = Required.Always)]
        public string DeploymentProcessID { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public string ArgumentKey { get; set; } = "<undefined>";

        [JsonProperty(Required = Required.Always)]
        public byte LevelID { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string? AppID { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string? AppPackageID { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string? EnvironmentID { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string? ArgumentValue { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string? Description { get; set; }
    }
}
